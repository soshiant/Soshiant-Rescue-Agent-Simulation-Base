/*
 * AmbulanceTeamStateEstimator.h
 *
 *  Created on: Nov 25, 2013
 *      Author: Pedram
 */

#include "Types.h"
#include "../WorldModel/Objects/Human.h"
#include "../WorldModel/Objects/AmbulanceTeam.h"
#include <algorithm>

#ifndef AMBULANCETEAMSTATEESTIMATOR_H_
#define AMBULANCETEAMSTATEESTIMATOR_H_

class AmbulanceTeam;

class AmbulanceTeamStateEstimator
{
public:
	AmbulanceTeamStateEstimator(AmbulanceTeam* self);
	virtual ~AmbulanceTeamStateEstimator();

	void substractTeamMate(AmbulanceTeam* teamMate);
	vector<AmbulanceTeam*> getWorkingOnMyTarget();
	int getEstimatedBusyTime();
	void progress(int time);
	Types::AmbulanceTeamState getState();
	void setState(Human* workingOn, int moveToRescue, int moveToRefuge, vector<AmbulanceTeam*> workingOnThisHuman);
	AmbulanceTeam* loader();

private:
	void progressBeforeRescue();
	void progressRescue(int time);
	void progressAfterRescue();

	vector<AmbulanceTeam*> workingOnMyTarget;
	AmbulanceTeam* self;
	Human* workingOn;

	u_int previousBusyTime;
	u_int moveToRescue;
	u_int moveToRefuge;
	u_int rescue;
	u_int load;
	u_int unload;
};

#endif /* AMBULANCETEAMSTATEESTIMATOR_H_ */
