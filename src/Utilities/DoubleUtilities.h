#ifndef _DOUBLEUTILITIES_H
#define	_DOUBLEUTILITIES_H

bool isEqual(double, double);
bool isLess(double, double);
bool isLessEqual(double, double);
bool hasSameSign(double, double);
int roundThisNumber(int n, int round);
int roundThisDoubleNumber(double n, int tRound);
bool isBetween(int number, int min, int max);

#endif	/* _DOUBLEUTILITIES_H */

