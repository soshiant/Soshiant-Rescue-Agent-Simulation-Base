/*
 * FireEstimator.cpp
 *
 *  Created on: Feb 3, 2014
 *      Author: arha
 */

#include "FireEstimator.h"

#define LOGLEVEL 1

FireEstimator::FireEstimator(WorldModel* world)
{
	this->world = world;

	setBuildingsEstimatedData();

	simulatedWorld = new SimulatedWorld(world);

	rng.seed(RANDOM_SEED);
	distribution = new boost::normal_distribution<>(0.15, 0.02);
	generator = new boost::variate_generator<boost::mt19937, boost::normal_distribution<>>(rng, *distribution);
}

FireEstimator::~FireEstimator()
{
	delete simulatedWorld;
}

void FireEstimator::estimateOnce()
{
	burnBuildings(world->getTime());

	coolBuildings();

	updateAirGrid();

	exchangeBuildings();

	coolBuildings();
}

void FireEstimator::burnBuildings(int time)
{
	for (Building* building : world->buildings)
	{
		if (building->getEstimatedData()->getTemperature() >= building->getEstimatedData()->getIgnitionPoint() && building->getEstimatedData()->getFuel() > 0
				&& building->getEstimatedData()->isFlammable())
		{
			if (!building->getEstimatedData()->isIgnited())
			{
				igniteBuilding(building, world->getTime());
			}

			float consumed = getConsumed(building);
			if (consumed > building->getEstimatedData()->getFuel())
			{
				consumed = building->getEstimatedData()->getFuel();
			}

			building->getEstimatedData()->setEnergy(building->getEstimatedData()->getEnergy() + consumed);
			building->getEstimatedData()->setFuel(building->getEstimatedData()->getFuel() - consumed);
		}
	}
}

void FireEstimator::coolBuildings()
{
	for (Building* building : world->buildings)
	{
		coolBuilding(building);
	}
}

void FireEstimator::updateAirGrid()
{
	vector<vector<double>> airTemp = simulatedWorld->getAirTemp();
	vector<vector<double>> newAirTemp (airTemp.size());

	for (u_int i = 0; i < airTemp.size(); i++)
	{
		vector<double> tmp(airTemp[0].size(), 0.);
//		newAirTemp.push_back(tmp);
		newAirTemp[i] = tmp;
	}

	Rectangle bound = world->getWorldBound();

	for (u_int x = 0; x < airTemp.size(); x++)
	{
		for (u_int y = 0; y < airTemp[x].size(); y++)
		{
			double dt = getAverageTemperature(x, y) - airTemp[x][y];
			double change = dt * AIR_TO_AIR_COEFFICIENT * TIME_STEP_LENGTH;
			newAirTemp[x][y] = max((airTemp[x][y] + change) * ENERGY_LOSS * TIME_STEP_LENGTH, 0.);

			if (!(newAirTemp[x][y] > std::numeric_limits<double>::lowest() && newAirTemp[x][y] < std::numeric_limits<double>::max()))
			{
				newAirTemp[x][y] = std::numeric_limits<double>::max() * 0.75;
			}
		}
	}

	simulatedWorld->setAirTemp(newAirTemp);
}

void FireEstimator::exchangeBuildings()
{
	vector<double> radiation;
	radiation.reserve(world->buildings.size());

	for (Building* building : world->buildings)
	{
		exchangeWithAir(building);
	}

	for (Building* building : world->buildings)
	{
		radiation.push_back(building->getEstimatedData()->getRadiationEnergy());
	}

	for (u_int i = 0; i < world->buildings.size(); i++)
	{
		if (radiation[i] == 0)
			continue;

		for (InSightBuilding* isb : world->buildings[i]->getInSightBuildings())
		{
			// TODO Optimize Coeff
			world->buildings[isb->buildingIndex]->getEstimatedData()->setEnergy(
					world->buildings[isb->buildingIndex]->getEstimatedData()->getEnergy() + radiation[i] * isb->value * 0.28f);
		}

		world->buildings[i]->getEstimatedData()->setEnergy(world->buildings[i]->getEstimatedData()->getEnergy() - radiation[i]);
	}
}

void FireEstimator::exchangeWithAir(Building* building)
{
	double oldEnergy = building->getEstimatedData()->getEnergy();
	double energyDelta = 0;

	for (array<int, 3> cell : building->getEstimatedData()->getCells())
	{
		int x = cell[0];
		int y = cell[1];
		double cover = cell[2] / (double) 100.0;
		double temperature = simulatedWorld->getAirCellTemp(x, y);
		double dT = temperature - building->getEstimatedData()->getTemperature();
		double energyTransferToBuilding = dT * AIR_TO_BUILDING_COEFFICIENT * TIME_STEP_LENGTH * cover * SAMPLE_SIZE;
		energyDelta += energyTransferToBuilding;
		double newCellTemperature = temperature - energyTransferToBuilding / (AIR_CELL_HEAT_CAPACITY * SAMPLE_SIZE);
		simulatedWorld->setAirCellTemp(x, y, newCellTemperature);
	}

	building->getEstimatedData()->setEnergy(oldEnergy + energyDelta);
}

void FireEstimator::coolBuilding(Building* building)
{
	double waterCoeff = (
			building->getEstimatedData()->getFieryness() > 0 && building->getEstimatedData()->getFieryness() < 4 ? WATER_COEFFICIENT : WATER_COEFFICIENT * WATER_COOLING_GAMMA);

	if (building->getEstimatedData()->getWaterQuantity() > 0)
	{
		double dE = building->getEstimatedData()->getEnergy();

		if (dE <= 0)
			return;

		double effect = building->getEstimatedData()->getWaterQuantity() * waterCoeff;
		int consumed = building->getEstimatedData()->getWaterQuantity();

		if (effect > dE)
		{
			double pc = 1 - ((effect - dE) / effect);
			effect *= pc;
			consumed *= pc;
		}

		building->getEstimatedData()->setWaterQuantity(building->getEstimatedData()->getWaterQuantity() - consumed);
		building->getEstimatedData()->setEnergy(building->getEstimatedData()->getEnergy() - effect);
	}
}

void FireEstimator::igniteBuilding(Building* building, int time)
{
	building->getEstimatedData()->setEnergy(building->getEstimatedData()->getCapacity() * building->getEstimatedData()->getIgnitionPoint() * 1.5);
	building->getEstimatedData()->setIgnition(true, time);
}

float FireEstimator::getConsumed(Building* building)
{
	if (building->getEstimatedData()->getFuel() == 0)
		return 0;

	float tf = (float) (building->getEstimatedData()->getTemperature() / 1000.0f);
	float lf = building->getEstimatedData()->getFuel() / building->getEstimatedData()->getInitialFuel();

	double ran = (*generator)();

	float f = (float) (tf * lf * ran);

	if (f < 0.005f)
		f = 0.005f;

	return building->getEstimatedData()->getInitialFuel() * f;
}

double FireEstimator::getAverageTemperature(int x, int y)
{
	return getNeighboursTemperature(x, y) / (8 * WEIGHT_GRID);
}

double FireEstimator::getNeighboursTemperature(int x, int y)
{
	double total = getTemperatureAt(x + 1, y - 1);
	total += getTemperatureAt(x + 1, y);
	total += getTemperatureAt(x + 1, y + 1);
	total += getTemperatureAt(x, y - 1);
	total += getTemperatureAt(x, y + 1);
	total += getTemperatureAt(x - 1, y - 1);
	total += getTemperatureAt(x - 1, y);
	total += getTemperatureAt(x - 1, y + 1);

	return total * WEIGHT_GRID;
}

double FireEstimator::getTemperatureAt(int x, int y)
{
	if (x < 0 || y < 0 || (u_int) x >= simulatedWorld->getAirTemp().size() || (u_int) y >= simulatedWorld->getAirTemp()[0].size())
	{
		return 0;
	}

	return simulatedWorld->getAirTemp()[x][y];
}

void FireEstimator::setBuildingsEstimatedData()
{
	for (Building* building : world->buildings)
	{
		if (building->getEstimatedData() != nullptr)
		{
			delete building->getEstimatedData();
		}

		building->estimatedData = new EstimatedData(building, simulatedWorld);
	}
}

void FireEstimator::extinguishBuilding(Building* building, int waterAmount)
{
	building->getEstimatedData()->setWaterQuantity(building->getEstimatedData()->getWaterQuantity() + waterAmount);
}

void FireEstimator::resetBuildingTemperature(Building* building)
{
	for (u_int i = 0; i < building->getEstimatedData()->getCells().size(); i++)
	{
		building->getEstimatedData()->getSimulatedWorld().setAirCellTemp(building->getEstimatedData()->getCells()[i][0], building->getEstimatedData()->getCells()[i][1], 0);
	}
}

void FireEstimator::resetEstimatedData()
{
	for (Building* building : world->buildings)
	{
		building->getEstimatedData()->setFieryness(building->getFieryness());
		building->getEstimatedData()->setTemperature(building->getTemperature());
	}

	simulatedWorld->resetAirCells();
}

