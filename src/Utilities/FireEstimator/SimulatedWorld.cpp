/*
 * SimulatedWorld.cpp
 *
 *  Created on: Nov 5, 2013
 *      Author: arya
 */

#include "SimulatedWorld.h"

#define LOGLEVEL 1

SimulatedWorld::SimulatedWorld(WorldModel* world)
{
	this->world = world;

	LOG(Main, 1) << "Set Capacity" << endl;
	capacity = (int) (SAMPLE_SIZE * SAMPLE_SIZE * AIR_HEIGHT * AIR_CAPACITY) / 1000000;

	LOG(Main, 1) << "Initialize Air [SimulatedWorld]" << endl;
	initializeAir();

	LOG(Main, 1) << "End of Constructor [SimulatedWorld]" << endl;
}

SimulatedWorld::~SimulatedWorld()
{
}

vector<vector<double>>& SimulatedWorld::getAirTemp()
{
	return airTemp;
}

void SimulatedWorld::setAirTemp(vector<vector<double>> airTemp)
{
	this->airTemp = airTemp;
}

void SimulatedWorld::setAirCellTemp(int x, int y, double temp)
{
	airTemp[x][y] = temp;
}

double SimulatedWorld::getAirCellTemp(int x, int y)
{
	return airTemp[x][y];
}

void SimulatedWorld::resetAirCells()
{
	for (int i = 0; i < airTemp.size(); i++)
	{
		for (int j = 0; j < airTemp[i].size(); j++)
		{
			airTemp[i][j] = 0;
		}
	}
}

void SimulatedWorld::initializeAir()
{
	int xSamples = 1 + abs(getMaxX() - getMinX()) / SAMPLE_SIZE;
	int ySamples = 1 + abs(getMaxY() - getMinY()) / SAMPLE_SIZE;

	for (int x = 0; x < xSamples; x++)
	{
		airTemp.emplace_back(ySamples, 0.);
	}

	for (Building* building : world->buildings)
	{
		building->getEstimatedData()->setSimulatedWorld(this);
	}
}

void SimulatedWorld::setBuildingCells(Building* building)
{
	building->getEstimatedData()->setSimulatedWorld(this);

	Rectangle buildingBound = building->getShape().getBound();

	int mini = (int) ((buildingBound.getPoint1().getX() - getMinX()) / SAMPLE_SIZE);
	int minj = (int) ((buildingBound.getPoint1().getY() - getMinY()) / SAMPLE_SIZE);
	int maxi = (int) ((buildingBound.getPoint2().getX() - getMinX()) / SAMPLE_SIZE) + 1;
	int maxj = (int) ((buildingBound.getPoint3().getY() - getMinY()) / SAMPLE_SIZE) + 1;

	if (mini < 0)
	{
		mini = 0;
	}
	if (minj < 0)
	{
		minj = 0;
	}
	if (maxi > (int) getAirTemp().size())
	{
		maxi = getAirTemp().size();
	}
	if (maxj > (int) getAirTemp()[0].size())
	{
		maxi = getAirTemp()[0].size();
	}

	for (int x = mini; x < maxi; x++)
	{
		for (int y = minj; y < maxj; y++)
		{
			Point point(x * SAMPLE_SIZE + getMinX(), y * SAMPLE_SIZE + getMinY());
			int p = Geometry::coverPercent(point, SAMPLE_SIZE, SAMPLE_SIZE, building->getShape());

			if (p > 0)
			{
				building->getEstimatedData()->getCells().push_back(array<int, 3> { {x, y, p}});
			}
		}
	}
}

int SimulatedWorld::getMinX()
{
//	double tmp = min(world->getWorldBound().getPoint1().getX(), world->getWorldBound().getPoint2().getX());
//	tmp = min(tmp, world->getWorldBound().getPoint3().getX());
//	tmp = min(tmp, world->getWorldBound().getPoint4().getX());
//	return (int) tmp;

	return world->getWorldBound().getPoint1().getX();
}

int SimulatedWorld::getMaxX()
{
//	double tmp = max(world->getWorldBound().getPoint1().getX(), world->getWorldBound().getPoint2().getX());
//	tmp = max(tmp, world->getWorldBound().getPoint3().getX());
//	tmp = max(tmp, world->getWorldBound().getPoint4().getX());
//	return (int) tmp;

	return world->getWorldBound().getPoint2().getX();
}

int SimulatedWorld::getMinY()
{
//	double tmp = min(world->getWorldBound().getPoint1().getY(), world->getWorldBound().getPoint2().getY());
//	tmp = min(tmp, world->getWorldBound().getPoint3().getY());
//	tmp = min(tmp, world->getWorldBound().getPoint4().getY());
//	return (int) tmp;
	return world->getWorldBound().getPoint1().getY();
}

int SimulatedWorld::getMaxY()
{
//	double tmp = max(world->getWorldBound().getPoint1().getY(), world->getWorldBound().getPoint2().getY());
//	tmp = max(tmp, world->getWorldBound().getPoint3().getY());
//	tmp = max(tmp, world->getWorldBound().getPoint4().getY());
//	return (int) tmp;
	return world->getWorldBound().getPoint3().getY();
}
