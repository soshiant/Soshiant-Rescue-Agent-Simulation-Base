/*
 * FireEstimator.h
 *
 *  Created on: Feb 3, 2014
 *      Author: arha
 */

#ifndef FIREESTIMATOR_H_
#define FIREESTIMATOR_H_

#include "../../WorldModel/WorldModel.h"
#include "SimulatedWorld.h"
#include <cmath>
#include <limits>
#include <assert.h>
#include <boost/math/distributions/normal.hpp>
#include <boost/random.hpp>

#define AIR_TO_BUILDING_COEFFICIENT 0.0015f
#define AIR_TO_AIR_COEFFICIENT 0.8f
#define ENERGY_LOSS 0.86f
#define RADIATION_COEFFICENT 0.011f
#define TIME_STEP_LENGTH 1.f
#define AIR_CELL_HEAT_CAPACITY 0.004f
#define WEIGHT_GRID 0.2f
#define WATER_COEFFICIENT 30.f
#define WATER_COOLING_GAMMA 0.2f

class FireEstimator
{
public:
	FireEstimator(WorldModel*);
	~FireEstimator();

	void estimateOnce();
	void extinguishBuilding(Building*, int);
	void setBuildingsEstimatedData();
	void resetEstimatedData();

	static void resetBuildingTemperature(Building*);

	SimulatedWorld* simulatedWorld = nullptr;

private:
	void burnBuildings(int);
	void coolBuildings();
	void updateAirGrid();
	void exchangeBuildings();
	void exchangeWithAir(Building*);
	void coolBuilding(Building*);
	void igniteBuilding(Building*, int);
	float getConsumed(Building*);
	double getTemperatureAt(int, int);
	double getNeighboursTemperature(int, int);
	double getAverageTemperature(int, int);

	WorldModel* world = nullptr;

	boost::mt19937 rng;
	boost::normal_distribution<>* distribution;
	boost::variate_generator<boost::mt19937, boost::normal_distribution<> >* generator;
};

#endif /* FIREESTIMATOR_H_ */
