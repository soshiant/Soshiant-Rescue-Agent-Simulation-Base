/*
 * EstimatedData.cpp
 *
 *  Created on: Oct 28, 2013
 *      Author: arya
 */

#include "EstimatedData.h"
#include "SimulatedWorld.h"

#define LOGLEVEL 1

EstimatedData::EstimatedData(Building *building, SimulatedWorld* simulatedWorld)
{
	realBuilding = building;

	this->waterQuantity = 0;
	this->capacity = -1;
	this->ignitionTime = -1;
	this->wasEverWatered = false;

	this->woodCapacity = 1.1f;
	this->steelCapacity = 1;
	this->concreteCapacity = 1.5f;

	this->woodIgnition = 47.f;
	this->steelIgnition = 47.f;
	this->concreteIgnition = 47.f;

	this->woodEnergy = 2400.f;
	this->steelEnergy = 800.f;
	this->concreteEnergy = 350.f;

	this->volume = realBuilding->getAreaGround() * realBuilding->getFloors() * FLOOR_HEIGHT;
	setCapacity(volume * getThermoCapacity());
	this->energy = 0;
	this->initialFuel = -1;
	this->fuel = getInitialFuel();

	setFieryness(0);
	setTemperature(0);

	this->simulatedWorld = simulatedWorld; // XXX It's null Idiot :|
}

EstimatedData::~EstimatedData()
{
}

double EstimatedData::getTemperature()
{
	double rv = getEnergy() / getCapacity();

	if (rv == std::numeric_limits<double>::infinity() || rv == -std::numeric_limits<double>::infinity() || rv == std::numeric_limits<double>::quiet_NaN())
		rv = MAX_DOUBLE * 0.75;

	return rv;
}

int EstimatedData::getFieryness()
{
	if (!isFlammable())
	{
		return 0;
	}

	if (getTemperature() >= getIgnitionPoint())
	{
		if (fuel >= getInitialFuel() * 0.66)
			return 1; // burning, slightly damaged
		if (fuel >= getInitialFuel() * 0.33)
			return 2; // burning, more damaged
		if (fuel > 0)
			return 3; // burning, severely damaged
	}

	if (fuel == getInitialFuel())
	{
		if (wasEverWatered)
			return 4; // not burnt, but watered-damaged
		else
			return 0; // not burnt, no water damage
	}

	if (fuel >= getInitialFuel() * 0.66)
		return 5; // extinguished, slightly damaged
	if (fuel >= getInitialFuel() * 0.33)
		return 6; // extinguished, more damaged
	if (fuel > 0)
		return 7; // extinguished, severely damaged

	return 8; // completely burnt down
}

float EstimatedData::getFuel()
{
	return fuel;
}

float EstimatedData::getFuelDensity()
{
	if (realBuilding->getBuildingCode() == 0)
		return woodEnergy;

	if (realBuilding->getBuildingCode() == 1)
		return steelEnergy;

	if (realBuilding->getBuildingCode() == 2)
		return concreteEnergy;
}

float EstimatedData::getInitialFuel()
{
	if (initialFuel <= 0)
	{
		volume = realBuilding->getAreaGround() * realBuilding->getFloors() * FLOOR_HEIGHT;
		initialFuel = getFuelDensity() * volume;
	}

	return initialFuel;
}

float EstimatedData::getIgnitionPoint()
{
	if (realBuilding->getBuildingCode() == 0)
		return woodIgnition;

	if (realBuilding->getBuildingCode() == 1)
		return steelIgnition;

	if (realBuilding->getBuildingCode() == 2)
		return concreteIgnition;
}

float EstimatedData::getVolume()
{
	return volume;
}

float EstimatedData::getThermoCapacity()
{
	if (realBuilding->getBuildingCode() == 0)
		return woodCapacity;

	if (realBuilding->getBuildingCode() == 1)
		return steelCapacity;

	if (realBuilding->getBuildingCode() == 2)
		return concreteCapacity;

	return -1;
}

float EstimatedData::getCapacity()
{
	if (capacity <= 0)
	{
		volume = realBuilding->getAreaGround() * realBuilding->getFloors() * FLOOR_HEIGHT;

		setCapacity(volume * getThermoCapacity());
	}

	return capacity;
}

double EstimatedData::getEnergy()
{
	if (energy == std::numeric_limits<double>::infinity() || energy == -std::numeric_limits<double>::infinity() || energy == std::numeric_limits<double>::quiet_NaN())
		energy = MAX_DOUBLE * 0.75;

	return energy;
}

int EstimatedData::getWaterQuantity()
{
	return waterQuantity;
}

vector<array<int, 3>>& EstimatedData::getCells()
{
	return cells;
}

void EstimatedData::setWaterQuantity(int waterQuantity)
{
	this->waterQuantity = waterQuantity;

	if (this->waterQuantity > 0)
	{
		wasEverWatered = true;
	}
}

void EstimatedData::setEnergy(double energy)
{
	this->energy = energy;
}

void EstimatedData::setCapacity(float capacity)
{
	this->capacity = capacity;
}

void EstimatedData::setFuel(float fuel)
{
	this->fuel = fuel;
}

void EstimatedData::setTemperature(double temperature)
{
	if (temperature > getTemperature())
	{
		for (u_int i = 0; i < cells.size(); i++)
		{
			simulatedWorld->setAirCellTemp(cells[i][0], cells[i][1], temperature);
		}
	}

	if (temperature > 0)
	{
		setWaterQuantity(0);
	}

	energy = getCapacity() * temperature;

	if (temperature >= getIgnitionPoint())
	{
		ignition = true;
	}
	else
	{
		ignition = false;
	}
}

void EstimatedData::setFieryness(int fieryness)
{
	if (fieryness > 3 && fieryness < 8)
	{
		wasEverWatered = true;
	}

	if (getFieryness() != fieryness)
	{
		if (fieryness == 0 || fieryness == 4)
		{
			fuel = getInitialFuel();
		}
		else
		{
			//TODO Find better way for fuel estimating
			if (fieryness == 1 || fieryness == 5)
				//					fuel = (float) (getInitialFuel() * 0.83);
				fuel = (float) (getInitialFuel() * 0.95);
			else
			{
				if (fieryness == 2 || fieryness == 6)
					//						fuel = (float) (getInitialFuel() * 0.495);
					fuel = (float) (getInitialFuel() * 0.60);
				else
				{
					if (fieryness == 3 || fieryness == 7)
						//							fuel = (float) (getInitialFuel() * 0.28);
						fuel = (float) (getInitialFuel() * 0.30);
					else
						fuel = 0;
				}
			}
		}
	}

	if (fieryness == 0 || (fieryness > 3 && fieryness < 8))
	{
		ignition = false;
	}
}

void EstimatedData::setVolume(float volume)
{
	this->volume = volume;
}

bool EstimatedData::isFlammable()
{
	return !(realBuilding->isRefuge() || realBuilding->isCenter());
}

bool EstimatedData::hasFire()
{
	if (getFieryness() >= 1 && getFieryness() <= 3)
		return true;

	return false;
}

bool EstimatedData::isIgnited()
{
	return ignition;
}

void EstimatedData::setIgnition(bool ignition, int time)
{
	this->ignition = ignition;
	this->ignitionTime = time;
}

double EstimatedData::getRadiationEnergy()
{
	//Q = T^4 * A * BoltzmannConst * RadiationConst
	double t = getTemperature() + 273;
	double radEn = pow(t, 4) * realBuilding->getWallsLength() * RADIATION_COEFFICENT * STEFAN_BOLTZMANN_CONSTANT;

	if (radEn == std::numeric_limits<double>::infinity() || radEn == -std::numeric_limits<double>::infinity() || radEn == std::numeric_limits<double>::quiet_NaN())
		radEn = MAX_DOUBLE * 0.75;

	if (radEn > getEnergy())
		radEn = getEnergy();

	return radEn;
}

void EstimatedData::setSimulatedWorld(SimulatedWorld* simulatedWorld)
{
	this->simulatedWorld = simulatedWorld;
}

SimulatedWorld& EstimatedData::getSimulatedWorld()
{
	return *simulatedWorld;
}
