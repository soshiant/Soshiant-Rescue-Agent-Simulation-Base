/*
 * EstimatedData.h
 *
 *  Created on: Oct 28, 2013
 *      Author: arya
 */

#ifndef ESTIMATEDDATA_H_
#define ESTIMATEDDATA_H_

#include "../../WorldModel/WorldModel.h"
#include <cmath>
#include <limits>
#include <array>
#include <boost/math/distributions/normal.hpp>
#include <boost/random.hpp>

#define GAMMA 0.5f
#define FLOOR_HEIGHT 7
#define STEFAN_BOLTZMANN_CONSTANT 0.000000056704
#define RADIATION_COEFFICENT 0.011f

class Building;
class SimulatedWorld;

class EstimatedData
{
public:
	EstimatedData(Building* building, SimulatedWorld* simulatedWorld);
	~EstimatedData();

	double getTemperature();
	int getFieryness();
	int getWaterQuantity();
	double getEnergy();
	float getFuel();
	float getCapacity();
	float getVolume();
	float getIgnitionPoint();
	float getThermoCapacity();
	float getInitialFuel();
	float getFuelDensity();
	double getRadiationEnergy();
	vector<array<int, 3>>& getCells();

	void setTemperature(double temperature);
	void setFieryness(int fieryness);
	void setFuel(float fuel);
	void setCapacity(float capacity);
	void setVolume(float volume);
	void setEnergy(double energy);
	void setWaterQuantity(int waterQuantity);
	void setIgnition(bool, int);
	void setSimulatedWorld(SimulatedWorld*);
	SimulatedWorld& getSimulatedWorld();

	bool isIgnited();
	bool isFlammable();
	bool hasFire();


private:
	Building* realBuilding;
	float fuel, initialFuel;
	double energy;
	bool ignition;
	int ignitionTime;
	float capacity;
	float volume;
	int waterQuantity;
	bool wasEverWatered;

	vector<array<int, 3>> cells;

	float woodIgnition, steelIgnition, concreteIgnition;
	float woodCapacity, steelCapacity, concreteCapacity;
	float woodEnergy, steelEnergy, concreteEnergy;

	SimulatedWorld* simulatedWorld = nullptr;
};

#endif /* ESTIMATEDDATA_H_ */
