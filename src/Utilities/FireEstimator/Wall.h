/*
 * Wall.h
 *
 *  Created on: Dec 1, 2013
 *      Author: arya
 */

#ifndef WALL_H_
#define WALL_H_

#include "../Geometry/Point.h"
#include "../Geometry/Segment.h"
#include <vector>

class WorldModel;
class Building;

using namespace Geometry;
using namespace std;

class Wall
{
public:
	Wall(int, int, int, int, int, float);
	Wall(Point, Point, int, float);
	~Wall();

	bool validate();
	int getRaysCount();
	int getOwnerIndex();
	double getLength();
	Segment& getSegment();
	Point getPoint1();
	Point getPoint2();

private:
	Segment segment;
	double length = 0;
	int ownerIndex = -1, rays = 0;
};

#endif /* WALL_H_ */
