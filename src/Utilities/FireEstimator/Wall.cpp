/*
 * Wall.cpp
 *
 *  Created on: Dec 1, 2013
 *      Author: arya
 */

#include "Wall.h"
#include "../../WorldModel/Objects/Building.h"

Wall::Wall(int x1, int y1, int x2, int y2, int buildingIndex, float rayRate)
{
	segment = Segment(Point(x1, y1), Point(x2, y2));
	length = segment.getLength();
	rays = (int) ceil(length * rayRate);
	this->ownerIndex = buildingIndex;
}

Wall::Wall(Point a, Point b, int buildingIndex, float rayRate)
{
	segment = Segment(a, b);
	length = segment.getLength();
	rays = (int) ceil(length * rayRate);
	this->ownerIndex = buildingIndex;
}

Wall::~Wall()
{
}

int Wall::getRaysCount()
{
	return rays;
}

int Wall::getOwnerIndex()
{
	return ownerIndex;
}

double Wall::getLength()
{
	return length;
}

Segment& Wall::getSegment()
{
	return segment;
}

Point Wall::getPoint1()
{
	return segment.getFirstPoint();
}

Point Wall::getPoint2()
{
	return segment.getSecondPoint();
}

bool Wall::validate()
{
	return !(getPoint1().getX() == getPoint2().getX() && getPoint1().getY() == getPoint2().getY());
}
