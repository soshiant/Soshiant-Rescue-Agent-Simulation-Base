/*
 * SimulatedWorld.h
 *
 *  Created on: Nov 5, 2013
 *      Author: arya
 */

#ifndef SIMULATEDWORLD_H_
#define SIMULATEDWORLD_H_

#include "../../WorldModel/WorldModel.h"
#include "EstimatedData.h"
#include <iostream>

#define AIR_CAPACITY 0.2f
#define AIR_HEIGHT 30
#define SAMPLE_SIZE 5000

using namespace std;

class EstimatedData;

class SimulatedWorld
{
public:
	SimulatedWorld(WorldModel* world);
	~SimulatedWorld();

	vector<vector<double>>& getAirTemp();
	void setAirTemp(vector<vector<double>> airTemp);
	void setAirCellTemp(int x, int y, double temp);
	double getAirCellTemp(int x, int y);
	void setBuildingCells(Building*);
	void resetAirCells();
	int getMinX();
	int getMaxX();
	int getMinY();
	int getMaxY();


private:
	void initializeAir();

	vector<vector<double>> airTemp;
	WorldModel* world;
	int capacity;
};

#endif /* SIMULATEDWORLD_H_ */
