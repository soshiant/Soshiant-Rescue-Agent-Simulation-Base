/********************************************************************
 ********************************************************************
 ** C++ class implementation of the Hungarian algorithm by David Schwarz, 2012
 **
 **
 ** O(n^3) implementation derived from libhungarian by Cyrill Stachniss, 2004
 **
 **
 ** Solving the Minimum Assignment Problem using the
 ** Hungarian Method.
 **
 ** ** This file may be freely copied and distributed! **
 **
 **
 ** This file is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied
 ** warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 ** PURPOSE.
 **
 ********************************************************************
 ********************************************************************/

#include <iostream>
#include <vector>

#ifndef HUNGARIAN_H
#define HUNGARIAN_H

using namespace std;

class HungarianAssignment
{

public:
	HungarianAssignment(vector<vector<int> > cost);
	vector<vector<bool> > hungarian();

private:
	void init_labels();
	void update_labels();
	void add_to_tree(int x, int prevx);
	void augment();

	vector<vector<int> > cost;          //cost matrix
	int n, max_match;        //n workers and n jobs
	vector<int> lx, ly;        //labels of X and Y parts
	vector<int> xy;               //xy[x] - vertex that is matched with x,
	vector<int> yx;               //yx[y] - vertex that is matched with y
	vector<bool> S, T;         //sets S and T in algorithm
	vector<int> slack;            //as in the algorithm description
	vector<int> slackx;           //slackx[y] such a vertex, that
	// l(slackx[y]) + l(y) - w(slackx[y],y) = slack[y]
	vector<int> prev;             //array for memorizing alternating paths

	vector<vector<bool> > result;
};

#endif
