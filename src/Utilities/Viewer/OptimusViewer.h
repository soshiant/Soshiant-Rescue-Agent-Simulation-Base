/*
 * OptimusViewer.h
 *
 *  Created on: Dec 5, 2013
 *      Author: arya
 */

#ifndef OPTIMUSVIEWER_H_
#define OPTIMUSVIEWER_H_

#include "../../WorldModel/WorldModel.h"

#if IS_STATIC

#include "../FireEstimator/EstimatedData.h"
#include "../Clustering/Slice.h"
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/OpenGL.hpp>
#include <sstream>
#include <boost/thread/mutex.hpp>
#include <math.h>

#define SSTR(x) dynamic_cast<std::ostringstream&>(std::ostringstream() << std::dec << x).str()

using namespace std;
using namespace sf;

namespace viewer
{
class OptimusViewer
{
public:
	OptimusViewer(WorldModel*, WorldGraph*);
	~OptimusViewer();

	void start();

	boost::mutex mutex;

private:
	void initializeLayersVisibility();
	void graphLayer();
	void agentLayer();
	void buildingLayer();
	void fireBlockLayer();
	void blockadeLayer();
	void civilianLayer();
	void roadLayer();
	void visionLayer();
	void drawKeyInfo();
	void handleEvents();
	void handleKeyboardInput(Event);
	Point convertToSFMLCoordination(Point);
	Color getBuildingFillColor(Building*);
	Color getEdgeColor(Edge*);
	Color getCivilianFillColor(Human*);

	RenderWindow* window;
	WorldModel* world;
	WorldGraph* worldGraph;
	View* view;
	Vector2i lastMousePos;
	Text text;
	Font font;
	bool agentLayerVis, buildingLayerVis, roadLayerVis, blockadeLayerVis, graphLayerVis, fireBlockLayerVis, visionLayerVis, validPartVis, pizzaSlices;
	bool useFireEstimator;
	int width, height;
};

} /* namespace viewer */

#endif /* OPTIMUSVIEWER_H_ */
#endif /* IS_STATIC */
