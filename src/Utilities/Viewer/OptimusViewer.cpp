/*
 * OptimusViewer.cpp
 *
 *  Created on: Dec 5, 2013
 *      Author: arya
 */

#include "OptimusViewer.h"
#if IS_STATIC
namespace viewer
{
	OptimusViewer::OptimusViewer(WorldModel* world, WorldGraph* worldGraph)
	{
		this->world = world;
		this->worldGraph = worldGraph;

		string type;
		if (world->self->isAmbulanceCenter())
			type = "AmbulanceCenter";
		else if (world->self->isAmbulanceTeam())
			type = "AmbulanceTeam";
		else if (world->self->isFireBrigade())
			type = "FireBrigade";
		else if (world->self->isFireStation())
			type = "FireStation";
		else if (world->self->isPoliceForce())
			type = "PoliceForce";
		else if (world->self->isPoliceOffice())
			type = "PoliceOffice";

		string id = SSTR(world->self->getId());
		string title = type + ": " + id;

		width = 800;
		height = 600;

		ContextSettings settings;
		settings.depthBits = 32;
		settings.stencilBits = 8;
		settings.antialiasingLevel = 8;
		settings.majorVersion = 3;
		settings.minorVersion = 0;
		window = new RenderWindow(VideoMode(width, height), title, Style::Default, settings);
		window->setFramerateLimit(18);
		view = new View(FloatRect(0, 0, width, height));
		view->setViewport(FloatRect(0.f, 0.f, 1.f, 1.f));
		window->setView(*view);
		window->clear(Color(130, 130, 130, 255));
		window->display();

		lastMousePos.x = -1;
		lastMousePos.y = -1;

		font.loadFromFile("RobotoCondensed-Regular.ttf");
		text.setFont(font);
		text.setCharacterSize(12);
		text.setColor(Color::White);
		text.setPosition(5, 5);

		initializeLayersVisibility();
	}

	OptimusViewer::~OptimusViewer()
	{
		delete window;
	}

	void OptimusViewer::initializeLayersVisibility()
	{
		roadLayerVis = true;
		buildingLayerVis = true;
		blockadeLayerVis = true;
		agentLayerVis = true;
		useFireEstimator = false;
		fireBlockLayerVis = false;
		graphLayerVis = false;
		visionLayerVis = false;
		validPartVis = false;
		pizzaSlices = false;
	}

	void OptimusViewer::start()
	{
		Clock c;
		while (true)
		{
			if (c.getElapsedTime().asMilliseconds() > 56)
			{
				window->clear(Color(120, 120, 120, 255));
				handleEvents();
				mutex.lock();
				// Draw!
				buildingLayer();
				roadLayer();
				fireBlockLayer();
				blockadeLayer();
				graphLayer();
				civilianLayer();
				agentLayer();
				visionLayer();
				drawKeyInfo();

				// Don't Draw! :|
				mutex.unlock();
				window->setView(*view);
				window->display();
			}
		}
	}

	void OptimusViewer::handleKeyboardInput(Event event)
	{
		if (event.key.code == Keyboard::Num1)
			buildingLayerVis = !buildingLayerVis;
		else if (event.key.code == Keyboard::Num2)
			agentLayerVis = !agentLayerVis;
		else if (event.key.code == Keyboard::Num3)
			blockadeLayerVis = !blockadeLayerVis;
		else if (event.key.code == Keyboard::Num4)
			roadLayerVis = !roadLayerVis;
		else if (event.key.code == Keyboard::Num5)
			useFireEstimator = !useFireEstimator;
		else if (event.key.code == Keyboard::Num6)
			visionLayerVis = !visionLayerVis;
		else if (event.key.code == Keyboard::Num7)
			graphLayerVis = !graphLayerVis;
		else if (event.key.code == Keyboard::Num8)
			fireBlockLayerVis = !fireBlockLayerVis;
		else if (event.key.code == Keyboard::Num9)
			validPartVis = !validPartVis;
		else if (event.key.code == Keyboard::Num0)
			pizzaSlices = !pizzaSlices;
	}

	void OptimusViewer::handleEvents()
	{
		Event event;

		while (window->pollEvent(event))
		{
			if (event.type == Event::KeyPressed)
				handleKeyboardInput(event);

			if (event.type == Event::MouseWheelMoved)
			{
				if (event.mouseWheel.delta > 0)
					view->zoom(0.87f);
				else if (event.mouseWheel.delta < 0)
					view->zoom(1.13f);
			}
			else if (Mouse::isButtonPressed(Mouse::Left))
			{
				Vector2i pixelPos = Mouse::getPosition(*window);

				if (lastMousePos.x == -1)
				{
					lastMousePos = pixelPos;
				}
				else
				{
					float zoom = window->getSize().x / view->getSize().x;
					Vector2i diff = pixelPos - lastMousePos;
					lastMousePos = pixelPos;
					view->move(-diff.x / zoom, -diff.y / zoom);
				}
			}
			else
			{
				lastMousePos.x = -1;
				lastMousePos.y = -1;
			}
		}
	}

	Point OptimusViewer::convertToSFMLCoordination(Point p)
	{
		double xScale = window->getSize().x / world->getWorldBound().getPoint2().getX();
		double yScale = window->getSize().y / world->getWorldBound().getPoint3().getY();

		p.setX(p.getX() * xScale);
		p.setY(window->getSize().y - (p.getY() * yScale));

		return p;
	}

	void OptimusViewer::drawKeyInfo()
	{
		string info = "Keys:\nBuildingLayer: Num1\nAgentLayer: Num2\nBlockadeLayer: Num3\nRoadLayer: Num4";
		text.setString(info);
		text.setPosition(view->getCenter().x - width / 2, view->getCenter().y - height / 2);
		//		float zoom = window->getSize().x / view->getSize().x;
		//		text.setCharacterSize(text.getCharacterSize() * zoom);

		window->draw(text);
	}

	void OptimusViewer::roadLayer()
	{
		if (!roadLayerVis)
			return;

		for (u_int i = 0; i < world->roads.size(); i++)
		{
			Road* r = world->roads[i];
			Color c = Color(215, 215, 215, 255);
//			for (FireBlock* block : world->fireBlocks)
//			{
//				if (find(block->getAroundRoadsForPoliceForces().begin(), block->getAroundRoadsForPoliceForces().end(), r) != block->getAroundRoadsForPoliceForces().end())
//				{
//					c = Color(255, 0, 0, 255);
//				}
//			}

			sf::ConvexShape convex;
			convex.setFillColor(c);
			convex.setOutlineThickness(0.45f);
			convex.setOutlineColor(Color(70, 70, 70, 255));
			convex.setPointCount(r->getShape().size());

			for (int j = 0; j < r->getShape().size(); j++)
			{
				Point p = convertToSFMLCoordination(r->getShape().getVertex(j));
				convex.setPoint(j, sf::Vector2f(p.getX(), p.getY()));
			}

			window->draw(convex);

			if (validPartVis)
			{
				for (Polygon poly : r->validParts[0])
				{
					sf::ConvexShape convex;
					convex.setFillColor(Color(100, 130, 50, 150));
					convex.setOutlineThickness(0.1f);
					convex.setOutlineColor(Color(255, 0, 255, 150));
					convex.setPointCount(poly.size());

					for (int j = 0; j < poly.size(); j++)
					{
						Point p = convertToSFMLCoordination(poly.getVertex(j));
						convex.setPoint(j, sf::Vector2f(p.getX(), p.getY()));
					}

					window->draw(convex);
				}

				for (Polygon poly : r->validParts[1])
				{
					sf::ConvexShape convex;
					convex.setFillColor(Color(150, 100, 80, 100));
					convex.setOutlineThickness(0.1f);
					convex.setOutlineColor(Color(0, 255, 255, 150));
					convex.setPointCount(poly.size());

					for (int j = 0; j < poly.size(); j++)
					{
						Point p = convertToSFMLCoordination(poly.getVertex(j));
						convex.setPoint(j, sf::Vector2f(p.getX(), p.getY()));
					}

					window->draw(convex);
				}
			}
		}

//		MotionlessObject* mo1 = world->motionlessObjects[126];
//		sf::ConvexShape convex;
//		convex.setFillColor(Color::Cyan);
//		convex.setOutlineThickness(0.1f);
//		convex.setOutlineColor(Color(255, 0, 255, 150));
//		convex.setPointCount(mo1->getShape().size());
//		for (int j = 0; j < mo1->getShape().size(); j++)
//		{
//			Point p = convertToSFMLCoordination(mo1->getShape().getVertex(j));
//			convex.setPoint(j, sf::Vector2f(p.getX(), p.getY()));
//		}
//		window->draw(convex);
//
//		MotionlessObject* mo2 = world->motionlessObjects[8];
//		convex.setFillColor(Color::Yellow);
//		convex.setOutlineThickness(0.1f);
//		convex.setOutlineColor(Color(255, 0, 255, 150));
//		convex.setPointCount(mo2->getShape().size());
//		for (int j = 0; j < mo2->getShape().size(); j++)
//		{
//			Point p = convertToSFMLCoordination(mo2->getShape().getVertex(j));
//			convex.setPoint(j, sf::Vector2f(p.getX(), p.getY()));
//		}
//		window->draw(convex);

//		for (int index : ((Road*) world->objects[6837])->buildingWithThisRoadCanSee)
//		{
//			Building* building = world->buildings[index];
//
//			ConvexShape convex;
//			convex.setPointCount(building->getShape().size());
//			for (int j = 0; j < building->getShape().size(); j++)
//			{
//				Point p = convertToSFMLCoordination(building->getShape().getVertex(j));
//				convex.setPoint(j, sf::Vector2f(p.getX(), p.getY()));
//			}
//			convex.setOutlineThickness(0.6f);
//			convex.setFillColor(Color(200, 200, 0, 255));
//			convex.setOutlineColor(sf::Color(40, 40, 40));
//
//			window->draw(convex);
//		}

//		for (Polygon poly : world->validParts)
//		{
//			sf::ConvexShape convex;
//			convex.setFillColor(Color(200, 120, rand() % 255, 255));
//			convex.setOutlineThickness(0.2f);
//			convex.setOutlineColor(Color(255, 0, 255, 255));
//			convex.setPointCount(poly.size());
//
//			for (int j = 0; j < poly.size(); j++)
//			{
//				Point p = convertToSFMLCoordination(poly.getVertex(j));
//				convex.setPoint(j, sf::Vector2f(p.getX(), p.getY()));
//			}
//
//			window->draw(convex);
//		}
	}

	void OptimusViewer::graphLayer()
	{
		if (!graphLayerVis)
			return;

		for (Edge* edge : worldGraph->getEdges())
		{
			Point fp = convertToSFMLCoordination(worldGraph->getNodes()[edge->getFirstNodeIndex()]->getRepresentivePoint());
			Point sp = convertToSFMLCoordination(worldGraph->getNodes()[edge->getSecondNodeIndex()]->getRepresentivePoint());

			Vertex v1 = sf::Vertex(sf::Vector2f((float) fp.getX(), (float) fp.getY()));
			Vertex v2 = sf::Vertex(sf::Vector2f((float) sp.getX(), (float) sp.getY()));

			sf::Vertex line[] =
			{v1, v2};
			Color lineColor = getEdgeColor(edge);
			line[0].color = lineColor;
//			lineColor.g += 200;
			line[1].color = lineColor;

			window->draw(line, 2, Lines);
		}

		for (Node* node : worldGraph->getNodes())
		{
			Point fp = convertToSFMLCoordination(node->getRepresentivePoint());

		}
	}

	void OptimusViewer::buildingLayer()
	{
		if (buildingLayerVis)
		{
			for (Building* building : world->buildings)
			{
				ConvexShape convex;
				convex.setPointCount(building->getShape().size());
				for (int j = 0; j < building->getShape().size(); j++)
				{
					Point p = convertToSFMLCoordination(building->getShape().getVertex(j));
					convex.setPoint(j, sf::Vector2f(p.getX(), p.getY()));
				}
				convex.setOutlineThickness(0.6f);
				convex.setFillColor(getBuildingFillColor(building));
				convex.setOutlineColor(sf::Color(40, 40, 40));

				window->draw(convex);
			}

			for (Building* building : world->suspiciousBuildings)
			{
				ConvexShape convex;
				convex.setPointCount(building->getShape().size());
				for (int j = 0; j < building->getShape().size(); j++)
				{
					Point p = convertToSFMLCoordination(building->getShape().getVertex(j));
					convex.setPoint(j, sf::Vector2f(p.getX(), p.getY()));
				}
				convex.setOutlineThickness(0.6f);
				convex.setFillColor(Color::Yellow);
				convex.setOutlineColor(sf::Color(40, 40, 40));

				window->draw(convex);
			}
		}

		if (world->bestSus != nullptr)
		{
			ConvexShape convex;
			convex.setPointCount(world->bestSus->getShape().size());
			for (int j = 0; j < world->bestSus->getShape().size(); j++)
			{
				Point p = convertToSFMLCoordination(world->bestSus->getShape().getVertex(j));
				convex.setPoint(j, sf::Vector2f(p.getX(), p.getY()));
			}
			convex.setOutlineThickness(0.6f);
			convex.setFillColor(Color::Green);
			convex.setOutlineColor(sf::Color(40, 40, 40));

			window->draw(convex);
		}

//		for (InSightBuilding* isb : ((Building*) world->objects[19490])->getInSightBuildings())
//		{
//			Building* building = world->buildings[isb->buildingIndex];
//			ConvexShape convex;
//			convex.setPointCount(building->getShape().size());
//			for (int j = 0; j < building->getShape().size(); j++)
//			{
//				Point p = convertToSFMLCoordination(building->getShape().getVertex(j));
//				convex.setPoint(j, sf::Vector2f(p.getX(), p.getY()));
//			}
//			convex.setOutlineThickness(0.6f);
//			convex.setFillColor(Color::Cyan);
//			convex.setOutlineColor(sf::Color(40, 40, 40));
//
//			window->draw(convex);
//		}

		if (pizzaSlices)
		{
			int cc = 0;
			for (Slice* slice : world->pizzaSlices)
			{
				Color c = Color(cc * 60 % 255, cc * 80 % 255, 50);
				for (Building* building : slice->getBuildings())
				{
					ConvexShape convex;
					convex.setPointCount(building->getShape().size());
					for (int j = 0; j < building->getShape().size(); j++)
					{
						Point p = convertToSFMLCoordination(building->getShape().getVertex(j));
						convex.setPoint(j, sf::Vector2f(p.getX(), p.getY()));
					}
					convex.setOutlineThickness(0.6f);
					convex.setFillColor(c);
					convex.setOutlineColor(Color(50, 50, 50));

					window->draw(convex);
				}
				cc++;
			}
		}

//		int id = 50017;
//		for (InSightBuilding* isb : ((Building*) world->objects[id])->getInSightBuildings())
//		{
//			Building* building = world->buildings[isb->buildingIndex];
//			ConvexShape convex;
//			convex.setPointCount(building->getShape().size());
//			for (int j = 0; j < building->getShape().size(); j++)
//			{
//				Point p = convertToSFMLCoordination(building->getShape().getVertex(j));
//				convex.setPoint(j, sf::Vector2f(p.getX(), p.getY()));
//			}
//			convex.setOutlineThickness(0.6f);
//			convex.setFillColor(Color::Red);
//			convex.setOutlineColor(sf::Color(40, 40, 40));
//
//			window->draw(convex);
//		}
//		Building* building = ((Building*) world->objects[id]);
//		ConvexShape convex2;
//		convex2.setPointCount(building->getShape().size());
//		for (int j = 0; j < building->getShape().size(); j++)
//		{
//			Point p = convertToSFMLCoordination(building->getShape().getVertex(j));
//			convex2.setPoint(j, sf::Vector2f(p.getX(), p.getY()));
//		}
//		convex2.setOutlineThickness(0.6f);
//		convex2.setFillColor(Color::Cyan);
//		convex2.setOutlineColor(sf::Color(40, 40, 40));
//
//		window->draw(convex2);
	}

	void OptimusViewer::fireBlockLayer()
	{
		if (!fireBlockLayerVis)
		{
			return;
		}

		for (u_int i = 0; i < world->fireBlocks.size(); i++)
		{
			if (world->fireBlocks[i] == nullptr)
			{
				continue;
			}

			vector<Building*> aroundBuildings = world->fireBlocks[i]->getAroundBuildings();

			for (Building* building : world->fireBlocks[i]->getAllBuildings())
			{
				ConvexShape convex;
				convex.setPointCount(building->getShape().size());
				for (int j = 0; j < building->getShape().size(); j++)
				{
					Point p = convertToSFMLCoordination(building->getShape().getVertex(j));
					convex.setPoint(j, sf::Vector2f(p.getX(), p.getY()));
				}
				convex.setOutlineThickness(0.6f);
				convex.setOutlineColor(sf::Color(40, 40, 40));

				Color color;
				if (std::find(aroundBuildings.begin(), aroundBuildings.end(), building) != aroundBuildings.end())
				{
					color = Color(200, 0, 0);
				}
				else
				{
					color = Color(255 - 70 * i % 256, 255 - 40 * i % 256, 255 - 30 * i % 256);
				}
				convex.setFillColor(color);

				window->draw(convex);

				for (Road* r : world->fireBlocks[i]->getAroundRoadsForPoliceForces())
				{
					sf::ConvexShape convex;
					convex.setFillColor(Color(255 - 70 * i % 256, 255 - 40 * i % 256, 255 - 30 * i % 256));
					convex.setOutlineThickness(0.45f);
					convex.setOutlineColor(Color(70, 70, 70, 255));
					convex.setPointCount(r->getShape().size());

					for (int j = 0; j < r->getShape().size(); j++)
					{
						Point p = convertToSFMLCoordination(r->getShape().getVertex(j));
						convex.setPoint(j, sf::Vector2f(p.getX(), p.getY()));
					}

					window->draw(convex);
				}
			}
		}
	}

	void OptimusViewer::blockadeLayer()
	{
		if (!blockadeLayerVis)
			return;

		for (Blockade* b : world->blockades)
		{
			for (int j = 0; j < b->getShape().size(); j++)
			{
				ConvexShape convex;
				Polygon increasedBlockade = b->getShape();		//increasePolygon(b->getShape(), 2*AGENT_RADIUS);
				convex.setPointCount(increasedBlockade.size());
				for (int j = 0; j < increasedBlockade.size(); j++)
				{
					Point p = convertToSFMLCoordination(increasedBlockade.getVertex(j));
					convex.setPoint(j, sf::Vector2f(p.getX(), p.getY()));
				}
				convex.setFillColor(Color::Black);
				convex.setOutlineThickness(0.1f);
				convex.setOutlineColor(sf::Color(100, 100, 100, 150));

				window->draw(convex);
			}
		}
	}

	void OptimusViewer::agentLayer()
	{
		if (!agentLayerVis)
			return;

		if (world->clearArea.size() > 0)
		{
			ConvexShape convex;
			convex.setPointCount(world->clearArea.size());
			for (int j = 0; j < world->clearArea.size(); j++)
			{
				Point p = convertToSFMLCoordination(world->clearArea.getVertex(j));
				convex.setPoint(j, sf::Vector2f(p.getX(), p.getY()));
			}
			convex.setOutlineThickness(0.1f);
			convex.setFillColor(sf::Color(0, 100, 255, 100));
			convex.setOutlineColor(sf::Color(0, 100, 255));

			window->draw(convex);
		}

		const float THICKNESS = 1.0f * (view->getSize().x / width);

		CircleShape circlee(5 * (view->getSize().x / width));
		circlee.setOutlineThickness(THICKNESS);
		circlee.setOutlineColor(Color::Yellow);
		circlee.setFillColor(Color::Red);
		Point posss = convertToSFMLCoordination(world->clearrrrrpoint);
		circlee.setPosition(posss.getX(), posss.getY());
		window->draw(circlee);

		for (u_int i = 0; i < world->platoons.size(); i++)
		{
//			double scale = window->getSize().x / world->getWorldBound().getPoint2().getX();
//			CircleShape realAgent(1000 * scale);
//			realAgent.setFillColor(Color(0, 0, 0, 0));
//			realAgent.setOutlineColor(Color::Cyan);
//			realAgent.setOutlineThickness(0.1);

			CircleShape circle(5 * (view->getSize().x / width));
			circle.setOutlineThickness(THICKNESS);
			if (world->platoons[i]->isFireBrigade())
			{
				circle.setOutlineColor(Color::Red);
				circle.setFillColor(Color::Red);
			}
			else if (world->platoons[i]->isAmbulanceTeam())
			{
				circle.setOutlineColor(Color::White);
				circle.setFillColor(Color::White);
			}
			else if (world->platoons[i]->isPoliceForce())
			{
				circle.setOutlineColor(Color::Blue);
				circle.setFillColor(Color::Blue);
			}

			if (world->self == world->platoons[i])
			{
				circle.setOutlineColor(Color::Yellow);
			}

			Point pos = convertToSFMLCoordination(world->platoons[i]->getPos());
			circle.setPosition(pos.getX(), pos.getY());
//			realAgent.setPosition(pos.getX(), pos.getY());
			window->draw(circle);

			if (world->getTime() > 3)
			{
				CircleShape node(2 * (view->getSize().x / width));
				node.setFillColor(Color::Magenta);
				if (world->platoons[i]->getRepresentiveNodeIndex() != -1)
				{
					Point nodePos = convertToSFMLCoordination(
							worldGraph->getNodes()[world->platoons[i]->getRepresentiveNodeIndex()]->getRepresentivePoint());
					node.setPosition(nodePos.getX(), nodePos.getY());
					window->draw(node);
				}
			}
//			window->draw(realAgent);
		}
	}

	void OptimusViewer::visionLayer()
	{
		if (!visionLayerVis)
		{
			return;
		}

		for (Segment ray : world->visionRays)
		{
			Point fp = convertToSFMLCoordination(ray.getFirstPoint());
			Point sp = convertToSFMLCoordination(ray.getSecondPoint());

			Vertex v1 = sf::Vertex(sf::Vector2f((float) fp.getX(), (float) fp.getY()));
			Vertex v2 = sf::Vertex(sf::Vector2f((float) sp.getX(), (float) sp.getY()));

			sf::Vertex line[] =
			{v1, v2};
			Color lineColor = Color::Red;
			line[0].color = lineColor;
			line[1].color = lineColor;

			window->draw(line, 2, Lines);
		}
	}

	void OptimusViewer::civilianLayer()
	{
		const float THICKNESS = 1.0f * (view->getSize().x / width);

		for (u_int i = 0; i < world->civilians.size(); i++)
		{
			CircleShape circle(5 * (view->getSize().x / width));
			circle.setOutlineThickness(THICKNESS);
			circle.setOutlineColor(Color::Green);
			circle.setFillColor(getCivilianFillColor(world->civilians[i]));

			Point pos = convertToSFMLCoordination(world->civilians[i]->getPos());
			circle.setPosition(pos.getX(), pos.getY());

			window->draw(circle);
		}
	}

	Color OptimusViewer::getEdgeColor(Edge* edge)
	{
		if (edge->getPassingMode() == PM_PASSABLE)
		{
			return Color::Blue;
		}
		else if (edge->getPassingMode() == PM_SEMI_PASSABLE)
		{
			return Color::Cyan;
		}
		else if (edge->getPassingMode() == PM_NOT_PASSABLE)
		{
			return Color::Red;
		}
		else
		{
			return Color::Black;
		}
	}

	Color OptimusViewer::getBuildingFillColor(Building* building)
	{
		if (building->isRefuge())
		{
			return Color::Green;
		}
		if (building->isCenter())
		{
			return Color::Cyan;
		}
		if (useFireEstimator)
		{
			if (building->estimatedData->getFieryness() == 0)
			{
				int brokeness = 150 - building->getBrokenness() / 2;
				if (building->getBrokenness() < 0 || building->getBrokenness() > 200)
					brokeness = 150;
				int color = max(0, brokeness);
				return Color(color, color, color, 255);
			}
			else if (building->estimatedData->getFieryness() == 1)
				return Color(176, 176, 56, 128);
			else if (building->estimatedData->getFieryness() == 2)
				return Color(204, 122, 50, 128);
			else if (building->estimatedData->getFieryness() == 3)
				return Color(160, 52, 52, 128);
			else if (building->estimatedData->getFieryness() == 4)
				return Color(50, 120, 130, 128);
			else if (building->estimatedData->getFieryness() == 5)
				return Color(100, 140, 210, 128);
			else if (building->estimatedData->getFieryness() == 6)
				return Color(100, 70, 190, 128);
			else if (building->estimatedData->getFieryness() == 7)
				return Color(80, 60, 140, 128);
			else if (building->estimatedData->getFieryness() == 8)
				return Color(0, 0, 0, 255);
		}
		else
		{
			if (building->getFieryness() == 0)
			{
				int brokeness = 150 - building->getBrokenness() / 2;
				if (building->getBrokenness() < 0 || building->getBrokenness() > 200)
					brokeness = 150;
				int color = max(0, brokeness);
				return Color(color, color, color, 255);
			}
			else if (building->getFieryness() == 1)
				return Color(176, 176, 56, 128);
			else if (building->getFieryness() == 2)
				return Color(204, 122, 50, 128);
			else if (building->getFieryness() == 3)
				return Color(160, 52, 52, 128);
			else if (building->getFieryness() == 4)
				return Color(50, 120, 130, 128);
			else if (building->getFieryness() == 5)
				return Color(100, 140, 210, 128);
			else if (building->getFieryness() == 6)
				return Color(100, 70, 190, 128);
			else if (building->getFieryness() == 7)
				return Color(80, 60, 140, 128);
			else if (building->getFieryness() == 8)
				return Color(0, 0, 0, 255);
		}
	}

	Color OptimusViewer::getCivilianFillColor(Human* human)
	{
		if (human->getTimeToDeath() < 20)
			return Color(50, 90, 50, 255);
		else if (human->getTimeToDeath() < 60)
			return Color(85, 140, 85, 255);
		else if (human->getTimeToDeath() < 120)
			return Color(120, 190, 120, 255);
		else if (human->getTimeToDeath() < 300)
			return Color(160, 230, 160, 255);
		else
			return Color::Green;
	}

} /* namespace viewer */
#endif /* IS_STATIC */
