/*
 * UpdatableHeap.h
 *
 *  Created on: Apr 29, 2014
 *      Author: arya
 */

#ifndef UPDATABLEHEAP_H_
#define UPDATABLEHEAP_H_

#include <vector>

using namespace std;

struct Entry
{
		int value;
		int index;
};

class UpdatableHeap
{
	public:
		void init(int size);
		void clear();
		void insert(int index, int value);
		void update(int index, int newValue);
		bool empty();
		int extractMin();

	private:
		int lChild(int id);
		int rChild(int id);
		int parent(int id);
		void bubbleUp(int id);
		void bubbleDown(int id);

		const static int ROOT = 0;

		int size;

		vector<int> places;
		vector<Entry> entries;
};

#endif /* UPDATABLEHEAP_H_ */
