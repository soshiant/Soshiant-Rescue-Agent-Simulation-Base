/*
 * UpdatableHeap.cpp
 *
 *  Created on: Apr 29, 2014
 *      Author: arya
 */

#include "UpdatableHeap.h"

int UpdatableHeap::lChild(int id)
{
	return id * 2 + 1;
}

int UpdatableHeap::rChild(int id)
{
	return id * 2 + 2;
}

int UpdatableHeap::parent(int id)
{
	return (id - 1) / 2;
}

void UpdatableHeap::bubbleUp(int id)
{
	if (id != ROOT && entries[id].value < entries[parent(id)].value)
	{
		places[entries[parent(id)].index] = id;
		places[entries[id].index] = parent(id);
		swap(entries[id], entries[parent(id)]);
		bubbleUp(parent(id));
	}
}

void UpdatableHeap::bubbleDown(int id)
{
	int minId = id;
	int minVal = entries[id].value;

	if (lChild(id) < size && minVal > entries[lChild(id)].value)
	{
		minId = lChild(id);
		minVal = entries[lChild(id)].value;
	}

	if (rChild(id) < size && minVal > entries[rChild(id)].value)
	{
		minId = rChild(id);
		minVal = entries[rChild(id)].value;
	}

	if (minId != id)
	{
		places[entries[minId].index] = id;
		places[entries[id].index] = minId;
		swap(entries[id], entries[minId]);
		bubbleDown(minId);
	}
}

void UpdatableHeap::init(int size)
{
	this->size = 0;
	places.resize(size, -1);
}

void UpdatableHeap::insert(int index, int value)
{
	places[index] = size;
	Entry newEntry;
	newEntry.index = index;
	newEntry.value = value;
	entries.push_back(newEntry);
	bubbleUp(size);
	size++;
}

void UpdatableHeap::update(int index, int newValue)
{
	int id = places[index];
	entries[id].value = newValue;

	if (entries[id].value < entries[parent(id)].value)
	{
		bubbleUp(id);
	}
	else
	{
		bubbleDown(id);
	}
}

int UpdatableHeap::extractMin()
{
	if (size == 0)
	{
		return -1;
	}
	
	int tmp = entries[ROOT].index;
	entries[ROOT] = entries[size - 1];
	places[entries[ROOT].index] = ROOT;
	entries.pop_back();
	size--;
	bubbleDown(ROOT);

	return tmp;
}

bool UpdatableHeap::empty()
{
	return size == 0;
}

void UpdatableHeap::clear()
{
	size = 0;
	entries.clear();
	for (int i = 0; i < (int) places.size(); i++)
	{
		places[i] = -1;
	}
}
