#ifndef __BASICS_H_
#define __BASICS_H_

#include <string>
#include <cmath>

namespace basics
{
extern void WarnLoadingTrue(std::string);
extern void WarnLoadingFalse(std::string);
}

#endif //__BISICS_H_
