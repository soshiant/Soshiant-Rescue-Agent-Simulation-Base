/*
 * AmbulanceTeamStateEstimator.cpp
 *
 *  Created on: Nov 25, 2013
 *      Author: Pedram
 */

#include "AmbulanceTeamStateEstimator.h"

AmbulanceTeamStateEstimator::AmbulanceTeamStateEstimator(AmbulanceTeam* self)
{
	this->self = self;
	workingOn = nullptr;
	previousBusyTime = 0;
	moveToRescue = 0;
	moveToRefuge = 0;
	rescue = 0;
	unload = 0;
	load = 0;
}

AmbulanceTeamStateEstimator::~AmbulanceTeamStateEstimator()
{
}

void AmbulanceTeamStateEstimator::progress(int time)
{
	if (workingOn == nullptr)
	{
		return;
	}
	if (!workingOn->isAvailable || workingOn->getMotionlessObject()->isRefuge())
	{
		workingOn = nullptr;
		previousBusyTime = 0;
		moveToRescue = 0;
		moveToRefuge = 0;
		rescue = 0;
		unload = 0;
		load = 0;
		return;
	}

	if (previousBusyTime > 0)
	{
		previousBusyTime--;
		return;
	}
	progressBeforeRescue();
	if (moveToRescue == 0)
	{
		progressRescue(time);
	}
	if (rescue == 0)
	{
		progressAfterRescue();
	}

	if (getEstimatedBusyTime() == 0)
	{
		this->workingOn = nullptr;
		this->previousBusyTime = 0;
		this->moveToRescue = 0;
		this->moveToRefuge = 0;
		this->rescue = 0;
		this->unload = 0;
		this->load = 0;
	}
}

int AmbulanceTeamStateEstimator::getEstimatedBusyTime()
{
	int team = workingOnMyTarget.size();
	if (team == 0)
	{
		return 0;
	}

	return previousBusyTime + moveToRescue + (rescue / team) + load + moveToRefuge + unload;
}

void AmbulanceTeamStateEstimator::progressBeforeRescue()
{
	if (self->getMotionlessObject() == workingOn->getMotionlessObject())
	{
		moveToRescue = 0;
	}
	else
	{
		moveToRescue = max(0, (int) moveToRescue - 1);
	}
}

void AmbulanceTeamStateEstimator::progressRescue(int time)
{
	if (workingOn->getLastCycleUpdated() == time) //Synchronize
	{
		int bSize = workingOn->getBuriednessHistory().size();
		if (bSize >= 2 && workingOn->getBuriednessHistory()[bSize - 1].buriedness != workingOn->getBuriednessHistory()[bSize - 2].buriedness)
		{
			rescue = max(0, (int) (workingOn->getBuriedness() - workingOnMyTarget.size()));
		}
		else
		{
			rescue = workingOn->getBuriedness();
		}
	}
	else
	{
		rescue = max(0, (int) (rescue - workingOnMyTarget.size()));
	}
}

void AmbulanceTeamStateEstimator::progressAfterRescue()
{
	if (load > 0)
	{
		load--;
	}
	else if (moveToRefuge > 0)
	{
		moveToRefuge--;
	}
	else if (unload > 0)
	{
		unload--;
	}
}

Types::AmbulanceTeamState AmbulanceTeamStateEstimator::getState()
{
	if (workingOn == nullptr)
	{
		return Types::AT_STATE_NONE;
	}
	if (moveToRescue > 0)
	{
		return Types::AT_STATE_MOVE_TO_RESCUE;
	}
	if (rescue > 0)
	{
		return Types::AT_STATE_RESCUE;
	}
	if (load > 0)
	{
		return Types::AT_STATE_LOAD;
	}
	if (moveToRefuge > 0)
	{
		return Types::AT_STATE_MOVE_TO_REFUGE;
	}
	if (unload > 0)
	{
		return Types::AT_STATE_UNLOAD;
	}

	return Types::AT_STATE_NONE;
}

void AmbulanceTeamStateEstimator::setState(Human* workingOn, int moveToRescue, int moveToRefuge, vector<AmbulanceTeam*> workingOnThisHuman)
{
	if (workingOn == nullptr || workingOnThisHuman.empty())
	{
		this->workingOn = nullptr;
		this->previousBusyTime = 0;
		this->moveToRescue = 0;
		this->moveToRefuge = 0;
		this->rescue = 0;
		this->unload = 0;
		this->load = 0;
		return;
	}

	this->previousBusyTime = getEstimatedBusyTime();
	this->workingOn = workingOn;
	this->workingOnMyTarget = workingOnThisHuman;
	this->rescue = workingOn->getBuriedness();
	if (workingOn->isPlatoon() || loader() != self)
	{
		this->load = 0;
		this->unload = 0;
		this->moveToRefuge = 0;
	}
	else
	{
		this->load = 1;
		this->unload = 1;
		this->moveToRefuge = moveToRefuge;
	}
	this->moveToRescue = moveToRescue;
}

AmbulanceTeam* AmbulanceTeamStateEstimator::loader()
{
	if (workingOnMyTarget.empty())
	{
		return nullptr;
	}

	u_int minId = MAX_INT, bestIndex = 0;
	for (u_int i = 0; i < workingOnMyTarget.size(); i++)
	{
		if (workingOnMyTarget[i]->getId() < minId)
		{
			minId = workingOnMyTarget[i]->getId();
			bestIndex = i;
		}
	}

	return workingOnMyTarget[bestIndex];
}

void AmbulanceTeamStateEstimator::substractTeamMate(AmbulanceTeam* teamMate)
{
	for (u_int i = 0; i < workingOnMyTarget.size(); i++)
	{
		if (teamMate == workingOnMyTarget[i])
		{
			workingOnMyTarget.erase(workingOnMyTarget.begin() + i);
			return;
		}
	}
}

vector<AmbulanceTeam*> AmbulanceTeamStateEstimator::getWorkingOnMyTarget()
{
	return workingOnMyTarget;
}
