#include "Cluster.h"

Cluster::Cluster()
{
}

Cluster::Cluster(vector<MotionlessObject*> members)
{
	this->members = members;
}

void Cluster::setCenter(MotionlessObject* center)
{
	this->center = center;
}

void Cluster::setMembers(vector<MotionlessObject*> member)
{
	this->members = member;
}

void Cluster::addMember(MotionlessObject* member)
{
	this->members.push_back(member);
}

void Cluster::setCenteralPoint(Point p)
{
	this->centeralPoint = p;
}

void Cluster::setCenteralPoint()
{
	if (members.size() == 0)
		return;

	u_int x = members[0]->getX(), y = members[0]->getY();

	for (int i = 1; i < (int) members.size(); i++)
	{
		x += members[i]->getX();
		y += members[i]->getY();
	}

	centeralPoint.setX(x / members.size());
	centeralPoint.setY(y / members.size());
}

vector<MotionlessObject *> &Cluster::getMembers()
{
	return members;
}

MotionlessObject * Cluster::getCenter()
{
	return center;
}

Point Cluster::getCenteralPoint()
{
	return centeralPoint;
}

int Cluster::airDistToEntity(MotionlessObject* obj)
{
	int bestDist = MAX_INT;

	for (int i = 0; i < (int) this->members.size(); i++)
	{
		int tempDist = distanceBetweenPoints(this->members[i]->getPos(), obj->getPos());
		if (tempDist < bestDist)
		{
			bestDist = tempDist;
		}
	}

	return bestDist;
}

int Cluster::airDistToCluster(Cluster* cluster)
{
	int bestDist = cluster->airDistToEntity(this->members[0]);

	for (int i = 0; i < (int) this->members.size(); i++)
	{
		int tempDist = cluster->airDistToEntity(this->members[i]);
		if (tempDist < bestDist)
			bestDist = tempDist;
	}

	return bestDist;
}

int Cluster::groundDistToEntity(MotionlessObject* obj, WorldGraph* wg, GraphMode mode)
{
	int bestDist = MAX_INT;

	for (int i = 0; i < (int) this->members.size(); i++)
	{
		int tempDist = wg->getDistance(this->members[i]->getWithoutBlockadeRepresentiveNodeIndex(), obj->getWithoutBlockadeRepresentiveNodeIndex(), mode);
		if (tempDist < bestDist)
		{
			bestDist = tempDist;
		}
	}

	return bestDist;
}

int Cluster::groundDistToCluster(Cluster* cluster, WorldGraph* wg, GraphMode mode)
{
	int bestDist = MAX_INT;
	for (int i = 0; i < (int) this->members.size(); i++)
	{
		int tempDist = cluster->groundDistToEntity(this->members[i], wg, mode);
		if (tempDist < bestDist)
			bestDist = tempDist;
	}

	return bestDist;
}

int Cluster::getAirDistanceToNearestEntity(MotionlessObject* motionless)
{
	int dist = MAX_INT;
	for (MotionlessObject* mo : getMembers())
	{
		if (mo->getPos().distFromPoint(motionless->getPos()) < dist)
		{
			dist = mo->getPos().distFromPoint(motionless->getPos());
		}

	}

	return dist;
}
