#ifndef CLUSTER_H
#define	CLUSTER_H

#include <vector>

#include "../../WorldGraph/WorldGraph.h"
#include "../../WorldModel/Objects/MotionlessObject.h"
#include "../Geometry/GeometryFunctions.h"
#include "../Geometry/Point.h"
#include "../Types.h"

class WorldGraph;

using namespace std;
using namespace Geometry;
using namespace Types;

class Cluster
{
private:
	vector<MotionlessObject *> members;
	MotionlessObject * center;
	Point centeralPoint;

public:
	Cluster();
	Cluster(vector<MotionlessObject*> members);
	void addMember(MotionlessObject * member);
	void setMembers(vector<MotionlessObject *> member);
	void setCenter(MotionlessObject * center);
	void setCenteralPoint(Point p);
	void setCenteralPoint();

	vector<MotionlessObject *> &getMembers();
	MotionlessObject * getCenter();
	Point getCenteralPoint();

	int airDistToEntity(MotionlessObject* obj);
	int airDistToCluster(Cluster* cluster);

	int getAirDistanceToNearestEntity(MotionlessObject*);

	int groundDistToEntity(MotionlessObject*, WorldGraph*, GraphMode);
	int groundDistToCluster(Cluster*, WorldGraph*, GraphMode);
	int score = -1;
	int ID = -1;
};

#endif	/* CLUSTER_H */
