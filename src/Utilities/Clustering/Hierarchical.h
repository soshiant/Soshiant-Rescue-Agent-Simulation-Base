#ifndef HIERARCHICAL_H
#define	HIERARCHICAL_H

#include <iostream>
#include <vector>
#include "Cluster.h"
#include "../../WorldModel/Objects/MotionlessObject.h"

using namespace std;

class Hierarchical
{

public:
	Hierarchical();
	Hierarchical(vector<MotionlessObject *> tempMembers, int tempnumberOfCluster);
	~Hierarchical();
	void calculate();
	vector<Cluster *> &getClusters();
	void setNumberOfClusters(int n);
	void setMembers(vector<MotionlessObject *> &mem);
	void logClustering();

private:
	vector<MotionlessObject *> members;
	vector<Cluster *> clusters;
	int numberOfCluster;
	void makeFirstClusters();
	void makeCluster();
	void updateClustersCenter();
	void updateCluster();
};

#endif	/* HIERARCHICAL_H */

