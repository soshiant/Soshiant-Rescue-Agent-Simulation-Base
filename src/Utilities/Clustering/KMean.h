#ifndef KMEAN_H
#define	KMEAN_H

#include <iostream>
#include <vector>
#include "Cluster.h"
#include "KMean.h"
#include "../../WorldModel/Objects/MotionlessObject.h"

using namespace std;

class KMean
{
	private:
		vector<MotionlessObject *> members;
		vector<Cluster *> clusters;
		int numberOfClusters;

		MotionlessObject * getFirstCenter(vector<MotionlessObject *> mem);
		vector<MotionlessObject*> makeCenter();
		MotionlessObject * getFarestMember(vector<MotionlessObject *> mem, vector<MotionlessObject *> centers);
		void makeClusters(vector<MotionlessObject *> centers);
		void setClusterForThisOne(MotionlessObject * m);
		void updateCenteralPoints(vector<Cluster *> clusters);
		void updateMembers(vector<MotionlessObject *> c);
		bool isNotHere(vector<MotionlessObject *> c, MotionlessObject * m);

	public:
		KMean();
		KMean(vector<MotionlessObject *> objects, int numberOfClusters);
		~KMean();
		void calculate();
		void fastCalculate();
		vector<Cluster *> &getClusters();
		void setNumberOfClusters(int a);
		void setMembers(vector<MotionlessObject *> &objects);
};

#endif	/* KMEAN_H */

