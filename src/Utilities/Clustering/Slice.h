/*
 * Slice.h
 *
 *  Created on: Mar 28, 2014
 *      Author: arha
 */

#ifndef SLICE_H_
#define SLICE_H_

#include "../../WorldGraph/WorldGraph.h"
#include "../../WorldModel/Objects/Building.h"
#include "../../WorldModel/Objects/FireBrigade.h"
#include <limits>

class WorldGraph;

class Slice
{
	public:
		Slice();
		~Slice();

		void addRoad(Road*);
		vector<Road*>& getRoads();
		void addBuilding(Building*);
		vector<Building*>& getBuildings();
		void assignAgentToSlice(Platoon*);
		vector<Platoon*>& getAssignedAgents();
		int getIndex();
		void setIndex(int);
		int getGroundDistanceToEntity(MotionlessObject*, WorldGraph*, GraphMode);
		int getAirDistanceToEntity(Point);
		u_int getTotalSliceBuildingsArea();

	private:
		vector<Road*> roads;
		vector<Building*> buildings;
		vector<Platoon*> assignedAgents;
		int index = -1;
		u_int totalBuildingsArea = -1;
};

#endif /* SLICE_H_ */
