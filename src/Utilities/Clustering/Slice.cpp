/*
 * Slice.cpp
 *
 *  Created on: Mar 28, 2014
 *      Author: arha
 */

#include "Slice.h"

Slice::Slice()
{
	// TODO Auto-generated constructor stub
	
}

Slice::~Slice()
{
	// TODO Auto-generated destructor stub
}

void Slice::addRoad(Road* road)
{
	roads.push_back(road);
}

vector<Road*>& Slice::getRoads()
{
	return roads;
}

void Slice::addBuilding(Building* building)
{
	buildings.push_back(building);
	building->slice = index;
	totalBuildingsArea += building->getAreaTotal();
}

void Slice::assignAgentToSlice(Platoon* agent)
{
	agent->pizzaSlice = index;
	assignedAgents.push_back(agent);
}

vector<Building*>& Slice::getBuildings()
{
	return buildings;
}

vector<Platoon*>& Slice::getAssignedAgents()
{
	return assignedAgents;
}

int Slice::getIndex()
{
	return index;
}

void Slice::setIndex(int index)
{
	this->index = index;
}

int Slice::getGroundDistanceToEntity(MotionlessObject* object, WorldGraph* worldGraph, GraphMode gm)
{
	int bestDist = numeric_limits<int>::max();
	for (Building* building : buildings)
	{
		int tempDist = worldGraph->getDistance(building->getWithoutBlockadeRepresentiveNodeIndex(), object->getWithoutBlockadeRepresentiveNodeIndex(), gm);
		if (tempDist < bestDist)
		{
			bestDist = tempDist;
		}
	}

	return bestDist;
}

int Slice::getAirDistanceToEntity(Point point)
{
	int bestDist = numeric_limits<int>::max();
	for (Building* building : buildings)
	{
		int tempDist = building->getPos().distFromPoint(point);
		if (tempDist < bestDist)
		{
			bestDist = tempDist;
		}
	}

	return bestDist;
}

u_int Slice::getTotalSliceBuildingsArea()
{
	if (totalBuildingsArea == -1)
	{
		totalBuildingsArea = 0;
		for (Building* building : buildings)
		{
			totalBuildingsArea += building->getAreaTotal();
		}
	}

	return totalBuildingsArea;
}
