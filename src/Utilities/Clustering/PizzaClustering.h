/*
 * PizzaClustering.h
 *
 *  Created on: Mar 27, 2014
 *      Author: arha
 */

#ifndef PIZZACLUSTERING_H_
#define PIZZACLUSTERING_H_

#include "../../WorldModel/Objects/RCRObject.h"
#include "Cluster.h"
#include "../Geometry/Point.h"
#include <vector>
#include <cassert>

using namespace std;

template<class T>
class PizzaClustering
{
	public:
		PizzaClustering(vector<T*> members, int clusters);
		~PizzaClustering();

		void calculate();
		vector<Cluster*> &getClusters();
		void setNumberOfClusters(int a);
		void setMembers(vector<T*> objects);

	private:
		vector<T*> members;
		vector<Cluster*> clusters;
		int clustersCount;
};

template<class T> PizzaClustering<T>::PizzaClustering(vector<T*> members, int clusters)
{
	this->members = members;
	this->clustersCount = clusters;
}

template<class T> PizzaClustering<T>::~PizzaClustering()
{
	for (int cCounter = 0; cCounter < (int) clusters.size(); cCounter++)
	{
		delete clusters[cCounter];
	}

	members.clear();
	clusters.clear();
}

template<class T> void PizzaClustering<T>::calculate()
{
	clusters.clear();

	double eachAngle = 360. / clustersCount;

	Point center(0, 0);
	for (int i = 0; i < members.size(); i++)
	{
		center.setX(members[i]->getPos().getX() + center.getX());
		center.setY(members[i]->getPos().getY() + center.getY());
	}
	center.setX(center.getX() / members.size());
	center.setY(center.getY() / members.size());

	for (int i = 0; i < clustersCount; i++)
	{
		clusters.push_back(new Cluster());
	}

	for (int i = 0; i < members.size(); i++)
	{
		double angle = getPolarAngle(members[i]->getPos().getY() - center.getY(), members[i]->getPos().getX() - center.getX());
		int index = angle / eachAngle;
		clusters[index]->addMember(members[i]);
	}
}

template<class T> vector<Cluster*>& PizzaClustering<T>::getClusters()
{
	return clusters;
}

template<class T> void PizzaClustering<T>::setNumberOfClusters(int a)
{
	clustersCount = a;
}

template<class T> void PizzaClustering<T>::setMembers(vector<T*> objects)
{
	this->members = objects;
}

#endif /* PIZZACLUSTERING_H_ */
