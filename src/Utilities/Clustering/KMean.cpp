#include "KMean.h"

#define LOGLEVEL 1

#define ITERATIONS 20

using namespace std;
using namespace Geometry;

bool mCompare(MotionlessObject * i, MotionlessObject * j)
{
	int xy1 = i->getPos().getX() + i->getPos().getY();
	int xy2 = j->getPos().getX() + j->getPos().getY();

	return xy1 < xy2;
}

KMean::KMean()
{
	members.clear();
	numberOfClusters = 0;
}

KMean::KMean(vector<MotionlessObject*> objects, int numberOfClusters)
{
	this->members = objects;
	this->numberOfClusters = numberOfClusters;
}

KMean::~KMean()
{
	for (int cCounter = 0; cCounter < (int) clusters.size(); cCounter++)
	{
		delete clusters[cCounter];
	}

	members.clear();
	clusters.clear();
}

void KMean::setNumberOfClusters(int a)
{
	numberOfClusters = a;
}

void KMean::setMembers(vector<MotionlessObject *> &objetcs)
{
	members = objetcs;
}

void KMean::calculate()
{
	vector<MotionlessObject *> centers = makeCenter();
	LOG(Main, 1) << endl;

	for (int i = 0; i < (int) centers.size(); i++)
	{
		LOG(Main, 1) << centers[i]->getId() << ",";
	}

	LOG(Main, 1) << endl;

	updateMembers(centers);

	makeClusters(centers);

	for (int i = 0; i < (int) members.size(); i++)
	{
		setClusterForThisOne(members[i]);
		updateCenteralPoints(clusters);
	}
}

void KMean::fastCalculate()
{
	if ((int) members.size() < numberOfClusters)
	{
		cout << "invalid input" << endl;
		return;
	}

	vector<MotionlessObject *> centers;
	for (int i = 0; i < numberOfClusters; i++)
	{
		centers.push_back(members[i * members.size() / numberOfClusters]);
	}

	for (int i = 0; i < (int) centers.size(); i++)
	{
		LOG(Main, 1) << centers[i]->getId() << ",";
	}

	makeClusters(centers);

	for (int i = 0; i < ITERATIONS; i++)
	{
		for (Cluster* c : clusters)
		{
			c->setCenteralPoint();

			vector<MotionlessObject*> emptyMembers;
			c->setMembers(emptyMembers);
		}

		for (MotionlessObject* member : members)
		{
			int minDist = MAX_INT;
			Cluster* best = nullptr;
			for (Cluster* c : clusters)
			{
				int dist = c->getCenteralPoint().distFromPoint(member->getPos());
				if (dist < minDist)
				{
					minDist = dist;
					best = c;
				}
			}

			if (best != nullptr)
			{
				best->addMember(member);
			}
			else
			{
				cout << "we have a serious problem here :|" << endl;
				return;
			}
		}
	}
}

void KMean::updateMembers(vector<MotionlessObject*> c)
{
	vector<MotionlessObject *> temp;

	for (int i = 0; i < (int) members.size(); i++)
	{
		bool addIt = true;
		for (int j = 0; j < (int) c.size(); j++)
		{
			if (members[i]->getId() == c[j]->getId())
			{
				addIt = false;
				break;
			}
		}

		if (addIt)
		{
			temp.push_back(members[i]);
		}
	}

	members.clear();
	members = temp;
}

void KMean::updateCenteralPoints(vector<Cluster*> c)
{
	for (int i = 0; i < (int) c.size(); i++)
	{
		int gX = 0, gY = 0;
		vector<MotionlessObject *> m = c[i]->getMembers();
		for (int j = 0; j < (int) m.size(); j++)
		{
			gX = gX + m[j]->getPos().getX();
			gY = gY + m[j]->getPos().getY();
		}

		gX = gX / m.size();
		gY = gY / m.size();

		c[i]->setCenteralPoint(Point(gX, gY));
	}
}

void KMean::setClusterForThisOne(MotionlessObject* m)
{
	int maxValue = numeric_limits<int>::min(), tempDistanceForMainDistance = 0, tempDistanceForFakeDistance = 0, tempMeanToMembers = 0;
	int bestClusterIndex = -1;
	int w1 = -6, w2 = -3, w3 = -2;

	for (int i = 0; i < (int) clusters.size(); i++)
	{
		tempDistanceForMainDistance = m->getPos().distFromPoint(clusters[i]->getCenteralPoint());
		tempDistanceForFakeDistance = m->getPos().distFromPoint(clusters[i]->getCenter()->getPos());
		tempMeanToMembers = 0;

		for (int j = 0; j < (int) clusters[i]->getMembers().size(); j++)
		{
			tempMeanToMembers += m->getPos().distFromPoint(clusters[i]->getMembers()[j]->getPos());
		}
		tempMeanToMembers = tempMeanToMembers / clusters[i]->getMembers().size();

		int tempValue = (tempDistanceForMainDistance * w1 + tempDistanceForFakeDistance * w2 + tempMeanToMembers * w3) / (w1 + w2 + w3) * -1;
		if (tempValue > maxValue)
		{
			maxValue = tempValue;
			bestClusterIndex = i;
		}
	}

	clusters[bestClusterIndex]->addMember(m);
}

void KMean::makeClusters(vector<MotionlessObject*> centers)
{
	clusters.clear();

	for (int i = 0; i < (int) centers.size(); i++)
	{
		Cluster * c = new Cluster();
		c->setCenter(centers[i]);
		c->addMember(centers[i]);
		c->setCenteralPoint(centers[i]->getPos());
		clusters.push_back(c);
	}
}

vector<MotionlessObject *> KMean::makeCenter()
{
	vector<MotionlessObject *> centers;

	MotionlessObject * firstCenter = getFirstCenter(members);
	centers.push_back(firstCenter);

	numberOfClusters--;

	for (int i = 0; i < numberOfClusters; i++)
	{
		MotionlessObject * nextCenter = getFarestMember(members, centers);
		centers.push_back(nextCenter);
	}

	return centers;
}

MotionlessObject * KMean::getFirstCenter(vector<MotionlessObject*> mem)
{
	MotionlessObject * bestObject = nullptr;
	int bestDistance = -1;
	for (int i = 0; i < (int) mem.size(); i++)
	{
		int tempDistance = -1;
		for (int j = 0; j < (int) mem.size(); j++)
		{
			tempDistance = tempDistance + mem[i]->getPos().distFromPoint(mem[j]->getPos());
		}

		if (tempDistance > bestDistance)
		{
			bestDistance = tempDistance;
			bestObject = mem[i];
		}
	}

	return bestObject;
}

MotionlessObject * KMean::getFarestMember(vector<MotionlessObject*> mem, vector<MotionlessObject*> centers)
{
	MotionlessObject * bestObject = nullptr;
	int bestDistance = -1, tempDistance = 0;
	for (int i = 0; i < (int) mem.size(); i++)
	{
		if (isNotHere(centers, mem[i]))
			continue;

		tempDistance = 0;
		for (int j = 0; j < (int) centers.size(); j++)
		{
			tempDistance = tempDistance + mem[i]->getPos().distFromPoint(centers[j]->getPos());
		}

		if (tempDistance > bestDistance)
		{
			bestDistance = tempDistance;
			bestObject = mem[i];
		}
	}

	return bestObject;
}

bool KMean::isNotHere(vector<MotionlessObject*> c, MotionlessObject* m)
{
	for (int i = 0; i < (int) c.size(); i++)
	{
		if (c[i]->getId() == m->getId())
		{
			return true;
		}
	}

	return false;
}

vector<Cluster *> &KMean::getClusters()
{
	return clusters;
}
