#include <algorithm>

#include "../Logger.h"
#include "../Debugger.h"
#include "Hierarchical.h"
#define LOGLEVEL 4

bool posCompare(MotionlessObject * i, MotionlessObject * j)
{
	int xy0 = i->getPos().getX() + i->getPos().getY();
	int xy1 = j->getPos().getX() + j->getPos().getY();

	return xy0 > xy1;
}

Hierarchical::Hierarchical()
{
}

Hierarchical::Hierarchical(vector<MotionlessObject*> tempMember, int tempnumberOfCluster)
{
	members = tempMember;
	numberOfCluster = tempnumberOfCluster;
}

Hierarchical::~Hierarchical()
{
	for (int i = 0; i < (int) clusters.size(); i++)
	{
		delete clusters[i];
	}

	members.clear();
	clusters.clear();
}

void Hierarchical::setNumberOfClusters(int n)
{
	numberOfCluster = n;
}

void Hierarchical::setMembers(vector<MotionlessObject *> &mem)
{
	members = mem;
}

void Hierarchical::calculate()
{
	makeFirstClusters();

	while ((int) clusters.size() > numberOfCluster)
	{
		LOG(Main, 1) << "cluster new size:             " << clusters.size() << endl;
		updateClustersCenter();
		makeCluster();
		updateCluster();
	}

	updateClustersCenter();
}

void Hierarchical::makeFirstClusters()
{
	for (int i = 0; i < (int) members.size(); i++)
	{
		Cluster * cluster = new Cluster();
		cluster->addMember(members[i]);
		clusters.push_back(cluster);
	}
}

void Hierarchical::makeCluster()
{
	int bestDistance = MAX_INT, tempDistance;
	int mergeIndex1 = -1, mergeIndex2 = -1;
	for (int i = 0; i < (int) clusters.size(); i++)
	{
		for (int j = 0; j < (int) clusters.size(); j++)
		{
			if (i == j)
				continue;

			tempDistance = clusters[i]->getCenteralPoint().distFromPoint(clusters[j]->getCenteralPoint());
			if (bestDistance > tempDistance)
			{
				bestDistance = tempDistance;
				mergeIndex1 = i;
				mergeIndex2 = j;
			}
		}
	}

	LOG(Main, 1) << "merge cluster " << clusters[mergeIndex1]->getMembers()[0]->getId() << " " << clusters[mergeIndex2]->getMembers()[0]->getId() << endl << endl;

	vector<MotionlessObject *> newMembers = clusters[mergeIndex1]->getMembers();
	for (int i = 0; i < (int) clusters[mergeIndex2]->getMembers().size(); i++)
	{
		newMembers.push_back(clusters[mergeIndex2]->getMembers()[i]);
	}

	delete clusters[mergeIndex2];
	clusters[mergeIndex2] = nullptr;

	clusters[mergeIndex1]->setMembers(newMembers);
}

void Hierarchical::updateClustersCenter()
{
	for (int i = 0; i < (int) clusters.size(); i++)
	{
		int gX = 0, gY = 0;

		for (int j = 0; j < (int) clusters[i]->getMembers().size(); j++)
		{
			gX = gX + clusters[i]->getMembers()[j]->getPos().getX();
			gY = gY + clusters[i]->getMembers()[j]->getPos().getY();
		}

		gX = gX / (int) clusters[i]->getMembers().size();
		gY = gY / (int) clusters[i]->getMembers().size();
		clusters[i]->setCenteralPoint(Point(gX, gY));
	}
}

void Hierarchical::updateCluster()
{
	vector<Cluster *> newCluters;
	for (int i = 0; i < (int) clusters.size(); i++)
	{
		if (clusters[i])
		{
			newCluters.push_back(clusters[i]);
		}
	}

	clusters.clear();
	clusters = newCluters;
}

vector<Cluster *> & Hierarchical::getClusters()
{
	return this->clusters;
}

void Hierarchical::logClustering()
{
	LOG(Main , 4) << "---------------------- Hierarchical Clustering ----------------------" << endl;
	LOG(Main , 4) << "Members:" << endl;
	for (int i = 0; i < (int) members.size(); i++)
	{
		LOG(Main , 4) << members[i]->getId() << ",";
	}
	LOG(Main , 4) << endl;
	LOG(Main , 4) << "Number of clusters: " << endl;
	LOG(Main , 4) << numberOfCluster << endl;
	LOG(Main , 4) << "Clusters:" << endl;
	for (int i = 0; i < (int) clusters.size(); i++)
	{
		LOG(Main , 4) << "Cluster: " << i << endl;
		for (int j = 0; j < (int) clusters[i]->getMembers().size(); j++)
		{
			LOG(Main , 4) << clusters[i]->getMembers()[j]->getId() << ",";
		}
		LOG(Main , 4) << endl;
	}
	LOG(Main , 4) << "---------------------------------------------------------------------" << endl;
}
