#include "DoubleUtilities.h"
#include <cmath>

using namespace std;

const double EPS = 1e-8;

bool isEqual(double a, double b)
{
	return abs(a - b) < EPS;
}

bool isLess(double a, double b)
{
	return a + EPS < b;
}

bool isLessEqual(double a, double b)
{
	return a <= b + EPS;
}

bool hasSameSign(double a, double b)
{
	if (isEqual(a, 0.0) && isEqual(b, 0.0))
	{
		return true;
	}
	if (isEqual(a, 0.0) || isEqual(b, 0.0))
	{
		return false;
	}
	return a * b > 0.0;
}

int roundThisNumber(int n, int tround)
{
	int i = n / tround;
	int newNumber = i * tround;
	if ((float) (n - newNumber) < (float) (tround / 2))
	{
		return newNumber;
	}
	else
	{
		i++;
		newNumber = i * tround;
		return newNumber;
	}
}

int roundThisDoubleNumber(double n, int tRound)
{
	int m = n;
	if (n - m > 0.5)
	{
		return (m + 1) * tRound;
	}
	else
	{
		return m * tRound;
	}
}

bool isBetween(int number, int min, int max)
{
	return number >= min && number <= max;
}
