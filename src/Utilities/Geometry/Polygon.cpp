#include "Polygon.h"
#include "GeometryFunctions.h"
#include "../Debugger.h"
#include <memory>

#define LOGLEVEL 1

using namespace std;
using namespace Geometry;

Polygon::Polygon()
{
	vertices.clear();
}

Polygon::Polygon(vector<Point> p)
{
	vertices = p;
}

Polygon::~Polygon()
{
}

void Polygon::setVertices(vector<Point> p)
{
	vertices = p;
}

void Polygon::addVertex(Point p)
{
	vertices.push_back(p);
}

vector<Point>& Polygon::getVertices()
{
	return vertices;
}

void Polygon::clear()
{
	vertices.clear();
}

Point Polygon::getVertex(int i)
{
	return vertices[i];
}

Segment Polygon::getSegment(int i)
{
	return Segment(vertices[i], vertices[(i + 1) % size()]);
}

Point Polygon::getMidPoint()
{
	double midX = 0, midY = 0;
	for (int i = 0; i < size(); i++)
	{
		midX += vertices[i].getX();
		midY += vertices[i].getY();
	}
	midX /= size();
	midY /= size();
	return Point(midX, midY);
}

Rectangle Polygon::getBound()
{
	int x1 = std::numeric_limits<int>::max(), x2 = std::numeric_limits<int>::min(), y1 = std::numeric_limits<int>::max(), y2 = std::numeric_limits<int>::min();

	for (u_int i = 0; i < vertices.size(); i++)
	{
		x1 = min(x1, (int) vertices[i].getX());
		y1 = min(y1, (int) vertices[i].getY());
		x2 = max(x2, (int) vertices[i].getX());
		y2 = max(y2, (int) vertices[i].getY());
	}

	return Rectangle(Point(x1, y1), Point(x2, y2));
}

bool Polygon::isInPolygon(Point p)
{
	// Clipper
	//returns 0 if false, +1 if true, -1 if pt ON polygon boundary
	//http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.88.5498&rep=rep1&type=pdf

	int result = 0;
	size_t count = size();
	if (count < 3)
	{
		return 0;
	}
	Point point = getVertex(0);

	for (size_t i = 1; i <= count; i++)
	{
		Point nextPoint = (i == count ? getVertex(0) : getVertex(i));
		if (nextPoint.getY() == p.getY())
		{
			if ((nextPoint.getX() == p.getX()) || (point.getY() == p.getY() && ((nextPoint.getX() > p.getX()) == (point.getX() < p.getX()))))
			{
				return -1;
			}
		}
		if ((point.getY() < p.getY()) != (nextPoint.getY() < p.getY()))
		{
			if (point.getX() >= p.getX())
			{
				if (nextPoint.getX() > p.getX())
				{
					result = 1 - result;
				}
				else
				{
					double d = (double) (point.getX() - p.getX()) * (nextPoint.getY() - p.getY()) - (double) (nextPoint.getX() - p.getX()) * (point.getY() - p.getY());
					if (!d)
					{
						return -1;
					}
					if ((d > 0) == (nextPoint.getY() > point.getY()))
					{
						result = 1 - result;
					}
				}
			}
			else
			{
				if (nextPoint.getX() > p.getX())
				{
					double d = (double) (point.getX() - p.getX()) * (nextPoint.getY() - p.getY()) - (double) (nextPoint.getX() - p.getX()) * (point.getY() - p.getY());
					if (!d)
					{
						return -1;
					}
					if ((d > 0) == (nextPoint.getY() > point.getY()))
					{
						result = 1 - result;
					}
				}
			}
		}
		point = nextPoint;
	}

	if (result == 0)
	{
		return false;
	}
	else
	{
		return true;
	}
}

int Polygon::size()
{
	return vertices.size();
}

ostream & operator <<(ostream &out, Polygon a)
{
	for (int i = 0; i < (int) a.size() - 1; i++)
	{
		out << a.getSegment(i) << ' ';
	}
	out << a.getSegment(a.size() - 1);
	return out;
}
