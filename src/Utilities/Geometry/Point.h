#ifndef __POINT_H_
#define __POINT_H_
#include <cmath>
#include <fstream>
#include "Vector.h"

namespace Geometry
{
class Vector;

class Point
{
public:
	Point();
	Point(double x, double y);
	virtual ~Point();

	void setX(double);
	void setY(double);
	double getX();
	double getY();

	void setFromVector(Vector);
	void setAsCartesian(double angle, double l);
	Vector asVector();

	bool operator<(Point); //this function is just for sorting (and other STL utilities)
	bool operator ==(Point);
	bool operator !=(Point);
	Point operator +(Vector);
	double distFromPoint(Point point);
	bool isMark;
private:
	double x, y;
};
}

std::ostream & operator<<(std::ostream&, Geometry::Point);
Geometry::Point operator -(Geometry::Point a, Geometry::Point b);
Geometry::Point operator +(Geometry::Point a, Geometry::Point b);

#endif
