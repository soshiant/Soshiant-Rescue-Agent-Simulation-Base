#ifndef SEGMENT_H
#define	SEGMENT_H

#include <cmath>
#include <vector>
#include <iostream>
#include "Point.h"
#include "Line.h"
#include "../DoubleUtilities.h"

using namespace std;

namespace Geometry
{
class Segment
{
public:

	Segment();
	Segment(Point a, Point b);
	Segment(Point a, double angle, double len);
	~Segment();

	void setFirstPoint(Point);
	void setSecondPoint(Point);
	Point getFirstPoint();
	Point getSecondPoint();
	vector<Point> getPieceOfSegment(int n);

	double getLength();
	Point getMiddlePoint();
	Vector asVector();
	Vector asRVector();
	Line asLine();
	double getTheta();
	bool operator ==(Segment);
	bool operator !=(Segment);

private:
	Point first;
	Point second;
};
}

std::ostream & operator<<(std::ostream&, Geometry::Segment);

#endif	/* SEGMENT_H */

