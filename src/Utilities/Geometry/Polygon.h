#ifndef _POLYGON_H
#define	_POLYGON_H

#include <iostream>
#include <vector>

namespace Geometry
{
class Rectangle;
class Segment;
class Point;

class Polygon
{
public:
	Polygon();
	Polygon(std::vector<Point>);
	virtual ~Polygon();
	int size();
	void setVertices(std::vector<Point>);
	void addVertex(Point);
	std::vector<Point>& getVertices();
	void clear();
	Point getVertex(int);
	Segment getSegment(int);
	Rectangle getBound();
	Point getMidPoint();
	bool isInPolygon(Point);

private:
	std::vector<Point> vertices;
};
}

std::ostream & operator<<(std::ostream&, Geometry::Polygon);

#endif
