#include "Segment.h"
#include "../Debugger.h"
#include "GeometryFunctions.h"
#define LOGLEVEL 1

using namespace std;
using namespace Geometry;

Segment::Segment()
{
}

Segment::Segment(Point a, Point b)
{
	first = a;
	second = b;
}

Segment::Segment(Point p, double angle, double len)
{
	this->first = p;
	this->second.setX(this->first.getX() + cos(angle) * len);
	this->second.setY(this->first.getY() + sin(angle) * len);
}

Segment::~Segment()
{
}

void Segment::setFirstPoint(Point a)
{
	first = a;
}

vector<Point> Segment::getPieceOfSegment(int n)
{
	vector<Point> points;

	double tmpX = second.getX() - first.getX();
	double tmpY = second.getY() - first.getY();

	tmpX = tmpX / double(n);
	tmpY = tmpY / double(n);

	for (int i = 1; i < n; i++)
	{
		int x = first.getX() + (i * tmpX);
		int y = first.getY() + (i * tmpY);

		points.push_back(Point(x, y));
	}

	return points;
}

void Segment::setSecondPoint(Point a)
{
	second = a;
}

Point Segment::getFirstPoint(void)
{
	return first;
}

Point Segment::getSecondPoint(void)
{
	return second;
}

double Segment::getLength(void)
{
	return sqrt(pow(first.getX() - second.getX(), 2) + pow(first.getY() - second.getY(), 2));
}

Point Segment::getMiddlePoint()
{
	return Point((first.getX() + second.getX()) / 2, (first.getY() + second.getY()) / 2);
}

Vector Segment::asVector()
{
	return second.asVector() - first.asVector();
}

Vector Segment::asRVector()
{
	return first.asVector() - second.asVector();
}

Line Segment::asLine()
{
	return Line(first, second);
}

bool Segment::operator ==(Segment segment)
{
	if (segment.getFirstPoint() == getFirstPoint() && segment.getSecondPoint() == getSecondPoint())
	{
		return true;
	}
	if (segment.getFirstPoint() == getSecondPoint() && segment.getSecondPoint() == getFirstPoint())
	{
		return true;
	}
	return false;
}

bool Segment::operator !=(Segment segment)
{
	if ((segment.getFirstPoint() != getFirstPoint() || segment.getSecondPoint() != getSecondPoint()) && (segment.getFirstPoint() != getSecondPoint() || segment.getSecondPoint() != getFirstPoint()))
	{
		return true;
	}
	return false;
}

ostream & operator <<(ostream &out, Segment a)
{
	out << "line" << a.getFirstPoint() << a.getSecondPoint() << ' ' << __color << ';';
	return out;
}

double Segment::getTheta()
{
	int angle = atan2(second.getY() - first.getY(), second.getX() - first.getX()) * RAD2DEG;
	return absoluteAngle(angle);
}
