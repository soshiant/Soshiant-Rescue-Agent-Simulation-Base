#ifndef _GEOMETRYFUNCTIONS_H
#define	_GEOMETRYFUNCTIONS_H

#include "Point.h"
#include "Segment.h"
#include "Polygon.h"
#include "Rectangle.h"

#include <cmath>
#include <algorithm>
#include <array>
#include <float.h>

#include <polyclipping/clipper.hpp>

#define DEG2RAD (M_PI / 180.0)
#define RAD2DEG (180.0 / M_PI)
#define PI 3.141592653589793238462

using namespace std;
namespace clipper = ClipperLib;

namespace Geometry
{
	Polygon getSharedPart(Polygon p1, Polygon p2);
	bool isInside(Polygon pol, Point p);
	bool possible2Cross(Segment seg, Point p);
	bool isParallel(Segment, Segment);
	bool isParallel(Line, Line);
	bool haveSharedPart(Segment, Segment, double);
	bool haveInstersection(Polygon, Polygon);
	double distanceToPolygon(Polygon, Point);
	double distanceToLine(Line, Point);
	double distanceToSegment(Segment, Point);
	Point getIntersectPoint(Line, Line);
	Line getPerpendicularLine(Line, Point);
	Point getPerpendicularPoint(Line, Point);
	Line getParallelLine(Line, Point);
	Vector getVectorBylength(Vector, double length);
	Polygon increasePolygon(Polygon&, double d); //it'l decrease polygon if d is less than 0
	clipper::Paths increasePolygon(clipper::Path&, double d); //it'l decrease polygon if d is less than 0
	int absoluteAngle(int angle);
	bool isOnPolygon(Polygon, Point);
	int coverPercent(Point& point, int width, int height, Polygon& polygon);
	Point getRandomOnLine(Point a, Point b);
	Point getRandomPoint(Point a, double length, Polygon owner);
	Point getRandomPoint(Point a, double length);
	double getPolarAngle(int y, int x);
	array<float, 2> getAffineFunction(float x1, float y1, float x2, float y2);

	void setColor(int);
	extern int __color;

//for use angleCompare you must set compare point by setComparePoint function
//all the points that wants to be sorted by this compare must be on one side of the compare point (else it can cause runtime errors)
	bool angleCompare(Point, Point);
	void setComparePoint(Point);
	extern Point __comparePoint;

	inline double distanceBetweenPoints(Point a, Point b)
	{
		return hypot(a.getX() - b.getX(), a.getY() - b.getY());
	}

	inline double crossProduct(Vector a, Vector b)
	{
		return a.getX() * b.getY() - a.getY() * b.getX();
	}

	inline double dotProduct(Vector a, Vector b)
	{
		return a.getX() * b.getX() + a.getY() * b.getY();
	}

	inline bool isInSameSide(Segment s, Point p1, Point p2)
	{
		return hasSameSign(crossProduct(s.asVector(), Segment(s.getFirstPoint(), p1).asVector()), crossProduct(s.asVector(), Segment(s.getFirstPoint(), p2).asVector()));
	}

	inline bool isInSameSide(Line l, Point p1, Point p2)
	{
		return isInSameSide(l.asSegment(), p1, p2);
	}

	inline bool isOnSegment(Segment s, Point p)
	{
		return isEqual(distanceToSegment(s, p), 0) && isLessEqual(min(s.getFirstPoint().getX(), s.getSecondPoint().getX()), p.getX())
				&& isLessEqual(p.getX(), max(s.getFirstPoint().getX(), s.getSecondPoint().getX()))
				&& isLessEqual(min(s.getFirstPoint().getY(), s.getSecondPoint().getY()), p.getY())
				&& isLessEqual(p.getY(), max(s.getFirstPoint().getY(), s.getSecondPoint().getY()));
	}

	inline bool isOnSegment2(Segment s, Point p)
	{
		return isEqual(s.getLength(), double(p.distFromPoint(s.getFirstPoint())) + double(p.distFromPoint(s.getSecondPoint())));
	}

	inline bool isIntersect(Segment s1, Segment s2)
	{
		return !isInSameSide(s1, s2.getFirstPoint(), s2.getSecondPoint()) && !isInSameSide(s2, s1.getFirstPoint(), s1.getSecondPoint());
	}

	inline bool isIntersect(Line l, Segment s)
	{
		return !isInSameSide(l, s.getFirstPoint(), s.getSecondPoint());
	}

	inline clipper::Path convertToClipperPath(Polygon shape, double scale)
	{
		clipper::Path result;
		for (Point point : shape.getVertices())
		{
			result.push_back(clipper::IntPoint(point.getX() * scale, point.getY() * scale));
		}
		return result;
	}

	inline Polygon convertToPolygon(clipper::Path shape, double scale)
	{
		Polygon polygon;
		for (clipper::IntPoint point : shape)
		{
			polygon.addVertex(Point(point.X * scale, point.Y * scale));
		}
		return polygon;
	}
}

#endif	/* _GEOMETRYFUNCTIONS_H */
