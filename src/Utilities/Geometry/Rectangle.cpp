#include "Rectangle.h"
#include "Point.h"
#include "../DoubleUtilities.h"

using namespace Geometry;

Rectangle::Rectangle(Point bottomLeft, Point topRight)
{
	addVertex(bottomLeft);
	addVertex(Point(topRight.getX(), bottomLeft.getY()));
	addVertex(topRight);
	addVertex(Point(bottomLeft.getX(), topRight.getY()));
}

Rectangle::Rectangle(Point bottomLeft, Point bottomRight, Point topRight, Point topLeft)
{
	addVertex(bottomLeft);
	addVertex(bottomRight);
	addVertex(topRight);
	addVertex(topLeft);
}

double Rectangle::getLength()
{
	return (getPoint1().distFromPoint(getPoint2()) > getPoint2().distFromPoint(getPoint3())) ? getPoint1().distFromPoint(getPoint2()) : getPoint2().distFromPoint(getPoint3());
}

double Rectangle::getWidth()
{
	return (getPoint1().distFromPoint(getPoint2()) < getPoint2().distFromPoint(getPoint3())) ? getPoint1().distFromPoint(getPoint2()) : getPoint2().distFromPoint(getPoint3());
}

double Rectangle::getArea()
{
	return this->getLength() * this->getWidth();
}

bool Rectangle::isInRectangle(Point point)
{
	return isLessEqual(getPoint1().getX(), point.getX()) && isLessEqual(point.getX(), getPoint3().getX()) && isLessEqual(point.getY(), getPoint3().getY()) && isLessEqual(getPoint1().getY(), point.getY());
}

Point Rectangle::getPoint1()
{
	return getVertex(0);
}

Point Rectangle::getPoint2()
{
	return getVertex(1);
}

Point Rectangle::getPoint3()
{
	return getVertex(2);
}

Point Rectangle::getPoint4()
{
	return getVertex(3);
}

double Rectangle::getMinX()
{
	double tmp = std::min(getPoint1().getX(), getPoint2().getX());
	tmp = std::min(tmp, getPoint3().getX());
	return std::min(tmp, getPoint4().getX());
}

double Rectangle::getMinY()
{
	double tmp = std::min(getPoint1().getY(), getPoint2().getY());
	tmp = std::min(tmp, getPoint3().getY());
	return std::min(tmp, getPoint4().getY());
}

double Rectangle::getMaxX()
{
	double tmp = std::max(getPoint1().getX(), getPoint2().getX());
	tmp = std::max(tmp, getPoint3().getX());
	return std::max(tmp, getPoint4().getX());
}

double Rectangle::getMaxY()
{
	double tmp = std::max(getPoint1().getY(), getPoint2().getY());
	tmp = std::max(tmp, getPoint3().getY());
	return std::max(tmp, getPoint4().getY());
}
