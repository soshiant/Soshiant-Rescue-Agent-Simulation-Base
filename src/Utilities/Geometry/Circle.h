#ifndef CIRCLE_H
#define	CIRCLE_H

#include <iostream>
#include "Point.h"

namespace Geometry
{

class Circle
{
public:

	Circle(Point C_center, double C_radius);

	Point getCenter(void);
	double getRadius(void);

	bool isInCircle(Point point);
	bool hasInterSectWithLine(Line line);

private:

	Point center;
	double radius;

};
}

std::ostream & operator<<(std::ostream&, Geometry::Circle);

#endif	/* CIRCLE_H */
