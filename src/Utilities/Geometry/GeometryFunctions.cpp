#include "GeometryFunctions.h"
#include "Circle.h"
#include "../Debugger.h"

#define LOGLEVEL 1

using namespace Geometry;
using namespace std;

bool Geometry::isParallel(Line l1, Line l2)
{
	if (l1.isHorizontal() && l2.isHorizontal())
	{
		return true;
	}
	if (l1.isHorizontal() || l2.isHorizontal())
	{
		return false;
	}
	return isEqual(l1.getM(), l2.getM());
}

bool Geometry::isParallel(Segment s1, Segment s2)
{
	return isEqual(crossProduct(s1.asVector(), s2.asVector()), 0.0);
}

bool Geometry::haveSharedPart(Segment seg1, Segment seg2, double e)
{
	Line line1 = seg1.asLine(), line2 = seg2.asLine();

	if (!isParallel(line1, line2))
	{
		return false;
	}
	if (abs(line1.getH() - line2.getH()) > e)
	{
		return false;
	}

	double totalLength = seg1.getLength() + seg2.getLength() + 2 * e;
	if (isLess(distanceBetweenPoints(seg1.getFirstPoint(), seg2.getFirstPoint()), totalLength)
			&& isLess(distanceBetweenPoints(seg1.getFirstPoint(), seg2.getSecondPoint()), totalLength))
	{
		if (isLess(distanceBetweenPoints(seg1.getSecondPoint(), seg2.getFirstPoint()), totalLength)
				&& isLess(distanceBetweenPoints(seg1.getSecondPoint(), seg2.getSecondPoint()), totalLength))
		{
			return true;
		}
	}

	return false;
}

bool Geometry::haveInstersection(Polygon polygon1, Polygon polygon2)
{
	for (int i = 0; i < polygon1.size(); i++)
	{
		if (polygon2.isInPolygon(polygon1.getVertex(i)))
		{
			return true;
		}
	}

	for (int i = 0; i < polygon2.size(); i++)
	{
		if (polygon1.isInPolygon(polygon2.getVertex(i)))
		{
			return true;
		}
	}

	for (int i = 0; i < polygon1.size(); i++)
	{
		for (int j = 0; j < polygon2.size(); j++)
		{
			if (isIntersect(polygon1.getSegment(i), polygon2.getSegment(j)))
			{
				return true;
			}
		}
	}

	return false;
}

double Geometry::distanceToPolygon(Polygon poly, Point point)
{
	double minDistance = numeric_limits<double>::max();

	for (int i = 0; i < poly.size(); i++)
	{
		double distance = distanceToSegment(poly.getSegment(i), point);
		if (distance < minDistance)
		{
			minDistance = distance;
		}
	}

	return minDistance;
}

double Geometry::distanceToLine(Line l, Point p)
{
	return distanceToSegment(l.asSegment(), p);
}

double Geometry::distanceToSegment(Segment s, Point p)
{
	Vector v1 = s.getFirstPoint().asVector();
	Vector v2 = s.getSecondPoint().asVector();
	const double len = (v1 - v2).getLength();
	const double len2 = len * len;

	if (len2 == 0.0f)
	{
		return distanceBetweenPoints(s.getFirstPoint(), s.getSecondPoint());
	}

	const double t = dotProduct(p.asVector() - v1, v2 - v1) / len2;
	if (t < 0.0)
	{
		return distanceBetweenPoints(p, v1.asPoint());
	}
	else if (t > 1.0)
	{
		return distanceBetweenPoints(p, v2.asPoint());
	}

	Vector projection = v1 + (v2 - v1) * t;
	return distanceBetweenPoints(p, projection.asPoint());

//	return abs(crossProduct(s.asVector(), Segment(s.getFirstPoint(), p).asVector())) / s.getLength();
}

Point Geometry::getIntersectPoint(Line l1, Line l2)
{
	if (l2.isHorizontal())
	{
		swap(l1, l2);
	}

	if (l1.isHorizontal())
	{
		return Point(l1.getH(), l2.getY(l1.getH()));
	}

	double x = (l2.getH() - l1.getH()) / (l1.getM() - l2.getM());
	double y = l1.getY(x);

	return Point(x, y);
}

Line Geometry::getPerpendicularLine(Line l, Point p)
{
	if (l.isHorizontal())
	{
		return Line(0, p.getY());
	}
	if (isEqual(l.getM(), 0))
	{
		Line tmp;
		tmp.setHorizontal();
		tmp.setH(p.getX());
		return tmp;
	}
	double perM = -1.0 / (l.getM());
	double perH = p.getY() - perM * p.getX();
	return Line(perM, perH);
}

Point Geometry::getPerpendicularPoint(Line l, Point p)
{
	return getIntersectPoint(l, getPerpendicularLine(l, p));
}

Line Geometry::getParallelLine(Line l, Point p)
{
	if (l.isHorizontal())
	{
		Line tmp;
		tmp.setHorizontal();
		tmp.setH(p.getX());
		return tmp;
	}
	double parH = p.getY() - l.getM() * p.getX();
	return Line(l.getM(), parH);
}

bool Geometry::isOnPolygon(Polygon shape, Point point)
{
	for (int i = 0; i < shape.size(); i++)
	{
		if (isOnSegment2(shape.getSegment(i), point))
		{
			return true;
		}
	}

	return false;
}

Vector Geometry::getVectorBylength(Vector v, double l)
{
	double m = l / v.getLength();
	return Vector(v.getX() * m, v.getY() * m);
}

Polygon Geometry::increasePolygon(Polygon &p, double d)
{
	clipper::Path shape;
	for (Point point : p.getVertices())
	{
		shape.push_back(clipper::IntPoint(point.getX(), point.getY()));
	}

	ClipperLib::ClipperOffset co;
	clipper::Paths resultShape;
	co.AddPath(shape, clipper::JoinType::jtSquare, clipper::EndType::etClosedPolygon);
	co.Execute(resultShape, d);

	if (resultShape.empty())
	{
		return Polygon();
	}
	else if (resultShape.size() > 1)
	{
		cout << "Ha?! [increasePolygon]" << endl;
	}

	Polygon res;
	for (clipper::IntPoint point : resultShape[0])
	{
		res.addVertex(Point(point.X, point.Y));
	}
	return res;

//	Point mid = p.getMidPoint();
//	double minD = 1e10, mPr = 0;
//
//	for (int i = 0; i < p.size(); i++)
//	{
//		Segment s = p.getSegment(i);
//		Point per = getPerpendicularPoint(s.asLine(), mid);
//		double d = distanceToSegment(s, mid);
//		if (isOnSegment(s, per) && isLess(d, minD))
//		{
//			minD = d;
//			mPr = crossProduct(Segment(mid, s.getFirstPoint()).asVector(), Segment(mid, s.getSecondPoint()).asVector());
//		}
//	}
//
//	vector<Line> lines;
//	for (int i = 0; i < p.size(); i++)
//	{
//		Segment seg = p.getSegment(i);
//		Segment s(mid, getPerpendicularPoint(seg.asLine(), mid));
//		double pr = crossProduct(Segment(mid, seg.getFirstPoint()).asVector(), Segment(mid, seg.getSecondPoint()).asVector());
//		Point disP = (s.getSecondPoint().asVector() + getVectorBylength(s.asRVector(), (isLess(pr * mPr, 0) ? d : -d))).asPoint();
//		lines.push_back(getParallelLine(p.getSegment(i).asLine(), disP));
//	}
//
//	Polygon newPolygon;
//	if (!isParallel(lines[p.size() - 1], lines[0]))
//	{
//		newPolygon.addVertex(getIntersectPoint(lines[p.size() - 1], lines[0]));
//	}
//	else
//	{
//		newPolygon.addVertex(getPerpendicularPoint(lines[0], p.getVertex(0)));
//	}
//	for (int i = 0; i < p.size() - 1; i++)
//	{
//		if (!isParallel(lines[i], lines[i + 1]))
//		{
//			newPolygon.addVertex(getIntersectPoint(lines[i], lines[i + 1]));
//		}
//		else
//		{
//			newPolygon.addVertex(getPerpendicularPoint(lines[i], p.getVertex(i + 1)));
//		}
//	}
//
//	return newPolygon;
}

clipper::Paths Geometry::increasePolygon(clipper::Path& shape, double d)
{
	clipper::ClipperOffset co;
	clipper::Paths resultShape;
	co.AddPath(shape, clipper::JoinType::jtSquare, clipper::EndType::etClosedPolygon);
	co.Execute(resultShape, d);

	return resultShape;
}

void Geometry::setColor(int c)
{
	__color = c;
}

void Geometry::setComparePoint(Point p)
{
	__comparePoint = p;
}

bool Geometry::angleCompare(Geometry::Point p1, Geometry::Point p2)
{
	if (p1 == p2)
	{
		return false;
	}
	return isLessEqual(crossProduct(Segment(__comparePoint, p1).asVector(), Segment(__comparePoint, p2).asVector()), 0.0);
}

int Geometry::absoluteAngle(int angle)
{
	if (angle > 360)
	{
		return angle - int(angle / 360) * 360;
	}
	else if (angle < 0)
	{
		return angle - (int(angle / 360) - 1) * 360;
	}
	else
	{
		return angle;
	}
}

int Geometry::coverPercent(Point& point, int width, int height, Polygon& polygon)
{
	int counter = 0;
	int dx = width / 10;
	int dy = height / 10;

	for (int i = 0; i < 10; i++)
	{
		for (int j = 0; j < 10; j++)
		{
			if (polygon.isInPolygon(Point(dx * i + point.getX(), dy * j + point.getY())))
			{
				counter++;
			}
		}
	}

	return counter;
}

array<float, 2> Geometry::getAffineFunction(float x1, float y1, float x2, float y2)
{
	array<float, 2> result =
	{FLT_MAX, FLT_MAX};

	if (x1 == x2)
	{
		return result;
	}

	result[0] = (y1 - y2) / (x1 - x2); // m
	result[1] = y1 - result[0] * x1; // b

	return result;
}

double Geometry::getPolarAngle(int y, int x)
{
	return absoluteAngle(round(atan2((double) y, (double) x) * RAD2DEG));
}

Point Geometry::getRandomPoint(Point a, double length, Polygon owner)
{
	double angle = ((double) rand() / (RAND_MAX)) * 2.0 * PI;

	double s = sin(angle);
	double c = cos(angle);
	double checkx = s * 100 + a.getX();
	double checky = c * 100 + a.getY();
	double x, y;

	if (owner.isInPolygon(Point(checkx, checky)))
	{
		x = -s * length;
		y = -c * length;
	}
	else
	{
		x = s * length;
		y = c * length;
	}

	return Point((int) x + a.getX(), (int) y + a.getY());
}

Point Geometry::getRandomPoint(Point a, double length)
{
	double angle = ((double) rand() / RAND_MAX) * 2.0 * PI;

	double x = sin(angle) * length;
	double y = cos(angle) * length;

	return Point((int) x + a.getX(), (int) y + a.getY());
}

Point Geometry::getRandomOnLine(Point a, Point b)
{
	array<float, 2> mb = getAffineFunction(a.getX(), a.getY(), b.getX(), b.getY());
	float dx = max(a.getX(), b.getX()) - min(a.getX(), b.getX());
	dx *= (double) rand() / RAND_MAX;
	dx += min(a.getX(), b.getX());

	if (mb[0] == FLT_MAX)
	{
		// vertical line
		int p = max(a.getY(), b.getY()) - min(a.getY(), b.getY());
		p = (int) (p * ((double) rand() / RAND_MAX));
		p = p + min(a.getY(), b.getY());
		return Point(a.getX(), p);
	}

	float y = mb[0] * dx + mb[1];
	return Point((int) dx, (int) y);
}

bool Geometry::isInside(Polygon pol, Point p)
{
	cout << "[Geometry::isInside] This function doesn't work correctly asshole" << endl;

	bool isInside = true;
	for (int i = 0; i < pol.size(); i++)
	{
		if (!isInSameSide(pol.getSegment(i), p, pol.getMidPoint()))
		{
			isInside = false;
			break;
		}
	}

	return isInside;
}

Polygon Geometry::getSharedPart(Polygon p1, Polygon p2)
{
	Polygon res = Polygon();
	for (int i = 0; i < p1.size(); i++)
	{
		Segment seg1 = p1.getSegment(i);
		for (int j = 0; j < p2.size(); j++)
		{
			Segment seg2 = p2.getSegment(j);

			bool intersect = false;

			if (isIntersect(seg1, seg2) && (!Geometry::isParallel(seg1, seg2)) && (!isEqual(seg1.asLine().getM(), seg2.asLine().getM())))
			{
				intersect = true;
				res.addVertex(getIntersectPoint(seg1.asLine(), seg2.asLine()));
			}

			if (!intersect)
			{
				if (isInside(p1, seg2.getFirstPoint()))
					res.addVertex(seg2.getFirstPoint());

				if (isInside(p1, seg2.getSecondPoint()))
					res.addVertex(seg2.getSecondPoint());
			}
		}

	}

	return res;
}

bool Geometry::possible2Cross(Segment seg, Point p)
{
	Point po = Geometry::getPerpendicularPoint(seg.asLine(), p);
	return Geometry::isOnSegment(seg, po);
}

