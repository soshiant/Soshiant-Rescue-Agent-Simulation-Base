#ifndef RECTANGLE_H
#define RECTANGLE_H

#include <iostream>
#include <cmath>
#include "Polygon.h"

namespace Geometry
{
//	class Polygon;
class Point;

class Rectangle: private Polygon
{
public:
	Rectangle(Point bottomLeft, Point topRight);
	Rectangle(Point bottomLeft, Point bottomRight, Point topRight, Point topLeft);

	double getLength();
	double getWidth();
	double getArea();
	bool isInRectangle(Point point);
	Point getPoint1();
	Point getPoint2();
	Point getPoint3();
	Point getPoint4();

	double getMinX();
	double getMinY();
	double getMaxX();
	double getMaxY();
};
}

#endif	/* RECTANGLE_H */
