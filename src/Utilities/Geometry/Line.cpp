#include <cmath>
#include "Line.h"
#include "../DoubleUtilities.h"
#include "Segment.h"

#define LOGLEVEL 1

using namespace std;
using namespace Geometry;

Line::Line()
{
	isHoriz = false;
}

Line::Line(double m, double h)
{
	this->m = m;
	this->h = h;
	isHoriz = false;
}

Line::Line(Point p, double degree)
{
	double degDivPi = (degree - M_PI_2) / M_PI, fracPart;
	modf(degDivPi, &fracPart);

	if (isEqual(fracPart, 0.0) || isEqual(fracPart, 1.0))
	{
		isHoriz = true;
		m = 0;
		h = p.getX();
		return;
	}

	isHoriz = false;
	m = tan(degree);
	h = p.getY() - m * p.getX();
}

Line::Line(Point p1, Point p2)
{
	if (isEqual(p1.getX(), p2.getX()))
	{
		isHoriz = true;
		m = 0;
		h = p1.getX();
		return;
	}

	isHoriz = false;
	m = (p1.getY() - p2.getY()) / (p1.getX() - p2.getX());
	h = p1.getY() - m * p1.getX();
}

Line::~Line()
{
}

void Line::setM(double m)
{
	this->m = m;
}

void Line::setH(double h)
{
	this->h = h;
}

void Line::setHorizontal(bool isHorizontal)
{
	isHoriz = isHorizontal;
}

double Line::getM()
{
	return m;
}

double Line::getH()
{
	return h;
}

bool Line::isHorizontal()
{
	return isHoriz;
}

double Line::getX(double y)
{
	if (isHoriz)
	{
		return h;
	}
	if (isEqual(m, 0.0) && isEqual(y, h))
	{
		return 0.0;
	}
	return (y - h) / m;
}

double Line::getY(double x)
{
	if (isHoriz && isEqual(x, h))
	{
		return 0.0;
	}
	return m * x + h;
}

Segment Line::asSegment()
{
	if (isHoriz)
	{
		return Segment(Point(h, 0.0), Point(h, 100.0));
	}

	return Segment(Point(0.0, getY(0.0)), Point(100.0, getY(100.0)));
}

double Line::getDistanceFromPoint(Point point)
{
	Segment thisLineSegment = asSegment();

	if (isHorizontal() == false)
	{
		float tempM = m;
		tempM = -1 / tempM;
		float y1 = tempM * (point.getX() - thisLineSegment.getFirstPoint().getX()) + thisLineSegment.getFirstPoint().getY();
		float y2 = tempM * (point.getX() - thisLineSegment.getSecondPoint().getX()) + thisLineSegment.getSecondPoint().getY();

		if (isBetween(y1, point.getY(), y2))
		{
			float a = thisLineSegment.getFirstPoint().distFromPoint(thisLineSegment.getSecondPoint());
			float b = point.distFromPoint(thisLineSegment.getFirstPoint());
			float c = point.distFromPoint(thisLineSegment.getSecondPoint());
			float p = (a + b + c) / 2;
			float s = (float) sqrt(p * (p - a) * (p - b) * (p - c));
			return 2 * s / a;
		}
	}
	else if (isBetween(thisLineSegment.getFirstPoint().getY(), point.getY(), thisLineSegment.getSecondPoint().getY()))
	{
		return abs(point.getX() - thisLineSegment.getFirstPoint().getX());
	}

	double d1 = point.distFromPoint(thisLineSegment.getFirstPoint());
	double d2 = point.distFromPoint(thisLineSegment.getSecondPoint());

	return min(d1, d2);
}
