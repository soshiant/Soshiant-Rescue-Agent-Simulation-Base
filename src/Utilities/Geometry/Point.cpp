#include "Point.h"
#include "../../Utilities/Debugger.h"
#include "../../Utilities/DoubleUtilities.h"
#include "GeometryFunctions.h"
#define LOGLEVEL 1

using namespace std;
using namespace Geometry;

Point::Point()
{
	isMark = false;
}

Point::Point(double x, double y)
{
	this->x = x;
	this->y = y;
	isMark = false;
}

Point::~Point()
{
}

void Point::setAsCartesian(double angle, double l)
{
	angle = angle * DEG2RAD;
	x = cos(angle) * l;
	y = sin(angle) * l;
}

void Point::setX(double x)
{
	this->x = x;
}

void Point::setY(double y)
{
	this->y = y;
}

double Point::getX()
{
	return x;
}

double Point::getY()
{
	return y;
}

void Point::setFromVector(Vector v)
{
	x = v.getX();
	y = v.getY();
}

Vector Point::asVector()
{
	return Vector(x, y);
}

bool Point::operator ==(Point a)
{
	return isEqual(x, a.x) && isEqual(y, a.y);
}

bool Point::operator !=(Point a)
{
	return !(isEqual(x, a.x) && isEqual(y, a.y));
}

bool Point::operator<(Point a)
{
	if (a == *this)
		return false;

	if (!isEqual(a.x, x))
		return a.x < x;

	return a.y < y;
}

Point Point::operator +(Vector v)
{
	return Point(x + v.getX(), y + v.getY());
}

Point operator -(Point a, Point b)
{
	int x = a.getX() - b.getX();
	int y = a.getY() - b.getY();
	Point p(x, y);
	return p;
}

Point operator +(Point a, Point b)
{
	int x = a.getX() + b.getX();
	int y = a.getY() + b.getY();
	Point p(x, y);
	return p;
}

ostream & operator <<(ostream &out, Point a)
{
	out << fixed << setprecision(0) << "(" << a.getX() << ", " << a.getY() << ")";
	return out;
}

double Point::distFromPoint(Point point)
{
	return distanceBetweenPoints(*this, point);
}
