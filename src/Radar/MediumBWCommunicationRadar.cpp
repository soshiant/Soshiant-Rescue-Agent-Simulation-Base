#include "MediumBWCommunicationRadar.h"

/********************************** Initialization **********************************/

MediumBWCommunicationRadar::MediumBWCommunicationRadar(WorldModel * wm, WorldGraph * wg, Command * command) :
		AbstractCommunicationRadar(wm, wg, command)
{
}

MediumBWCommunicationRadar::~MediumBWCommunicationRadar()
{
}

void MediumBWCommunicationRadar::init()
{
	AbstractCommunicationRadar::init();
}

void MediumBWCommunicationRadar::subscribe()
{
	AbstractCommunicationRadar::subscribe();
}

void MediumBWCommunicationRadar::setMyChanel()
{
	if (radarMode == RM_NO_COMMUNICATION)
	{
		return;
	}

	vector<RadioChannel *> chanelInUse;

	if (numOfSubscribeForPlatoons >= (int) radioChannels.size())
	{
		chanelInUse = radioChannels;
	}
	else
	{
		for (int i = 0; i < numOfSubscribeForPlatoons; i++)
		{
			chanelInUse.push_back(radioChannels[i]);
		}
	}

	listeningChanels = chanelInUse;

	if (!self()->isPlatoon())
	{
		LOG(Main , 1) << "[MediumBWCommunicationRadar][Size][setMyChanel] I'm Center !!!" << endl;
		myChanel = chanelInUse[0];
	}
	else
	{
		int myIndex = 0;
		int allSayersCount = 0;
		for (int i = 0; i < (int) world->platoons.size(); i++)
		{
			allSayersCount++;
			if (world->platoons[i]->getId() == self()->getId())
				myIndex = allSayersCount - 1;
		}

		for (int i = 0; i < (int) world->ambulanceCenters.size(); i++)
		{
			allSayersCount++;
			if (world->ambulanceCenters[i]->getId() == self()->getId())
				myIndex = allSayersCount - 1;
		}

		for (int i = 0; i < (int) world->policeOffices.size(); i++)
		{
			allSayersCount++;
			if (world->policeOffices[i]->getId() == self()->getId())
				myIndex = allSayersCount - 1;
		}

		for (int i = 0; i < (int) world->fireStations.size(); i++)
		{
			allSayersCount++;
			if (world->fireStations[i]->getId() == self()->getId())
				myIndex = allSayersCount - 1;
		}

		if (chanelInUse.size() == 1)
		{
			myChanel = chanelInUse[0];

			if (allSayersCount != 0)
			{
				myMinimalSize = myChanel->getBandWidth() / allSayersCount;
			}
			return;
		}

		int myRadioChanelIndex = myIndex % chanelInUse.size();
		myChanel = chanelInUse[myRadioChanelIndex];

		if (myChanel && myChanel->getBandWidth() != 0 && allSayersCount != 0 && chanelInUse.size() != 0 && (allSayersCount / chanelInUse.size()) != 0)
		{
			myMinimalSize = myChanel->getBandWidth() / (allSayersCount / chanelInUse.size());
		}
	}
	LOG(Main , 1) << "[MediumBWCommunicationRadar][Size][setMyChanel] myChannelNum: " << myChanel->getChannelNum() << endl;
}

void MediumBWCommunicationRadar::setSizes()
{
	NOT_RELATED_EDGE_INDEX = ceil(log2l(worldGraph->notRelatedToRoadEdges.size()));
	MOTIONLESS_INDEX = ceil(log2l(world->motionlessObjects.size()));
	AMBULANCE_INDEX = ceil(log2l(world->ambulanceTeams.size()));
	NODE_INDEX = ceil(log2l(worldGraph->getNodes().size()));
	BUILDING_INDEX = ceil(log2l(world->buildings.size()));
	PLATOON_INDEX = ceil(log2l(world->platoons.size()));
	TIME_TO_DEATH = ceil(log2l(512 / timeToDeathCoef));
	TEMPERATURE = ceil(log2l(2048 / temperatureCoef));
	CIVILIAN_ANGLE = ceil(log2l(360 / angleCoef));
	ROAD_INDEX = ceil(log2l(world->roads.size()));
	ID_SIZE = ceil(log2l(MAX_INT));

	EXTINGUISHED_FIERYNESS = 2;
	BURIEDNESS = 7;
	FIERYNESS = 2;
	ONE_BIT_SIZE = 1;
	INDEX_SIZE = 4;

	TOTOAL_POSITION = NODE_INDEX + ONE_BIT_SIZE + ONE_BIT_SIZE;

	if (myMinimalSize != 0)
	{
		maxEdgeForSend = myMinimalSize - INDEX_SIZE - TOTOAL_POSITION - INDEX_SIZE - BUILDING_INDEX - INDEX_SIZE - ID_SIZE - TIME_TO_DEATH;

		if (maxEdgeForSend <= 0)
		{
			maxEdgeForSend = 8;
		}
	}

	LOG(Main , 1) << "[MediumBWCommunicationRadar][Size] ROAD_INDEX_SIZE   " << ROAD_INDEX << endl;
	LOG(Main , 1) << "[MediumBWCommunicationRadar][Size] NODE_INDEX_SIZE   " << NODE_INDEX << endl;
	LOG(Main , 1) << "[MediumBWCommunicationRadar][Size] MOTIONLESS_INDEX_SIZE   " << MOTIONLESS_INDEX << endl;
	LOG(Main , 1) << "[MediumBWCommunicationRadar][Size] MAX_EDGE_FOR_SEND: " << maxEdgeForSend << endl;
}

/********************************** Send Messages **********************************/

void MediumBWCommunicationRadar::shareSenseWorldAndSendOrders()
{
	LOG(Main , 1) << "---------------------[MediumBWCommunicationRadar]-----------------shareSenseWorldAndSendOrders-----------------" << endl;
	LOG(Main , 1) << "[MediumBWCommunicationRadar] Begins!" << endl;

	try
	{
		BoolStream msg;
		msg.clear();
		msg.setSize(1000);
		sizeOfMessage = 0;

		sendBuriednessProperty(msg, sizeOfMessage);
		LOG(Main , 1) << "[MediumBWCommunicationRadar][Share] My Buriedness setted: " << sizeOfMessage << endl;
		sendMyPositionProperty(msg, sizeOfMessage);
		LOG(Main , 1) << "[MediumBWCommunicationRadar][Share] My position setted: " << sizeOfMessage << endl;
		sendAmbulanceCommands(msg, sizeOfMessage);
		LOG(Main , 1) << "[MediumBWCommunicationRadar][Share] sendAmbulaceCommands setted to : " << sizeOfMessage << endl;

		if (doIHaveUsefulDataForSendInRadar()) //TODO Abnormal civilian bug
		{
			sendBuildings(msg, sizeOfMessage);
			LOG(Main , 1) << "[MediumBWCommunicationRadar][Share] Buildings in fire setted: " << sizeOfMessage << endl;
			sendCivilians(msg, sizeOfMessage);
			LOG(Main , 1) << "[MediumBWCommunicationRadar][Share] civilians setted: " << sizeOfMessage << endl;
			sendRoads(msg, sizeOfMessage);
			LOG(Main , 1) << "[MediumBWCommunicationRadar][Share] roads setted: " << sizeOfMessage << endl;
			sendEdges(msg, sizeOfMessage);
			LOG(Main , 1) << "[MediumBWCommunicationRadar][Share] edges setted: " << sizeOfMessage << endl;
		}
//		sendHasWaterIsCarry(msg, sizeOfMessage);
//		LOG(Main , 1) << "[MediumBWCommunicationRadar][Share] My Has water isCarry setted: " << sizeOfMessage << endl;

		if (sizeOfMessage < INDEX_SIZE - 1 && self()->isCenter())
		{
			return;
		}

		msg.push_back(bits(MP_END, INDEX_SIZE));
		sizeOfMessage = sizeOfMessage + INDEX_SIZE;

		LOG(Main , 1) << "Final message size: " << sizeOfMessage << endl;

		sizeOfMessage = ceil(sizeOfMessage / 8.);

		msg.setSize(sizeOfMessage);
		LOG(Main , 1) << "sizeOfMessage: " << sizeOfMessage << endl;
		LOG(Main , 1) << "Msg size: " << msg.getVal().size() << endl;
		LOG(Main , 1) << msg.getVal(" - ") << endl;
		LOG(Main , 1) << "Msg For send: " << msg.getValue().size() << endl;

		LOG(Main , 1) << "Radar ends!" << endl;
		LOG(Main , 1) << "--------------------------------------------------" << endl;

		if (self()->isCenter())
		{
			for (int i = 0; i < numOfSubscribeForPlatoons; i++)
			{
				command->speak(radioChannels[i]->getChannelNum(), msg.getValue());
			}
		}
		else
		{
			command->speak(myChanel->getChannelNum(), msg.getValue());
		}

		sendedData->amISendMyLastCycleData = true;

		if (!voiceChannels.empty() && world->self->isPlatoon())
		{
			clRadar->shareDataOnCommunicationlessModel();
		}
		if (myChanel == nullptr)
		{
			return;
		}
	}
	catch (exception& e)
	{
		cout << "I just catched a fucking exception!! " << e.what() << endl;
	}
	catch (...)
	{
		cout << "An unknown exception " << endl;
	}
}

void MediumBWCommunicationRadar::shareSenseWorldAfterDecision()
{
}

void MediumBWCommunicationRadar::sendBuildings(BoolStream &msg, int& size)
{
	vector<Building*> validBuildings;
	for (Building* b : world->buildings)
	{
		if (isValidBuilding(b))
		{
			validBuildings.push_back(b);
		}
	}
	sort(validBuildings.begin(), validBuildings.end(), buildingComparator);

	for (Building* building : validBuildings)
	{
		if (building->getFieryness() >= 1 && building->getFieryness() <= 3)
		{
			msg.push_back(bits(MP_BUILDING_IN_FIRE, INDEX_SIZE));
			msg.push_back(bits(building->buildingIndex, BUILDING_INDEX));
			msg.push_back(bits(building->getFieryness(), FIERYNESS));

			int temperature = round(building->getTemperature() / temperatureCoef);
			if ((float) building->getTemperature() >= building->getEstimatedData()->getIgnitionPoint() && (float) temperature * temperatureCoef < building->getEstimatedData()->getIgnitionPoint())
			{
				temperature++;
			}

			msg.push_back(bits(temperature > pow(2, TEMPERATURE) - 1 ? pow(2, TEMPERATURE) - 1 : temperature, TEMPERATURE));

			size += INDEX_SIZE + BUILDING_INDEX + FIERYNESS + TEMPERATURE;
			sendedData->removeThisBuildingFromExtinguishedBuildings(building);
			sendedData->sendedBuildingsInFire.push_back(building);
		}
		if (building->getFieryness() >= 5 && building->getFieryness() <= 7)
		{
			msg.push_back(bits(MP_BUILDING_EXTINGUISHED, INDEX_SIZE));
			msg.push_back(bits(building->buildingIndex, BUILDING_INDEX));
			msg.push_back(bits(building->getFieryness() - 5, EXTINGUISHED_FIERYNESS));

			size += INDEX_SIZE + BUILDING_INDEX + EXTINGUISHED_FIERYNESS;
			sendedData->removeThisBuildingFromBuildingsInFire(building);
			sendedData->sendedBuildingsExtinguished.push_back(building);
		}
		if (building->getFieryness() == 8)
		{
			msg.push_back(bits(MP_BUILDING_BURRNED, INDEX_SIZE));
			msg.push_back(bits(building->buildingIndex, BUILDING_INDEX));

			size += INDEX_SIZE + BUILDING_INDEX;
			sendedData->removeThisBuildingFromBuildingsInFire(building);
			sendedData->removeThisBuildingFromExtinguishedBuildings(building);
			sendedData->sendedBuildingsBurned.push_back(building);
		}
		building->setLastCycleSentInRadar(world->getTime());
		building->setNumberOfSent(building->getNumberOfSent() + 1);
	}
}

void MediumBWCommunicationRadar::sendBuriednessProperty(BoolStream &msg, int& size)
{
	if (world->self->isPlatoon() == false)
	{
		return;
	}
	if (world->getTime() > 5)
	{
		int bSize = self()->getBuriednessHistory().size() - 1;
		LOG(Main,1) << "bSize: " << bSize << endl;
		if (bSize > 1)
		{
			if ((self()->getBuriednessHistory()[bSize].buriedness == self()->getBuriednessHistory()[bSize - 1].buriedness) && sendedData->amISendMyLastCycleData)
			{
				return;
			}
		}
		else if (self()->getBuriedness() == 0)
		{
			return;
		}
	}
	else if (self()->getBuriedness() == 0)
	{
		return;
	}

	msg.push_back(bits(MP_BURIEDNESS, INDEX_SIZE));

	int buriednessForSend = self()->getBuriedness() > 127 ? 127 : self()->getBuriedness();
	msg.push_back(bits(buriednessForSend, BURIEDNESS));
	size = size + INDEX_SIZE + BURIEDNESS;
	LOG(Main ,1) << "[MediumBWCommunicationRadar] [sendBuriednessProperty]" << "AgentID: " << self()->getId() << " BuriednessForSend: " << buriednessForSend << endl;
}

void MediumBWCommunicationRadar::sendMyPositionProperty(BoolStream &msg, int& size)
{
	int lowestCycleForSendingPosition = 5;

	if (world->self->isPlatoon() == false)
	{
		return;
	}

	if (world->getTime() >= lowestCycleForSendingPosition && (self()->isMoved() == false && sendedData->amISendMyLastCycleData))
	{
		if ((self()->getRepresentiveNodeIndex() == -1 && world->getTime() % 10 == self()->platoonIndex % 10) == false)
		{
			return;
		}
	}

	size = size + INDEX_SIZE + TOTOAL_POSITION;
	msg.push_back(bits(MP_POSITION, INDEX_SIZE));

	if (self()->getRepresentiveNodeIndex() != -1)
	{
		msg.push_back(bits(self()->getRepresentiveNodeIndex(), NODE_INDEX));
		msg.push_back(bits(0, ONE_BIT_SIZE));
		int firstMotionless = worldGraph->getNodes()[self()->getRepresentiveNodeIndex()]->getFirstMotionlessIndex();
		msg.push_back(bits(firstMotionless == self()->getMotionlessObject()->motionlessIndex, ONE_BIT_SIZE));
		return;
	}

	msg.push_back(bits(self()->getWithoutBlockadeRepresentiveNodeIndex(), NODE_INDEX));
	msg.push_back(bits(1, ONE_BIT_SIZE));
	LOG(Main, 1) << "Nodes: " << worldGraph->getNodes().size() << "Node INdex : " << self()->getWithoutBlockadeRepresentiveNodeIndex() << "Base Index" << self()->getMotionlessObject()->getId() << endl;
	int firstMotionless = worldGraph->getNodes()[self()->getWithoutBlockadeRepresentiveNodeIndex()]->getFirstMotionlessIndex();
	msg.push_back(bits(firstMotionless == self()->getMotionlessObject()->motionlessIndex, ONE_BIT_SIZE));
}

void MediumBWCommunicationRadar::sendAmbulanceCommands(BoolStream &msg, int & size)
{
	LOG(Main , 1) << "--------------------------------------------------------" << endl;
	LOG(Main , 1) << "SendAmbulaceCommands" << endl;
	if (!self()->isAmbulanceTeam())
	{
		return;
	}

	AmbulanceTeam* me = (AmbulanceTeam*) world->self;
	if (!me->isCapitan)
	{
		return;
	}

	vector<AmbulanceTeam*> ats;
	if (me->shouldSendAssignThisCycle)
	{
		LOG(Main , 1) << "me->shouldSendAssignThisCycle" << endl;
		for (AmbulanceTeam* at : world->ambulanceTeams)
		{
			if (at->centerTarget != nullptr && at->sentOnRadarTime <= at->assignmentTime && at != self())
			{
				ats.push_back(at);
			}
		}

		if (ats.empty())
		{
			return;
		}

		msg.push_back(bits(MP_AMBULANCE_COMMAND, INDEX_SIZE));
		msg.push_back(bits(ats.size() - 1, AMBULANCE_INDEX));
		size += INDEX_SIZE + AMBULANCE_INDEX;

		for (u_int i = 0; i < ats.size(); i++)
		{
			msg.push_back(bits(ats[i]->ambulanceTeamIndex, AMBULANCE_INDEX));
			LOG(Main, 1) << "sending " << ats[i]->getId() << " target " << endl;
			msg.push_back(bits(ats[i]->centerTarget->isPlatoon(), ONE_BIT_SIZE));
			if (ats[i]->centerTarget->isPlatoon())
			{
				msg.push_back(bits(((Platoon*) ats[i]->centerTarget)->platoonIndex, PLATOON_INDEX));
				size += PLATOON_INDEX;
			}
			else
			{
				msg.push_back(bits(ats[i]->centerTarget->getFirstTimeMotionless()->motionlessIndex, MOTIONLESS_INDEX));

				int angle = ((Civilian*) ats[i]->centerTarget)->getAngle();
				int distance = ((Civilian*) ats[i]->centerTarget)->getDistanceToCenter();
				msg.push_back(bits(angle == 0 && distance == 0, ONE_BIT_SIZE));
				size += ONE_BIT_SIZE + MOTIONLESS_INDEX;

				if (angle == 0 && distance == 0)
				{
					size += AMBULANCE_INDEX + ONE_BIT_SIZE;
					ats[i]->sentOnRadarTime = world->getTime();

					continue;
				}

				msg.push_back(bits(angle, CIVILIAN_ANGLE));
				msg.push_back(bits(distance, ((Civilian*) ats[i]->centerTarget)->getDistanceToCenterSize()));
				size += CIVILIAN_ANGLE + ((Civilian*) ats[i]->centerTarget)->getDistanceToCenterSize();
			}

			size += AMBULANCE_INDEX + ONE_BIT_SIZE;
			ats[i]->sentOnRadarTime = world->getTime();
		}
	}
}

void MediumBWCommunicationRadar::sendHasWaterIsCarry(BoolStream &msg, int& size)
{
	if (world->self->isPlatoon() == false || self()->isPoliceForce())
		return;

	if (self()->isFireBrigade())
	{
		FireBrigade * self = ((FireBrigade *) world->self);
		bool doIHaveWater = false;

		if (self->getWaterQuantity() >= maxWaterForExtinguish)
		{
			doIHaveWater = true;
		}

		if (doIHaveWater != self->hasWater() || sendedData->amISendMyLastCycleData == false)
		{
			size = size + INDEX_SIZE;
			msg.push_back(bits(MP_HAS_WATER_IS_CARRY, INDEX_SIZE));
			msg.push_back((bits(doIHaveWater, ONE_BIT_SIZE)));
			size += ONE_BIT_SIZE;
		}

		self->setHasWaterValue(doIHaveWater);
		return;
	}

	if (self()->isAmbulanceTeam())
	{
		AmbulanceTeam * self = (AmbulanceTeam*) world->self;
		bool doIHaveCivilian = false;

		for (int i = 0; i < (int) world->civilians.size(); i++)
		{
			if (world->civilians[i]->getLastCycleUpdatedBySense() == world->getTime() && world->civilians[i]->getPosition() == this->self()->getId())
			{
				doIHaveCivilian = true;
				break;
			}
		}

		if (doIHaveCivilian != self->isCarry() || sendedData->amISendMyLastCycleData == false)
		{
			size = size + INDEX_SIZE;
			msg.push_back(bits(MP_HAS_WATER_IS_CARRY, INDEX_SIZE));
			msg.push_back(bits(doIHaveCivilian, ONE_BIT_SIZE));
			size += ONE_BIT_SIZE;
		}
		self->setIsCarry(doIHaveCivilian);
	}
}

void MediumBWCommunicationRadar::sendCivilians(BoolStream &msg, int& size)
{
	for (Civilian* civ : world->civilians)
	{
		if (!isValidCivilian(civ))
		{
			continue;
		}
		if (!isFertCivilian(civ))
		{
			msg.push_back(bits(MP_NORMAL_CIVILIAN, INDEX_SIZE));

			int timeToDeath = round(civ->getTimeToDeath() / timeToDeathCoef);
			msg.push_back(bits(timeToDeath > pow(2, TIME_TO_DEATH) - 1 ? pow(2, TIME_TO_DEATH) - 1 : timeToDeath, TIME_TO_DEATH));

			if (civ->getRepresentiveNodeIndex() == -1)
			{
				msg.push_back(bits(civ->getWithoutBlockadeRepresentiveNodeIndex(), NODE_INDEX));
				msg.push_back(bits(1, ONE_BIT_SIZE));
				int firstMotionless = worldGraph->getNodes()[civ->getWithoutBlockadeRepresentiveNodeIndex()]->getFirstMotionlessIndex();
				msg.push_back(bits(firstMotionless == civ->getMotionlessObject()->motionlessIndex, ONE_BIT_SIZE));
			}
			else
			{
				msg.push_back(bits(civ->getRepresentiveNodeIndex(), NODE_INDEX));
				msg.push_back(bits(0, ONE_BIT_SIZE));
				int firstMotionless = worldGraph->getNodes()[civ->getRepresentiveNodeIndex()]->getFirstMotionlessIndex();
				msg.push_back(bits(firstMotionless == civ->getMotionlessObject()->motionlessIndex, ONE_BIT_SIZE));
			}
			msg.push_back(bits(civ->getBuriedness() > 127 ? 127 : civ->getBuriedness(), BURIEDNESS));

			size += (INDEX_SIZE + TIME_TO_DEATH + BURIEDNESS + TOTOAL_POSITION);

			int angle = civ->getAngle();
			LOG(Main , LOGLEVEL) << " fucking angle : " << civ->getAngle();
			int dist = civ->getDistanceToCenter();
			LOG(Main , LOGLEVEL) << " fucking distance : " << dist;

			msg.push_back(bits(angle == 0 && dist == 0, ONE_BIT_SIZE));
			size += ONE_BIT_SIZE;
			civ->setLastCycleSentInRadar(world->getTime());
			sendedData->sendedAvailableCivilians.push_back(civ);

			if (angle == 0 && dist == 0)
			{
				continue;
			}
			else
			{
				msg.push_back(bits(angle, CIVILIAN_ANGLE));
				msg.push_back(bits(dist, civ->getDistanceToCenterSize()));
				size += (civ->getDistanceToCenterSize() + CIVILIAN_ANGLE);
			}
		}
		else
		{
			msg.push_back(bits(MP_ABNORMAL_CIVILIAN, INDEX_SIZE));
			LOG(Main , 1) << "faggot id : " << civ->getId() << endl;
			LOG(Main , 1) << "faggot fake id : " << civ->getFakeId() << endl;
			msg.push_back(bits(civ->getFirstTimeMotionless()->motionlessIndex, MOTIONLESS_INDEX));
			msg.push_back(bits(civ->getAngle(), CIVILIAN_ANGLE));
			msg.push_back(bits(civ->getDistanceToCenter(), civ->getDistanceToCenterSize()));

			LOG(Main , 1) << civ->getAngle() << "  " << civ->getDistanceToCenter();

			size += INDEX_SIZE + CIVILIAN_ANGLE + MOTIONLESS_INDEX + civ->getDistanceToCenterSize();
			civ->setLastCycleSentInRadar(world->getTime());
			sendedData->sendedFertCivilians.push_back(civ);
		}
	}
}

void MediumBWCommunicationRadar::sendRoads(BoolStream &msg, int& size)
{
	vector<int> semiOpenRoads;
	vector<int> openRoads;
	vector<int> blockedRoads;
	int numberOfEdge = 0;

	for (u_int i = 0; i < world->roads.size(); i++)
	{
		if (maxEdgeForSend <= numberOfEdge)
		{
			break;
		}

		if (isValidRoad(world->roads[i]))
		{
			if (isClear(world->roads[i]) == false && isFullBlocked(world->roads[i]) == false)
			{
				semiOpenRoads.push_back(i);
				numberOfEdge = numberOfEdge + world->roads[i]->getRelativeEdgesIndexes().size();
			}
			else if (isClear(world->roads[i]))
			{
				openRoads.push_back(i);
			}
			else
			{
				blockedRoads.push_back(i);
			}
			sendedData->sendedRoads.push_back(world->roads[i]);
			LOG(Main , 1) << world->roads[i]->getId() << ",";
		}
	}

	if (!openRoads.empty())
	{
		if ((INDEX_SIZE + 1) * (int) openRoads.size() < ROAD_INDEX + 1)
		{
			for (int roadIndex : openRoads)
			{
				world->roads[roadIndex]->setNumberOfSent(world->roads[roadIndex]->getNumberOfSent() + 1);
				world->roads[roadIndex]->setLastCycleSentInRadar(world->getTime());
				msg.push_back(bits(MP_CLEAR_ROAD_DATA, INDEX_SIZE));
				msg.push_back((bits(0, ONE_BIT_SIZE)));
				msg.push_back(bits(roadIndex, ROAD_INDEX));

				size += INDEX_SIZE + ONE_BIT_SIZE + ROAD_INDEX;
				sendedData->sendedRoads.push_back(world->roads[roadIndex]);
			}
		}
		else
		{
			msg.push_back(bits(MP_CLEAR_ROAD_DATA, INDEX_SIZE));
			msg.push_back(bits(1, ONE_BIT_SIZE));
			msg.push_back(bits(openRoads.size() - 1, ROAD_INDEX));
			size += (INDEX_SIZE + ONE_BIT_SIZE + ROAD_INDEX);

			for (int roadIndex : openRoads)
			{
				world->roads[roadIndex]->setNumberOfSent(world->roads[roadIndex]->getNumberOfSent() + 1);
				world->roads[roadIndex]->setLastCycleSentInRadar(world->getTime());
				msg.push_back(bits(roadIndex, ROAD_INDEX));

				size += ROAD_INDEX;
				sendedData->sendedRoads.push_back(world->roads[roadIndex]);
			}
		}
	}

	if (!blockedRoads.empty())
	{
		if ((INDEX_SIZE + 1) * (int) blockedRoads.size() < ROAD_INDEX + 1)
		{
			for (int roadIndex : blockedRoads)
			{
				world->roads[roadIndex]->setNumberOfSent(world->roads[roadIndex]->getNumberOfSent() + 1);
				world->roads[roadIndex]->setLastCycleSentInRadar(world->getTime());
				msg.push_back(bits(MP_FULL_BLOCKADED_ROAD_DATA, INDEX_SIZE));
				msg.push_back((bits(0, ONE_BIT_SIZE)));
				msg.push_back(bits(roadIndex, ROAD_INDEX));

				size += INDEX_SIZE + ONE_BIT_SIZE + ROAD_INDEX;
				sendedData->sendedRoads.push_back(world->roads[roadIndex]);
			}
		}
		else
		{
			msg.push_back(bits(MP_FULL_BLOCKADED_ROAD_DATA, INDEX_SIZE));
			msg.push_back(bits(1, ONE_BIT_SIZE));
			msg.push_back(bits(blockedRoads.size() - 1, ROAD_INDEX));
			size += (INDEX_SIZE + ONE_BIT_SIZE + ROAD_INDEX);

			for (int roadIndex : blockedRoads)
			{
				world->roads[roadIndex]->setNumberOfSent(world->roads[roadIndex]->getNumberOfSent() + 1);
				world->roads[roadIndex]->setLastCycleSentInRadar(world->getTime());
				msg.push_back(bits(roadIndex, ROAD_INDEX));

				size += ROAD_INDEX;
				sendedData->sendedRoads.push_back(world->roads[roadIndex]);
			}
		}
	}

	if (!semiOpenRoads.empty())
	{
		if ((INDEX_SIZE + 1) * (int) semiOpenRoads.size() < ROAD_INDEX + 1)
		{
			for (int roadIndex : semiOpenRoads)
			{
				msg.push_back(bits(MP_ROAD_BLOCKADE_DATA, INDEX_SIZE));
				msg.push_back(bits(0, ONE_BIT_SIZE));
				msg.push_back(bits(roadIndex, ROAD_INDEX));
				size += (ROAD_INDEX + ONE_BIT_SIZE + INDEX_SIZE);

				for (int edgeIndex : world->roads[roadIndex]->getRelativeEdgesIndexes())
				{
					msg.push_back(bits(isPassableEdge(worldGraph->getEdges()[edgeIndex], true), ONE_BIT_SIZE));
					size++;
				}
				world->roads[roadIndex]->setNumberOfSent(world->roads[roadIndex]->getNumberOfSent() + 1);
				world->roads[roadIndex]->setLastCycleSentInRadar(world->getTime());
				sendedData->sendedRoads.push_back(world->roads[roadIndex]);
			}
		}
		else
		{
			msg.push_back(bits(MP_ROAD_BLOCKADE_DATA, INDEX_SIZE));
			msg.push_back(bits(1, ONE_BIT_SIZE));
			msg.push_back(bits(semiOpenRoads.size() - 1, ROAD_INDEX));
			size += (ROAD_INDEX + INDEX_SIZE + ONE_BIT_SIZE);

			for (int roadIndex : semiOpenRoads)
			{
				msg.push_back(bits(roadIndex, ROAD_INDEX));
				size += ROAD_INDEX;
				for (int edgeIndex : world->roads[roadIndex]->getRelativeEdgesIndexes())
				{
					msg.push_back(bits(isPassableEdge(worldGraph->getEdges()[edgeIndex], true), ONE_BIT_SIZE));
					size++;
				}
				world->roads[roadIndex]->setNumberOfSent(world->roads[roadIndex]->getNumberOfSent() + 1);
				world->roads[roadIndex]->setLastCycleSentInRadar(world->getTime());
				sendedData->sendedRoads.push_back(world->roads[roadIndex]);
			}
		}
	}
	LOG(Main , 1) << "number of edge: " << numberOfEdge << endl;
}

void MediumBWCommunicationRadar::sendEdges(BoolStream &msg, int& size)
{
	int edgeCount = 0;
	for (u_int i = 0; i < worldGraph->notRelatedToRoadEdges.size(); i++)
	{
		if (isValidEdge(worldGraph->notRelatedToRoadEdges[i]))
		{
			msg.push_back(bits(MP_EDGE_PASSABILITY_DATA, INDEX_SIZE));
			msg.push_back(bits(i, NOT_RELATED_EDGE_INDEX));
			msg.push_back(bits(worldGraph->notRelatedToRoadEdges[i]->getPassingMode() == PM_PASSABLE, ONE_BIT_SIZE));

			edgeCount++;
			sendedData->sendThisEdge(worldGraph->notRelatedToRoadEdges[i]);
			size += INDEX_SIZE + NOT_RELATED_EDGE_INDEX + ONE_BIT_SIZE;
		}
	}
	LOG(Main , 1) << "special edges, sent " << edgeCount << "edges" << endl;
}

/********************************** Set Messages **********************************/

bool MediumBWCommunicationRadar::isValidBuilding(Building *building)
{
	if (building->getLastCycleUpdatedBySense() != world->getTime())
	{
		return false;
	}
	if (building->fierynessDuringTime[building->fierynessDuringTime.size() - 1] != building->fierynessDuringTime[building->fierynessDuringTime.size() - 2])
	{
		LOG(Main , 1) << "building: " << building->getId() << building->fierynessDuringTime[building->fierynessDuringTime.size() - 1] << " != " << building->fierynessDuringTime[building->fierynessDuringTime.size() - 2] << endl;
		return true;
	}
	else
	{
		if (world->getTime() < 5 && building->hasFire())
		{
			return true;
		}
		return false;
	}
}

bool MediumBWCommunicationRadar::isValidEdge(Edge* edge)
{
	if (edge->isVirtual())
	{
		return false;
	}
	if (world->motionlessObjects[edge->getMotionlessIndex()]->getLastCycleUpdatedBySense() != world->getTime())
	{
		return false;
	}
	if (edge->getPassingMode() == PM_UNKNOWN)
	{
		return false;
	}
	if (((edge->getHasToBeSent() <= 3 || edge->getHasToBeSent() != world->getTime()) && sendedData->sendThisEdge(edge)) || edge->getHasToBeSent() == -1)
	{
		LOG(Main , 1) << " lel fgt." << endl;
		return false;
	}

	LOG(Main , 1) << "Sending this edge : " << edge->getHasToBeSent() << endl;
	return true;
}

bool MediumBWCommunicationRadar::isValidRoad(Road* road)
{
	if (road->getLastCycleUpdatedBySense() != world->getTime())
	{
		return false;
	}

	LOG(Main , 1) << "This road: " << road->getId() << " has to be sent: " << road->getHasToBeSent() << " time " << world->getTime();

	if (((road->getHasToBeSent() <= 3 || road->getHasToBeSent() != world->getTime()) && sendedData->sendThisRoad(road)) || road->getHasToBeSent() == -1)
	{
		LOG(Main , 1) << " lel fgt." << endl;
		return false;
	}

	LOG(Main , 1) << " going to send it" << endl;
	return true;
}

bool MediumBWCommunicationRadar::isFertCivilian(Civilian * civilian)
{
	return !civilian->isAvailable;
}

bool MediumBWCommunicationRadar::isValidCivilian(Civilian *civilian)
{
	if (civilian == nullptr)
	{
		return false;
	}

	if (civilian->getLastCycleUpdatedBySense() != world->getTime() && civilian->getLastCycleUpdatedBySense() != world->getTime() - 1)
	{
		return false;
	}

	if (civilian->getMotionlessObject()->isRefuge())
	{
		return false;
	}

	if (civilian->getBuriedness() == 0)
	{
		if (!(civilian->getMotionlessObject()->isBuilding() && ((Building*) civilian->getMotionlessObject())->hasFire()))
		{
			if (civilian->getPosition() != self()->getId())
			{
				return false;
			}
		}
	}

	if (!civilian->isAvailable)
	{
		if (sendedData->sendThisFertCivilians(civilian))
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	if (world->refuges.empty() && civilian->getMotionlessObject()->isRoad())
	{
		return false;
	}

//	if (sendedData->sendThisAvailableCivilian(civilian))
//	{
//		int hSize = civilian->getHpHistory().size();
//		if (hSize >= 2 && (int) civilian->getHp() == civilian->getHpHistory()[hSize - 2].hp)
//		{
	//			int dSize = civilian->getDamageHistory().size();
	//			if (dSize >= 2 && (int) civilian->getDamage() == civilian->getDamageHistory()[dSize - 2].damage)
	//			{
	//				int bSize = civilian->getBuriednessHistory().size();
	//				if (bSize >= 2 && (int) civilian->getBuriedness() == civilian->getBuriednessHistory()[bSize - 2].buriedness)
	//				{
//			LOG(Main , 1) << "civilian : " << civilian->getId() << " has been recognized as a fgt civilian " << endl;
//			return false;
	//				}
	//				else if (civilian->getLastCycleSentInRadar() == world->getTime() - 1)
	//				{
	//					return false;
	//				}
	//			}
//		}
//	}

	LOG(Main , 1) << "civilian : " << civilian->getId() << " has been recognized as a valid civilian " << endl;
	return true;
}

bool MediumBWCommunicationRadar::isClear(Road* road)
{
	for (int i = 0; i < (int) road->getRelativeEdgesIndexes().size(); i++)
	{
		if (!isPassableEdge(worldGraph->getEdges()[road->getRelativeEdgesIndexes()[i]], true))
		{
			return false;
		}
	}
	return true;
}

bool MediumBWCommunicationRadar::isFullBlocked(Road* road)
{
	for (int i = 0; i < (int) road->getRelativeEdgesIndexes().size(); i++)
	{
		if (isPassableEdge(worldGraph->getEdges()[road->getRelativeEdgesIndexes()[i]]))
		{
			return false;
		}
	}
	return true;
}

bool MediumBWCommunicationRadar::doIHaveUsefulDataForSendInRadar()
{
	int maxDistanceForNears = 10 * AGENT_RADIUS;

	vector<Platoon *> nearPlatoons;
	for (int i = 0; i < (int) world->platoons.size(); i++)
	{
		if (world->platoons[i]->isAvailable == false)
		{
			continue;
		}

		if (world->platoons[i]->getHp() == 0)
		{
			continue;
		}

		if (world->platoons[i]->getLastCycleUpdatedBySense() != world->getTime())
		{
			continue;
		}

		if (world->platoons[i]->getPos().distFromPoint(self()->getPos()) < maxDistanceForNears)
		{
			nearPlatoons.push_back(world->platoons[i]);
		}
		else if (self()->getMotionlessObject()->isBuilding() && world->platoons[i]->getMotionlessObject()->getId() == self()->getMotionlessObject()->getId())
		{
			nearPlatoons.push_back(world->platoons[i]);
		}
	}

	if (nearPlatoons.size() <= 1)
	{
		return true;
	}

	sort(nearPlatoons.begin(), nearPlatoons.end());
	if (self()->getId() == nearPlatoons[0]->getId())
	{
		return true;
	}

	return false;
}

/********************************** Receive Messages **********************************/

void MediumBWCommunicationRadar::analyzeMessage(vector<byte>& radarMsg, int& offset, int sizeOfContent, int senderId, int channel)
{
	try
	{
		LOG(Main , 1) << endl << "*********************************[MediumBWCommunicationRadar]**********************************" << endl << endl;
		LOG(Main , 1) << "Analyze message!" << endl;
		LOG(Main , 1) << "Radar message begins:     " << senderId << "       with size:      " << sizeOfContent + 1 << " 		from chanel : 		" << channel << endl;
		BoolStream msg(sizeOfContent);
		msg.setValue(vector<byte>(radarMsg.begin() + offset, radarMsg.begin() + offset + sizeOfContent));
		LOG(Main , 1) << "sizeofcontent:  " << sizeOfContent << endl;
		LOG(Main , 1) << "offset:  " << offset << endl;
		LOG(Main, 1) << "sender: " << senderId << endl;
		if (radarMode != RM_NO_COMMUNICATION)
		{
			LOG(Main , 1) << "Msg:  " << msg.getVal(" - ") << endl;
		}

		sizeOfContent = sizeOfContent * 8;
		LOG(Main , 1) << "sizeofcontent:  " << sizeOfContent << endl;

		bool isRadio = true;
		for (int i = 0; i < (int) voiceChannels.size(); i++)
		{
			if (voiceChannels[i]->getChannelNum() == channel)
			{
				isRadio = false;
				break;
			}
		}

		if (!isRadio)
		{
			LOG(Main , 1) << "parsing voice channel messages" << endl;
			clRadar->analyzeMessage(msg, senderId, sizeOfContent, channel);
			LOG(Main, 1) << "is voice" << endl;
			return;
		}

		if (isRadio == false && world->selfID == senderId)
		{
			return;
		}

		if (world->objects[senderId] == nullptr)
		{
			LOG(Main , 1) << "what the actual fuck?!?" << endl;
			return;
		}

		if (world->objects[senderId]->isCivilian())
		{
			return;
		}

		if (world->objects[senderId]->isPlatoon())
		{
			Platoon * sender = (Platoon *) world->objects[senderId];
			sender->setLastCycleUpdated(world->getTime());
			sender->needPoliceHelp = false;
		}

		if (msg.size() <= 1)
		{
			LOG(Main , 1) << "this message has noise!" << endl;

			if (senderId == world->selfID)
			{
				sendedData->amISendMyLastCycleData = false;
				removeLastCycleSendedData();
			}

			return;
		}

		if (world->self->isPlatoon())
		{
			if (isBetween(world->getTime() - self()->getLastCycleUpdated(), 1, 1))
			{
				for (int i = 0; i < (int) world->buildings.size(); i++)
				{
					if (isBetween(world->getTime() - world->buildings[i]->getLastCycleSentInRadar(), 1, 1))
					{
						world->buildings[i]->setLastCycleUpdatedBySense(world->getTime());

						if (world->buildings[i]->hasFire())
						{
							sendedData->removeThisBuildingFromBuildingsInFire(world->buildings[i]);
						}
						else
						{
							sendedData->removeThisBuildingFromExtinguishedBuildings(world->buildings[i]);
						}
					}
				}
			}
		}

		while (sizeOfContent > 3)
		{
			int header = msg.pop_bits(INDEX_SIZE).getA();
			LOG(Main , 1) << "----------------------------------------------" << endl;
			LOG(Main , 1) << "Header is: " << header << endl;

			sizeOfContent = sizeOfContent - INDEX_SIZE;

			switch (header)
			{
			case MP_POSITION:
			{
				int nodeIndex = msg.pop_bits(NODE_INDEX).getA();
				int isStucked = msg.pop_bits(ONE_BIT_SIZE).getA();
				int side = msg.pop_bits(ONE_BIT_SIZE).getA();

				LOG(Main , 1) << "Node Index: " << nodeIndex << endl;
				LOG(Main , 1) << "isStucked: " << isStucked << endl;
				LOG(Main , 1) << "Side: " << side << endl;

				Platoon * sender = ((Platoon *) world->objects[senderId]);
				if (sender->getLastCycleUpdatedBySense() != world->getTime())
				{
					setPosition(sender, nodeIndex, isStucked, side);
				}

				sizeOfContent = sizeOfContent - TOTOAL_POSITION;
				break;
			}
			case MP_BURIEDNESS:
			{
				int buriedness = msg.pop_bits(BURIEDNESS).getA();

				Platoon * sender = ((Platoon *) world->objects[senderId]);

				if (sender->getLastCycleUpdatedBySense() != world->getTime())
				{
					sender->setBuriedness(buriedness);
				}

				sizeOfContent = sizeOfContent - BURIEDNESS;
				LOG(Main , 1) << "Buriedness of : " << sender->getId() << " is updated!" << endl;

				break;
			}
			case MP_HAS_WATER_IS_CARRY:
			{
				Platoon * sender = ((Platoon *) world->objects[senderId]);

				if (sender->isFireBrigade())
				{
					FireBrigade * senderFireBrigade = ((FireBrigade *) sender);
					int hasWater = msg.pop_bits(ONE_BIT_SIZE).getA();
					senderFireBrigade->setHasWaterValue(hasWater == 1);

					LOG(Main , 1) << "hasWater updated!" << endl;
				}

				if (sender->isAmbulanceTeam())
				{
					AmbulanceTeam * senderAmbulance = ((AmbulanceTeam *) sender);
					int isCarry = msg.pop_bits(ONE_BIT_SIZE).getA();
					senderAmbulance->setIsCarry(isCarry == 1);

					LOG(Main , 1) << "isCarry is updated!" << endl;
				}

				sizeOfContent -= ONE_BIT_SIZE;

				break;
			}
			case MP_BUILDING_IN_FIRE:
			{
				int buildingInFireIndex = msg.pop_bits(BUILDING_INDEX).getA();
				int fieryness = msg.pop_bits(FIERYNESS).getA();
				int tempreture = temperatureCoef * msg.pop_bits(TEMPERATURE).getA();

				if (world->buildings[buildingInFireIndex]->getLastCycleUpdatedBySense() != world->getTime())
				{
					world->buildings[buildingInFireIndex]->setFieryness(fieryness);
					world->buildings[buildingInFireIndex]->setTemperature(tempreture);
					world->buildings[buildingInFireIndex]->setLastCycleUpdated(world->getTime());
				}

				sizeOfContent = sizeOfContent - BUILDING_INDEX - FIERYNESS - TEMPERATURE;
				LOG(Main , 1) << "Building " << world->buildings[buildingInFireIndex]->getId() << " has fire!" << endl;

				break;
			}
			case MP_BUILDING_BURRNED:
			{
				int buildingInFireIndex = msg.pop_bits(BUILDING_INDEX).getA();
				if (world->buildings[buildingInFireIndex]->getLastCycleUpdatedBySense() != world->getTime())
				{
					world->buildings[buildingInFireIndex]->setFieryness(8);
					world->buildings[buildingInFireIndex]->setLastCycleUpdated(world->getTime());
					sizeOfContent = sizeOfContent - BUILDING_INDEX;
					LOG(Main , 1) << "Building " << world->buildings[buildingInFireIndex]->getId() << " Burned!" << endl;
				}
				break;
			}
			case MP_BUILDING_EXTINGUISHED:
			{
				int buildingIndex = msg.pop_bits(BUILDING_INDEX).getA();
				int fieryness = msg.pop_bits(EXTINGUISHED_FIERYNESS).getA();

				if (world->buildings[buildingIndex]->getLastCycleUpdatedBySense() != world->getTime())
				{
					world->buildings[buildingIndex]->setFieryness(fieryness + 5);
					world->buildings[buildingIndex]->setTemperature(0);
					world->buildings[buildingIndex]->setLastCycleUpdated(world->getTime());
				}
				sizeOfContent = sizeOfContent - BUILDING_INDEX - EXTINGUISHED_FIERYNESS;
				LOG(Main , 1) << "Building " << world->buildings[buildingIndex]->getId() << " Extinguished Fire!" << endl;
				break;
			}
			case MP_NORMAL_CIVILIAN:
			{
				int ttd = msg.pop_bits(TIME_TO_DEATH).getA() * timeToDeathCoef - 1;
				int pos = msg.pop_bits(NODE_INDEX).getA();
				int isStucked = msg.pop_bits(ONE_BIT_SIZE).getA();
				int side = msg.pop_bits(ONE_BIT_SIZE).getA();
				int buriedness = msg.pop_bits(BURIEDNESS).getA();

				int motionless = 0;
				if (side == 1)
				{
					motionless = worldGraph->getNodes()[pos]->getFirstMotionlessIndex();
				}
				else
				{
					motionless = worldGraph->getNodes()[pos]->getSecondMotionlessIndex();
				}
				sizeOfContent -= (INDEX_SIZE + TOTOAL_POSITION + BURIEDNESS + TIME_TO_DEATH);

				int angle = 0;
				int distance = 0;
				int zeroDistanceAndAngle = msg.pop_bits(ONE_BIT_SIZE).getA();
				sizeOfContent -= ONE_BIT_SIZE;
				if (zeroDistanceAndAngle == 0)
				{
					angle = msg.pop_bits(CIVILIAN_ANGLE).getA();
					sizeOfContent -= CIVILIAN_ANGLE;
					distance = msg.pop_bits(ceil(log2l(world->motionlessObjects[motionless]->getMaxDistanceFromCenter() / distanceCoef))).getA();
					sizeOfContent -= ceil(log2l(world->motionlessObjects[motionless]->getMaxDistanceFromCenter() / distanceCoef));
				}

				LOG(Main , 1) << "angle : " << angle << endl;
				LOG(Main , 1) << "distance : " << distance << endl;
				Civilian* civ = new Civilian(motionless, angle, distance, ceil(log2l(world->motionlessObjects[motionless]->getMaxDistanceFromCenter() / distanceCoef)));
				int fakeId = civ->getFakeId();

				Segment seg(world->motionlessObjects[motionless]->getPos(), angle * angleCoef, distance * distanceCoef);
				civ->setX(seg.getSecondPoint().getX());
				civ->setY(seg.getSecondPoint().getY());

				civ->setRealX(seg.getSecondPoint().getX());
				civ->setRealY(seg.getSecondPoint().getY());

				LOG(Main , 1) << "Civ fake id: " << fakeId << endl;
				LOG(Main , 1) << "Pos: " << world->motionlessObjects[motionless]->getId() << endl;
				LOG(Main , 1) << "side: " << side << endl;
				LOG(Main , 1) << "Is stucked!	" << isStucked << endl;
				LOG(Main , 1) << "Buriedness : " << buriedness << endl;
				LOG(Main , 1) << "Time To Death: " << ttd << endl;

				if (world->fakeCivilians[fakeId])
				{
					LOG(Main , 1) << "update time: " << world->fakeCivilians[fakeId]->getLastCycleUpdatedBySense() << endl;
				}
				if (world->fakeCivilians[fakeId] == nullptr)
				{
					LOG(Main , 1) << "Be jane madaram nadidam!! " << endl;

					setPosition(civ, pos, isStucked, side);
					civ->setBuriedness(buriedness);
					civ->setTimeToDeath(ttd);

					civ->humanIndex = world->humans.size();
					civ->civilianIndex = world->civilians.size();
					civ->isAvailable = true;
					civ->setLastCycleSentInRadar(world->getTime() - 1);
					civ->setLastCycleUpdated(world->getTime());
					world->fakeCivilians[fakeId] = civ;
					world->civilians.push_back(civ);
					world->humans.push_back(civ);
				}
				else
				{
					delete civ;

					if (world->fakeCivilians[fakeId]->getLastCycleUpdatedBySense() != world->getTime())
					{
						setPosition(world->fakeCivilians[fakeId], pos, isStucked, side);
						world->fakeCivilians[fakeId]->setBuriedness(buriedness);
						world->fakeCivilians[fakeId]->setTimeToDeath(ttd);

						world->fakeCivilians[fakeId]->isAvailable = true;
						world->fakeCivilians[fakeId]->setLastCycleSentInRadar(world->getTime() - 1);
						world->fakeCivilians[fakeId]->setLastCycleUpdated(world->getTime());
					}
				}

				break;
			}
			case MP_ABNORMAL_CIVILIAN:
			{
				int motionless = msg.pop_bits(MOTIONLESS_INDEX).getA();
				int angle = msg.pop_bits(CIVILIAN_ANGLE).getA();
				MotionlessObject* mo = world->motionlessObjects[motionless];
				int dist = msg.pop_bits(ceil(log2l(mo->getMaxDistanceFromCenter() / distanceCoef))).getA();

				Civilian* civ = new Civilian(motionless, angle, dist, ceil(log2l(mo->getMaxDistanceFromCenter() / distanceCoef)));
				int fakeId = civ->getFakeId();
				delete civ;

				LOG(Main , 1) << "This id is fert civ: " << fakeId << " " << world->motionlessObjects[motionless]->getId() << endl;

				if (world->fakeCivilians[fakeId] != nullptr)
				{
					world->fakeCivilians[fakeId]->isAvailable = false;
					world->fakeCivilians[fakeId]->setBuriedness(0);
					world->fakeCivilians[fakeId]->setLastCycleSentInRadar(world->getTime());
					world->fakeCivilians[fakeId]->setLastCycleUpdated(world->getTime());
				}

				sizeOfContent = sizeOfContent - CIVILIAN_ANGLE - MOTIONLESS_INDEX - ceil(log2l(world->motionlessObjects[motionless]->getMaxDistanceFromCenter() / distanceCoef));

				break;
			}
			case MP_FULL_BLOCKADED_ROAD_DATA:
			{
				int isPro = msg.pop_bits(ONE_BIT_SIZE).getA(); //so pro!!!!
				sizeOfContent -= ONE_BIT_SIZE;
				if (isPro == 1)
				{
					int roadSize = msg.pop_bits(ROAD_INDEX).getA() + 1;
					sizeOfContent -= ROAD_INDEX;
					for (int i = 0; i < roadSize; i++)
					{
						int roadIndex = msg.pop_bits(ROAD_INDEX).getA();
						sizeOfContent -= ROAD_INDEX;
						Road* road = world->roads[roadIndex];
						if (road->getLastCycleUpdatedBySense() != world->getTime())
						{
							for (int i = 0; i < (int) road->getRelativeEdgesIndexes().size(); i++)
							{
								worldGraph->getEdges()[road->getRelativeEdgesIndexes()[i]]->setPassingMode(PM_NOT_PASSABLE);
							}
							road->setLastCycleUpdated(world->getTime());
							LOG(Main , 1) << "This road is clear: " << road->getId() << endl;
						}
					}
				}
				else
				{
					int roadIndex = msg.pop_bits(ROAD_INDEX).getA();
					sizeOfContent -= ROAD_INDEX;
					Road* road = world->roads[roadIndex];
					if (road->getLastCycleUpdatedBySense() != world->getTime())
					{
						for (int i = 0; i < (int) road->getRelativeEdgesIndexes().size(); i++)
						{
							worldGraph->getEdges()[road->getRelativeEdgesIndexes()[i]]->setPassingMode(PM_NOT_PASSABLE);
						}
						road->setLastCycleUpdated(world->getTime());
						LOG(Main , 1) << "This road is full blockade: " << road->getId() << endl;
					}
				}

				break;
			}
			case MP_CLEAR_ROAD_DATA:
			{
				int isPro = msg.pop_bits(ONE_BIT_SIZE).getA(); //so pro!!!!
				sizeOfContent -= ONE_BIT_SIZE;
				if (isPro == 1)
				{
					int roadSize = msg.pop_bits(ROAD_INDEX).getA() + 1;
					sizeOfContent -= ROAD_INDEX;
					for (int i = 0; i < roadSize; i++)
					{
						int roadIndex = msg.pop_bits(ROAD_INDEX).getA();
						sizeOfContent -= ROAD_INDEX;
						Road* road = world->roads[roadIndex];
						if (road->getLastCycleUpdatedBySense() != world->getTime())
						{
							for (int i = 0; i < (int) road->getRelativeEdgesIndexes().size(); i++)
							{
								worldGraph->getEdges()[road->getRelativeEdgesIndexes()[i]]->setPassingMode(PM_PASSABLE);
							}
							road->setLastCycleUpdated(world->getTime());
							LOG(Main , 1) << "This road is clear: " << road->getId() << endl;
						}
					}
				}
				else
				{
					int roadIndex = msg.pop_bits(ROAD_INDEX).getA();
					sizeOfContent -= ROAD_INDEX;
					Road* road = world->roads[roadIndex];
					if (road->getLastCycleUpdatedBySense() != world->getTime())
					{
						for (int i = 0; i < (int) road->getRelativeEdgesIndexes().size(); i++)
						{
							worldGraph->getEdges()[road->getRelativeEdgesIndexes()[i]]->setPassingMode(PM_PASSABLE);
						}
						road->setLastCycleUpdated(world->getTime());
						LOG(Main , 1) << "This road is clear: " << road->getId() << endl;
					}
				}

				break;
			}
			case MP_ROAD_BLOCKADE_DATA:
			{
				int isPro = msg.pop_bits(ONE_BIT_SIZE).getA();
				sizeOfContent -= ONE_BIT_SIZE;
				if (isPro == 1)
				{
					int roadSize = msg.pop_bits(ROAD_INDEX).getA() + 1;
					sizeOfContent -= ROAD_INDEX;
					for (int i = 0; i < roadSize; i++)
					{
						int roadIndex = msg.pop_bits(ROAD_INDEX).getA();
						sizeOfContent -= ROAD_INDEX;
						if (world->roads[roadIndex]->getLastCycleUpdatedBySense() != world->getTime())
						{
							LOG(Main , 1) << "Road " << world->roads[roadIndex]->getId() << " is going to update!" << endl;
							for (int edgeIndex : world->roads[roadIndex]->getRelativeEdgesIndexes())
							{
								worldGraph->getEdges()[edgeIndex]->setPassingMode(msg.pop_bool() ? PM_PASSABLE : PM_NOT_PASSABLE);
							}
						}
						else
						{
							for (int i = 0; i < (int) world->roads[roadIndex]->getRelativeEdgesIndexes().size(); i++)
							{
								msg.pop_bool();
							}
						}
						world->roads[roadIndex]->setLastCycleUpdated(world->getTime());
						sizeOfContent -= world->roads[roadIndex]->getRelativeEdgesIndexes().size();
					}
				}
				else
				{
					int roadIndex = msg.pop_bits(ROAD_INDEX).getA();
					Road * road = world->roads[roadIndex];
					LOG(Main , 1) << "Road " << road->getId() << " recieved data!!" << endl;

					if (road->getLastCycleUpdatedBySense() != world->getTime())
					{
						LOG(Main , 1) << "Road " << road->getId() << " is going to update!" << endl;
						for (int edgeIndex : road->getRelativeEdgesIndexes())
						{
							worldGraph->getEdges()[edgeIndex]->setPassingMode(msg.pop_bool() ? PM_PASSABLE : PM_NOT_PASSABLE);
						}
					}
					else
					{
						for (int i = 0; i < (int) road->getRelativeEdgesIndexes().size(); i++)
						{
							msg.pop_bool();
						}
					}

					road->setLastCycleUpdated(world->getTime());
					sizeOfContent = sizeOfContent - ROAD_INDEX - (int) road->getRelativeEdgesIndexes().size();
				}

				break;
			}
			case MP_AMBULANCE_COMMAND:
			{
				LOG(Main , 1) << "MP_AMBULANCE_COMMAND" << endl;
				int ambulanceQuantity = 1 + msg.pop_bits(AMBULANCE_INDEX).getA();
				sizeOfContent = sizeOfContent - AMBULANCE_INDEX;
				LOG(Main , 1) << "1" << endl;
				for (int i = 0; i < ambulanceQuantity; i++)
				{
					int ambIndex = msg.pop_bits(AMBULANCE_INDEX).getA();
					LOG(Main, 1) << "to ambulance: " << world->ambulanceTeams[ambIndex]->getId() << endl;
					int isPlatoon = msg.pop_bits(ONE_BIT_SIZE).getA();
					sizeOfContent -= (AMBULANCE_INDEX + ONE_BIT_SIZE);

					if (isPlatoon == 1)
					{
						int platoonIndex = msg.pop_bits(PLATOON_INDEX).getA();
						world->ambulanceTeams[ambIndex]->centerTarget = world->platoons[platoonIndex];
						sizeOfContent -= (PLATOON_INDEX);
					}
					else
					{
						int motionlessIndex = msg.pop_bits(MOTIONLESS_INDEX).getA();
						int distanceSize = ceil(log2l(world->motionlessObjects[motionlessIndex]->getMaxDistanceFromCenter() / distanceCoef));

						int angle = 0;
						int distance = 0;
						int zeroAngleAndDistance = msg.pop_bits(ONE_BIT_SIZE).getA();
						if (zeroAngleAndDistance == 0)
						{
							angle = msg.pop_bits(CIVILIAN_ANGLE).getA();
							distance = msg.pop_bits(distanceSize).getA();
							sizeOfContent -= (CIVILIAN_ANGLE + distanceSize);
						}
						sizeOfContent -= (MOTIONLESS_INDEX + ONE_BIT_SIZE);

						Civilian* civ = new Civilian(motionlessIndex, angle, distance, distanceSize);
						if (world->fakeCivilians[civ->getFakeId()] == nullptr)
						{
							int node = world->motionlessObjects[motionlessIndex]->getRepresentiveNodeIndex();
							int side = (motionlessIndex == worldGraph->getNodes()[node]->getFirstMotionlessIndex() ? 1 : 0);
							setPosition(civ, world->motionlessObjects[motionlessIndex]->getRepresentiveNodeIndex(), 0, side);

							world->ambulanceTeams[ambIndex]->centerTarget = civ;
							world->fakeCivilians[civ->getFakeId()] = civ;
							world->civilians.push_back(civ);
							world->humans.push_back(civ);

							world->ambulanceTeams[ambIndex]->centerTarget = world->fakeCivilians[civ->getFakeId()];
						}
						else
						{
							world->ambulanceTeams[ambIndex]->centerTarget = world->fakeCivilians[civ->getFakeId()];
							delete civ;
						}
					}
				}
				break;
			}
			case MP_EDGE_PASSABILITY_DATA:
			{
				int edgeIndex = msg.pop_bits(NOT_RELATED_EDGE_INDEX).getA();
				int passingMode = msg.pop_bits(ONE_BIT_SIZE).getA();

				int motionlessIndex = worldGraph->getEdges()[worldGraph->notRelatedToRoadEdges[edgeIndex]->edgeIndex]->getMotionlessIndex();
				if (world->motionlessObjects[motionlessIndex]->getLastCycleUpdatedBySense() != world->getTime())
				{
					if (passingMode == 1)
					{
						worldGraph->getEdges()[worldGraph->notRelatedToRoadEdges[edgeIndex]->edgeIndex]->setPassingMode(PM_PASSABLE);
					}
					else
					{
						worldGraph->getEdges()[worldGraph->notRelatedToRoadEdges[edgeIndex]->edgeIndex]->setPassingMode(PM_NOT_PASSABLE);
					}
				}
				break;
			}
			case MP_END:
			{
				LOG(Main , 1) << "End of story :'(" << endl;
				sizeOfContent = 0;
				break;
			}
			default:
			{
				LOG(Main , 1) << "What the !? :D" << endl;
				break;
			}
			}
		}
	}
	catch (exception& e)
	{
		cout << "Exception in analyze message " << e.what() << endl;
	}
	catch (...)
	{
		cout << "Unknown exception in analyze message" << endl;
	}
}

void MediumBWCommunicationRadar::removeLastCycleSendedData()
{
	for (int i = 0; i < (int) world->buildings.size(); i++)
	{
		if (world->buildings[i]->getLastCycleSentInRadar() == world->getTime() - 1)
		{
			sendedData->removeThisBuildingFromBuildingsInFire(world->buildings[i]);
			sendedData->removeThisBuildingFromExtinguishedBuildings(world->buildings[i]);
		}
	}

	for (int i = 0; i < (int) world->civilians.size(); i++)
	{
		if (world->civilians[i]->getLastCycleSentInRadar() == world->getTime() - 1)
		{
			sendedData->removeAvailableThisCivilianFromSendedCivilians(world->civilians[i]);
			sendedData->removeThisFertCivilianFromSendedCivilians(world->civilians[i]);
		}
	}

	for (int i = 0; i < (int) world->roads.size(); i++)
	{
		if (world->roads[i]->getLastCycleSentInRadar() == world->getTime() - 1)
		{
			sendedData->removeThisRoadFromSendedRoads(world->roads[i]);
		}
	}
}
