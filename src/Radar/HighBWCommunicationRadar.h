#ifndef HIGHBWCOMMUNICATIONRADAR_H
#define HIGHBWCOMMUNICATIONRADAR_H

#include "AbstractCommunicationradar.h"

class HighBWCommunicationRadar: public AbstractCommunicationRadar
{
public:

	HighBWCommunicationRadar(WorldModel* wm, WorldGraph* wg, Command* command);

	void subscribe();
	virtual void init();
	virtual void analyzeMessage(vector<byte>& radarMsg, int& offset, int sizeOfContent, int senderId, int channel);
	virtual void shareSenseWorldAndSendOrders();
	virtual void shareSenseWorldAfterDecision();
	virtual ~HighBWCommunicationRadar();
private:
	int myMinimalSize;
	int maxEdgeForSend;

	virtual void setMyChanel();
	virtual void setSizes();
	void sendBuriednessProperty(BoolStream &msg, int& size);
	void sendMyPositionProperty(BoolStream &msg, int& size);
	void sendBuildings(BoolStream &msg, int& size);
	void sendCivilians(BoolStream& msg, int& size);
	void sendHasWaterIsCarry(BoolStream &msg, int& size);
	void sendRoads(BoolStream &msg, int& size);
	void sendEdges(BoolStream &msg, int& size);
	void sendAmbulanceCommands(BoolStream &msg, int& size);
	void removeLastCycleSendedData();

	bool isValidEdge(Edge* edge);
	bool isValidRoad(Road* road);
	bool isFertCivilian(Civilian * civilian);
	bool isValidBuilding(Building* building);
	bool isValidCivilian(Civilian* Civilian);

	bool isClear(Road* road);
	bool isFullBlocked(Road* road);
	bool doIHaveUsefulDataForSendInRadar();
};

#endif // HIGHBWCOMMUNICATIONRADAR_H
