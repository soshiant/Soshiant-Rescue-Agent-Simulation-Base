/*
 * LowBWCommunicationRadar.cpp
 *
 *  Created on: Jan 6, 2014
 *      Author: pedram
 */

#include "LowBWCommunicationRadar.h"

LowBWCommunicationRadar::LowBWCommunicationRadar(WorldModel * wm, WorldGraph * wg, Command * command) :
		AbstractCommunicationRadar(wm, wg, command)
{
}

LowBWCommunicationRadar::~LowBWCommunicationRadar()
{
}

void LowBWCommunicationRadar::init()
{
	AbstractCommunicationRadar::init();
}

void LowBWCommunicationRadar::subscribe()
{
	AbstractCommunicationRadar::subscribe();
}

void LowBWCommunicationRadar::setMyChanel()
{
	if (radarMode == RM_NO_COMMUNICATION)
	{
		myChanel = nullptr;
		return;
	}

	vector<RadioChannel *> chanelInUse;

	if (numOfSubscribeForPlatoons >= (int) radioChannels.size())
	{
		chanelInUse = radioChannels;
	}
	else
	{
		for (int i = 0; i < numOfSubscribeForPlatoons; i++)
		{
			chanelInUse.push_back(radioChannels[i]);
		}
	}

	listeningChanels = chanelInUse;

	if (!self()->isPlatoon())
	{
		LOG(Main , 1) << "[LowBWCommunicationRadar][Size][setMyChanel] I'm Center !!!" << endl;
		myChanel = chanelInUse[0];
	}
	else
	{
		int myIndex = 0;
		int allSayersCount = 0;
		for (int i = 0; i < (int) world->platoons.size(); i++)
		{
			allSayersCount++;
			if (world->platoons[i]->getId() == self()->getId())
				myIndex = allSayersCount - 1;
		}

		for (int i = 0; i < (int) world->ambulanceCenters.size(); i++)
		{
			allSayersCount++;
			if (world->ambulanceCenters[i]->getId() == self()->getId())
				myIndex = allSayersCount - 1;
		}

		for (int i = 0; i < (int) world->policeOffices.size(); i++)
		{
			allSayersCount++;
			if (world->policeOffices[i]->getId() == self()->getId())
				myIndex = allSayersCount - 1;
		}

		for (int i = 0; i < (int) world->fireStations.size(); i++)
		{
			allSayersCount++;
			if (world->fireStations[i]->getId() == self()->getId())
				myIndex = allSayersCount - 1;
		}

		int myRadioChanelIndex = myIndex % chanelInUse.size();
		myChanel = chanelInUse[myRadioChanelIndex];

	}
	LOG(Main , 1) << "[LowBWCommunicationRadar][Size][setMyChanel] myChannelNum: " << myChanel->getChannelNum() << endl;
}

void LowBWCommunicationRadar::setSizes()
{
	BUILDING_INDEX = ceil(log2l(world->buildings.size()));
	INDEX_SIZE = 1;
}

void LowBWCommunicationRadar::shareSenseWorldAndSendOrders()
{
	LOG(Main , 1) << "---------------------[LowBWCommunicationRadar]-----------------shareSenseWorldAndSendOrders-----------------" << endl;
	LOG(Main , 1) << "[LowBWCommunicationRadar] Begins!" << endl;

	if (!voiceChannels.empty() && world->self->isPlatoon())
	{
		clRadar->shareDataOnCommunicationlessModel();
	}

	if (myChanel == nullptr)
	{
		return;
	}

	if (world->getTime() % 30 == 0)
	{
		sendedData->sendedBuildingsInFire.clear();
	}

	BoolStream msg;
	msg.clear();
	msg.setSize(1000);
	sizeOfMessage = 0;

	if (doIHaveUsefulDataForSendInRadar())
	{
		sendBuriedness(msg, sizeOfMessage);
		LOG(Main , 1) << "buriedness setting finished size : " << sizeOfMessage << endl;
		sendBuildings(msg, sizeOfMessage);
		LOG(Main , 1) << "firey buildings setting finished size : " << sizeOfMessage << endl;
	}

	if (sizeOfMessage > 1)
	{
		LOG(Main , 1) << "Final message size: " << sizeOfMessage << endl;

		sizeOfMessage = ceil(sizeOfMessage / 8.);

		msg.setSize(sizeOfMessage);
		LOG(Main , 1) << "sizeOfMessage: " << sizeOfMessage << endl;
		LOG(Main , 1) << "Msg size: " << msg.getVal().size() << endl;
		LOG(Main , 1) << msg.getVal(" - ") << endl;
		LOG(Main , 1) << "Msg For send: " << msg.getValue().size() << endl;

		LOG(Main , 1) << "Radar ends!" << endl;
		LOG(Main , 1) << "--------------------------------------------------" << endl;

		if (self()->isAmbulanceCenter() || self()->isFireStation() || self()->isPoliceOffice())
		{
			for (int i = 0; i < numOfSubscribeForPlatoons; i++)
			{
				command->speak(radioChannels[i]->getChannelNum(), msg.getValue());
			}
		}
		else
		{
			cerr << "(myChanel == nullptr) : " << (myChanel == nullptr) << endl;
			command->speak(myChanel->getChannelNum(), msg.getValue());
		}

		sendedData->amISendMyLastCycleData = true;
	}
	else
	{
		LOG(Main , 1) << "no speak, much use of bandwidth, very pro, such sacrifice, wow" << endl;
	}
}

void LowBWCommunicationRadar::shareSenseWorldAfterDecision()
{
}

void LowBWCommunicationRadar::sendBuriedness(BoolStream &msg, int& size)
{
	if (world->self->isPlatoon() == false)
	{
		return;
	}
	if (world->getTime() > 5)
	{
		int bSize = self()->getBuriednessHistory().size() - 1;
		LOG(Main,1) << "bSize: " << bSize << endl;
		if (bSize > 1)
		{
			if ((self()->getBuriednessHistory()[bSize].buriedness == self()->getBuriednessHistory()[bSize - 1].buriedness) && sendedData->amISendMyLastCycleData)
			{
				return;
			}
		}
		else if (self()->getBuriedness() == 0)
		{
			return;
		}
	}
	else if (self()->getBuriedness() == 0)
	{
		return;
	}

	msg.push_back(bits(LCP_AGENT_DATA, INDEX_SIZE));

	size += INDEX_SIZE;
	LOG(Main ,1) << "[LowBWCommunicationRadar] [sendBuriednessProperty]" << "AgentID: " << self()->getId() << endl;
}

void LowBWCommunicationRadar::sendBuildings(BoolStream &msg, int& size)
{
	vector<Building*> validBuildings;
	for (Building* b : world->buildings)
	{
		for (int fieryness : b->fierynessDuringTime)
		{
			if (fieryness >= 4 && fieryness <= 7)
			{
				sendedData->removeThisBuildingFromBuildingsInFire(b);
				break;
			}
		}

		if (b->getLastCycleUpdatedBySense() == world->getTime() && b->hasFire() && !sendedData->sendThisBuildingInFire(b))
		{
			msg.push_back(bits(LCP_BUILDING_DATA, INDEX_SIZE));
			msg.push_back(bits(b->buildingIndex, BUILDING_INDEX));
			sendedData->sendedBuildingsInFire.push_back(b);
			size += INDEX_SIZE + BUILDING_INDEX;
		}
	}
}

void LowBWCommunicationRadar::analyzeMessage(vector<byte>& radarMsg, int& offset, int sizeOfContent, int senderId, int channel)
{
	try
	{
		LOG(Main , 1) << endl << "*********************************[LowBWCommunicationRadar]**********************************" << endl << endl;
		LOG(Main , 1) << "Analyze message!" << endl;
		LOG(Main , 1) << "Radar message begins:     " << senderId << "       with size:      " << sizeOfContent + 1 << " 		from channel : 		" << channel << endl;
		BoolStream msg(sizeOfContent);
		msg.setValue(vector<byte>(radarMsg.begin() + offset, radarMsg.begin() + offset + sizeOfContent));
		LOG(Main , 1) << "sizeofcontent:  " << sizeOfContent << endl;
		LOG(Main , 1) << "offset:  " << offset << endl;
		if (radarMode != RM_NO_COMMUNICATION)
		{
			LOG(Main , 1) << "Msg:  " << msg.getVal(" - ") << endl;
		}

		sizeOfContent = sizeOfContent * 8;
		LOG(Main , 1) << "sizeofcontent:  " << sizeOfContent << endl;

		bool isRadio = true;
		for (int i = 0; i < (int) voiceChannels.size(); i++)
		{
			if (voiceChannels[i]->getChannelNum() == channel)
			{
				isRadio = false;
				break;
			}
		}

		if (!isRadio)
		{
			LOG(Main , 1) << "parsing voice channel messages" << endl;
			clRadar->analyzeMessage(msg, senderId, sizeOfContent, channel);
			LOG(Main, 1) << "is voice" << endl;
			return;
		}

		if (isRadio == false && world->selfID == senderId)
		{
			return;
		}

		if (world->objects[senderId] == nullptr)
		{
			LOG(Main , 1) << "what the actual fuck?!?" << endl;
			return;
		}

		if (world->objects[senderId]->isCivilian())
		{
			return;
		}

		bool finished = false;
		while (sizeOfContent > 1)
		{
			int header = msg.pop_bits(INDEX_SIZE).getA();
			LOG(Main , 1) << "----------------------------------------------" << endl;
			LOG(Main , 1) << "Header is: " << header << endl;

			sizeOfContent = sizeOfContent - INDEX_SIZE;

			switch (header)
			{
				case LCP_AGENT_DATA:
				{
					if (finished)
					{
						LOG(Main , 1) << "fucking finished message analytics" << endl;
						return;
					}
					if (world->objects[senderId]->getLastCycleUpdatedBySense() != world->getTime())
					{
						((Platoon*) world->objects[senderId])->setBuriedness(30);
						((Platoon*) world->objects[senderId])->setLastCycleUpdated(world->getTime());
					}
					finished = true;

					break;
				}
				case LCP_BUILDING_DATA:
				{
					int onFireIndex = msg.pop_bits(BUILDING_INDEX).getA();

					if (world->buildings[onFireIndex]->getLastCycleUpdatedBySense() != world->getTime())
					{
						world->buildings[onFireIndex]->setFieryness(2);
						world->buildings[onFireIndex]->setTemperature(DEFAULT_BUILDING_TEMPERATURE);
					}

					sizeOfContent -= BUILDING_INDEX;
					finished = true;
					break;
				}
				default:
				{
					LOG(Main , 1) << "wow many ghost wofe wofe who sent this much scare!" << endl;
					break;
				}
			}
		}
	}
	catch (exception& e)
	{
		cout << "Exception in low comm analyze message " << e.what() << endl;
	}
	catch (...)
	{
		cout << "Unknown exception in low comm analyze message" << endl;
	}
}

bool LowBWCommunicationRadar::doIHaveUsefulDataForSendInRadar()
{
	int maxDistanceForNears = 10 * AGENT_RADIUS;

	vector<Platoon *> nearPlatoons;
	for (int i = 0; i < (int) world->platoons.size(); i++)
	{
		if (world->platoons[i]->isAvailable == false)
		{
			continue;
		}

		if (world->platoons[i]->getHp() == 0)
		{
			continue;
		}

		if (world->platoons[i]->getLastCycleUpdatedBySense() != world->getTime())
		{
			continue;
		}

		if (world->platoons[i]->getPos().distFromPoint(self()->getPos()) < maxDistanceForNears)
		{
			nearPlatoons.push_back(world->platoons[i]);
		}
		else if (self()->getMotionlessObject()->isBuilding() && world->platoons[i]->getMotionlessObject()->getId() == self()->getMotionlessObject()->getId())
		{
			nearPlatoons.push_back(world->platoons[i]);
		}
	}

	if (nearPlatoons.size() <= 1)
	{
		return true;
	}

	sort(nearPlatoons.begin(), nearPlatoons.end());
	if (self()->getId() == nearPlatoons[0]->getId())
	{
		return true;
	}

	return false;
}
