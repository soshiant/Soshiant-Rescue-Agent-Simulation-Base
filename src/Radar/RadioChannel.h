#ifndef _CHANNEL_H
#define	_CHANNEL_H
#include <iostream>
#include <string>
#include "../Utilities/Types.h"
#include "AbstractChannel.h"

class RadioChannel: public AbstractChannel
{
public:
	RadioChannel();
	RadioChannel(std::string type, int channelNum, int bandWidth);
	virtual ~RadioChannel();
	int getBandWidth() const;
	Types::ChannelJob getChannelJob() const;
	void setChannelJob(Types::ChannelJob cj);
private:
	int bandWidth;
	Types::ChannelJob channelJob;
};

#endif	/* _CHANNEL_H */

