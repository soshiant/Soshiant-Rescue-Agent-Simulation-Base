/*
 * MediumBWCommunicationRadar.h
 *
 *  Created on: Nov 26, 2013
 *      Author: ali
 */

#ifndef MEDIUMBWCOMMUNICATIONRADAR_H_
#define MEDIUMBWCOMMUNICATIONRADAR_H_

#include "AbstractCommunicationradar.h"

class MediumBWCommunicationRadar: public AbstractCommunicationRadar
{
public:
	MediumBWCommunicationRadar(WorldModel *wm, WorldGraph *wg, Command *command);

	void subscribe();
	virtual void init();
	virtual void analyzeMessage(vector<byte>& radarMsg, int& offset, int sizeOfContent, int senderId, int channel);
	virtual void shareSenseWorldAndSendOrders();
	virtual void shareSenseWorldAfterDecision();

private:
	int myMinimalSize;
	int maxEdgeForSend;

	void removeLastCycleSendedData();
	virtual void setMyChanel();
	virtual void setSizes();

	void sendBuriednessProperty(BoolStream &msg, int& size);
	void sendMyPositionProperty(BoolStream &msg, int& size);
	void sendBuildings(BoolStream &msg, int& size);
	void sendCivilians(BoolStream &msg, int& size);
	void sendHasWaterIsCarry(BoolStream &msg, int& size);
	void sendRoads(BoolStream &msg, int& size);
	void sendEdges(BoolStream &msg, int& size);
	void sendAmbulanceCommands(BoolStream &msg, int & size);

	bool isValidEdge(Edge* edge);
	bool isValidRoad(Road* road);
	bool isFertCivilian(Civilian * civilian);
	bool isValidBuilding(Building* building);
	bool isValidCivilian(Civilian* Civilian);
	bool isClear(Road* road);
	bool isFullBlocked(Road* road);
	bool doIHaveUsefulDataForSendInRadar();

	virtual ~MediumBWCommunicationRadar();
};

#endif /* MEDIUMBWCOMMUNICATIONRADAR_H_ */
