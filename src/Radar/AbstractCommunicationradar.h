#ifndef ABSTRACTCOMMUNICATIONRADAR_H
#define ABSTRACTCOMMUNICATIONRADAR_H

#include <algorithm>
#include <iostream>
#include <exception>
#include <iterator>
#include <vector>
#include <cmath>

#include "../Decision/Command.h"
#include "../Utilities/Types.h"
#include "../WorldModel/WorldModel.h"
#include "CommunicationlessRadar.h"
#include "RadioChannel.h"
#include "SendedData.h"
#include "VoiceChannel.h"
#include "../Utilities/FireEstimator/EstimatedData.h"

class CommunicationlessRadar;
class WorldGraph;
class Command;
class SendedData;

#define temperatureCoef 8
#define timeToDeathCoef 4

#define angleCoef 3
#define distanceCoef 400

using namespace Types;
using namespace std;

class AbstractCommunicationRadar
{
public:
	AbstractCommunicationRadar(WorldModel * wm, WorldGraph * wg, Command * command);
	virtual void init();
	virtual ~AbstractCommunicationRadar();
	bool static channelComparator(RadioChannel* c1, RadioChannel* c2);
	bool static buildingComparator(Building* b1, Building* b2);
	void subscribe();
	virtual void shareSenseWorldAndSendOrders();
	virtual void shareSenseWorldAfterDecision();
	virtual void analyzeMessage(std::vector<byte>& msg, int& offset, int sizeOfContent, int senderId, int channel);

	RadarMode getRadarMode();
	void setRadarMode(RadarMode mode);
protected:

	WorldModel * world;
	WorldGraph * worldGraph;
	Command * command;
	SendedData * sendedData;

	Platoon * self();

	vector<RadioChannel *> radioChannels;
	vector<RadioChannel *> listeningChanels;
	vector<VoiceChannel *> voiceChannels;

	RadarMode radarMode;
	RadioChannel * myChanel;

	CommunicationlessRadar* clRadar;

	int numOfSubscribeForPlatoons;
	int numOfSubscribeForCenters;

	virtual void setSizes();
	virtual void setMyChanel();

	//Sizes
	int sizeOfMessage;
	int INDEX_SIZE;
	int BURIEDNESS;
	int TIME_TO_DEATH;
	int CIVILIAN_ANGLE;
	int ONE_BIT_SIZE;
	int PLATOON_INDEX;
	int FIERYNESS;
	int EXTINGUISHED_FIERYNESS;
	int TEMPERATURE;
	int ID_SIZE;
	int BUILDING_INDEX;
	int NOT_RELATED_EDGE_INDEX;
	int ROAD_INDEX;
	int NODE_INDEX;
	int CIVILIAN_IN_BUILDING_INDEX;
	int MOTIONLESS_INDEX;
	int AMBULANCE_INDEX;
	int TOTOAL_POSITION;
	//--------------------------------------//
	int maxWaterForExtinguish;

	//Model Updating
	void setPosition(Human * human, int node, int isStucked, int side);
};

#endif // ABSTRACTCOMMUNICATIONRADAR_H
