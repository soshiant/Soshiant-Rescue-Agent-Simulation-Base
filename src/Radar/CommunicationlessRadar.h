/*
 * CommunicationlessRadar.h
 *
 *  Created on: Feb 20, 2012
 *      Author: alimohammad
 *      Modifier: pedram
 */

#ifndef COMMUNICATIONLESSRADAR_H_
#define COMMUNICATIONLESSRADAR_H_

#include "../WorldModel/WorldModel.h"
#include "../WorldGraph/WorldGraph.h"
#include "VoiceChannel.h"
#include "RadioChannel.h"
#include "../Decision/Command.h"
#include "../Utilities/BoolStream.h"
#include <vector>

class Command;

class CommunicationlessRadar
{

public:

	CommunicationlessRadar(WorldModel * wm, WorldGraph * wg, Command * command);
	void shareDataOnCommunicationlessModel();
	void analyzeMessage(BoolStream & msg, int sender, int sizeOfContent, int channel);
	void init();

private:
	WorldModel * world;
	WorldGraph * worldGraph;
	Command * command;

	//Sizes
	int INDEX_SIZE;
	int BURIEDNESS_SIZE;
	int TIME_TO_DEATH_SIZE;
	int ONE_BIT_SIZE;
	int FIERYNESS_SIZE;
	int ID_SIZE;
	int CIVILIAN_ANGLE;
	int BUILDING_INDEX_SIZE;
	int ROAD_INDEX_SIZE;
	int NODE_INDEX_SIZE;
	int MOTIONLESS_INDEX;
	int CIVILIAN_IN_BUILDING_INDEX;
	int TOTOAL_POSITION_SIZE;
	int TOTAL_CIVILIAN_SIZE;
	int ABNORMAL_CIVILIAN_SIZE;
	int TIME_SIZE;
	int BUILDING_TIME_SIZE;
	int TEMPERATURE_SIZE;
	int PLATOON_INDEX_SIZE;

	void setSizes();
	vector<VoiceChannel *> voiceChannels;
	VoiceChannel * myVoiceChanel;

	void setBuildingData(BoolStream &msg, int &size, bool sethasfire);
	void setCivilianData(BoolStream &msg, int &size);
	void setAgentsData(BoolStream &msg, int &size);
	void setRoadData(BoolStream &msg, int &size);
	void setEnd(BoolStream &msg, int &size);

	void setPosition(Human * human, int node, int isStucked, int side);

	Platoon * self();

	int sizeOfMessage;
};

#endif /* COMMUNICATIONLESSRADAR_H_ */
