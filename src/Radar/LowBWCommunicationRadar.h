/*
 * LowBWCommunicationRadar.h
 *
 *  Created on: Jan 6, 2014
 *      Author: pedram
 */

#ifndef LOWBWCOMMUNICATIONRADAR_H_
#define LOWBWCOMMUNICATIONRADAR_H_

#include "AbstractCommunicationradar.h"

#define DEFAULT_BUILDING_TEMPERATURE 80

class LowBWCommunicationRadar: public AbstractCommunicationRadar
{
public:
	LowBWCommunicationRadar(WorldModel * wm, WorldGraph * wg, Command * command);
	void subscribe();
	virtual void init();
	virtual void analyzeMessage(vector<byte>& radarMsg, int& offset, int sizeOfContent, int senderId, int channel);
	virtual void shareSenseWorldAndSendOrders();
	virtual void shareSenseWorldAfterDecision();
	virtual ~LowBWCommunicationRadar();
private:
	virtual void setMyChanel();
	virtual void setSizes();

	void sendBuriedness(BoolStream &msg, int& size);
	void sendBuildings(BoolStream &msg, int& size);

	bool doIHaveUsefulDataForSendInRadar();
};

#endif /* LOWBWCOMMUNICATIONRADAR_H_ */
