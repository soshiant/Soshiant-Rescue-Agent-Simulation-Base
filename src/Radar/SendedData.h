#ifndef SENDEDDATA_H
#define SENDEDDATA_H
#include "../WorldGraph/WorldGraph.h"
#include "../WorldModel/WorldModel.h"

class WorldGraph;

class SendedData
{
public:
	SendedData(WorldModel * wm, WorldGraph * wg);

	vector<Building *> sendedBuildingsInFire;
	vector<Building *> sendedBuildingsExtinguished;
	vector<Building *> sendedBuildingsBurned;
	vector<Civilian *> sendedAvailableCivilians;
	vector<Civilian *> sendedFertCivilians;
	vector<Road *> sendedRoads;
	vector<Edge *> sendedEdges;

	void clear();
	bool sendThisBurnedBuilding(Building * building);
	bool sendThisBuildingInFire(Building * building);
	bool sendThisExtinguishedBuilding(Building * building);
	bool sendThisAvailableCivilian(Civilian * civ);
	bool sendThisFertCivilians(Civilian * civ);
	bool sendThisRoad(Road * road);
	bool sendThisEdge(Edge * road);

	void removeThisBuildingFromExtinguishedBuildings(Building * building);
	void removeThisBuildingFromBuildingsInFire(Building * building);
	void removeAvailableThisCivilianFromSendedCivilians(Civilian * civ);
	void removeThisFertCivilianFromSendedCivilians(Civilian * civ);
	void removeThisRoadFromSendedRoads(Road * road);

	bool amISendMyLastCycleData;

private:
	WorldModel * world;
	WorldGraph * worldGraph;

};

#endif // SENDEDDATA_H
