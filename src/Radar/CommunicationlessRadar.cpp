/*
 * CommunicationlessRadar.cpp
 *
 *  Created on: Feb 20, 2012
 *      Author: alimohammad
 */

#include "CommunicationlessRadar.h"

#define LOGLEVEL 0
#define MAX_BUILDING_FOR_SEND 15
#define MAX_CIVILIAN_FOR_SEND 15

using namespace Types;
using namespace Geometry;
using namespace std;

bool voiceChanelSizeComparator(VoiceChannel * a, VoiceChannel * b)
{
	return a->getMaxSize() > b->getMaxSize();
}

bool dataLastCycleUpdatedComparator(RCRObject * a, RCRObject * b)
{
	return a->getLastCycleUpdated() > b->getLastCycleUpdated();
}

CommunicationlessRadar::CommunicationlessRadar(WorldModel * wm, WorldGraph * wg, Command * c)
{
	this->world = wm;
	this->worldGraph = wg;
	this->command = c;
	this->myVoiceChanel = nullptr;
}

void CommunicationlessRadar::analyzeMessage(BoolStream & msg, int senderId, int sizeOfContent, int channel)
{
	if (world->selfID == senderId)
	{
		return;
	}

	if (world->objects[senderId] == nullptr)
	{
		int range = 0;
		for (VoiceChannel* voice : voiceChannels)
		{
			if (voice->getChannelNum() == channel)
			{
				range = voice->getRange();
				break;
			}
		}
		if (world->self->isPlatoon())
		{
			for (Building* b : world->buildings)
			{
				if (b->getPos().distFromPoint(self()->getPos()) <= range)
				{
					b->setCivilianProbability(b->getCivilianProbability() + 1);
				}
			}
		}

		return;
	}

	if (world->objects[senderId]->isCivilian())
	{
		return;
	}

	Platoon * sender = ((Platoon *) world->objects[senderId]);
	sender->setLastCycleUpdated(world->getTime());
	sender->isAvailable = true;

	LOG(Main , 1) << "####################################################" << endl;
	LOG(Main , LOGLEVEL) << "Sender ID: " << senderId << endl;
	LOG(Main , 1) << msg.getVal(" - ") << endl;

	while (sizeOfContent > 2)
	{
		int header = msg.pop_bits(INDEX_SIZE).getA();
		sizeOfContent = sizeOfContent - INDEX_SIZE;

		LOG(Main , 1) << "Header: " << header << " SizeOfContent: " << sizeOfContent << endl;

		switch (header)
		{
		case NCP_AGENT_DATA:
		{
			int platoonIndex = msg.pop_bits(PLATOON_INDEX_SIZE).getA();
			int buriedness = msg.pop_bits(BURIEDNESS_SIZE).getA();
			int time = msg.pop_bits(TIME_SIZE).getA() + 1;

			LOG(Main, 1) << "Agent : " << world->platoons[platoonIndex]->getId() << " " << buriedness << " " << time << endl;

			Platoon * thisPlatoon = world->platoons[platoonIndex];

			if (thisPlatoon->getLastCycleUpdatedBySense() != world->getTime() && thisPlatoon->getLastCycleUpdated() < world->getTime() - time)
			{
				thisPlatoon->setBuriedness(buriedness);
				thisPlatoon->setLastCycleUpdated(world->getTime() - time);
			}

			sizeOfContent = sizeOfContent - (PLATOON_INDEX_SIZE + BURIEDNESS_SIZE + TIME_SIZE);

			LOG(Main , 1) << thisPlatoon->getId() << " buriedness updated: " << buriedness << endl;

			break;
		}
		case NCP_BUILDING_DATA:
		{
			int buildingIndex = msg.pop_bits(BUILDING_INDEX_SIZE).getA();
			if (buildingIndex == (int)world->buildings.size()) //END!!!
			{
				LOG(Main , 1) << "End of Story! :'(" << endl;
				return;
			}

			int fieryness = msg.pop_bits(FIERYNESS_SIZE).getA() + 1;
			int time = msg.pop_bits(BUILDING_TIME_SIZE).getA() + 1;

			LOG(Main, 1) << "building index : " << buildingIndex << endl;

			Building * thisBuilding = world->buildings[buildingIndex];

			if (thisBuilding->getLastCycleUpdatedBySense() != world->getTime() && thisBuilding->getLastCycleUpdated() < world->getTime() - time)
			{
				if (fieryness < 4)
				{
					thisBuilding->setLastCycleInFire(max(world->getTime() - time, thisBuilding->getLastCycleInFire()));
				}

				thisBuilding->setFieryness(fieryness);
				thisBuilding->setLastCycleUpdated(world->getTime() - time);
			}

			LOG(Main, 1) << "size of content before subtraction: " << sizeOfContent << endl;

			sizeOfContent = sizeOfContent - (BUILDING_INDEX_SIZE + BUILDING_TIME_SIZE + FIERYNESS_SIZE);

			LOG(Main, 1) << "size of content after subtraction: " << sizeOfContent << endl;

			LOG(Main , 1) << "Building: " << world->buildings[buildingIndex]->getId() << " fieryness: " << fieryness << " update: " << world->buildings[buildingIndex]->getLastCycleUpdated() << endl;

			break;
		}
		case NCP_CIVILIAN_DATA:
		{
			bool isAvailable = msg.pop_bool();
			sizeOfContent -= ONE_BIT_SIZE;

			if (isAvailable)
			{
				int pos = msg.pop_bits(NODE_INDEX_SIZE).getA();
				int isStucked = msg.pop_bits(ONE_BIT_SIZE).getA();
				int side = msg.pop_bits(ONE_BIT_SIZE).getA();

				int motionless = worldGraph->getNodes()[pos]->getMotionlessIndex();

				int angle = msg.pop_bits(CIVILIAN_ANGLE).getA();
				int distance = msg.pop_bits(ceil(log2l(world->motionlessObjects[motionless]->getMaxDistanceFromCenter() / distanceCoef))).getA();
				LOG(Main, 1) << "Distance size: " << ceil(log2l(world->motionlessObjects[motionless]->getMaxDistanceFromCenter() / distanceCoef)) << endl;
				LOG(Main, 1) << "angle is : " << angle << endl;
				LOG(Main, 1) << "distance is : " << distance << endl;

				int ttd = msg.pop_bits(TIME_TO_DEATH_SIZE).getA() * timeToDeathCoef - 1;
				int buriedness = msg.pop_bits(BURIEDNESS_SIZE).getA();
				int updatedTime = world->getTime() - (msg.pop_bits(TIME_SIZE).getA() + 1);

				Civilian* civ = new Civilian(motionless, angle, distance, ceil(log2l(world->motionlessObjects[motionless]->getMaxDistanceFromCenter() / distanceCoef)));
				int fakeId = civ->getFakeId();

				Segment seg(world->motionlessObjects[motionless]->getPos(), angle * angleCoef, distance * distanceCoef);
				civ->setX(seg.getSecondPoint().getX());
				civ->setY(seg.getSecondPoint().getY());

				civ->setRealX(seg.getSecondPoint().getX());
				civ->setRealY(seg.getSecondPoint().getY());

				LOG(Main, 1) << "Civ fake id: " << fakeId << endl;
				LOG(Main, 1) << "Pos: " << world->motionlessObjects[motionless]->getId() << endl;
				LOG(Main, 1) << "Pos Index: " << motionless << endl;
				LOG(Main, 1) << "side: " << side << endl;
				LOG(Main, 1) << "Is stucked!	" << isStucked << endl;
				LOG(Main, 1) << "Time To Death: " << ttd << endl;
				LOG(Main, 1) << "Buriedness : " << buriedness << endl;
				LOG(Main, 1) << "Update time: " << updatedTime << endl;

				if (world->fakeCivilians[fakeId])
				{
					LOG(Main , 1) << "update time: " << world->fakeCivilians[fakeId]->getLastCycleUpdatedBySense() << endl;
				}
				if (world->fakeCivilians[fakeId] == nullptr)
				{
					LOG(Main , 1) << "Be jane madaram nadidam!! " << endl;

					setPosition(civ, pos, isStucked, side);
					civ->setBuriedness(buriedness);
					civ->setTimeToDeath(ttd);

					civ->humanIndex = world->humans.size();
					civ->civilianIndex = world->civilians.size();
					civ->isAvailable = true;
//					civ->setLastCycleSentInRadar(world->getTime() - 1);
					civ->setLastCycleUpdated(updatedTime);
					world->fakeCivilians[fakeId] = civ;
					world->civilians.push_back(civ);
					world->humans.push_back(civ);
				}
				else
				{
					delete civ;

					if (world->fakeCivilians[fakeId]->getLastCycleUpdated() < updatedTime)
					{
						setPosition(world->fakeCivilians[fakeId], pos, isStucked, side);
						world->fakeCivilians[fakeId]->setBuriedness(buriedness);
						world->fakeCivilians[fakeId]->setTimeToDeath(ttd);

						world->fakeCivilians[fakeId]->isAvailable = true;
//						world->fakeCivilians[fakeId]->setLastCycleSentInRadar(world->getTime() - 1);
						world->fakeCivilians[fakeId]->setLastCycleUpdated(updatedTime);
					}
				}

				sizeOfContent -= (TOTOAL_POSITION_SIZE + CIVILIAN_ANGLE + TIME_SIZE + BURIEDNESS_SIZE + TIME_TO_DEATH_SIZE + ceil(log2l(world->motionlessObjects[motionless]->getMaxDistanceFromCenter())));
			}
			else
			{
				int motionlessIndex = msg.pop_bits(MOTIONLESS_INDEX).getA();
				int angle = msg.pop_bits(CIVILIAN_ANGLE).getA();
				int distance = msg.pop_bits(ceil(log2l(world->motionlessObjects[motionlessIndex]->getMaxDistanceFromCenter() / distanceCoef))).getA();
				Civilian* civ = new Civilian(motionlessIndex, angle, distance, ceil(log2l(world->motionlessObjects[motionlessIndex]->getMaxDistanceFromCenter() / distanceCoef)));
				int fakeId = civ->getFakeId();

				delete civ;

				LOG(Main, 1) << "Not available civilian fake id: " << fakeId << endl;
				LOG(Main, 1) << "motionless Index: " << motionlessIndex << endl;
				LOG(Main, 1) << "angle: " << angle << endl;
				LOG(Main, 1) << "distance: " << distance << endl;

				if (world->fakeCivilians[fakeId] != nullptr)
				{
					world->fakeCivilians[fakeId]->isAvailable = false;
				}

				sizeOfContent -= (MOTIONLESS_INDEX + CIVILIAN_ANGLE + ceil(log2l(world->motionlessObjects[motionlessIndex]->getMaxDistanceFromCenter() / distanceCoef)));
			}

			break;
		}
		case NCP_ROAD_DATA:
		{
			int roadIndex = msg.pop_bits(ROAD_INDEX_SIZE).getA();
			Road * road = world->roads[roadIndex];
			int time = msg.pop_bits(TIME_SIZE).getA() + 1;

			LOG(Main, 4) << "road index: " << roadIndex << endl;
			LOG(Main , 4) << "Road " << road->getId() << " recieved data!!" << endl;

			if (road->getLastCycleUpdatedBySense() != world->getTime() && road->getLastCycleUpdated() < world->getTime() - time)
			{
				LOG(Main , 4) << "Road " << road->getId() << " is going to update!" << endl;

				for (int i = 0; i < (int) road->getRelativeEdgesIndexes().size(); i++)
				{
					bool passingMode = msg.pop_bool();
					worldGraph->getEdges()[road->getRelativeEdgesIndexes()[i]]->setPassingMode(passingMode ? PM_PASSABLE : PM_NOT_PASSABLE);
				}
			}
			else
			{
				for (int i = 0; i < (int) road->getRelativeEdgesIndexes().size(); i++)
				{
					msg.pop_bool();
				}
			}

			road->setLastCycleUpdated(world->getTime() - time);
			sizeOfContent = sizeOfContent - (ROAD_INDEX_SIZE + (int) road->getRelativeEdgesIndexes().size() + TIME_SIZE);

			LOG(Main , 4) << "road: " << road->getId() << " updated!" << endl;

			break;
		}
		default:
		{
			LOG(Main , 4) << "Shit!" << endl;
			break;
		}
		}
	}
}

void CommunicationlessRadar::shareDataOnCommunicationlessModel()
{
	if (myVoiceChanel == nullptr)
	{
		return;
	}

	LOG(Main , 1) << "Share No commi!" << endl;

	BoolStream msg(1000);
	sizeOfMessage = 0;
	int mySize = myVoiceChanel->getMaxSize() * 8;

	if (mySize >= BUILDING_INDEX_SIZE) //For end packet
	{
		sizeOfMessage += (BUILDING_INDEX_SIZE + INDEX_SIZE);
		mySize -= (BUILDING_INDEX_SIZE + INDEX_SIZE);
	}
	else
	{
		LOG(Main, 1) << "Seriously?! Radar Bandwidth is shit" << endl;
		return;
	}

	LOG(Main , 1) << "mySize: " << mySize << endl;

	setAgentsData(msg, mySize);
	setBuildingData(msg, mySize, true);
	setCivilianData(msg, mySize);
	setBuildingData(msg, mySize, false);
	setRoadData(msg, mySize);
	setEnd(msg, mySize);

	sizeOfMessage = ceil(sizeOfMessage / 8.);

	LOG(Main , 1) << "sizeOfMessage:  " << sizeOfMessage << endl;
	LOG(Main , 1) << "mySize:  " << mySize << endl;
	LOG(Main , 1) << "Message: " << msg.getVal(" - ") << endl;
	msg.setSize(sizeOfMessage);
	command->speak(myVoiceChanel->getChannelNum(), msg.getValue());
}

void CommunicationlessRadar::setRoadData(BoolStream & msg, int &size)
{
	vector<Road *> roads;

	for (int i = 0; i < (int) world->roads.size(); i++)
	{
		if (world->roads[i]->getLastCycleUpdated() != 0 && world->getTime() - world->roads[i]->getLastCycleUpdated() < 30)
		{
			bool isValid = true;
			for (int edgeIndex : world->roads[i]->getRelativeEdgesIndexes())
			{
				if (worldGraph->getEdges()[edgeIndex]->getPassingMode() == PM_UNKNOWN)
				{
					isValid = false;
					break;
				}
			}

			if (isValid)
			{
				roads.push_back(world->roads[i]);
				LOG(Main , 4) << world->roads[i]->getId() << " is going to send!" << endl;
			}
		}
	}

	if (roads.empty())
	{
		return;
	}

	sort(roads.begin(), roads.end(), dataLastCycleUpdatedComparator);

	for (int i = 0; i < (int) roads.size(); i++)
	{
		if (size > INDEX_SIZE + ROAD_INDEX_SIZE + TIME_SIZE + (int) roads[i]->getRelativeEdgesIndexes().size())
		{
			LOG(Main , 4) << "ROad: " << roads[i]->getId() << endl;
			int deltaTime = world->getTime() - roads[i]->getLastCycleUpdated();

			msg.push_back(bits(NCP_ROAD_DATA, INDEX_SIZE));
			msg.push_back(bits(roads[i]->roadIndex, ROAD_INDEX_SIZE));
			msg.push_back(bits(deltaTime, TIME_SIZE));

			for (int j = 0; j < (int) roads[i]->getRelativeEdgesIndexes().size(); j++)
			{
				msg.push_back(isPassableEdge(worldGraph->getEdges()[roads[i]->getRelativeEdgesIndexes()[j]], true));
				LOG (Main , 4) << isPassableEdge(worldGraph->getEdges()[roads[i]->getRelativeEdgesIndexes()[j]], true) << "";
			}
			LOG(Main, 4) << endl;

			size -= (INDEX_SIZE + ROAD_INDEX_SIZE + TIME_SIZE + roads[i]->getRelativeEdgesIndexes().size());
			sizeOfMessage += (INDEX_SIZE + ROAD_INDEX_SIZE + TIME_SIZE + roads[i]->getRelativeEdgesIndexes().size());
		}
		else
		{
			break;
		}
	}
}

void CommunicationlessRadar::setAgentsData(BoolStream &msg, int &size)
{
	int maxPlatoonsForSend = 10;

	if (world->self->isPlatoon())
	{
		self()->setLastCycleUpdated(world->getTime());
	}

	vector<Platoon *> sendingPlatoons;

	for (int i = 0; i < (int) world->platoons.size(); i++)
	{
		bool valid = false;
		for (u_int j = 0; j < world->platoons[i]->getBuriednessHistory().size(); j++)
		{
			if (world->getTime() - world->platoons[i]->getLastCycleUpdated() > 30)
			{
				continue;
			}

			if (world->platoons[i]->getBuriednessHistory()[j].buriedness != 0)
			{
				valid = true;
				break;
			}
		}

		if (valid)
		{
			sendingPlatoons.push_back(world->platoons[i]);
		}
	}

	sort(sendingPlatoons.begin(), sendingPlatoons.end(), dataLastCycleUpdatedComparator);

	sendingPlatoons.resize(min(maxPlatoonsForSend, (int) sendingPlatoons.size()));

	for (int i = 0; i < (int) sendingPlatoons.size(); i++)
	{
		if (size > INDEX_SIZE + BURIEDNESS_SIZE + PLATOON_INDEX_SIZE + TIME_SIZE)
		{
			msg.push_back(bits(NCP_AGENT_DATA, INDEX_SIZE));
			msg.push_back(bits(sendingPlatoons[i]->platoonIndex, PLATOON_INDEX_SIZE));
			msg.push_back(bits(sendingPlatoons[i]->getBuriedness() > 127 ? 127 : sendingPlatoons[i]->getBuriedness(), BURIEDNESS_SIZE));
			msg.push_back(bits(world->getTime() - sendingPlatoons[i]->getLastCycleUpdated(), TIME_SIZE));

			LOG(Main, 1) << sendingPlatoons[i]->getId() << "sended, buriedness : " << sendingPlatoons[i]->getBuriedness() << endl;

			size -= (INDEX_SIZE + BURIEDNESS_SIZE + PLATOON_INDEX_SIZE + TIME_SIZE);
			sizeOfMessage += (INDEX_SIZE + BURIEDNESS_SIZE + PLATOON_INDEX_SIZE + TIME_SIZE);
		}
	}

}

void CommunicationlessRadar::setCivilianData(BoolStream &msg, int &size)
{
	LOG(Main, 1) << "Sending civilian data " << size << endl;

	vector<Civilian *> civiliansForSend;
	for (int i = 0; i < (int) world->civilians.size(); i++)
	{
//		if (world->civilians[i]->getMotionlessObject()->isRefuge())
//		{
//			continue;
//		}
//
		if (world->civilians[i]->getBuriedness() == 0)
		{
			if (!(world->civilians[i]->getMotionlessObject()->isBuilding() && ((Building*) world->civilians[i]->getMotionlessObject())->hasFire()))
			{
				if (world->civilians[i]->getPosition() != self()->getId())
				{
					continue;
				}
			}
		}

		if (world->civilians[i]->isAvailable && world->civilians[i]->getTimeToDeath() < 40)
		{
			continue;
		}

		if (world->getTime() - world->civilians[i]->getLastCycleUpdated() < 30)
		{
			civiliansForSend.push_back(world->civilians[i]);
		}
	}

	sort(civiliansForSend.begin(), civiliansForSend.end(), dataLastCycleUpdatedComparator);

	civiliansForSend.resize(min(MAX_CIVILIAN_FOR_SEND, (int) civiliansForSend.size()));
	for (Civilian* civ : civiliansForSend)
	{
		if (civ->isAvailable && size > TOTAL_CIVILIAN_SIZE)
		{
			msg.push_back(bits(NCP_CIVILIAN_DATA, INDEX_SIZE));
			msg.push_back(bits(civ->isAvailable, ONE_BIT_SIZE));

			if (civ->getFirstTimeMotionless() != civ->getMotionlessObject()) //XXX TOF
			{
				LOG(Main, 1) << "What the fuck?! Jash ba avalesh yeki nist" << endl;
				civ->setFirstTimeMotionless(civ->getMotionlessObject());
			}

			if (civ->getRepresentiveNodeIndex() == -1)
			{
				msg.push_back(bits(civ->getWithoutBlockadeRepresentiveNodeIndex(), NODE_INDEX_SIZE));
				msg.push_back(bits(1, ONE_BIT_SIZE));
				int motionless = worldGraph->getNodes()[civ->getWithoutBlockadeRepresentiveNodeIndex()]->getMotionlessIndex();
				msg.push_back(bits(motionless == civ->getMotionlessObject()->motionlessIndex, ONE_BIT_SIZE));

				LOG(Main, 1) << "isStuck: " << 1 << endl;
				LOG(Main, 1) << "Side: " << (motionless == civ->getMotionlessObject()->motionlessIndex) << endl;
			}
			else
			{
				msg.push_back(bits(civ->getRepresentiveNodeIndex(), NODE_INDEX_SIZE));
				msg.push_back(bits(0, ONE_BIT_SIZE));
				int motionless = worldGraph->getNodes()[civ->getRepresentiveNodeIndex()]->getMotionlessIndex();
				msg.push_back(bits(motionless == civ->getMotionlessObject()->motionlessIndex, ONE_BIT_SIZE));

				LOG(Main, 1) << "isStuck: " << 0 << endl;
				LOG(Main, 1) << "Side: " << (motionless == civ->getMotionlessObject()->motionlessIndex) << endl;
			}

			msg.push_back(bits(civ->getAngle(), CIVILIAN_ANGLE));
			msg.push_back(bits(civ->getDistanceToCenter(), civ->getDistanceToCenterSize()));
			LOG(Main, 1) << "Distance to center size: " << civ->getDistanceToCenterSize();
			LOG(Main, 1) << "MOTIONLESSES " << civ->getFirstTimeMotionless()->getId() << " " << civ->getMotionlessObject()->getId() << endl;

			int timeToDeath = round(civ->getTimeToDeath() / timeToDeathCoef);
			LOG(Main, 1) << "Sending Time To Death: " << round(civ->getTimeToDeath() / timeToDeathCoef) << endl;
			msg.push_back(bits(timeToDeath > pow(2, TIME_TO_DEATH_SIZE) - 1 ? pow(2, TIME_TO_DEATH_SIZE) - 1 : timeToDeath, TIME_TO_DEATH_SIZE));

			msg.push_back(bits(civ->getBuriedness() > 127 ? 127 : civ->getBuriedness(), BURIEDNESS_SIZE));
			msg.push_back(bits(world->getTime() - civ->getLastCycleUpdated(), TIME_SIZE));

			LOG(Main, 1) << "Available civilian sending: " << size << endl;
			LOG(Main, 1) << "Motionless Index: " << civ->getMotionlessObject()->motionlessIndex << endl;
			LOG(Main, 1) << "Motionless Id: " << civ->getMotionlessObject()->getId() << endl;
			LOG(Main, 1) << "Angle: " << civ->getAngle() << endl;
			LOG(Main, 1) << "Distance: " << civ->getDistanceToCenter() << endl;
			LOG(Main, 1) << "Time To Death: " << civ->getTimeToDeath() << endl;
			LOG(Main, 1) << "Buriedness: " << civ->getBuriedness() << endl;
			LOG(Main, 1) << "update time : " << world->getTime() - civ->getLastCycleUpdated() << endl;

			size -= (INDEX_SIZE + TOTOAL_POSITION_SIZE + TIME_TO_DEATH_SIZE + BURIEDNESS_SIZE + TIME_SIZE + ONE_BIT_SIZE + civ->getDistanceToCenterSize() + CIVILIAN_ANGLE);
			sizeOfMessage += (INDEX_SIZE + TOTOAL_POSITION_SIZE + TIME_TO_DEATH_SIZE + BURIEDNESS_SIZE + TIME_SIZE + ONE_BIT_SIZE + civ->getDistanceToCenterSize() + CIVILIAN_ANGLE);
		}
		else if (!civ->isAvailable && size > ABNORMAL_CIVILIAN_SIZE)
		{
			msg.push_back(bits(NCP_CIVILIAN_DATA, INDEX_SIZE));
			msg.push_back(bits(civ->isAvailable, ONE_BIT_SIZE));
			msg.push_back(bits(civ->getFirstTimeMotionless()->motionlessIndex, MOTIONLESS_INDEX));
			msg.push_back(bits(civ->getAngle(), CIVILIAN_ANGLE));
			msg.push_back(bits(civ->getDistanceToCenter(), civ->getDistanceToCenterSize()));

			LOG(Main, 1) << civ->getDistanceToCenterSize() << endl;
			LOG(Main, 1) << "Not available civilian sending: " << size << endl;
			LOG(Main, 1) << "Motionless Index: " << civ->getFirstTimeMotionless()->motionlessIndex << endl;
			LOG(Main, 1) << "Angle: " << civ->getAngle() << endl;
			LOG(Main, 1) << "Distance To Center: " << civ->getDistanceToCenter() << endl;

			size -= (INDEX_SIZE + ONE_BIT_SIZE + MOTIONLESS_INDEX + CIVILIAN_ANGLE + TIME_SIZE + civ->getDistanceToCenterSize());
			sizeOfMessage += (INDEX_SIZE + ONE_BIT_SIZE + MOTIONLESS_INDEX + CIVILIAN_ANGLE + TIME_SIZE + civ->getDistanceToCenterSize());
		}
		else
		{
			break;
		}
	}
}

void CommunicationlessRadar::setBuildingData(BoolStream & msg, int &size, bool sethasfire)
{
	vector<Building *> buildingForSend;

	LOG(Main , 1) << "Sending building datas " << size << endl;

	for (int i = 0; i < (int) world->buildings.size(); i++)
	{
		if (world->buildings[i]->getLastCycleUpdated() != 0 && world->getTime() - world->buildings[i]->getLastCycleUpdated() < 60)
		{
			if (world->buildings[i]->hasFire() && sethasfire)
			{
				buildingForSend.push_back(world->buildings[i]);
			}
			else if (isBetween(world->buildings[i]->getFieryness(), 4, 8) && sethasfire == false)
			{
				buildingForSend.push_back(world->buildings[i]);
			}
		}
	}

	if (buildingForSend.empty())
	{
		return;
	}

	sort(buildingForSend.begin(), buildingForSend.end(), dataLastCycleUpdatedComparator);

	int maxBuidlingForSend = min(MAX_BUILDING_FOR_SEND, (int) buildingForSend.size());

	buildingForSend.resize(maxBuidlingForSend);

	for (int i = 0; i < (int) buildingForSend.size(); i++)
	{
		if (size > INDEX_SIZE + BUILDING_INDEX_SIZE + ONE_BIT_SIZE + BUILDING_TIME_SIZE)
		{
			int deltaTime = world->getTime() - buildingForSend[i]->getLastCycleUpdated();

			LOG(Main , 1) << "building: " << buildingForSend[i]->getId() << " is going to send! hasfire: " << buildingForSend[i]->hasFire() << " update time: " << buildingForSend[i]->getLastCycleUpdated() << endl;

			msg.push_back(bits(NCP_BUILDING_DATA, INDEX_SIZE));
			msg.push_back(bits(buildingForSend[i]->buildingIndex, BUILDING_INDEX_SIZE));

			LOG(Main, 1) << "building for send index : " << buildingForSend[i]->buildingIndex << endl;

			msg.push_back(bits(buildingForSend[i]->getFieryness() - 1, FIERYNESS_SIZE));
			msg.push_back(bits(deltaTime, BUILDING_TIME_SIZE));

			size = size - (INDEX_SIZE + BUILDING_INDEX_SIZE + FIERYNESS_SIZE + BUILDING_TIME_SIZE);
			sizeOfMessage = sizeOfMessage + (INDEX_SIZE + BUILDING_INDEX_SIZE + FIERYNESS_SIZE + BUILDING_TIME_SIZE);
		}
		else
		{
			break;
		}
	}

	LOG(Main, 1) << "Send building datas finished " << size << endl;
}

void CommunicationlessRadar::setEnd(BoolStream &msg, int &size)
{
	msg.push_back(bits(NCP_BUILDING_DATA, INDEX_SIZE));

	msg.push_back(bits(world->buildings.size(), BUILDING_INDEX_SIZE));
}

void CommunicationlessRadar::setSizes()
{
	INDEX_SIZE = 2;

	LOG(Main , 1) << "hi! " << world->buildings.size() << endl;
	BUILDING_INDEX_SIZE = ceil(log2l(world->buildings.size()));

	LOG(Main , 1) << "hi! " << world->roads.size() << endl;
	ROAD_INDEX_SIZE = ceil(log2l(world->roads.size()));

	LOG(Main , 1) << "hi! " << worldGraph->getNodes().size() << endl;
	NODE_INDEX_SIZE = ceil(log2l(worldGraph->getNodes().size()));

	PLATOON_INDEX_SIZE = ceil(log2l(world->platoons.size()));

	TEMPERATURE_SIZE = ceil(log2l(2048 / temperatureCoef));

	ID_SIZE = ceil(log2l(MAX_INT));

	CIVILIAN_ANGLE = ceil(log2l(360 / angleCoef));

	MOTIONLESS_INDEX = ceil(log2l(world->motionlessObjects.size()));

	FIERYNESS_SIZE = 3;

	BURIEDNESS_SIZE = 7;
	TIME_TO_DEATH_SIZE = ceil(log2l(512 / timeToDeathCoef));

	ONE_BIT_SIZE = 1;

	TOTOAL_POSITION_SIZE = NODE_INDEX_SIZE + ONE_BIT_SIZE + ONE_BIT_SIZE;
	TIME_SIZE = 5;
	BUILDING_TIME_SIZE = 6;

	int maximumDistanceToCenter = -1;
	for (MotionlessObject* mo : world->motionlessObjects)
	{
		if (mo->getMaxDistanceFromCenter() > maximumDistanceToCenter)
		{
			maximumDistanceToCenter = mo->getMaxDistanceFromCenter();
		}
	}
	maximumDistanceToCenter = ceil(log2l(maximumDistanceToCenter / distanceCoef));

	TOTAL_CIVILIAN_SIZE = INDEX_SIZE + CIVILIAN_ANGLE + TIME_TO_DEATH_SIZE + BURIEDNESS_SIZE + TOTOAL_POSITION_SIZE + maximumDistanceToCenter + ONE_BIT_SIZE + TIME_SIZE;
	ABNORMAL_CIVILIAN_SIZE = INDEX_SIZE + MOTIONLESS_INDEX + CIVILIAN_ANGLE + maximumDistanceToCenter + ONE_BIT_SIZE;


	LOG(Main , 1) << "[Radar][Size] TOTAL_CIVILIAN_SIZE   " << TOTAL_CIVILIAN_SIZE << endl;
	LOG(Main , 1) << "[Radar][Size] ABNORMAL_CIVILIAN_SIZE   " << ABNORMAL_CIVILIAN_SIZE << endl;
	LOG(Main , 1) << "[Radar][Size] BUILDING_INDEX_SIZE   " << BUILDING_INDEX_SIZE << endl;
	LOG(Main , 1) << "[Radar][Size] ROAD_INDEX_SIZE   " << ROAD_INDEX_SIZE << endl;
	LOG(Main , 1) << "[Radar][Size] NODE_INDEX_SIZE   " << NODE_INDEX_SIZE << endl;
}

void CommunicationlessRadar::init()
{
	int numOfChannels = atoi(world->config[Encodings::getConfigType(CT_NUM_OF_CHANNELS)].c_str());
	myVoiceChanel = nullptr;

	for (int i = 0; i < numOfChannels; i++)
	{
		stringstream num;
		num << i;
		string typeKey = Encodings::getConfigType(CT_COMMUNICATION_PREFIX) + num.str() + Encodings::getConfigType(CT_TYPE_SUFFIX);

		if (world->config[typeKey] == Encodings::getConfigType(CT_TYPE_VOICE))
		{
			LOG(Main , 1) << "i : " << i << " is voice!" << endl;
			string maxSizeKey = Encodings::getConfigType(CT_COMMUNICATION_PREFIX) + num.str() + Encodings::getConfigType(CT_MESSAGE_SIZE_SUFFIX);
			int maxSize = atoi(world->config[maxSizeKey].c_str());
			string maxNumOfMessageKey = Encodings::getConfigType(CT_COMMUNICATION_PREFIX) + num.str() + Encodings::getConfigType(CT_MESSAGE_MAX_SUFFIX);
			int maxNumOfMessage = atoi(world->config[maxNumOfMessageKey].c_str());
			string rangeKey = Encodings::getConfigType(CT_COMMUNICATION_PREFIX) + num.str() + Encodings::getConfigType(CT_VOICE_RANGE);
			int range = atoi(world->config[rangeKey].c_str());
			VoiceChannel *newChannel = new VoiceChannel("Voice", i, maxSize, maxNumOfMessage, range);
			voiceChannels.push_back(newChannel);

			LOG(Main , 1) << "size of chanel : " << newChannel->getMaxSize() << endl;
		}
		else
		{
			LOG(Main, 1) << "Unknown type of channel " << i << " :" << world->config[typeKey] << endl;
		}
	}

	if (voiceChannels.empty())
	{
		return;
	}

	sort(voiceChannels.begin(), voiceChannels.end(), voiceChanelSizeComparator);
	myVoiceChanel = voiceChannels[0];
	LOG(Main , 1) << "my voice chanel size: " << myVoiceChanel->getMaxSize() << endl;

	setSizes();
}

void CommunicationlessRadar::setPosition(Human * human, int node, int isStucked, int side)
{
	if (isStucked == 0)
	{
		human->setRepresentiveNodeIndex(node);
		human->setWithoutBlockadeRepresentiveNodeIndex(node);
	}
	else
	{
		human->setRepresentiveNodeIndex(-1);
		human->setWithoutBlockadeRepresentiveNodeIndex(node);
	}

	if (side == 1)
	{
		human->setPosition(world->motionlessObjects[worldGraph->getNodes()[node]->getFirstMotionlessIndex()]->getId());
		human->setMotionlessObject(world->motionlessObjects[worldGraph->getNodes()[node]->getFirstMotionlessIndex()]);
	}
	else
	{
		human->setPosition(world->motionlessObjects[worldGraph->getNodes()[node]->getSecondMotionlessIndex()]->getId());
		human->setFirstTimeMotionless(world->motionlessObjects[worldGraph->getNodes()[node]->getSecondMotionlessIndex()]);
	}

	LOG(Main , 1) << "The postion of new civilian has been setted!" << endl;
}

Platoon * CommunicationlessRadar::self()
{
	return ((Platoon *) world->self);
}
