#include "AbstractCommunicationradar.h"

#define LOGLEVEL 0

AbstractCommunicationRadar::AbstractCommunicationRadar(WorldModel *wm, WorldGraph *wg, Command *command)
{
	this->world = wm;
	this->worldGraph = wg;
	this->command = command;
	this->myChanel = nullptr;
	clRadar = new CommunicationlessRadar(wm, wg, command);
}

bool AbstractCommunicationRadar::channelComparator(RadioChannel* c1, RadioChannel* c2)
{
	return c1->getBandWidth() > c2->getBandWidth();
}

bool AbstractCommunicationRadar::buildingComparator(Building* b1, Building* b2)
{
	return b2->getLastCycleSentInRadar() > b1->getLastCycleSentInRadar();
}

void AbstractCommunicationRadar::init()
{
	this->numOfSubscribeForCenters = atoi(world->config[Encodings::getConfigType(CT_CENTER_MAX_CHANNELS)].c_str());
	this->numOfSubscribeForPlatoons = atoi(world->config[Encodings::getConfigType(CT_PLATOON_MAX_CHANNELS)].c_str());
	this->myChanel = nullptr;

	clRadar->init();

	int numOfChannels = atoi(world->config[Encodings::getConfigType(CT_NUM_OF_CHANNELS)].c_str());
	for (int i = 0; i < numOfChannels; i++)
	{
		std::stringstream num;
		num << i;
		string typeKey = Encodings::getConfigType(CT_COMMUNICATION_PREFIX) + num.str() + Encodings::getConfigType(CT_TYPE_SUFFIX);
		if (world->config[typeKey] == Encodings::getConfigType(CT_TYPE_RADAR))
		{
			string bandwidthKey = Encodings::getConfigType(CT_COMMUNICATION_PREFIX) + num.str() + Encodings::getConfigType(CT_BANDWIDTH_SUFIX);
			int bandwidth = atoi(world->config[bandwidthKey].c_str());
			RadioChannel *newChannel = new RadioChannel("radio", i, bandwidth);
			radioChannels.push_back(newChannel);
		}
		else if (world->config[typeKey] == Encodings::getConfigType(CT_TYPE_VOICE))
		{
			LOG(Main , 1) << "i : " << i << " is voice!" << endl;
			string maxSizeKey = Encodings::getConfigType(CT_COMMUNICATION_PREFIX) + num.str() + Encodings::getConfigType(CT_MESSAGE_SIZE_SUFFIX);
			int maxSize = atoi(world->config[maxSizeKey].c_str());
			string maxNumOfMessageKey = Encodings::getConfigType(CT_COMMUNICATION_PREFIX) + num.str() + Encodings::getConfigType(CT_MESSAGE_MAX_SUFFIX);
			int maxNumOfMessage = atoi(world->config[maxNumOfMessageKey].c_str());
			string rangeKey = Encodings::getConfigType(CT_COMMUNICATION_PREFIX) + num.str() + Encodings::getConfigType(CT_VOICE_RANGE);
			int range = atoi(world->config[rangeKey].c_str());
			VoiceChannel *newChannel = new VoiceChannel("Voice", i, maxSize, maxNumOfMessage, range);
			voiceChannels.push_back(newChannel);
		}
		else
		{
			LOG(Main, 1) << "Unknown type of channel " << i << " :" << world->config[typeKey] << endl;
		}
	}

	if (self()->isPlatoon())
	{
		if (numOfSubscribeForPlatoons == 0 || radioChannels.size() == 0)
		{
			world->setIsNoComm(true);
			return;
		}
		else if (radioChannels.size() != 0)
		{
			world->setIsNoComm(false);
		}
	}
	else
	{
		if (numOfSubscribeForCenters == 0 || radioChannels.size() == 0)
		{
			world->setIsNoComm(true);
			return;
		}
		else if (radioChannels.size() != 0)
		{
			world->setIsNoComm(false);
		}
	}

	if (radioChannels.size() > 0)
	{
		sort(radioChannels.begin(), radioChannels.end(), channelComparator);
	}

	setMyChanel();
	setSizes();

	maxWaterForExtinguish = atoi(world->config["fire.extinguish.max-sum"].c_str());
	sendedData = new SendedData(world, worldGraph);
	sendedData->clear();
}

void AbstractCommunicationRadar::setPosition(Human * human, int node, int isStucked, int side)
{
	human->setPosition(world->motionlessObjects[worldGraph->getNodes()[node]->getMotionlessIndex()]->getId());
	human->setMotionlessObject(world->motionlessObjects[worldGraph->getNodes()[node]->getMotionlessIndex()]);

	if (isStucked == 0)
	{
		human->setRepresentiveNodeIndex(node);
		human->setWithoutBlockadeRepresentiveNodeIndex(node);
	}
	else
	{
		human->setRepresentiveNodeIndex(-1);
		human->setWithoutBlockadeRepresentiveNodeIndex(node);
	}
}

Platoon* AbstractCommunicationRadar::self()
{
	return ((Platoon *) world->self);
}

void AbstractCommunicationRadar::subscribe()
{
	vector<int> chanelsForListen;

	for (int i = 0; i < (int) listeningChanels.size(); i++)
	{
		chanelsForListen.push_back(listeningChanels[i]->getChannelNum());
		LOG(Main, 1) << "Subscribing: " << listeningChanels[i]->getChannelNum() << endl;
	}

	LOG(Main, 1) << "Subscribing......" << endl;

	command->subscribe(chanelsForListen);
}

AbstractCommunicationRadar::~AbstractCommunicationRadar()
{
}

void AbstractCommunicationRadar::setMyChanel()
{
}
void AbstractCommunicationRadar::setSizes()
{
}
void AbstractCommunicationRadar::shareSenseWorldAfterDecision()
{
}
void AbstractCommunicationRadar::shareSenseWorldAndSendOrders()
{
}
void AbstractCommunicationRadar::analyzeMessage(std::vector<byte> &msg, int &offset, int sizeOfContent, int senderId, int channel)
{

}

void AbstractCommunicationRadar::setRadarMode(RadarMode mode)
{
	this->radarMode = mode;
}

RadarMode AbstractCommunicationRadar::getRadarMode()
{
	return radarMode;
}
