#include "SendedData.h"

SendedData::SendedData(WorldModel * wm, WorldGraph * wg)
{
	this->world = wm;
	this->worldGraph = wg;
}

void SendedData::removeThisFertCivilianFromSendedCivilians(Civilian * civ)
{
	for (int i = 0; i < (int) sendedFertCivilians.size(); i++)
	{
		if (sendedFertCivilians[i]->getId() == civ->getId())
		{
			sendedFertCivilians.erase(sendedFertCivilians.begin() + i);
			i--;
		}
	}
}

void SendedData::removeThisRoadFromSendedRoads(Road * r)
{
	for (int i = 0; i < (int) sendedRoads.size(); i++)
	{
		if (sendedRoads[i]->getId() == r->getId())
		{
			sendedRoads.erase(sendedRoads.begin() + i);
			i--;
		}
	}
}

void SendedData::removeAvailableThisCivilianFromSendedCivilians(Civilian * civ)
{
	for (int i = 0; i < (int) sendedAvailableCivilians.size(); i++)
	{
		if (sendedAvailableCivilians[i]->getId() == civ->getId())
		{
			sendedAvailableCivilians.erase(sendedAvailableCivilians.begin() + i);
			i--;
		}
	}
}

void SendedData::removeThisBuildingFromBuildingsInFire(Building * building)
{
	for (int i = 0; i < (int) sendedBuildingsInFire.size(); i++)
	{
		if (sendedBuildingsInFire[i]->getId() == building->getId())
		{
			sendedBuildingsInFire.erase(sendedBuildingsInFire.begin() + i);
			i--;
		}
	}
}

void SendedData::removeThisBuildingFromExtinguishedBuildings(Building * building)
{
	for (int i = 0; i < (int) sendedBuildingsExtinguished.size(); i++)
	{
		if (sendedBuildingsExtinguished[i]->getId() == building->getId())
		{
			sendedBuildingsExtinguished.erase(sendedBuildingsExtinguished.begin() + i);
			i--;
		}
	}
}

bool SendedData::sendThisRoad(Road * r)
{
	for (int i = 0; i < (int) sendedRoads.size(); i++)
	{
		if (sendedRoads[i]->getId() == r->getId())
		{
			return true;
		}
	}
	return false;
}

bool SendedData::sendThisEdge(Edge * e)
{
	for (int i = 0; i < (int) sendedEdges.size(); i++)
	{
		if (sendedEdges[i]->edgeIndex == e->edgeIndex)
		{
			return true;
		}
	}
	return false;
}

bool SendedData::sendThisFertCivilians(Civilian * civ)
{
	for (int i = 0; i < (int) sendedFertCivilians.size(); i++)
	{
		if (sendedFertCivilians[i]->getId() == civ->getId())
		{
			return true;
		}
	}

	return false;
}

bool SendedData::sendThisAvailableCivilian(Civilian * civ)
{
	for (int i = 0; i < (int) sendedAvailableCivilians.size(); i++)
	{
		if (sendedAvailableCivilians[i]->getId() == civ->getId())
		{
			return true;
		}
	}

	return false;
}

bool SendedData::sendThisBurnedBuilding(Building * building)
{
	for (int i = 0; i < (int) sendedBuildingsBurned.size(); i++)
	{
		if (sendedBuildingsBurned[i]->getId() == building->getId())
		{
			return true;
		}
	}
	return false;
}

bool SendedData::sendThisBuildingInFire(Building * building)
{
	for (int i = 0; i < (int) sendedBuildingsInFire.size(); i++)
	{
		if (sendedBuildingsInFire[i]->getId() == building->getId())
		{
			return true;
		}
	}

	return false;
}

bool SendedData::sendThisExtinguishedBuilding(Building * building)
{
	for (int i = 0; i < (int) sendedBuildingsExtinguished.size(); i++)
	{
		if (sendedBuildingsExtinguished[i]->getId() == building->getId())
		{
			return true;
		}
	}

	return false;
}

void SendedData::clear()
{
	sendedBuildingsInFire.clear();
	sendedBuildingsExtinguished.clear();
	sendedAvailableCivilians.clear();
	sendedRoads.clear();
}
