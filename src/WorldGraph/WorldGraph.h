#ifndef _WORLDGRAPH_H
#define	_WORLDGRAPH_H

#include "../WorldModel/WorldModel.h"
#include "../WorldModel/Objects/Road.h"
#include "../Utilities/Debugger.h"
#include "../Utilities/Geometry/Segment.h"
#include "../Utilities/UpdatableHeap.h"
#include "../WorldModel/Objects/Node.h"
#include "../WorldModel/Objects/Edge.h"
#include "../WorldModel/Objects/VirtualEdge.h"
#include "../Utilities/Types.h"
#include <algorithm>
#include <cstring>
#include <queue>
#include <stack>
#include <map>

using namespace std;
using namespace Types;

#define PASSING_CHECK_LEVELS 2
#define CLIPPER_SCALE double(100000)

const static int INF = (1u << 30) - 1;

struct DistanceTableEntry
{
		int distance;
		int parentNodeIndex;
		DistanceTableEntry(int dist = INF, int parent = -1);
};

bool isPassableEdge(Edge* edge, bool defaultBlocked = false);

class WorldGraph
{
	public:
		WorldGraph(WorldModel*);
		virtual ~WorldGraph();

		void init();
		void withoutPrecomputationInit();
		void update();

		/******************** Getter/Setter/Tofs ********************/
		vector<Node*>& getNodes();
		vector<Edge*>& getEdges();
		void addNode(Node*);
		void addEdge(Edge*);
		void addWithBlockadeDistanceFindingNodesIndex(int);
		void addWithoutBlockadeDistanceFindingNodesIndex(int);
		void addDistanceFindingMotiolessIndex(int motionlessIndex);
		void addDistanceFindingHumanIndex(int humanIndex);

		/******************** Path Planners ********************/
		vector<int> getIDPath(vector<int>& nodePath);
		vector<int> getPath(int node1Index, int node2Index, Types::GraphMode graphMode);
		vector<int> getOnlinePath(int node1Index, int node2Index, Types::GraphMode graphMode);

		/************ Reachability/Distance Calculators *************/
		bool isReachableXYInRoad(Road* road, Point firstPoint, Point secondPoint);
		bool isReachable(int node1Index, int node2Index, Types::GraphMode graphMode);
		bool onlineIsReachable(int node1Index, int node2Index, Types::GraphMode graphMode);
		int getOnlineDistance(int node1Index, int node2Index, Types::GraphMode graphMode);
		int getDistance(int node1Index, int node2Index, Types::GraphMode graphMode);
		int getAirDistance(int node1Index, int node2Index);

		// Passing
		void updatePassingModesAndRepresentiveNodes(int motionLessIndex);
		void updateHumanWithoutBlockadeRepresentiveNode(int humanIndex);

		/******************** Getter/Setter ********************/
		const vector<int>& getDistanceFindingHumanIndexes() const;
		const vector<int>& getDistanceFindingMotionlessIndexes() const;
		const vector<int>& getWithBlockadeDistanceFindingNodesIndexes() const;
		const vector<int>& getWithoutBlockadeDistanceFindingNodesIndexes() const;
		const vector<int>& getDefaultBlockedModeComponents() const;
		const vector<int>& getDefaultNotBlockedModeComponents() const;
		const vector<DistanceTableEntry>& getDefaultBlockedDijkstraDistanceTable() const;
		const vector<vector<DistanceTableEntry> >& getDefaultBlockedDistanceTable() const;
		const vector<DistanceTableEntry>& getDefaultNotBlockedDijkstraDistanceTable() const;
		const vector<vector<DistanceTableEntry> >& getDefaultNotBlockedDistanceTable() const;
		const vector<DistanceTableEntry>& getWithoutBlockadeDijkstraDistanceTable() const;
		const vector<vector<DistanceTableEntry> >& getWithoutBlockadeDistanceTable() const;
		void setDefaultBlockedModeComponents(const vector<int>& defaultBlockedModeComponents);
		void setDefaultNotBlockedModeComponents(const vector<int>& defaultNotBlockedModeComponents);
		void setDefaultBlockedDistanceTable(const vector<vector<DistanceTableEntry> >& defaultBlockedDistanceTable);
		void setDefaultBlockedDijkstraDistanceTable(const vector<DistanceTableEntry>& defaultBlockedDijkstraDistanceTable);
		void setDefaultNotBlockedDijkstraDistanceTable(const vector<DistanceTableEntry>& defaultNotBlockedDijkstraDistanceTable);
		void setDefaultNotBlockedDistanceTable(const vector<vector<DistanceTableEntry> >& defaultNotBlockedDistanceTable);
		void setWithoutBlockadeDijkstraDistanceTable(const vector<DistanceTableEntry>& withoutBlockadeDijkstraDistanceTable);
		void setWithoutBlockadeDistanceTable(const vector<vector<DistanceTableEntry> >& withoutBlockadeDistanceTable);

		vector<Edge*> notRelatedToRoadEdges;

	private:

		UpdatableHeap heap;

		void createGraph();

		/******************** Getter/Setter/Tofs ********************/
		int tofLengthForSpecialEdge(Edge*);
		void setDistanceFindingNodesAndSelfNodeIndexes();

		/**************** Graph Traversal Algorithms ****************/
		void applyDFS();
		bool applyOnlineBFS(int nodeIndex, int targetIndex, Types::GraphMode gm);
		void applyDefaultBlockedBFS();
		void applyDefaultNotBlockedBFS();
		void applyWithoutBlockadeBFS();
		void applyDefaultBlockedDijkstra();
		void applyDefaultNotBlockedDijkstra();
		void applyWithoutBlockadeDijkstra();
		void recursiveDefaultBlockedDFS(int, int);
		void recursiveDefaultNotBlockedDFS(int, int);
		void recursiveDijkstraPathFind(vector<DistanceTableEntry>&, vector<int>&, int);

		WorldModel* world;
		int withBlockadeSelfRepresentingNodeIndex;
		int withoutBlockadeSelfRepresentingNodeIndex;
		vector<Node*> nodes;
		vector<Edge*> edges;
		vector<bool> mark;
		vector<int> parents;
		vector<int> defaultBlockedModeComponents;
		vector<int> defaultNotBlockedModeComponents;
		vector<int> withBlockadeDistanceFindingNodesIndexes;
		vector<int> withoutBlockadeDistanceFindingNodesIndexes;
		vector<int> distanceFindingMotionlessIndexes;
		vector<int> distanceFindingHumanIndexes;
		vector<vector<DistanceTableEntry>> defaultBlockedDistanceTable;
		vector<vector<DistanceTableEntry>> defaultNotBlockedDistanceTable;
		vector<vector<DistanceTableEntry>> withoutBlockadeDistanceTable;
		vector<DistanceTableEntry> defaultBlockedDijkstraDistanceTable;
		vector<DistanceTableEntry> defaultNotBlockedDijkstraDistanceTable;
		vector<DistanceTableEntry> withoutBlockadeDijkstraDistanceTable;

		// Passing
		void initRepresentiveNodes();
		void setValidParts(int roadIndex);
		void setEdgePassability(int roadIndex, int edgeIndex);
		void updateHumansRepresentiveNode(Road*);
		bool withoutBlockadeMotionless(int motionlessIndex);
		int getRelativeValidPart(int, Segment, int);
		int getRelativeValidPart(int, Point, int);
		vector<Blockade*> getAllNeighborBlockades(Road*);

		map<Node*, int> nodesValidParts;
};

#endif	/* _WORLDGRAPH_H */
