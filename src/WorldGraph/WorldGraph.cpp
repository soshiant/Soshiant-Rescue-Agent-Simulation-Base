#include "WorldGraph.h"
#include "../Utilities/Geometry/Circle.h"

#define LOGLEVEL 4

using namespace Types;
using namespace Geometry;

DistanceTableEntry::DistanceTableEntry(int dist, int parent)
{
	distance = dist;
	parentNodeIndex = parent;
}

WorldGraph::WorldGraph(WorldModel* world)
{
	this->world = world;
	withBlockadeSelfRepresentingNodeIndex = -1;
	withoutBlockadeSelfRepresentingNodeIndex = -1;
}

WorldGraph::~WorldGraph()
{
	for (u_int i = 0; i < nodes.size(); i++)
	{
		delete nodes[i];
	}
	nodes.clear();

	for (u_int i = 0; i < edges.size(); i++)
	{
		delete edges[i];
	}
	edges.clear();
}

void WorldGraph::init()
{
	createGraph();

	initRepresentiveNodes();

	vector<bool> edges(getEdges().size(), false);
	for (Road* r : world->roads)
	{
		for (int edgeIndex : r->getRelativeEdgesIndexes())
		{
			edges[edgeIndex] = true;
		}
	}
	for (u_int i = 0; i < edges.size(); i++)
	{
		if (!edges[i])
		{
			notRelatedToRoadEdges.push_back(getEdges()[i]);
		}
	}
	sort(notRelatedToRoadEdges.begin(), notRelatedToRoadEdges.end(), [](Edge* edge1, Edge* edge2)
	{
		return edge1->edgeIndex > edge2->edgeIndex;
	});
	LOG(Main, 1) << "Special edges size : " << notRelatedToRoadEdges.size() << endl;
}

void WorldGraph::withoutPrecomputationInit()
{
	//	defaultBlockedModeComponents.resize(nodes.size());
	//	defaultNotBlockedModeComponents.resize(nodes.size());
	//	defaultBlockedDistanceTable.resize(nodes.size());
	//	defaultNotBlockedDistanceTable.resize(nodes.size());
	//	withoutBlockadeDistanceTable.resize(nodes.size());
	heap.init(nodes.size());
}

void WorldGraph::createGraph()
{
	// Create Nodes
	int numOfNodes = 0;
	for (MotionlessObject* motionless : world->motionlessObjects)
	{
		int edgeIndex = -1;
		for (int neighbor : motionless->getIDs())
		{
			edgeIndex++;
			if (neighbor == -1)
			{
				continue;
			}

			Node* newNode = new Node(motionless->motionlessIndex);
			newNode->setRepresentivePoint(motionless->getShape().getSegment(edgeIndex).getMiddlePoint());
			newNode->setEdgeIndex(edgeIndex);
			newNode->nodeIndex = numOfNodes;

			motionless->addRelativeNodeIndex(numOfNodes);

			nodes.push_back(newNode);
			numOfNodes++;
		}
	}

	LOG(Main, 1) << "WorldGraph has " << numOfNodes << " nodes." << endl;

	// Resizing distance tables
	defaultBlockedModeComponents.resize(numOfNodes);
	defaultNotBlockedModeComponents.resize(numOfNodes);
	defaultBlockedDistanceTable.resize(numOfNodes);
	defaultNotBlockedDistanceTable.resize(numOfNodes);
	withoutBlockadeDistanceTable.resize(numOfNodes);
	heap.init(numOfNodes);

	// Create Edges
	int numOfEdges = 0;
	for (MotionlessObject* motionless : world->motionlessObjects)
	{
		for (int j = 0; j < (int) motionless->getRelativeNodesIndexes().size(); j++)
		{
			for (int k = j + 1; k < (int) motionless->getRelativeNodesIndexes().size(); k++)
			{
				int firstNodeIndex = motionless->getRelativeNodesIndexes()[j];
				int secondNodeIndex = motionless->getRelativeNodesIndexes()[k];

				Edge* newEdge = new Edge(firstNodeIndex, secondNodeIndex);
				newEdge->setLength(
						nodes[firstNodeIndex]->getRepresentivePoint().distFromPoint(nodes[secondNodeIndex]->getRepresentivePoint()));
				newEdge->setMotionlessIndex(motionless->motionlessIndex);
				newEdge->edgeIndex = numOfEdges;

				nodes[firstNodeIndex]->addNeighbour(secondNodeIndex);
				nodes[secondNodeIndex]->addNeighbour(firstNodeIndex);

				nodes[firstNodeIndex]->addEdgeIndex(numOfEdges);
				nodes[secondNodeIndex]->addEdgeIndex(numOfEdges);

				motionless->addRelativeEdgeIndex(numOfEdges);

				edges.push_back(newEdge);
				numOfEdges++;
			}
		}
	}

	// Create VirtualEdges
	vector<bool> flag(numOfNodes, false);
	for (MotionlessObject* motionless : world->motionlessObjects)
	{
		for (int firstNodeIndex : motionless->getRelativeNodesIndexes())
		{
			if (flag[nodes[firstNodeIndex]->nodeIndex])
			{
				continue;
			}

			int secondMotionlessIndex = motionless->getIDs()[nodes[firstNodeIndex]->getEdgeIndex()];

			for (int secondNodeIndex : world->motionlessObjects[secondMotionlessIndex]->getRelativeNodesIndexes())
			{
				int secondEdgeIndex = nodes[secondNodeIndex]->getEdgeIndex();
				if (world->motionlessObjects[secondMotionlessIndex]->getIDs()[secondEdgeIndex] == motionless->motionlessIndex)
				{
					VirtualEdge* newVirtualEdge = new VirtualEdge(firstNodeIndex, secondNodeIndex);
					newVirtualEdge->setLength(
							nodes[firstNodeIndex]->getRepresentivePoint().distFromPoint(nodes[secondNodeIndex]->getRepresentivePoint()));
					newVirtualEdge->setMotionlessIndex(-1);
					newVirtualEdge->edgeIndex = numOfEdges;

					nodes[firstNodeIndex]->addNeighbour(secondNodeIndex);
					nodes[secondNodeIndex]->addNeighbour(firstNodeIndex);

					nodes[firstNodeIndex]->addEdgeIndex(numOfEdges);
					nodes[secondNodeIndex]->addEdgeIndex(numOfEdges);

					edges.push_back(newVirtualEdge);

					flag[firstNodeIndex] = true;
					flag[secondNodeIndex] = true;
					numOfEdges++;
					break;
				}
			}
		}
	}

	LOG(Main, 1) << "WorldGraph has " << numOfEdges << " edges." << endl;
	LOG(Main, 1) << "Graph creation finished." << endl;
}

void WorldGraph::update()
{
	defaultNotBlockedDijkstraDistanceTable.clear();
	withoutBlockadeDijkstraDistanceTable.clear();
	defaultBlockedDijkstraDistanceTable.clear();

	setDistanceFindingNodesAndSelfNodeIndexes();

	applyDFS();

	if (world->self->isFireBrigade())
	{
		applyDefaultNotBlockedBFS();
	}
	else if (world->self->isAmbulanceTeam())
	{
		applyDefaultNotBlockedBFS();
	}
	else if (world->self->isPoliceForce())
	{
		applyWithoutBlockadeBFS();
	}

	LOG(Main, 1) << "WorldGraph updated" << endl;
}

/******************** Getter/Setter/Tofs ********************/

int WorldGraph::tofLengthForSpecialEdge(Edge* edge)
{
	if (edge->getMotionlessIndex() == -1)
	{
		return 0;
	}

	if (world->motionlessObjects[edge->getMotionlessIndex()]->isBuilding()
			&& ((Building*) world->motionlessObjects[edge->getMotionlessIndex()])->hasFire())
	{
		return edge->getLength() * 7;
	}

	return 0;
}

void WorldGraph::addDistanceFindingMotiolessIndex(int motionlessIndex)
{
	distanceFindingMotionlessIndexes.push_back(motionlessIndex);
}

void WorldGraph::addDistanceFindingHumanIndex(int humanIndex)
{
	distanceFindingHumanIndexes.push_back(humanIndex);
}

vector<Node*>& WorldGraph::getNodes()
{
	return nodes;
}

vector<Edge*>& WorldGraph::getEdges()
{
	return edges;
}

void WorldGraph::addNode(Node* node)
{
	nodes.push_back(node);
}

void WorldGraph::addEdge(Edge* edge)
{
	edges.push_back(edge);
}

void WorldGraph::addWithBlockadeDistanceFindingNodesIndex(int index)
{
	withBlockadeDistanceFindingNodesIndexes.push_back(index);
}

void WorldGraph::addWithoutBlockadeDistanceFindingNodesIndex(int index)
{
	withoutBlockadeDistanceFindingNodesIndexes.push_back(index);
}

void WorldGraph::setDistanceFindingNodesAndSelfNodeIndexes()
{
	withBlockadeDistanceFindingNodesIndexes.clear();
	withBlockadeDistanceFindingNodesIndexes.shrink_to_fit();

	withoutBlockadeDistanceFindingNodesIndexes.clear();
	withoutBlockadeDistanceFindingNodesIndexes.shrink_to_fit();

	for (int i = 0; i < (int) distanceFindingHumanIndexes.size(); i++)
	{
		int nodeIndex = world->humans[distanceFindingHumanIndexes[i]]->getRepresentiveNodeIndex();
		if (nodeIndex != -1)
		{
			withBlockadeDistanceFindingNodesIndexes.push_back(nodeIndex);
		}
		nodeIndex = world->humans[distanceFindingHumanIndexes[i]]->getWithoutBlockadeRepresentiveNodeIndex();
		withoutBlockadeDistanceFindingNodesIndexes.push_back(nodeIndex);
	}

	for (int i = 0; i < (int) distanceFindingMotionlessIndexes.size(); i++)
	{
		int nodeIndex = world->motionlessObjects[distanceFindingMotionlessIndexes[i]]->getRepresentiveNodeIndex();
		if (nodeIndex != -1)
		{
			withBlockadeDistanceFindingNodesIndexes.push_back(nodeIndex);
		}
		nodeIndex = world->motionlessObjects[distanceFindingMotionlessIndexes[i]]->getWithoutBlockadeRepresentiveNodeIndex();
		withoutBlockadeDistanceFindingNodesIndexes.push_back(nodeIndex);
	}

	withBlockadeSelfRepresentingNodeIndex = ((Human *) world->self)->getRepresentiveNodeIndex();
	withoutBlockadeSelfRepresentingNodeIndex = ((Human *) world->self)->getWithoutBlockadeRepresentiveNodeIndex();
}

/********************** Path Planners **********************/

vector<int> WorldGraph::getOnlinePath(int node1Index, int node2Index, GraphMode graphMode)
{
	//node1Index must be the representive node of self object
	int rootIndex = node1Index;
	queue<int> que;
	vector<int> path;

	LOG(Main , 8) << "Online path!	" << node1Index << " to " << node2Index << " with " << graphMode << endl;

	mark.clear();
	mark.resize(nodes.size(), false);
	mark[rootIndex] = true;

	parents.clear();
	parents.resize(nodes.size(), -1);
	bool isReachable = false;

	que.push(rootIndex);

	while (!que.empty())
	{
		bool doBreak = false;
		int curNode = que.front();

		for (int j = 0; j < (int) nodes[curNode]->getNeighbours().size(); j++)
		{
			int neighbour = nodes[curNode]->getNeighbours()[j];
			int edge = nodes[curNode]->getEdgeIndexes()[j];

			switch (graphMode)
			{
				case GM_DEFAULT_BLOCKED:
				{
					if (!isPassableEdge(edges[edge], true))
					{
						continue;
					}
					break;
				}
				case GM_DEFAULT_NOT_BLOCKED:
				{
					if (!isPassableEdge(edges[edge]))
					{
						continue;
					}
					break;
				}
				default:
				{
					break;
				}
			}

			if (!mark[neighbour])
			{
				mark[neighbour] = true;
				parents[neighbour] = curNode;
				que.push(neighbour);
			}

			if (neighbour == node2Index)
			{
				isReachable = true;
				doBreak = true;
				break;
			}

		}

		if (doBreak)
		{
			break;
		}

		que.pop();
	}

	if (isReachable)
	{
		int tempCounter = node2Index;

		while (parents[tempCounter] != -1)
		{
			path.push_back(tempCounter);
			tempCounter = parents[tempCounter];
		}

		path.push_back(node1Index);

		reverse(path.begin(), path.end());
	}
	else
	{
		path.push_back(node1Index);
	}

	return path;
}

vector<int> WorldGraph::getPath(int node1Index, int node2Index, GraphMode graphMode)
{
	vector<int> path;
	if (node1Index == -1 || node2Index == -1 || !isReachable(node1Index, node2Index, graphMode))
	{
		return path;
	}

	switch (graphMode)
	{
		case GM_DEFAULT_BLOCKED:
		{
			if (defaultBlockedDijkstraDistanceTable.empty())
			{
				applyDefaultBlockedDijkstra();
			}

			if (node1Index == withBlockadeSelfRepresentingNodeIndex && defaultBlockedDijkstraDistanceTable.size() > 0)
			{
				recursiveDijkstraPathFind(defaultBlockedDijkstraDistanceTable, path, node2Index);
			}

			return path;
			break;
		}
		case GM_DEFAULT_NOT_BLOCKED:
		{
			if (defaultNotBlockedDijkstraDistanceTable.empty())
			{
				applyDefaultNotBlockedDijkstra();
			}

			if (node1Index == withBlockadeSelfRepresentingNodeIndex && defaultNotBlockedDijkstraDistanceTable.size() > 0)
			{
				recursiveDijkstraPathFind(defaultNotBlockedDijkstraDistanceTable, path, node2Index);
			}

			return path;
			break;
		}
		case GM_WITHOUT_BLOCKADE:
		{
			if (withoutBlockadeDijkstraDistanceTable.empty())
			{
				applyWithoutBlockadeDijkstra();
			}

			if (node1Index == withoutBlockadeSelfRepresentingNodeIndex && withoutBlockadeDijkstraDistanceTable.size() > 0)
			{
				recursiveDijkstraPathFind(withoutBlockadeDijkstraDistanceTable, path, node2Index);
			}

			return path;
			break;
		}
		default:
		{
			vector<int> temp;
			temp.push_back(0);
			return temp;
		}
	}
}

vector<int> WorldGraph::getIDPath(vector<int>& nodePath)
{
	vector<int> idPath;
	idPath.clear();

	for (int i = 0; i < (int) nodePath.size(); i++)
	{
		if (idPath.empty() || world->motionlessObjects[nodes[nodePath[i]]->getMotionlessIndex()]->getId() != idPath[idPath.size() - 1])
		{
			idPath.push_back(world->motionlessObjects[nodes[nodePath[i]]->getMotionlessIndex()]->getId());
		}
	}

	return idPath;
}

/************ Reachability/Distance Calculators *************/

bool WorldGraph::onlineIsReachable(int node1Index, int node2Index, GraphMode graphMode)
{
	if (node1Index == -1 || node2Index == -1)
	{
		return false;
	}

	if (node1Index == node2Index)
	{
		return true;
	}

	if (graphMode == GM_WITHOUT_BLOCKADE)
	{
		return true;
	}

	mark.clear();
	mark.shrink_to_fit();
	mark.resize(nodes.size(), false);
	return applyOnlineBFS(node1Index, node2Index, graphMode);
}

bool WorldGraph::isReachableXYInRoad(Road* road, Point firstPoint, Point secondPoint)
{
	LOG(Main, 1) << "You are using reachableXY in wrong way asshole" << endl;
	cout << "You are using reachableXY in wrong way asshole" << endl;
	return false;
}

bool WorldGraph::isReachable(int node1Index, int node2Index, GraphMode graphMode)
{
	if (node1Index == -1 || node2Index == -1)
	{
		return false;
	}

	switch (graphMode)
	{
		case GM_DEFAULT_BLOCKED:
		{
			return defaultBlockedModeComponents[node1Index] == defaultBlockedModeComponents[node2Index];
			break;
		}
		case GM_DEFAULT_NOT_BLOCKED:
		{
			return defaultNotBlockedModeComponents[node1Index] == defaultNotBlockedModeComponents[node2Index];
			break;
		}
		case GM_WITHOUT_BLOCKADE:
		{
			return true;
			break;
		}
		default:
		{
			return true;
			break;
		}
	}
}

int WorldGraph::getDistance(int node1Index, int node2Index, GraphMode graphMode)
{
	//node1Index must be in the finding distance humans or motionless representive nodes

	if (world->teamConfig["MoveAndWorldGraphSettings"]["GlobalSettings"]["UseAirDistances"].asInt() == 2)
	{
		return getAirDistance(node1Index, node2Index);
	}
	if (node1Index == -1 || node2Index == -1 || !isReachable(node1Index, node2Index, graphMode))
	{
		return INF;
	}

	switch (graphMode)
	{
		case GM_DEFAULT_BLOCKED:
		{
			if (world->teamConfig["MoveAndWorldGraphSettings"]["GlobalSettings"]["UseAirDistances"].asInt() == 1
					&& defaultBlockedDistanceTable[node1Index].size() == 0)
			{
				return getAirDistance(node1Index, node2Index);
			}

			return defaultBlockedDistanceTable[node1Index][node2Index].distance;
			break;
		}
		case GM_DEFAULT_NOT_BLOCKED:
		{
			if (world->teamConfig["MoveAndWorldGraphSettings"]["GlobalSettings"]["UseAirDistances"].asInt() == 1
					&& defaultNotBlockedDistanceTable[node1Index].size() == 0)
			{
				return getAirDistance(node1Index, node2Index);
			}

			return defaultNotBlockedDistanceTable[node1Index][node2Index].distance;
			break;
		}
		case GM_WITHOUT_BLOCKADE:
		{
			if (world->teamConfig["MoveAndWorldGraphSettings"]["GlobalSettings"]["UseAirDistances"].asInt() == 1
					&& withoutBlockadeDistanceTable[node1Index].size() == 0)
			{
				return getAirDistance(node1Index, node2Index);
			}

			return withoutBlockadeDistanceTable[node1Index][node2Index].distance;
			break;
		}
		default:
		{
			return 0;
			break;
		}
	}
}

int WorldGraph::getAirDistance(int node1Index, int node2Index)
{
	if (node1Index == -1 || node2Index == -1)
	{
		return INF;
	}

	return (int) distanceBetweenPoints(nodes[node1Index]->getRepresentivePoint(), nodes[node2Index]->getRepresentivePoint());
}

int WorldGraph::getOnlineDistance(int node1Index, int node2Index, Types::GraphMode graphMode)
{
	vector<int> path = getOnlinePath(node1Index, node2Index, graphMode);
	int totalDistance = 0;

	for (int i = 0; i < (int) path.size() - 1; i++)
	{
		totalDistance += nodes[path[i]]->getRepresentivePoint().distFromPoint(nodes[path[i + 1]]->getRepresentivePoint());
	}

	return totalDistance;
}

/**************** Graph Traversal Algorithms ****************/

void WorldGraph::applyDFS()
{
	int componentsCount = 0;
	mark.clear();
	mark.shrink_to_fit();
	mark.resize(nodes.size(), false);
	for (int i = 0; i < (int) nodes.size(); i++)
	{
		if (!mark[i])
		{
			recursiveDefaultBlockedDFS(i, componentsCount);
			componentsCount++;
		}
	}

	componentsCount = 0;
	mark.clear();
	mark.shrink_to_fit();
	mark.resize(nodes.size(), false);
	for (int i = 0; i < (int) nodes.size(); i++)
	{
		if (!mark[i])
		{
			recursiveDefaultNotBlockedDFS(i, componentsCount);
			componentsCount++;
		}
	}

	mark.clear();
	mark.shrink_to_fit();
}

bool WorldGraph::applyOnlineBFS(int nodeIndex, int targetIndex, Types::GraphMode gm)
{
	int rootIndex = nodeIndex;
	queue<int> que;

	mark.clear();
	mark.shrink_to_fit();
	mark.resize(nodes.size(), false);
	mark[rootIndex] = true;
	que.push(rootIndex);
	while (!que.empty())
	{
		int curNode = que.front();
		for (int j = 0; j < (int) nodes[curNode]->getNeighbours().size(); j++)
		{
			int neighbour = nodes[curNode]->getNeighbours()[j];
			int edge = nodes[curNode]->getEdgeIndexes()[j];

			switch (gm)
			{
				case GM_DEFAULT_BLOCKED:
				{
					if (!isPassableEdge(edges[edge], true))
					{
						continue;
					}
					break;
				}
				case GM_DEFAULT_NOT_BLOCKED:
				{
					if (!isPassableEdge(edges[edge]))
					{
						continue;
					}
					break;
				}
				default:
				{
					break;
				}
			}

			if (!mark[neighbour])
			{
				mark[neighbour] = true;

				if (neighbour == targetIndex)
				{
					return true;
				}

				que.push(neighbour);
			}
		}
		que.pop();
	}

	return false;
}

void WorldGraph::applyDefaultBlockedBFS()
{
	LOG(Main, 2) << "before Default Blocked BFS" << endl;
	queue<int> que;
	for (int i = 0; i < (int) defaultBlockedDistanceTable.size(); i++)
	{
		defaultBlockedDistanceTable[i].clear();
		defaultBlockedDistanceTable[i].shrink_to_fit();
	}
	for (int i = 0; i < (int) withBlockadeDistanceFindingNodesIndexes.size(); i++)
	{
		int rootIndex = withBlockadeDistanceFindingNodesIndexes[i];

		if ((int) defaultBlockedDistanceTable[rootIndex].size() > 0)
		{
			continue;
		}
		mark.clear();
		mark.resize(nodes.size(), false);
		defaultBlockedDistanceTable[rootIndex].resize(nodes.size());
		defaultBlockedDistanceTable[rootIndex][rootIndex].distance = 0;
		mark[rootIndex] = true;
		que.push(rootIndex);
		while (!que.empty())
		{
			int curNode = que.front();
			for (int j = 0; j < (int) nodes[curNode]->getNeighbours().size(); j++)
			{
				int neighbour = nodes[curNode]->getNeighbours()[j];
				int edge = nodes[curNode]->getEdgeIndexes()[j];
				if (!mark[neighbour] && isPassableEdge(edges[edge], true))
				{
					defaultBlockedDistanceTable[rootIndex][neighbour].distance = defaultBlockedDistanceTable[rootIndex][curNode].distance
							+ edges[edge]->getLength() + +tofLengthForSpecialEdge(edges[edge]);
					defaultBlockedDistanceTable[rootIndex][neighbour].parentNodeIndex = curNode;
					mark[neighbour] = true;
					que.push(neighbour);
				}
			}
			que.pop();
		}
	}
	LOG(Main, 2) << "after Default Blocked BFS" << endl;
}

void WorldGraph::applyDefaultNotBlockedBFS()
{
	LOG(Main, 2) << "before Default Not Blocked BFS" << endl;

	queue<int> que;
	for (int i = 0; i < (int) defaultNotBlockedDistanceTable.size(); i++)
	{
		defaultNotBlockedDistanceTable[i].clear();
		defaultNotBlockedDistanceTable[i].shrink_to_fit();
	}

	for (int i = 0; i < (int) withBlockadeDistanceFindingNodesIndexes.size(); i++)
	{
		int rootIndex = withBlockadeDistanceFindingNodesIndexes[i];

		if ((int) defaultNotBlockedDistanceTable[rootIndex].size() > 0)
		{
			continue;
		}

		mark.clear();
		mark.resize(nodes.size(), false);

		defaultNotBlockedDistanceTable[rootIndex].resize(nodes.size());
		defaultNotBlockedDistanceTable[rootIndex][rootIndex].distance = 0;
		mark[rootIndex] = true;
		que.push(rootIndex);

		while (!que.empty())
		{
			int currentNode = que.front();
			for (int j = 0; j < (int) nodes[currentNode]->getNeighbours().size(); j++)
			{
				int neighbour = nodes[currentNode]->getNeighbours()[j];
				int edge = nodes[currentNode]->getEdgeIndexes()[j];
				if (!mark[neighbour] && isPassableEdge(edges[edge]))
				{
					defaultNotBlockedDistanceTable[rootIndex][neighbour].distance =
							defaultNotBlockedDistanceTable[rootIndex][currentNode].distance + edges[edge]->getLength()
									+ tofLengthForSpecialEdge(edges[edge]);
					defaultNotBlockedDistanceTable[rootIndex][neighbour].parentNodeIndex = currentNode;
					mark[neighbour] = true;
					que.push(neighbour);
				}
			}
			que.pop();
		}
	}

	LOG(Main, 2) << "after Default Not Blocked BFS" << endl;
}

void WorldGraph::applyWithoutBlockadeBFS()
{
	LOG(Main, 2) << "before Without Blockade BFS" << endl;
	queue<int> que;
	for (int i = 0; i < (int) withoutBlockadeDistanceTable.size(); i++)
	{
		withoutBlockadeDistanceTable[i].clear();
		withoutBlockadeDistanceTable[i].shrink_to_fit();
	}
	for (int i = 0; i < (int) withoutBlockadeDistanceFindingNodesIndexes.size(); i++)
	{
		int rootIndex = withoutBlockadeDistanceFindingNodesIndexes[i];
		if ((int) withoutBlockadeDistanceTable[rootIndex].size() > 0)
		{
			continue;
		}
		mark.clear();
		mark.resize(nodes.size(), false);
		withoutBlockadeDistanceTable[rootIndex].resize(nodes.size());
		withoutBlockadeDistanceTable[rootIndex][rootIndex].distance = 0;
		mark[rootIndex] = true;
		que.push(rootIndex);
		while (!que.empty())
		{
			int curNode = que.front();
			for (int j = 0; j < (int) nodes[curNode]->getNeighbours().size(); j++)
			{
				int neighbour = nodes[curNode]->getNeighbours()[j];
				int edge = nodes[curNode]->getEdgeIndexes()[j];
				if (!mark[neighbour])
				{
					withoutBlockadeDistanceTable[rootIndex][neighbour].distance = withoutBlockadeDistanceTable[rootIndex][curNode].distance
							+ edges[edge]->getLength() + tofLengthForSpecialEdge(edges[edge]);
					withoutBlockadeDistanceTable[rootIndex][neighbour].parentNodeIndex = curNode;
					mark[neighbour] = true;
					que.push(neighbour);
				}
			}
			que.pop();
		}
	}
	LOG(Main, 2) << "after Without Blockade BFS: " << endl;
}

void WorldGraph::applyDefaultBlockedDijkstra()
{
	heap.clear();
	int rootNode = withBlockadeSelfRepresentingNodeIndex;
	defaultBlockedDijkstraDistanceTable.clear();
	defaultBlockedDijkstraDistanceTable.shrink_to_fit();
	defaultBlockedDijkstraDistanceTable.resize(nodes.size());
	mark.clear();
	mark.shrink_to_fit();
	mark.resize(nodes.size(), false);
	for (int i = 0; i < (int) nodes.size(); i++)
	{
		heap.insert(i, INF);
	}
	defaultBlockedDijkstraDistanceTable[rootNode].distance = 0;
	heap.update(rootNode, 0);
	for (int i = 0; i < (int) nodes.size(); i++)
	{
		int curNode = heap.extractMin();
		if (defaultBlockedDijkstraDistanceTable[curNode].distance == INF)
		{
			break;
		}
		mark[curNode] = true;
		for (int j = 0; j < (int) nodes[curNode]->getNeighbours().size(); j++)
		{
			int neighbour = nodes[curNode]->getNeighbours()[j];
			int edge = nodes[curNode]->getEdgeIndexes()[j];
			if (!mark[neighbour] && isPassableEdge(edges[edge], true)
					&& defaultBlockedDijkstraDistanceTable[curNode].distance + edges[edge]->getLength()
							< defaultBlockedDijkstraDistanceTable[neighbour].distance)
			{
				defaultBlockedDijkstraDistanceTable[neighbour].distance = defaultBlockedDijkstraDistanceTable[curNode].distance
						+ edges[edge]->getLength() + +tofLengthForSpecialEdge(edges[edge]);
				defaultBlockedDijkstraDistanceTable[neighbour].parentNodeIndex = curNode;
				heap.update(neighbour, defaultBlockedDijkstraDistanceTable[neighbour].distance);
			}
		}
	}
}

void WorldGraph::applyDefaultNotBlockedDijkstra()
{
	LOG(Main, 5) << "Dijkstra! " << endl;

	if (withBlockadeSelfRepresentingNodeIndex < 0)
	{
		return;
	}

	heap.clear();
	int rootNode = withBlockadeSelfRepresentingNodeIndex;
	LOG(Main, 5) << "rootNode: " << rootNode << endl;

	defaultNotBlockedDijkstraDistanceTable.clear();
	defaultNotBlockedDijkstraDistanceTable.shrink_to_fit();
	defaultNotBlockedDijkstraDistanceTable.resize(nodes.size());

	mark.clear();
	mark.resize(nodes.size(), false);

	for (int i = 0; i < (int) nodes.size(); i++)
	{
		heap.insert(i, INF);
	}

	defaultNotBlockedDijkstraDistanceTable[rootNode].distance = 0;
	heap.update(rootNode, 0);

	for (int i = 0; i < (int) nodes.size(); i++)
	{
		int curNode = heap.extractMin();

		if (defaultNotBlockedDijkstraDistanceTable[curNode].distance == INF)
		{
			break;
		}

		mark[curNode] = true;

		for (int j = 0; j < (int) nodes[curNode]->getNeighbours().size(); j++)
		{
			int neighbour = nodes[curNode]->getNeighbours()[j];
			int edge = nodes[curNode]->getEdgeIndexes()[j];

			if (!mark[neighbour] && isPassableEdge(edges[edge])
					&& defaultNotBlockedDijkstraDistanceTable[curNode].distance + edges[edge]->getLength()
							< defaultNotBlockedDijkstraDistanceTable[neighbour].distance)
			{
				defaultNotBlockedDijkstraDistanceTable[neighbour].distance = defaultNotBlockedDijkstraDistanceTable[curNode].distance
						+ edges[edge]->getLength() + tofLengthForSpecialEdge(edges[edge]);
				defaultNotBlockedDijkstraDistanceTable[neighbour].parentNodeIndex = curNode;
				heap.update(neighbour, defaultNotBlockedDijkstraDistanceTable[neighbour].distance);
			}
		}
	}
}

void WorldGraph::applyWithoutBlockadeDijkstra()
{
	heap.clear();
	int rootNode = withoutBlockadeSelfRepresentingNodeIndex;
	withoutBlockadeDijkstraDistanceTable.clear();
	withoutBlockadeDijkstraDistanceTable.shrink_to_fit();
	withoutBlockadeDijkstraDistanceTable.resize(nodes.size());
	mark.clear();
	mark.resize(nodes.size(), false);
	for (int i = 0; i < (int) nodes.size(); i++)
	{
		heap.insert(i, INF);
	}
	withoutBlockadeDijkstraDistanceTable[rootNode].distance = 0;
	heap.update(rootNode, 0);
	for (int i = 0; i < (int) nodes.size(); i++)
	{
		int curNode = heap.extractMin();
		if (withoutBlockadeDijkstraDistanceTable[curNode].distance == INF)
		{
			break;
		}
		mark[curNode] = true;
		for (int j = 0; j < (int) nodes[curNode]->getNeighbours().size(); j++)
		{
			int neighbour = nodes[curNode]->getNeighbours()[j];
			int edge = nodes[curNode]->getEdgeIndexes()[j];
			if (!mark[neighbour]
					&& withoutBlockadeDijkstraDistanceTable[curNode].distance + edges[edge]->getLength()
							< withoutBlockadeDijkstraDistanceTable[neighbour].distance)
			{
				withoutBlockadeDijkstraDistanceTable[neighbour].distance = withoutBlockadeDijkstraDistanceTable[curNode].distance
						+ edges[edge]->getLength() + tofLengthForSpecialEdge(edges[edge]);
				withoutBlockadeDijkstraDistanceTable[neighbour].parentNodeIndex = curNode;
				heap.update(neighbour, withoutBlockadeDijkstraDistanceTable[neighbour].distance);
			}
		}
	}
}

void WorldGraph::recursiveDefaultBlockedDFS(int nodeIndex, int comp)
{
	mark[nodeIndex] = true;
	defaultBlockedModeComponents[nodeIndex] = comp;
	for (int i = 0; i < (int) nodes[nodeIndex]->getNeighbours().size(); i++)
	{
		int neighbour = nodes[nodeIndex]->getNeighbours()[i];
		if (!mark[neighbour] && isPassableEdge(edges[nodes[nodeIndex]->getEdgeIndexes()[i]], true))
		{
			recursiveDefaultBlockedDFS(neighbour, comp);
		}
	}
}

void WorldGraph::recursiveDefaultNotBlockedDFS(int nodeIndex, int comp)
{
	mark[nodeIndex] = true;
	defaultNotBlockedModeComponents[nodeIndex] = comp;
	for (int i = 0; i < (int) nodes[nodeIndex]->getNeighbours().size(); i++)
	{
		int neighbour = nodes[nodeIndex]->getNeighbours()[i];
		if (!mark[neighbour] && isPassableEdge(edges[nodes[nodeIndex]->getEdgeIndexes()[i]]))
		{
			recursiveDefaultNotBlockedDFS(neighbour, comp);
		}
	}
}

void WorldGraph::recursiveDijkstraPathFind(std::vector<DistanceTableEntry>& distanceTable, std::vector<int>& path, int nodeID)
{
	if (distanceTable[nodeID].parentNodeIndex != -1)
	{
		recursiveDijkstraPathFind(distanceTable, path, distanceTable[nodeID].parentNodeIndex);
	}
	path.push_back(nodeID);
}
