#include "WorldGraph.h"
#include "../Utilities/Geometry/Circle.h"

#define LOGLEVEL 0

using namespace std;
using namespace Types;
using namespace Geometry;

bool isPassableEdge(Edge* edge, bool defaultBlocked)
{
	if (edge->isVirtual())
	{
		return true;
	}

	if (edge->getPassingMode() == PM_PASSABLE || edge->getPassingMode() == PM_SEMI_PASSABLE)
	{
		return true;
	}
	if (!defaultBlocked && edge->getPassingMode() == PM_UNKNOWN)
	{
		return true;
	}
	return false;
}

void WorldGraph::updatePassingModesAndRepresentiveNodes(int motionlessIndex)
{
	if (withoutBlockadeMotionless(motionlessIndex))
	{
		return;
	}

	Road* road = (Road*) world->motionlessObjects[motionlessIndex];

	nodesValidParts.clear();

	setValidParts(road->roadIndex);

	for (int edgeIndex : road->getRelativeEdgesIndexes())
	{
		setEdgePassability(road->roadIndex, edgeIndex);
	}

	updateHumansRepresentiveNode(road);
}

void WorldGraph::setEdgePassability(int roadIndex, int edgeIndex)
{
	Edge* edge = edges[edgeIndex];
	Node* node1 = nodes[edge->getFirstNodeIndex()];
	Node* node2 = nodes[edge->getSecondNodeIndex()];

	Segment segment1 = world->roads[roadIndex]->getShape().getSegment(node1->getEdgeIndex());
	Segment segment2 = world->roads[roadIndex]->getShape().getSegment(node2->getEdgeIndex());

	edge->setPassingMode(PM_UNKNOWN);

	// Level 1
	int node1ValidPartL1 = getRelativeValidPart(roadIndex, segment1, 0);
	if (node1ValidPartL1 != -1)
	{
		nodesValidParts[node1] = node1ValidPartL1 + 1;
	}

	int node2ValidPartL1 = getRelativeValidPart(roadIndex, segment2, 0);
	if (node2ValidPartL1 != -1)
	{
		nodesValidParts[node2] = node2ValidPartL1 + 1;
	}

	if (node1ValidPartL1 != -1 && node2ValidPartL1 == node1ValidPartL1)
	{
		edge->setPassingMode(PM_PASSABLE);
		return;
	}

	// Level 2
	int node1ValidPartL2 = getRelativeValidPart(roadIndex, segment1, 1);
	if (node1ValidPartL2 != -1)
	{
		nodesValidParts[node1] = node1ValidPartL2 + 1;
	}

	int node2ValidPartL2 = getRelativeValidPart(roadIndex, segment2, 1);
	if (node2ValidPartL2 != -1)
	{
		nodesValidParts[node2] = node2ValidPartL2 + 1;
	}

	if (node1ValidPartL2 != -1 && node2ValidPartL2 == node1ValidPartL2)
	{
		edge->setPassingMode(PM_SEMI_PASSABLE);
		return;
	}

	edge->setPassingMode(PM_NOT_PASSABLE);
}

int WorldGraph::getRelativeValidPart(int roadIndex, Segment segment, int level)
{
	for (u_int i = 0; i < world->roads[roadIndex]->validParts[level].size(); i++)
	{
		if (world->roads[roadIndex]->validParts[level][i].isInPolygon(segment.getFirstPoint())
				|| world->roads[roadIndex]->validParts[level][i].isInPolygon(segment.getSecondPoint()))
		{
			return i;
		}

		for (int j = 0; j < world->roads[roadIndex]->validParts[level][i].size(); j++)
		{
			if (isIntersect(world->roads[roadIndex]->validParts[level][i].getSegment(j), segment))
			{
				return i;
			}
		}
	}
	return -1;
}

int WorldGraph::getRelativeValidPart(int roadIndex, Point point, int level)
{
	for (u_int i = 0; i < world->roads[roadIndex]->validParts[level].size(); i++)
	{
		if (world->roads[roadIndex]->validParts[level][i].isInPolygon(point))
		{
			return i;
		}
	}

	return -1;
}

void WorldGraph::setValidParts(int roadIndex)
{
	clipper::Path roadShape = convertToClipperPath(world->roads[roadIndex]->getShape(), CLIPPER_SCALE);
	roadShape = increasePolygon(roadShape, 20. * CLIPPER_SCALE)[0];

	for (int i = 0; i < PASSING_CHECK_LEVELS; i++)
	{
		clipper::Paths blockadesShape, validParts;
		clipper::Clipper cli;

//		for (Blockade* blockade : getAllNeighborBlockades(world->roads[roadIndex]))
		for (Blockade* blockade : world->roads[roadIndex]->getBlockades())
		{
			blockadesShape.push_back(convertToClipperPath(blockade->increasedShapes[i], CLIPPER_SCALE));
		}

		cli.AddPaths(blockadesShape, clipper::ptClip, true);
		cli.AddPath(roadShape, clipper::ptSubject, true);
		cli.Execute(clipper::ctDifference, validParts, clipper::pftNonZero, clipper::pftNonZero);

		for (clipper::Path part : validParts)
		{
			world->roads[roadIndex]->validParts[i].push_back(convertToPolygon(part, 1 / CLIPPER_SCALE));
		}
	}
}

vector<Blockade*> WorldGraph::getAllNeighborBlockades(Road* road)
{
	vector<Blockade*> blockades;

	for (Blockade* blockade : road->getBlockades())
	{
		blockades.push_back(blockade);
	}

	for (int neighbor : road->getNeighbours())
	{
		if (world->motionlessObjects[neighbor]->isRoad())
		{
			for (Blockade* blockade : ((Road*) world->motionlessObjects[neighbor])->getBlockades())
			{
				blockades.push_back(blockade);
			}
		}
	}

	return blockades;
}

bool WorldGraph::withoutBlockadeMotionless(int motionlessIndex)
{
	MotionlessObject* motionless = world->motionlessObjects[motionlessIndex];

	if (motionless->isBuilding() || ((Road*) motionless)->getBlockadeIds().empty())
	{
		LOG(Main, 1) << "Motionless " << motionless->getId() << " doesn't have any blockade." << endl;

		motionless->setRepresentiveNodeIndex(motionless->getWithoutBlockadeRepresentiveNodeIndex());
		for (int index : motionless->getRelativeEdgesIndexes())
		{
			edges[index]->setPassingMode(PM_PASSABLE);
		}
		for (int index : motionless->getInnerHumansIndexes())
		{
			Human* human = world->humans[index];
			human->setRepresentiveNodeIndex(human->getWithoutBlockadeRepresentiveNodeIndex());
			LOG(Main, 1) << human->getId() << endl;
		}

		return true;
	}

	return false;
}

void WorldGraph::updateHumanWithoutBlockadeRepresentiveNode(int humanIndex)
{
	Human* human = world->humans[humanIndex];
	MotionlessObject *pos = human->getMotionlessObject();
	double minD = numeric_limits<double>::max(), minNodeId = -1;

	for (int i = 0; i < (int) pos->getRelativeNodesIndexes().size(); i++)
	{
		int nodeId = pos->getRelativeNodesIndexes()[i];
		double dist = distanceBetweenPoints(human->getPos(), nodes[nodeId]->getRepresentivePoint());
		if (isLess(dist, minD))
		{
			minD = dist;
			minNodeId = nodeId;
		}
	}

	human->setWithoutBlockadeRepresentiveNodeIndex(minNodeId);
}

void WorldGraph::updateHumansRepresentiveNode(Road* road)
{
	for (int index : road->getInnerHumansIndexes())
	{
		Human* human = world->humans[index];

		if (human->isPoliceForce())
		{
			bool flag = false;
			for (Blockade* blockade : road->getBlockades())
			{
				if (increasePolygon(blockade->getShape(), 400.).isInPolygon(human->getPos()))
				{
					human->setRepresentiveNodeIndex(-1);
					flag = true;
					break;
				}
			}

			if (!flag)
			{
				double minDist = numeric_limits<double>::max();
				int nearestNode = -1;
				for (int nodeIndex : road->getRelativeNodesIndexes())
				{
					bool flag2 = false;
					for (Blockade* blockade : world->blockades)
					{
						if (increasePolygon(blockade->getShape(), 100.).isInPolygon(nodes[nodeIndex]->getRepresentivePoint()))
						{
							flag2 = true;
							break;
						}
					}

					if (flag2)
					{
						continue;
					}

					double dist = nodes[nodeIndex]->getRepresentivePoint().distFromPoint(human->getPos());
					if (dist < minDist)
					{
						nearestNode = nodeIndex;
						minDist = dist;
					}
				}

				human->setRepresentiveNodeIndex(nearestNode);
			}
		}
		else
		{
			int validPart = getRelativeValidPart(road->roadIndex, human->getPos(), 1);

			if (validPart == -1)
			{
				human->setRepresentiveNodeIndex(-1);
			}
			else
			{
				double minDist = numeric_limits<double>::max();
				int nearestNode = -1;
				for (int nodeIndex : road->getRelativeNodesIndexes())
				{
					if (nodesValidParts[nodes[nodeIndex]] > 0 && nodesValidParts[nodes[nodeIndex]] - 1 == validPart)
					{
						double dist = nodes[nodeIndex]->getRepresentivePoint().distFromPoint(human->getPos());
						if (dist < minDist)
						{
							nearestNode = nodeIndex;
							minDist = dist;
						}
					}
				}

				human->setRepresentiveNodeIndex(nearestNode);
			}
		}
	}
}

void WorldGraph::initRepresentiveNodes()
{
	for (int i = 0; i < (int) world->motionlessObjects.size(); i++)
	{
		world->motionlessObjects[i]->setRepresentiveNodeIndex(world->motionlessObjects[i]->getRelativeNodesIndexes()[0]);
		world->motionlessObjects[i]->setWithoutBlockadeRepresentiveNodeIndex(world->motionlessObjects[i]->getRelativeNodesIndexes()[0]);
	}

	for (int i = 0; i < (int) world->humans.size(); i++)
	{
		world->humans[i]->setRepresentiveNodeIndex(world->humans[i]->getMotionlessObject()->getRepresentiveNodeIndex());
		world->humans[i]->setWithoutBlockadeRepresentiveNodeIndex(
				world->humans[i]->getMotionlessObject()->getWithoutBlockadeRepresentiveNodeIndex());
	}
}

/******************** Getter/Setter ********************/

const vector<int>& WorldGraph::getDistanceFindingHumanIndexes() const
{
	return distanceFindingHumanIndexes;
}

const vector<int>& WorldGraph::getDistanceFindingMotionlessIndexes() const
{
	return distanceFindingMotionlessIndexes;
}

const vector<int>& WorldGraph::getWithBlockadeDistanceFindingNodesIndexes() const
{
	return withBlockadeDistanceFindingNodesIndexes;
}

const vector<int>& WorldGraph::getWithoutBlockadeDistanceFindingNodesIndexes() const
{
	return withoutBlockadeDistanceFindingNodesIndexes;
}

const vector<int>& WorldGraph::getDefaultBlockedModeComponents() const
{
	return defaultBlockedModeComponents;
}

const vector<int>& WorldGraph::getDefaultNotBlockedModeComponents() const
{
	return defaultNotBlockedModeComponents;
}

const vector<DistanceTableEntry>& WorldGraph::getDefaultBlockedDijkstraDistanceTable() const
{
	return defaultBlockedDijkstraDistanceTable;
}

const vector<vector<DistanceTableEntry> >& WorldGraph::getDefaultBlockedDistanceTable() const
{
	return defaultBlockedDistanceTable;
}

void WorldGraph::setDefaultBlockedDistanceTable(const vector<vector<DistanceTableEntry> >& defaultBlockedDistanceTable)
{
	this->defaultBlockedDistanceTable = defaultBlockedDistanceTable;
}

const vector<DistanceTableEntry>& WorldGraph::getDefaultNotBlockedDijkstraDistanceTable() const
{
	return defaultNotBlockedDijkstraDistanceTable;
}

const vector<vector<DistanceTableEntry> >& WorldGraph::getDefaultNotBlockedDistanceTable() const
{
	return defaultNotBlockedDistanceTable;
}

const vector<DistanceTableEntry>& WorldGraph::getWithoutBlockadeDijkstraDistanceTable() const
{
	return withoutBlockadeDijkstraDistanceTable;
}

const vector<vector<DistanceTableEntry> >& WorldGraph::getWithoutBlockadeDistanceTable() const
{
	return withoutBlockadeDistanceTable;
}

void WorldGraph::setDefaultBlockedModeComponents(const vector<int>& defaultBlockedModeComponents)
{
	this->defaultBlockedModeComponents = defaultBlockedModeComponents;
}

void WorldGraph::setDefaultNotBlockedModeComponents(const vector<int>& defaultNotBlockedModeComponents)
{
	this->defaultNotBlockedModeComponents = defaultNotBlockedModeComponents;
}

void WorldGraph::setDefaultBlockedDijkstraDistanceTable(const vector<DistanceTableEntry>& defaultBlockedDijkstraDistanceTable)
{
	this->defaultBlockedDijkstraDistanceTable = defaultBlockedDijkstraDistanceTable;
}

void WorldGraph::setDefaultNotBlockedDijkstraDistanceTable(const vector<DistanceTableEntry>& defaultNotBlockedDijkstraDistanceTable)
{
	this->defaultNotBlockedDijkstraDistanceTable = defaultNotBlockedDijkstraDistanceTable;
}

void WorldGraph::setDefaultNotBlockedDistanceTable(const vector<vector<DistanceTableEntry> >& defaultNotBlockedDistanceTable)
{
	this->defaultNotBlockedDistanceTable = defaultNotBlockedDistanceTable;
}

void WorldGraph::setWithoutBlockadeDijkstraDistanceTable(const vector<DistanceTableEntry>& withoutBlockadeDijkstraDistanceTable)
{
	this->withoutBlockadeDijkstraDistanceTable = withoutBlockadeDijkstraDistanceTable;
}

void WorldGraph::setWithoutBlockadeDistanceTable(const vector<vector<DistanceTableEntry> >& withoutBlockadeDistanceTable)
{
	this->withoutBlockadeDistanceTable = withoutBlockadeDistanceTable;
}
