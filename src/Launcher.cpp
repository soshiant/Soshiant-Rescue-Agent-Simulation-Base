/*    Soshiant's Rescue Agent Simulation Base
 *    Copyright (C) 2015  The Soshiant Team
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/*
 * Main.cpp
 * 
 *  Created on: Dec 17, 2009
 *      Author: Mehran Akhavan
 *      Modifier: Eqbal Sarjami
 *      Has some bugs, fixed! :D
 */

#define LOGLEVEL 1

#include <iostream>

#include "Decision/Agent.h"
#include "Decision/AmbulanceTeamAgent.h"
#include "Decision/FireBrigadeAgent.h"
#include "Decision/FireStationAgent.h"
#include "Decision/PoliceForceAgent.h"
#include "Decision/PoliceOfficeAgent.h"
#include "Decision/AmbulanceCenterAgent.h"
#include "Utilities/Geometry/OneTimeSettings.h"

using namespace std;
using namespace basics;
using namespace Types;
const char FB = '1', FS = '2', PF = '3', PO = '4', AT = '5', AC = '6';

int runAgent(EntityType et, string ip, int port, int offlineId, bool precompute, double coeff)
{
	Agent* agent = nullptr;

	switch (et)
	{
	case ET_AMBULANCE_TEAM:
		agent = new AmbulanceTeamAgent();
		break;
	case ET_AMBULANCE_CENTER:
		agent = new AmbulanceCenterAgent();
		break;
	case ET_FIRE_BRIGADE:
		agent = new FireBrigadeAgent();
		break;
	case ET_FIRE_STATION:
		agent = new FireStationAgent();
		break;
	case ET_POLICE_FORCE:
		agent = new PoliceForceAgent();
		break;
	case ET_POLICE_OFFICE:
		agent = new PoliceOfficeAgent();
		break;
	default:
		break;
	}

	agent->isPrecomputeAgent = precompute;
	agent->initData();

	if (ip != "offline")
		agent->setConnectionParameters(ip, port);

	int retVal = agent->initConnection(et, offlineId);

	if (retVal < 0)
		return retVal;

	agent->run(coeff);

	return 0;
}

EntityType parseArgs(char arg)
{
	switch (arg)
	{
	case FB:
		return ET_FIRE_BRIGADE;
		break;
	case FS:
		return ET_FIRE_STATION;
		break;
	case AT:
		return ET_AMBULANCE_TEAM;
		break;
	case AC:
		return ET_AMBULANCE_CENTER;
		break;
	case PF:
		return ET_POLICE_FORCE;
		break;
	case PO:
		return ET_POLICE_OFFICE;
		break;
	default:
		return ET_UNKNOWN;
	}
}

int main(int argc, char *argv[])
{
	if (argc < 2)
	{
		cout << "Usage: ./ type{FB=1 FS=2 PF=3 PO=4 AT=5 AC=6} [ip{default=0}]" << endl;
		return 0;
	}

	EntityType type = parseArgs(argv[1][0]);
	string ip = "0";
	int offlineId = -1;
	bool precompute = false;
	double coeff = 0;

	if (argc >= 3)
	{
		ip = argv[2];
		if (ip == "offline")
		{
			if (argc >= 4)
			{
				sscanf(argv[3], "%d", &offlineId);
				cout << "Offline playing for agent with id: " << offlineId << endl;
				if (argc >= 5)
				{
					string pc = "";
					pc = argv[4];
					if (pc == "--precompute")
					{
						precompute = true;
					}
				}
			}
			else
			{
				cout << "OFFLINE USAGE: ./type{FB=1 FS=2 PF=3 PO=4 AT=5 AC=6} offline id" << endl;
				return 0;
			}

		}
		else if (argc >= 4)
		{
			string pc = "";
			pc = argv[3];
			if (pc == "--precompute")
			{
				precompute = true;
			}
			else
			{
				coeff = atof(argv[3]);
			}
		}
	}

	return runAgent(type, ip, 0, offlineId, precompute, coeff);
}
