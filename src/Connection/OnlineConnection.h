#ifndef __CONNECTION_H_
#define __CONNECTION_H_

#include "TCPSocket.h"
#include "AbstractConnection.h"
#include <string>
#include <vector>
#include <chrono>

using namespace std;

typedef unsigned char byte;

class OnlineConnection: public AbstractConnection
{
protected:
	std::string mHost;
	unsigned mPort;

	bool isDelete;

	bool selectInput();

public:
	TCPSocket mSocket;

public:
	OnlineConnection(const std::string& = "127.0.0.1", unsigned = 3100);
	virtual ~OnlineConnection();

	static OnlineConnection * instance();

	bool init();
	bool sendMessage(const std::vector<byte> &msg);
	bool getMessage(std::vector<byte> &msg);

	void done();
};

#endif //__CONNECTION_H_
