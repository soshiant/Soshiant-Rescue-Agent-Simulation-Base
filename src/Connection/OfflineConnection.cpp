#include "OfflineConnection.h"

using namespace std;

OfflineConnection::OfflineConnection(string fileDir)
{
	in.open(fileDir.c_str(), ifstream::in);
}

bool OfflineConnection::init()
{
	return true;
}

OfflineConnection::~OfflineConnection()
{
	in.close();
}

bool OfflineConnection::getMessage(std::vector<byte>& msg)
{
	u_int size = 0;
	if (in.eof())
		return false;
	msg.clear();
	in >> size;
	char *message = new char[size];
	in.read(message, 1);
	if (in.eof())
		return false;
	in.read(message, size);
	msg.assign(message, message + size);
	return true;
}

bool OfflineConnection::sendMessage(const std::vector<byte>& msg)
{
	return true;
}

void OfflineConnection::done()
{
	in.close();
}
