#ifndef _MESSAGE_H
#define _MESSAGE_H

#include <vector>
#include "../Utilities/Types.h"

class Message
{
public:
	Message();
	virtual ~Message();

	void setAKConnect(Types::EntityType agentType, int tempID);
	void setAKACKnowledge(int tempId, int id);
	std::vector<byte> getMessage();

	void setAKClear(int selfID, int time, int targetID);
	void setAKClearArea(int selfID, int time, int targetX, int targetY);
	void setAKExtinguish(int selfID, int time, int targetID, int water);
	void setAKLoad(int selfID, int time, int targetID);
	void setAKMove(int selfID, int time, std::vector<int> &path, int destinationX, int destinationY);
	void setAKRescue(int selfID, int time, int targetID);
	void setAKRest(int selfID, int time);
	void setAKSay(int selfID, int time, std::string sayMessage);
	void setAKSpeak(int selfID, int time, int channel, std::vector<byte> speakMessage);
	void setAKSubscribe(int selfID, int time, std::vector<int> channel);
	void setAKTell(int selfID, int time, std::string tellMessage);
	void setAKUnload(int selfID, int time);

private:
	std::vector<byte> message;

	std::vector<byte> getAKConnectContent(Types::EntityType agentType, int tempID);
};

#endif	/* _MESSAGE_H */

