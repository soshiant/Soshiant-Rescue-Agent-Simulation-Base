#include "WorldModel.h"
#include "../Utilities/Geometry/Circle.h"
#include "../Utilities/FireEstimator/Wall.h"

#define LOGLEVEL 11

#define MAP_RECOGNITION_RANGE 5
#define MAX_CYCLE_FOR_NOTING_AVAILABLE_NORMAL 15
#define MAX_CYCLE_FOR_NOTING_AVAILABLE_NO_COMMI 40
#define HP_DAMAGE_HISTORY_SIZE 30
#define MAX_SIGHT_LENGTH_MAGIC_NUMBER 0

using namespace std;
using namespace Types;
using namespace Geometry;
using namespace Encodings;

WorldModel::WorldModel(Config &config) :
		teamConfig(config)
{
	self = new RCRObject(0);
	this->m_time = 0;
	world = this;
	worldBound = nullptr;
}

string WorldModel::init(vector<byte> message, EntityType type, bool amIThePreComputer)
{
	string result = Parser::analyzeMessage(world, message);
	//	LOG(Main, 11) << "Before Complete" << endl;
	complete(type, amIThePreComputer);
	//	LOG(Main, 11) << "After Complete" << endl;
	return result;
}

void WorldModel::complete(EntityType type, bool amIThePreComputer)
{
	if (type == ET_FIRE_BRIGADE)
	{
		for (int i = 0; i < (int) fireBrigades.size(); i++)
			if (this->selfID == (int) fireBrigades[i]->getId())
				self = fireBrigades[i];
	}
	else if (type == ET_FIRE_STATION)
	{
		for (int i = 0; i < (int) fireStations.size(); i++)
			if (this->selfID == (int) fireStations[i]->getId())
				self = fireStations[i];
	}
	else if (type == ET_AMBULANCE_TEAM)
	{
		for (int i = 0; i < (int) ambulanceTeams.size(); i++)
			if (this->selfID == (int) ambulanceTeams[i]->getId())
				self = ambulanceTeams[i];
	}
	else if (type == ET_AMBULANCE_CENTER)
	{
		for (int i = 0; i < (int) ambulanceCenters.size(); i++)
			if (this->selfID == (int) ambulanceCenters[i]->getId())
				self = ambulanceCenters[i];
	}
	else if (type == ET_POLICE_FORCE)
	{
		for (int i = 0; i < (int) policeForces.size(); i++)
			if (this->selfID == (int) policeForces[i]->getId())
				self = policeForces[i];
	}
	else if (type == ET_POLICE_OFFICE)
	{
		for (int i = 0; i < (int) policeOffices.size(); i++)
			if (this->selfID == (int) policeOffices[i]->getId())
				self = policeOffices[i];
	}

	if (self->isPlatoon())
	{
		((Platoon *) self)->amIThePreComputer = amIThePreComputer;
	}

	sortAndSetObjectsIndexes();
	updatePositions();

	//////// Map Recognition /////////
	setMapName();
	//////// Map Recognition /////////

	if (amIThePreComputer)
	{
		generateCloserThan();
	}

	maxSightRange = atoi(config["perception.los.max-distance"].c_str());
	rayCount = (config.find("perception.los.ray-count") != config.end()) ? atoi(config["perception.los.ray-count"].c_str()) : 72;
	maxFireTank = atoi(world->config["fire.tank.maximum"].c_str());
}

void WorldModel::sortAndSetObjectsIndexes()
{
	sort(ambulanceCenters.begin(), ambulanceCenters.end(), RCRObjectCompare);
	for (int i = 0; i < (int) ambulanceCenters.size(); i++)
		ambulanceCenters[i]->ambulanceCenterIndex = i;
	sort(ambulanceTeams.begin(), ambulanceTeams.end(), RCRObjectCompare);
	for (int i = 0; i < (int) ambulanceTeams.size(); i++)
		ambulanceTeams[i]->ambulanceTeamIndex = i;
	sort(fireBrigades.begin(), fireBrigades.end(), RCRObjectCompare);
	for (int i = 0; i < (int) fireBrigades.size(); i++)
		fireBrigades[i]->fireBrigadeIndex = i;
	sort(fireStations.begin(), fireStations.end(), RCRObjectCompare);
	for (int i = 0; i < (int) fireStations.size(); i++)
		fireStations[i]->fireStationIndex = i;
	sort(policeForces.begin(), policeForces.end(), RCRObjectCompare);
	for (int i = 0; i < (int) policeForces.size(); i++)
		policeForces[i]->policeForceIndex = i;
	sort(policeOffices.begin(), policeOffices.end(), RCRObjectCompare);
	for (int i = 0; i < (int) policeOffices.size(); i++)
		policeOffices[i]->policeOfficeIndex = i;
	sort(humans.begin(), humans.end(), RCRObjectCompare);
	for (int i = 0; i < (int) humans.size(); i++)
		humans[i]->humanIndex = i;
	sort(platoons.begin(), platoons.end(), RCRObjectCompare);
	for (int i = 0; i < (int) platoons.size(); i++)
		platoons[i]->platoonIndex = i;
	sort(buildings.begin(), buildings.end(), RCRObjectCompare);
	for (int i = 0; i < (int) buildings.size(); i++)
		buildings[i]->buildingIndex = i;
	sort(gasStations.begin(), gasStations.end(), RCRObjectCompare);
	for (int i = 0; i < (int) gasStations.size(); i++)
		gasStations[i]->gasStationIndex = i;
	sort(roads.begin(), roads.end(), RCRObjectCompare);
	for (int i = 0; i < (int) roads.size(); i++)
		roads[i]->roadIndex = i;
	sort(hydrants.begin(), hydrants.end(), RCRObjectCompare);
	for (int i = 0; i < (int) hydrants.size(); i++)
		hydrants[i]->hydrantIndex = i;
	sort(refuges.begin(), refuges.end(), RCRObjectCompare);
	for (int i = 0; i < (int) refuges.size(); i++)
		refuges[i]->refugeIndex = i;
	sort(motionlessObjects.begin(), motionlessObjects.end(), RCRObjectCompare);
	for (int i = 0; i < (int) motionlessObjects.size(); i++)
		motionlessObjects[i]->motionlessIndex = i;
	sort(humans.begin(), humans.end(), RCRObjectCompare);
	for (int i = 0; i < (int) humans.size(); i++)
		humans[i]->humanIndex = i;

	for (int i = 0; i < (int) motionlessObjects.size(); i++)
	{
		for (int j = 0; j < (int) motionlessObjects[i]->getIDs().size(); j++)
		{
			if (motionlessObjects[i]->getIDs()[j] != 0)
			{
				MotionlessObject *tmp = (MotionlessObject*) objects[motionlessObjects[i]->getIDs()[j]];
				motionlessObjects[i]->setID(j, tmp->motionlessIndex);
				motionlessObjects[i]->addNeighbour(tmp->motionlessIndex);
			}
			else
			{
				motionlessObjects[i]->setID(j, -1);
			}
		}
	}
}

string WorldModel::update(vector<byte> msg)
{
	LOG(Main, 3) << "updating starts!" << endl;
	string result = Parser::analyzeMessage(world, msg);
	LOG(Main, 3) << "sense message analyzed!" << endl;

	sort(civilians.begin(), civilians.end(), RCRObjectCompare);
	for (u_int i = 0; i < civilians.size(); i++)
	{
		civilians[i]->civilianIndex = i;
	}
	LOG(Main, 11) << "civ updated!" << endl;
	computeVisionRays();
	LOG(Main, 11) << "vision rays computed!" << endl;
	updatePositions();
	LOG(Main, 11) << "positions updated!" << endl;
	updateAgents();
	LOG(Main, 11) << "dead agents updated!" << endl;
	setLastCylceUpdatesOfCiviliansPlace();
	LOG(Main, 11) << "last cycle updated!" << endl;
	updateBlockades();
	LOG(Main, 11) << "blocakdes updated!" << endl;
	increaseBlockades();
	LOG(Main, 11) << "blocakdes increased!" << endl;
	setHasBeenSeens();
	LOG(Main, 11) << "has been seen updated!" << endl;
	updateHpAndDamageHistory();
	LOG(Main, 11) << "damages and healths updated!" << endl;
	updateInnerHumans();
	LOG(Main, 11) << "humans in motionless updated!" << endl;
	updatePassingModesAndRepresentiveNodes();
	LOG(Main, 11) << "passing mode updated!" << endl;
	updateCivilians();
	LOG(Main, 11) << "available civilians updated!" << endl;
	updateMotionlessInnerHumans();
	LOG(Main ,11) << "civilian in motionless updated!" << endl;
	updateBuildings();
	LOG(Main ,11) << "buildings updated!!" << endl;
	updateRoads();
	LOG(Main ,11) << "roads updated!!" << endl;
	//	setRoadsValidParts();
	//	LOG(Main, 11) << "roadsValidParts updated!!" << endl;
	updateInSightObjects();
	LOG(Main ,11) << "inSightObjects updated!!" << endl;
	updateBuildingsSearchStates();
	LOG(Main ,11) << "buildingsSearchState updated!!" << endl;
	return result;
}

void WorldModel::computeVisionRays()
{
	visionRays.clear();

	vector<Building*> inRangeBuildings = getBuildingsInRange(self->getPos(), maxSightRange);
	const double ANGLE = 2. * double(M_PI) / double(rayCount);

	for (int i = 0; i < rayCount; i++)
	{
		Segment ray(self->getPos(), ANGLE * i, maxSightRange);

		Building* nearestBuilding = nullptr;
		double nearestDistance = MAX_DOUBLE;

		for (Building* building : inRangeBuildings)
		{
			double tmpNearestDistance = MAX_DOUBLE;

			for (int j = 0; j < building->getShape().size(); j++)
			{
				if (building->getServerIDs()[j] == 0) // If it wasn't entrance
				{
					if (Geometry::isIntersect(building->getShape().getSegment(j), ray)
							&& !isParallel(ray.asLine(), building->getShape().getSegment(j).asLine()))
					{
						double tmp = Geometry::getIntersectPoint(building->getShape().getSegment(j).asLine(), ray.asLine()).distFromPoint(
								ray.getFirstPoint());
						if (tmp < tmpNearestDistance)
						{
							tmpNearestDistance = tmp;
						}
					}
				}
			}
			if (tmpNearestDistance != MAX_DOUBLE && tmpNearestDistance <= nearestDistance)
			{
				nearestDistance = tmpNearestDistance;
				nearestBuilding = building;
			}
		}

		if (nearestBuilding == nullptr)
		{
			visionRays.push_back(ray);
		}
		else
		{
			visionRays.push_back(Segment(ray.getFirstPoint(), ray.getTheta() * DEG2RAD, nearestDistance));
		}
	}
}

void WorldModel::removeAllBlockades()
{
	for (Road* road : roads)
	{
		road->setLastCycleBlockades(road->getBlockadeIds().size());
		for (Blockade* blockade : road->getBlockades())
		{
			objects[blockade->getId()] = nullptr;
			delete blockade;
		}
		road->getBlockades().clear();
		road->getBlockadeIds().clear();
	}

	blockades.clear();
}

void WorldModel::updateInSightObjects()
{
	vector<RCRObject*> inRangeObjects = getObjectsInRange(self->getPos(), maxSightRange, true);
	inSightObjects.clear();

	for (Segment ray : visionRays)
	{
		for (RCRObject* inSight : getIntersectedObjects(ray, inRangeObjects, true))
		{
			inSightObjects.push_back(inSight);
		}
	}

	inSightObjects.resize(unique(inSightObjects.begin(), inSightObjects.end()) - inSightObjects.begin());

	for (RCRObject* object : inSightObjects)
	{
		object->lastTimeSeen = getTime();

		if (object->isMotionlessObject())
		{
			object->setLastCycleUpdatedBySense(world->getTime());
		}
	}
}

void WorldModel::updateRoads() //TODO check
{
	if (radar->getRadarMode() == RM_NO_COMMUNICATION || radar->getRadarMode() == RM_LOW)
	{
		for (int i = 0; i < (int) roads.size(); i++)
		{
			if (getTime() - roads[i]->getLastCycleUpdated() >= 30 && roads[i]->getHasToBeSent() != -1)
			{
				Road * thisRoad = roads[i];

				for (int j = 0; j < (int) thisRoad->getRelativeEdgesIndexes().size(); j++)
				{
					worldGraph->getEdges()[thisRoad->getRelativeEdgesIndexes()[j]]->setPassingMode(PM_UNKNOWN);
				}

				for (int j = 0; j < (int) thisRoad->getRelativeEdgesIndexes().size(); j++)
				{
					worldGraph->getEdges()[thisRoad->getRelativeEdgesIndexes()[j]]->setPassingMode(PM_UNKNOWN);
				}

				thisRoad->setHasToBeSent(-1);
			}
		}
	}
}

void WorldModel::updateBuildings()
{
	for (Building* b : buildings)
	{
		if (b->getLastCycleUpdatedBySense() == getTime())
		{
			setHardToExtinguish(b);
		}

		b->fierynessDuringTime.push_back(b->getFieryness());

		if (b->fierynessDuringTime.size() > 3)
		{
			b->fierynessDuringTime.erase(b->fierynessDuringTime.begin() + 0);
		}
	}
}

bool WorldModel::isMapLarge()
{
	return getMapName() == MN_BERLIN || getMapName() == MN_ISTANBUL || getMapName() == MN_PARIS || getMapName() == MN_MEXICO
			|| getMapName() == MN_EINDHOVEN || getMapName() == MN_TEHRAN;
}

void WorldModel::setHardToExtinguish(Building * b)
{
	if (b->hasFire() == false)
	{
		b->setHardToExtinguish(false);
	}

	b->setHardToExtinguish(false);

	if (isMapLarge())
	{
		if (b->getTemperature() >= 500 && b->getAreaTotal() >= 5000)
		{
			b->setHardToExtinguish(true);
		}
	}
	else
	{
		if (b->getTemperature() >= 500 && b->getAreaTotal() >= 1500)
		{
			b->setHardToExtinguish(true);
			return;
		}
	}
}

void WorldModel::updateHpAndDamageHistory()
{
	for (int i = 0; i < (int) humans.size(); i++)
	{
		//		if (humans[i]->getPositionHistory()) /// TODO Forut yadesh biad in vase chie

		if (humans[i]->getLastCycleUpdated() == getTime())
		{
			DamageDuringTheTime dt;
			HpDuringTheTime ht;
			BuriednessDuringTime bt;

			dt.time = getTime();
			ht.time = getTime();
			bt.time = getTime();

			dt.damage = humans[i]->getDamage();
			ht.hp = humans[i]->getHp();
			bt.buriedness = humans[i]->getBuriedness();

			humans[i]->addDamageHistory(dt);
			humans[i]->addHpHistory(ht);
			humans[i]->addBuriednessHistory(bt);

			if (humans[i]->getDamageHistory().size() >= HP_DAMAGE_HISTORY_SIZE)
			{
				humans[i]->deleteFirstDamageHistory();
			}

			if (humans[i]->getHpHistory().size() >= HP_DAMAGE_HISTORY_SIZE)
			{
				humans[i]->deleteFirstHpHistory();
			}

			if (humans[i]->getBuriednessHistory().size() >= 10)
			{
				humans[i]->deleteFirstBuriednessHistory();
			}
		}
	}

	if (self->isPlatoon())
	{
		Platoon * me = ((Platoon *) self);

		if (me->getMotionlessObject()->isRefuge())
		{
			me->clearHpHistory();
			me->clearDamageHistory();
			me->clearBuriednessHistory();
		}
	}
}

void WorldModel::setWorldGraph(WorldGraph *worldGraph)
{
	this->worldGraph = worldGraph;
}

void WorldModel::setLastCylceUpdatesOfCiviliansPlace()
{
	for (int i = 0; i < (int) civilians.size(); i++)
	{
		if (civilians[i]->isAvailable == false)
		{
			continue;
		}

		if (civilians[i]->getLastCycleUpdated() == world->getTime())
		{
			civilians[i]->getMotionlessObject()->setLastCycleUpdated(world->getTime());
		}
	}
}

void WorldModel::updatePositions()
{
	for (Human* human : platoons)
	{
		if (human->getMotionlessObject() == nullptr || human->getMotionlessObject()->getId() != human->getPosition())
		{
			human->setMotionlessObject((MotionlessObject*) objects[human->getPosition()]);
		}
	}

	for (Human* human : humans)
	{
		if (human->isCivilian())
		{
			bool flag = false;
			for (int j = 0; j < (int) ambulanceTeams.size(); j++)
			{
				if (ambulanceTeams[j]->getId() == human->getPosition())
				{
					human->setMotionlessObject((MotionlessObject*) objects[ambulanceTeams[j]->getPosition()]);
					human->isAvailable = false; // the civilian is in an ambulance so it is not available
					flag = true;
					break;
				}
			}

			if (human->getMotionlessObject() == nullptr || (human->getMotionlessObject()->getId() != human->getPosition() && !flag))
			{
				human->setMotionlessObject((MotionlessObject*) objects[human->getPosition()]);
			}
			((Civilian*) human)->setDistanceToCenterSize(ceil(log2l(human->getFirstTimeMotionless()->getMaxDistanceFromCenter() / 400)));
		}
		else if (human->getLastCycleUpdatedBySense() != getTime())
		{
			human->setX(human->getMotionlessObject()->getX());
			human->setY(human->getMotionlessObject()->getY());
		}
	}

	if (self->isPlatoon())
	{
		((Platoon*) self)->getMotionlessObject()->iHaveBeenHere = true;
	}

	for (int i = 0; i < (int) platoons.size(); i++)
	{
		if (platoons[i]->isAvailable == false)
		{
			continue;
		}

		platoons[i]->positionHistory.push_back(platoons[i]->getMotionlessObject());

		if (platoons[i]->positionHistory.size() > 10)
		{
			platoons[i]->positionHistory.erase(platoons[i]->positionHistory.begin());
		}
	}
}

void WorldModel::updateBlockades()
{
	for (Blockade* blockade : blockades)
	{
		if (blockade->getLastCycleUpdated() != getTime())
		{
			LOG(Main, 11) << "I cleared blockade: " << blockade->getId() << endl;
			Road *road = (Road*) objects[blockade->getPosition()];

			for (u_int i = 0; i < road->getBlockades().size(); i++)
			{
				if (road->getBlockades()[i]->getId() == blockade->getId())
				{
					road->getBlockades().erase(road->getBlockades().begin() + i);
					break;
				}
			}
			objects[blockade->getId()] = nullptr;
			blockades.erase(blockade);
		}
	}

	for (int i = 0; i < (int) roads.size(); i++)
	{
		for (int j = 0; j < PASSING_CHECK_LEVELS; j++)
		{
			roads[i]->validParts[j].clear();
		}

		if (roads[i]->getLastCycleUpdatedBySense() == getTime())
		{
			roads[i]->setLastCycleBlockades(roads[i]->getBlockadeIds().size());
			roads[i]->clearBlockades();
			for (int j = 0; j < (int) roads[i]->getBlockadeIds().size(); j++)
			{
				if ((Blockade*) objects[roads[i]->getBlockadeIds()[j]] == nullptr)
				{
					LOG(Main, 11) << "Blockade is null " << roads[i]->getId() << endl;
					roads[i]->getBlockadeIds().erase(roads[i]->getBlockadeIds().begin() + j);
					j--;
					continue;
				}
				roads[i]->addBlockade((Blockade*) objects[roads[i]->getBlockadeIds()[j]]);
			}
		}
		else
		{
			roads[i]->clearBlockades();
		}
	}
}

void WorldModel::increaseBlockades()
{
	for (Blockade* blockade : blockades)
	{
		blockade->increasedShapes[0] = Geometry::increasePolygon(blockade->getShape(), BLOCKADE_INCREASE_SIZE_L1);
		blockade->increasedShapes[1] = Geometry::increasePolygon(blockade->getShape(), BLOCKADE_INCREASE_SIZE_L2);
	}
}

void WorldModel::setHasBeenSeens()
{
	/*
	 * TODO:
	 *
	 * idea 12 must be added here
	 *
	 */

	if (self->isPlatoon())
	{
		for (Segment ray : visionRays)
		{
			for (MotionlessObject* mo : ((Platoon*) self)->getMotionlessObject()->closerThan30B)
			{
				if (Geometry::distanceToSegment(ray, mo->getPos()) < 200)
				{
					((Building*) mo)->lastTimeSearchedForCivilian = getTime();
				}
//				if (mo->getServerIDs()[i] != 0 && Geometry::isIntersect(mo->getShape().getSegment(i), ray) && Geometry::distanceToSegment(ray, mo->getPos()) < AGENT_RADIUS)
//				{
//				}
			}
		}
	}

	for (int i = 0; i < (int) platoons.size(); i++)
	{
		MotionlessObject* position = platoons[i]->getMotionlessObject();
		position->lastTimeSeen = getTime();

		if (position->isBuilding())
		{
			position->lastTimeSeen = getTime();
			((Building*) position)->lastTimeSearchedForCivilian = getTime();
		}
		else if (position->isRoad())
		{
			for (int j = 0; j < (int) position->closerThan30R.size(); j++)
			{
				Road * thisRoad = (Road*) position->closerThan30R[j];
				if (thisRoad->getPos().distFromPoint(position->getPos()) < 1000)
				{
					thisRoad->lastTimeSeen = getTime();
				}
			}
		}
	}
}

void WorldModel::updateCivilians()
{
	for (int i = 0; i < (int) civilians.size(); i++)
	{
		LOG(Main , 1) << "check:	" << civilians[i]->getId() << "   " << civilians[i]->isAvailable << " " << civilians[i]->getPosition()
				<< endl;

		if (civilians[i]->getDamage() <= 0 && civilians[i]->getBuriedness() > 0)
		{
			int damagePerception = atoi(config[Encodings::getConfigType(CT_DAMAGE_PERCEPTION)].c_str());
			civilians[i]->setDamage(damagePerception / 3);
		}

		if (civilians[i]->getLastCycleUpdatedBySense() != getTime())
		{
			if (civilians[i]->isAvailable)
			{
				civilians[i]->setTimeToDeath(civilians[i]->getTimeToDeath() - 1);
			}

			if (self->isPlatoon()
					&& ((Platoon *) world->self)->getMotionlessObject()->getId() == civilians[i]->getMotionlessObject()->getId()) //TODO Use Vision
			{
				if (radar->getRadarMode() == RM_NO_COMMUNICATION || radar->getRadarMode() == RM_LOW) //XXX Tof felan bashe in!!
				{
					civilians[i]->setLastCycleUpdated(getTime());
				}
				civilians[i]->isAvailable = false;
			}

			continue;
		}

		if (civilians[i]->getHp() < 1 && civilians[i]->getFirstDeathSignal() == -1)
		{
			civilians[i]->setFirstDeathSignal(getTime());
		}

		if (civilians[i]->getMotionlessObject()->isRefuge())
		{
			civilians[i]->isAvailable = false;
		}

		if (civilians[i]->getMotionlessObject()->isRoad() && civilians[i]->getBuriedness() == 0)
		{
			civilians[i]->isAvailable = false;
		}

		if (civilians[i]->getHp() < 1)
		{
			civilians[i]->isAvailable = false;
		}

		for (int j = 0; j < (int) ambulanceTeams.size(); j++)
		{
			if (world->civilians[i]->getPosition() == ambulanceTeams[j]->getId())
			{
				civilians[i]->isAvailable = false;
			}
		}
		if (civilians[i]->isAvailable)
		{
			civilians[i]->calculateTimeToDeath(getTime());
		}

		world->fakeCivilians[civilians[i]->getFakeId()] = civilians[i];
	}
}

void WorldModel::updateMotionlessInnerHumans()
{
	for (int i = 0; i < (int) motionlessObjects.size(); i++)
	{
		motionlessObjects[i]->clearInsideCivilians();
		motionlessObjects[i]->clearInsidePlatoons();
	}

	for (int i = 0; i < (int) civilians.size(); i++)
	{
		if (civilians[i]->isAvailable)
		{
			civilians[i]->getMotionlessObject()->addInsideCivilianIndex(civilians[i]->civilianIndex);
		}
	}

	for (int i = 0; i < (int) platoons.size(); i++)
	{
		if (platoons[i]->isAvailable)
		{
			platoons[i]->getMotionlessObject()->addInsidePlatoonIndex(platoons[i]->platoonIndex);
		}
	}
}

void WorldModel::updateBuildingsSearchStates()
{
	for (Building* building : buildings)
	{
		building->clearSearchStates();

		if (building->lastTimeSearchedForCivilian != -1)
		{
			building->addSearchStates(BSS_SEARCHED);
		}
		else
		{
			building->addSearchStates(BSS_NOT_SEARCHED);
		}

		/*		if (isMapLarge())
		 {
		 if (getTime() - building->lastTimeSearchedForCivilian > 50 && !building->getInsidePlatoonIndexes().empty())
		 {
		 building->addSearchStates(BSS_NEED_UPDATE);
		 }
		 }
		 else
		 {
		 if (getTime() - building->lastTimeSearchedForCivilian > 30 && !building->getInsidePlatoonIndexes().empty())
		 {
		 building->addSearchStates(BSS_NEED_UPDATE);
		 }
		 }
		 */
		Building* isInFire = nullptr;
		for (u_int j = 0; j < building->closerThan30B.size(); j++)
		{
			isInFire = (Building*) building->closerThan30B[j];
			if (isInFire->getTemperature() > 0 && world->getTime() - building->lastTimeSeen < 15)
			{
				building->addSearchStates(BSS_CLOSE_2_FIRE);
				break;
			}
		}

		if (building->getTemperature() > 0 && (world->getTime() - building->lastTimeSeen) < 15)
		{
			building->addSearchStates(BSS_HAS_FIRE);
		}

		if (building->getCivilianProbability() > 0)
		{
			building->addSearchStates(BSS_CLOSE_2_CIVILIANS_SHOUT);
		}
	}
}

void WorldModel::setInitialDistanceFindingObjects()
{
	if (self->isPoliceForce())
	{
		for (int i = 0; i < (int) policeForces.size(); i++)
		{
			worldGraph->addDistanceFindingHumanIndex(policeForces[i]->humanIndex);
		}
		for (int i = 0; i < (int) refuges.size(); i++)
		{
			worldGraph->addDistanceFindingMotiolessIndex(refuges[i]->motionlessIndex);
		}
	}
	else if (self->isAmbulanceTeam())
	{
		for (int i = 0; i < (int) ambulanceTeams.size(); i++)
		{
			worldGraph->addDistanceFindingHumanIndex(ambulanceTeams[i]->humanIndex);
		}
		for (int i = 0; i < (int) refuges.size(); i++)
		{
			worldGraph->addDistanceFindingMotiolessIndex(refuges[i]->motionlessIndex);
		}
	}
	else if (self->isFireBrigade())
	{
		for (int i = 0; i < (int) fireBrigades.size(); i++)
		{
			worldGraph->addDistanceFindingHumanIndex(fireBrigades[i]->humanIndex);
		}
		for (int i = 0; i < (int) refuges.size(); i++)
		{
			worldGraph->addDistanceFindingMotiolessIndex(refuges[i]->motionlessIndex);
		}
	}
}

void WorldModel::updateAgents()
{
	if (radar->getRadarMode() == RM_NO_COMMUNICATION || radar->getRadarMode() == RM_LOW)
	{
		for (int i = 0; i < (int) platoons.size(); i++)
		{
			if (self->isPlatoon() && platoons[i] != self)
			{
				LOG(Main, 11) << ((Platoon*) self)->getMotionlessObject()->getId() << ", "
						<< platoons[i]->getMotionlessObject()->getId() << endl;
				if (((Platoon*) self)->getMotionlessObject()->getId() == platoons[i]->getMotionlessObject()->getId())
				{
					LOG(Main, 11) << "1" << endl;
				}
				if (platoons[i]->getLastCycleUpdatedBySense() != getTime())
				{
					LOG(Main, 11) << "2" << endl;
				}

				if (((Platoon*) self)->getMotionlessObject() == platoons[i]->getMotionlessObject()
						&& platoons[i]->getLastCycleUpdatedBySense() != getTime())
				{
					LOG(Main, 11) << "Reseting node index for: " << platoons[i]->getId() << endl;
					platoons[i]->setRepresentiveNodeIndex(platoons[i]->getWithoutBlockadeRepresentiveNodeIndex());
					platoons[i]->setBuriedness(0);
				}
			}
			if (getTime() - platoons[i]->getLastCycleUpdated() >= 30)
			{
				platoons[i]->setBuriedness(0);
			}

			if (world->isNoComm())
			{
				if (platoons[i]->getDamage() == 0 && platoons[i]->getBuriedness() > 0)
				{
					int damagePerception = atoi(config[Encodings::getConfigType(CT_DAMAGE_PERCEPTION)].c_str());
					platoons[i]->setDamage(damagePerception / 3);
				}
//				if (getTime() - platoons[i]->getLastCycleUpdated() > MAX_CYCLE_FOR_NOTING_AVAILABLE_NO_COMMI && platoons[i]->getId() != self->getId())
//				{
//					platoons[i]->isAvailable = false;
//				}
//				else
//				{
//					platoons[i]->isAvailable = true;
//				}
			}
		}
	}
	else
	{
		for (int i = 0; i < (int) platoons.size(); i++)
		{
			if (platoons[i]->getDamage() == 0 && platoons[i]->getBuriedness() > 0)
			{
				int damagePerception = atoi(config[Encodings::getConfigType(CT_DAMAGE_PERCEPTION)].c_str());
				platoons[i]->setDamage(damagePerception / 3);
			}

			if (platoons[i]->isAvailable == false)
			{
				continue;
			}

			if (platoons[i]->getLastCycleUpdatedBySense() == getTime() && platoons[i]->getHp() == 0)
			{
				platoons[i]->isAvailable = false;
				LOG(Main , 1) << platoons[i]->getId() << " ";
			}

			if (getTime() - platoons[i]->getLastCycleUpdated() > MAX_CYCLE_FOR_NOTING_AVAILABLE_NORMAL)
			{
				platoons[i]->isAvailable = false;
				LOG(Main , 1) << platoons[i]->getId() << " ";
			}
		}
	}
}

void WorldModel::updateInnerHumans()
{
	for (int i = 0; i < (int) motionlessObjects.size(); i++)
	{
		motionlessObjects[i]->clearInnerHumans();
	}

	for (int i = 0; i < (int) humans.size(); i++)
	{
		if (humans[i]->isCivilian() && ((Civilian *) humans[i])->isAvailable == false)
		{
			continue;
		}

//		if (humans[i]->getLastCycleUpdatedBySense() == m_time) // TODO Check This Shiet.
//		{
		humans[i]->getMotionlessObject()->addInnerHumanIndex(i);
//		}
//		else if (humans[i]->isCivilian())
//		{
//			LOG(Main, 11) << "#CIVILIAN#" << endl;
//			LOG(Main, 11) << "ID : " << humans[i]->getId();
//			LOG(Main, 11) << "MotionlessID : " << humans[i]->getMotionlessObject()->getId() << "  :  " << humans[i]->getPosition() << endl;
//		}
	}
}

void WorldModel::updatePassingModesAndRepresentiveNodes()
{
	if (!world->self->isPlatoon())
	{
		return;
	}

	for (int i = 0; i < (int) humans.size(); i++)
	{
		if (humans[i]->isCivilian() && !((Civilian*) humans[i])->isAvailable)
		{
			continue;
		}

		if (humans[i]->getLastCycleUpdatedBySense() == getTime())
		{
			worldGraph->updateHumanWithoutBlockadeRepresentiveNode(i);
		}
	}

	LOG(Main, 3) << "Update Passing Modes And Representive Nodes started." << endl;

	for (int mCounter = 0; mCounter < (int) motionlessObjects.size(); mCounter++)
	{
		if (motionlessObjects[mCounter]->getLastCycleUpdatedBySense() == getTime())
		{
			vector<int> lastTimePassing;
			for (int j = 0; j < (int) motionlessObjects[mCounter]->getRelativeEdgesIndexes().size(); j++)
			{
				lastTimePassing.push_back(
						worldGraph->getEdges()[motionlessObjects[mCounter]->getRelativeEdgesIndexes()[j]]->getPassingMode());
			}

			worldGraph->updatePassingModesAndRepresentiveNodes(mCounter);

			bool sendThisMotionless = false;

			for (int j = 0; j < (int) motionlessObjects[mCounter]->getRelativeEdgesIndexes().size(); j++)
			{
				int thisEdgePassing = worldGraph->getEdges()[motionlessObjects[mCounter]->getRelativeEdgesIndexes()[j]]->getPassingMode();
				if (thisEdgePassing != lastTimePassing[j])
				{
					worldGraph->getEdges()[motionlessObjects[mCounter]->getRelativeEdgesIndexes()[j]]->setHasToBeSent(getTime());
					sendThisMotionless = true;
					break;
				}
			}

			if (motionlessObjects[mCounter]->isRoad() && sendThisMotionless)
			{
				Road* thisRoad = ((Road*) motionlessObjects[mCounter]);
				thisRoad->setHasToBeSent(world->getTime());
			}

		}
	}

	LOG(Main, 3) << "Update Passing Modes And Representive Nodes finished." << endl;
}

void WorldModel::setTime(int time)
{
	this->m_time = time;
}

WorldModel::~WorldModel()
{
}

int WorldModel::getTime()
{
	return m_time;
}

void WorldModel::generateCloserThan()
{
	for (int i = 0; i < (int) motionlessObjects.size(); i++)
	{
		for (int j = 0; j < (int) motionlessObjects.size(); j++)
		{
			if (i == j)
			{
				continue;
			}

			if (motionlessObjects[i]->getPos().distFromPoint(motionlessObjects[j]->getPos()) < 30000)
			{
				motionlessObjects[i]->closerThan30M.push_back(motionlessObjects[j]);
			}

			if (motionlessObjects[i]->getPos().distFromPoint(motionlessObjects[j]->getPos()) < 50000)
			{
				motionlessObjects[i]->closerThan50M.push_back(motionlessObjects[j]);
			}

			if (motionlessObjects[i]->getPos().distFromPoint(motionlessObjects[j]->getPos()) < 30000 && motionlessObjects[j]->isBuilding())
			{
				motionlessObjects[i]->closerThan30B.push_back(motionlessObjects[j]);
			}

			if (motionlessObjects[i]->getPos().distFromPoint(motionlessObjects[j]->getPos()) < 50000 && motionlessObjects[j]->isBuilding())
			{
				motionlessObjects[i]->closerThan50B.push_back(motionlessObjects[j]);
			}

			if (motionlessObjects[i]->getPos().distFromPoint(motionlessObjects[j]->getPos()) < 30000 && motionlessObjects[j]->isRoad())
			{
				motionlessObjects[i]->closerThan30R.push_back(motionlessObjects[j]);
			}

			if (motionlessObjects[i]->getPos().distFromPoint(motionlessObjects[j]->getPos()) < 50000 && motionlessObjects[j]->isRoad())
			{
				motionlessObjects[i]->closerThan50R.push_back(motionlessObjects[j]);
			}
		}
	}
}

void WorldModel::setBuildingsInSightPoints()
{
	for (int bCounter = 0; bCounter < (int) buildings.size(); bCounter++)
	{
		for (int bpCounter = 0; bpCounter < (int) buildings[bCounter]->getShape().getVertices().size(); bpCounter++)
		{
			for (int rCounter = 0; rCounter < (int) buildings[bCounter]->closerThan30R.size(); rCounter++)
			{
				if (buildings[bCounter]->getShape().getVertices()[bpCounter].distFromPoint(
						buildings[bCounter]->closerThan30R[rCounter]->getPos()) > 27500)
				{
					continue;
				}

				Segment buildingToRoad(buildings[bCounter]->getShape().getVertices()[bpCounter],
						buildings[bCounter]->closerThan30R[rCounter]->getPos());
				bool canSeeFromThisRoad = true;

				for (int bnCounter = 0; bnCounter < (int) buildings[bCounter]->closerThan30B.size(); bnCounter++)
				{
					for (int bnpCounter = 0;
							bnpCounter < (int) buildings[bCounter]->closerThan30B[bnCounter]->getShape().getVertices().size(); bnpCounter++)
					{
						int thisPoint = bnpCounter;
						int nextPoint = bnpCounter + 1;

						if (bnpCounter == (int) buildings[bCounter]->closerThan30B[bnCounter]->getShape().getVertices().size() - 1)
						{
							nextPoint = 0;
						}

						Segment thisBuildingShape(buildings[bCounter]->closerThan30B[bnCounter]->getShape().getVertices()[thisPoint],
								buildings[bCounter]->closerThan30B[bnCounter]->getShape().getVertices()[nextPoint]);

						if (isIntersect(buildingToRoad, thisBuildingShape))
						{
							canSeeFromThisRoad = false;
							break;
						}
					}

					if (canSeeFromThisRoad == false)
					{
						break;
					}
				}

				if (canSeeFromThisRoad)
				{
					((Road *) buildings[bCounter]->closerThan30R[rCounter])->buildingWithThisRoadCanSee.push_back(
							buildings[bCounter]->buildingIndex);
					buildings[bCounter]->canSee.push_back((Road *) buildings[bCounter]->closerThan30R[rCounter]);
					//					LOG(Main, 11) << "bcounter : " << bCounter << " rCouunter : " << rCounter << " CT30R : " << buildings[bCounter]->closerThan30R.size() << " buildings size : " << buildings.size() << endl;
					//					LOG(Main, 11) << "bId : "<< buildings[bCounter]->getId() << endl;
					//					LOG(Main, 11) << "roads : " << roads.size() << " road:  "<< ((Road *) buildings[bCounter]->closerThan30R[rCounter])->getId() << " " << ((Road *) buildings[bCounter]->closerThan30R[rCounter])->getPos() << endl;
					//					Road* road = ((Road *) buildings[bCounter]->closerThan30R[rCounter]);
					//					int bIndex = buildings[bCounter]->buildingIndex;
					//					LOG(Main, 11) << "size : " << road->buildingWithThisRoadCanSee.size() << endl;
					//					road->buildingWithThisRoadCanSee.push_back(12);
				}
			}
		}
	}
}

void WorldModel::setCanseeBuildings()
{
	const int RAY_COUNT = rayCount / 2;
	const double ANGLE = 2. * (double) M_PI / RAY_COUNT;

	for (Building* building : buildings)
	{
		building->canSee.clear();
	}

	for (Road* road : roads)
	{
		road->buildingWithThisRoadCanSee.clear();

		// Set In Range Buildings
		vector<Building*> inRangeBuildings = getBuildingsInRange(road->getPos(), maxSightRange);
		vector<Building*> inSightBuildings;

		for (int i = 0; i < RAY_COUNT; i++)
		{
			Segment ray(road->getPos(), ANGLE * i, maxSightRange);
			Building* res = getIntersectedBuilding(ray, inRangeBuildings);
			if (res != nullptr)
			{
				inSightBuildings.push_back(res);
			}
		}

		for (Building* object : inSightBuildings)
		{
			if (std::find(road->buildingWithThisRoadCanSee.begin(), road->buildingWithThisRoadCanSee.end(),
					((Building*) object)->buildingIndex) == road->buildingWithThisRoadCanSee.end())
			{
				road->buildingWithThisRoadCanSee.push_back(object->buildingIndex);
				object->canSee.push_back(road);
			}
		}
	}
}

vector<RCRObject*> WorldModel::getInSightObjects()
{
	return inSightObjects;
}

vector<RCRObject*> WorldModel::getInSightObjects(Point point, int rayCount, bool includeBlockades)
{
	vector<RCRObject*> inRangeObjects = getObjectsInRange(point, maxSightRange + MAX_SIGHT_LENGTH_MAGIC_NUMBER, includeBlockades);
	const double ANGLE = 2. * (double) M_PI / rayCount;
	vector<RCRObject*> result;

	for (int i = 0; i < rayCount; i++)
	{
		Segment ray(point, ANGLE * i, maxSightRange);

		for (RCRObject* inSight : getIntersectedObjects(ray, inRangeObjects, includeBlockades))
		{
			result.push_back(inSight);
		}
	}

	return result;
}

Building* WorldModel::getIntersectedBuilding(Segment ray, vector<Building*> inRangeBuildings)
{
	Building* result = nullptr;
	double nearest = MAX_DOUBLE;

	for (Building* building : inRangeBuildings)
	{
		double nearest2 = MAX_DOUBLE;
		for (int i = 0; i < building->getShape().size(); i++)
		{
			if (Geometry::isIntersect(building->getShape().getSegment(i), ray)
					&& !isParallel(ray.asLine(), building->getShape().getSegment(i).asLine()))
			{
				double tmp = Geometry::getIntersectPoint(building->getShape().getSegment(i).asLine(), ray.asLine()).distFromPoint(
						ray.getFirstPoint());
				if (tmp < nearest2)
				{
					nearest2 = tmp;
				}
			}
		}

		if (nearest2 < nearest)
		{
			nearest = nearest2;
			result = building;
		}
	}

	return result;
}

vector<RCRObject*> WorldModel::getIntersectedObjects(Segment ray, vector<RCRObject*> inRanges, bool includeBlockades)
{
	vector<pair<RCRObject*, double>> intersected;
	for (RCRObject* object : inRanges)
	{
		double nearest = MAX_DOUBLE;

		if (object->isRoad())
		{
			for (int i = 0; i < ((Road*) object)->getShape().size(); i++)
			{
				if (Geometry::isIntersect(((Road*) object)->getShape().getSegment(i), ray)
						&& !isParallel(ray.asLine(), ((Road*) object)->getShape().getSegment(i).asLine()))
				{
					double tmp =
							Geometry::getIntersectPoint(((Road*) object)->getShape().getSegment(i).asLine(), ray.asLine()).distFromPoint(
									ray.getFirstPoint());
					if (tmp < nearest)
					{
						nearest = tmp;
					}
				}
			}
		}
		else if (object->isBuilding())
		{
			for (int i = 0; i < ((Building*) object)->getShape().size(); i++)
			{
				if (((Building*) object)->getServerIDs()[i] == 0) // If it wasn't entrance
				{
					if (Geometry::isIntersect(((Building*) object)->getShape().getSegment(i), ray)
							&& !isParallel(ray.asLine(), ((Building*) object)->getShape().getSegment(i).asLine()))
					{
						double tmp =
								Geometry::getIntersectPoint(((Building*) object)->getShape().getSegment(i).asLine(), ray.asLine()).distFromPoint(
										ray.getFirstPoint());
						if (tmp < nearest)
						{
							nearest = tmp;
						}
					}
				}
			}
		}
		else if (includeBlockades && object->isBlockade())
		{
			for (int i = 0; i < ((Blockade*) object)->getShape().size(); i++)
			{
				if (Geometry::isIntersect(((Blockade*) object)->getShape().getSegment(i), ray)
						&& !isParallel(ray.asLine(), ((Blockade*) object)->getShape().getSegment(i).asLine()))
				{
					double tmp =
							Geometry::getIntersectPoint(((Blockade*) object)->getShape().getSegment(i).asLine(), ray.asLine()).distFromPoint(
									ray.getFirstPoint());
					if (tmp < nearest)
					{
						nearest = tmp;
					}
				}
			}
		}

		if (nearest != MAX_DOUBLE)
		{
			pair<RCRObject*, double> p;
			p.first = object;
			p.second = nearest;
			intersected.push_back(p);
		}
	}

	// Sort intersected entity
	sort(intersected.begin(), intersected.end(), [](pair<RCRObject*, double> a, pair<RCRObject*, double> b)
	{
		return b.second > a.second;
	});

	vector<RCRObject*> res;
	for (pair<RCRObject*, double> object : intersected)
	{
		res.push_back(object.first);

		if (object.first->isBuilding())
		{
			break;
		}
	}

	return res;
}

vector<Building*> WorldModel::getBuildingsInRange(Point point, float distance)
{
	vector<Building*> result;
	for (Building* building : buildings)
	{
		for (int i = 0; i < building->getShape().size(); i++)
		{
			if (distanceToSegment(building->getShape().getSegment(i), point) <= distance)
			{
				result.push_back(building);
				break;
			}
		}
	}

	return result;
}

vector<RCRObject*> WorldModel::getObjectsInRange(Point point, float distance, bool includeBlockades)
{
	vector<RCRObject*> result;

	for (MotionlessObject* m : motionlessObjects)
	{
		if (m->getPos().distFromPoint(point) < distance)
		{
			result.push_back(m);
		}
		else if (m->getPos().distFromPoint(point) < distance + 20000)
		{
			for (int i = 0; i < m->getShape().size(); i++)
			{
				if (distanceToSegment(m->getShape().getSegment(i), point) <= distance)
				{
					result.push_back(m);
					break;
				}
			}
		}
	}
	//	for (Human* h : humans)
	//	{
	//		if (h->getPos().distFromPoint(object->getPos()) <= distance)
	//		{
	//			result.push_back(h);
	//		}
	//	}
	if (includeBlockades)
	{
		for (Blockade* b : blockades)
		{
			for (int i = 0; i < b->getShape().size(); i++)
			{
				if (distanceToSegment(b->getShape().getSegment(i), point) <= distance)
				{
					result.push_back(b);
					break;
				}
			}
		}
	}

	return result;
}

bool doesThisMapFit(int buildings, int roads, MapName map, int range)
{
	int realBuildings = getMapDetails(map, true).first, realRoads = getMapDetails(map, true).second;
	bool fitOld = (isBetween(buildings, realBuildings - range, realBuildings + range)
			|| isBetween(roads, realRoads - range, realRoads + range));

	realBuildings = getMapDetails(map, false).first, realRoads = getMapDetails(map, false).second;
	bool fitNew = (isBetween(buildings, realBuildings - range, realBuildings + range)
			|| isBetween(roads, realRoads - range, realRoads + range));

	bool isNewDefined = !(realBuildings == 0 && realRoads == 0);

	return (fitOld || (isNewDefined && fitNew));
}

void WorldModel::setMapName()
{
	for (int i = MN_KOBE; i != MN_TEST; i++)
	{
		MapName map = (MapName(i));
		if (doesThisMapFit(buildings.size(), roads.size(), map, MAP_RECOGNITION_RANGE) == true)
		{
			mapName = map;
			return;
		}
	}
	mapName = MN_UNKNOWN;
}

MapName WorldModel::getMapName()
{
	return mapName;
}

void WorldModel::setIsNoComm(bool a)
{
	radarComm = a;
}

bool WorldModel::isNoComm()
{
	return radarComm;
}

Rectangle WorldModel::getWorldBound()
{
	if (worldBound == nullptr)
		setWorldBound();

	return *worldBound;
}

void WorldModel::setWorldBound()
{
	// TODO Use objects apexes to calculate bound

	int x1 = numeric_limits<int>::max(), y1 = numeric_limits<int>::max(), x2 = numeric_limits<int>::min(), y2 = numeric_limits<int>::min();

	for (Building* building : buildings)
	{
		int x = building->getX();
		int y = building->getY();

		x1 = min(x, x1);
		x2 = max(x, x2);
		y1 = min(y, y1);
		y2 = max(y, y2);
	}

	for (Road* road : roads)
	{
		int x = road->getX();
		int y = road->getY();

		x1 = min(x, x1);
		x2 = max(x, x2);
		y1 = min(y, y1);
		y2 = max(y, y2);
	}

	worldBound = new Rectangle(Point(x1, y1), Point(x2, y2));
}

void WorldModel::setInSightPointsBuildings()
{
	u_int inRangeDistance;
	switch (getMapName())
	{
		case MN_EINDHOVEN:
		case MN_BERLIN:
		case MN_TEHRAN:
		case MN_MEXICO:
			inRangeDistance = 100000;
			break;
		default:
			inRangeDistance = 70000;
			break;
	}

	setBuildingsWalls();

	for (Building* building : buildings)
	{
		int totalRays = 0;
		byte* connectedBuildings = new byte[buildings.size()];
		for (u_int i = 0; i < buildings.size(); i++)
		{
			connectedBuildings[i] = 0;
		}
		vector<Building*> inRangeBuildings = getBuildingsInRange(building->getPos(), inRangeDistance);

		for (Wall* wall : building->walls)
		{
			findWallHits(wall, connectedBuildings, inRangeBuildings);
			totalRays += wall->getRaysCount();
		}

		float base = totalRays;
		for (u_int i = 0; i < buildings.size(); i++)
		{
			if (connectedBuildings[i] == 0)
			{
				continue;
			}

			float f = (float) connectedBuildings[i] / base;

//			if ((buildings[i]->getPos().distFromPoint(building->getPos()) < 30000 && f > 0.01) || (buildings[i]->getPos().distFromPoint(building->getPos()) < 35000 && f > 0.015)
//					|| f > 0.02)
			if (f > 0.01)
			{
				building->inSightBuildings.push_back(new InSightBuilding(f, i));
			}
		}

		delete[] connectedBuildings;
	}
}

void WorldModel::findWallHits(Wall* wall, u_char* connectedBuildings, vector<Building*>& inRangeBuildings)
{
	for (int emitted = 0; emitted < wall->getRaysCount(); emitted++)
	{
		Point start = getRandomOnLine(wall->getPoint1(), wall->getPoint2());
		Point end = getRandomPoint(start, MAX_SAMPLE_DISTANCE, world->buildings[wall->getOwnerIndex()]->getShape());
		Segment ray(start, end);

		Wall* closest = nullptr;
		double minDist = MAX_DOUBLE;

		for (Building* building : inRangeBuildings)
		{
			if (building->buildingIndex == wall->getOwnerIndex())
			{
				continue;
			}

			// Find Intersection
			for (Wall* wall2 : building->walls)
			{
				if (isIntersect(ray, wall2->getSegment()))
				{
					Point cross = getIntersectPoint(ray.asLine(), wall2->getSegment().asLine());

					double dist = cross.distFromPoint(start);
					if (dist < minDist)
					{
						minDist = dist;
						closest = wall2;
					}
				}
			}
		}

		if (closest == nullptr)
		{
			continue;
		}

		if (closest != wall)
		{
			connectedBuildings[closest->getOwnerIndex()]++;
		}
	}
}

void WorldModel::setBuildingsWalls()
{
	bool isLargeMap = isMapLarge();
	for (Building* building : buildings)
	{
		setBuildingWalls(building, isLargeMap);
	}
}

void WorldModel::setBuildingWalls(Building* building, bool isLargeMap)
{
	building->walls.clear();
	Polygon shape = building->getShape();

	float rayRate = isLargeMap ? 0.001f : 0.0025f;
	double wallsLength = 0;
	int fx = shape.getVertex(0).getX();
	int fy = shape.getVertex(0).getY();
	int lx = fx;
	int ly = fy;

	building->setWallsLength(0);
	building->walls.resize(shape.size());

	for (int n = 1; n < shape.size(); n++)
	{
		int tx = shape.getVertex(n).getX();
		int ty = shape.getVertex(n).getY();
		Wall* w = new Wall(lx, ly, tx, ty, building->buildingIndex, rayRate);

		if (w->validate())
		{
			building->walls[n - 1] = w;
			wallsLength += FLOOR_HEIGHT * 1000 * distanceBetweenPoints(Point(lx, ly), Point(tx, ty));
		}

		lx = tx;
		ly = ty;
	}
	building->walls[shape.size() - 1] = new Wall(lx, ly, fx, fy, building->buildingIndex, rayRate);

	building->setWallsLength(wallsLength / 1000000.0);
}

u_int WorldModel::getCityArea()
{
	if (cityArea > 0)
	{
		return cityArea;
	}

	for (Building* building : buildings)
	{
		cityArea += building->getAreaGround();
	}

	return cityArea;
}

vector<Segment> WorldModel::getVisionRays(Point sourcePoint, MotionlessObject* sourceMotionLess, int rayCount, int rayLength,
		bool ignorInSideBuildingRays)
{
	//Creating walls
	vector<Segment> walls;
	vector<Segment> entrances;
	for (MotionlessObject* mo : sourceMotionLess->closerThan50B)
	{
		for (int j = 0; j < mo->getShape().size(); j++)
		{
			if (mo->getServerIDs()[j] == 0)
			{
				if (mo->getShape().getSegment(j).getFirstPoint().distFromPoint(sourcePoint) > maxSightRange
						&& mo->getShape().getSegment(j).getSecondPoint().distFromPoint(sourcePoint) > maxSightRange)
				{
					continue;
				}

				walls.push_back(mo->getShape().getSegment(j));
			}
		}
	}

	for (int i = 0; i < sourceMotionLess->getShape().size(); i++)
	{
		if (sourceMotionLess->getServerIDs()[i] == 0)
		{
			walls.push_back(sourceMotionLess->getShape().getSegment(i));
		}
		else
		{
			entrances.push_back(sourceMotionLess->getShape().getSegment(i));
		}
	}

	//Cutting rays to intersection points from closerThan30 buildings.
	vector<Segment> visions;
	for (int i = 0; i <= 360; i += 360 / rayCount)
	{
		Segment ray = Segment(sourcePoint, i, rayLength);
		for (Segment wall : walls)
		{
			if (wall.getMiddlePoint().distFromPoint(sourcePoint) < (wall.getLength() / 2) + ray.getLength()) //asle hamar
			{
				if (Geometry::isIntersect(ray, wall))
				{
					ray = Segment(sourcePoint, i, (sourcePoint.distFromPoint(getIntersectPoint(ray.asLine(), wall.asLine()))) - 200);
				}
			}
		}

		if (ignorInSideBuildingRays == false)
		{
			visions.push_back(ray);
		}
		else
		{
			for (Segment entrance : entrances)
			{
				if (Geometry::isIntersect(ray, entrance))
				{
					visions.push_back(ray);
				}
			}
		}
	}

	return visions;
}

vector<Segment> WorldModel::getVisionRays(Point sourcePoint, MotionlessObject* sourceMotionLess, int rayCount, int rayLength,
		Building* target, int minValidAngel, int maxValidAngel)
{
	//Creating walls
	vector<Segment> walls;
	vector<Segment> entrances;

	for (MotionlessObject* mo : sourceMotionLess->closerThan50B)
	{
		for (int j = 0; j < mo->getShape().size(); j++)
		{
			if (mo->getServerIDs()[j] == 0)
			{
				if (mo->getShape().getSegment(j).getFirstPoint().distFromPoint(sourcePoint) > maxSightRange
						&& mo->getShape().getSegment(j).getSecondPoint().distFromPoint(sourcePoint) > maxSightRange)
				{
					continue;
				}

				walls.push_back(mo->getShape().getSegment(j));
			}
		}
	}

	for (int i = 0; i < sourceMotionLess->getShape().size(); i++)
	{
		if (sourceMotionLess->getServerIDs()[i] == 0)
		{
			walls.push_back(sourceMotionLess->getShape().getSegment(i));
		}
	}

	for (int i = 0; i < target->getShape().size(); i++)
	{
		if (sourceMotionLess->getServerIDs()[i] != 0)
		{
			entrances.push_back(target->getShape().getSegment(i));
		}
	}

	//Cutting rays to intersection points from closerThan30 buildings.
	vector<Segment> visions;

	while (maxValidAngel % (360 / rayCount) != 0)
	{
		maxValidAngel--;
	}

	while (minValidAngel % (360 / rayCount) != 0)
	{
		minValidAngel++;
	}

	for (int i = minValidAngel; i <= maxValidAngel; i += 360 / rayCount)
	{
		Segment ray = Segment(sourcePoint, i, rayLength);
		for (Segment wall : walls)
		{
			if (wall.getMiddlePoint().distFromPoint(sourcePoint) < (wall.getLength() / 2) + ray.getLength()) // Asle hemar
			{
				if (Geometry::isIntersect(ray, wall))
				{
					ray = Segment(sourcePoint, i, (sourcePoint.distFromPoint(getIntersectPoint(ray.asLine(), wall.asLine()))) - 200);
				}
			}
		}

		for (Segment entrance : entrances)
		{
			if (Geometry::isIntersect(ray, entrance))
			{
				visions.push_back(ray);
			}
		}
	}

	return visions;
}

bool visionComparator(Segment seg1, Segment seg2)
{
	return seg1.getTheta() < seg2.getTheta();
}

bool searchShapeComparator(SearchShape* sp1, SearchShape* sp2)
{
	if (sp1->searchMotionless->isRoad() && sp1->searchMotionless->isRoad())
	{
		return ((Road*) sp1->searchMotionless)->buildingWithThisRoadCanSee.size()
				> ((Road*) sp2->searchMotionless)->buildingWithThisRoadCanSee.size();
	}

	return sp1->searchMotionless->isRoad();
}

void WorldModel::setBuildingsVisionRays()
{
	LOG(Main, 11) << "[Start]SettingBuildingsVisionRays" << endl;
	for (Building* building : buildings)
	{
		building->visionRays = getVisionRays(building->getPos(), building, rayCount, maxSightRange, true);
		sort(building->visionRays.begin(), building->visionRays.end(), visionComparator);
	}
	LOG(Main, 11) << "[End]SettingBuildingsVisionRays" << endl;
}

void WorldModel::setSearchShapes()
{
	LOG(Main, 11) << "[Start]SettingBuildingsSearchShapes" << endl;
	for (Building* building : buildings)
	{
		building->searchArea = new SearchArea();

		ClipperLib::Path clip;

		clip.push_back(ClipperLib::IntPoint(building->getX(), building->getY()));
		for (Segment vision : building->visionRays)
		{
			clip.push_back(ClipperLib::IntPoint(vision.getSecondPoint().getX(), vision.getSecondPoint().getY()));
		}

		for (MotionlessObject* mo : building->closerThan30R)
		{
			ClipperLib::Path subj;
			vector<ClipperLib::Path> solution;

			for (Point p : mo->getShape().getVertices())
			{
				subj.push_back(ClipperLib::IntPoint(p.getX(), p.getY()));
			}

			ClipperLib::Clipper clipper;

			clipper.AddPath(subj, ClipperLib::ptSubject, true);
			clipper.AddPath(clip, ClipperLib::ptClip, true);
			clipper.Execute(ClipperLib::ctIntersection, solution, ClipperLib::pftNonZero, ClipperLib::pftNonZero);

			for (ClipperLib::Path pol : solution)
			{
				vector<Point> points;

				for (ClipperLib::IntPoint ip : pol)
				{
					points.push_back(Point(ip.X, ip.Y));
				}

				building->searchArea->addSearchShape(mo, Polygon(points), vector<Point>());
			}
		}
	}
	LOG(Main, 11) << "[end]SettingBuildingsSearchShapes" << endl;
}

void WorldModel::setSearchPoints()
{
	LOG(Main, 11) << "[start][setSearchPoints]" << endl;
	for (Building* building : buildings)
	{
		sort(building->searchArea->seachShapes.begin(), building->searchArea->seachShapes.end(), searchShapeComparator);
		LOG(Main, 1) << "BIndex: " << building->buildingIndex << endl;
		vector<Point> sources;
		int count = 0;
		for (SearchShape* sp : building->searchArea->seachShapes)
		{
			sources.clear();
			sources.push_back(sp->polygon.getMidPoint());

			int minimumEdge = MAX_INT;
			for (int i = 0; i < sp->polygon.size(); i++)
			{
				if (sp->polygon.getSegment(i).getLength() < minimumEdge)
				{
					minimumEdge = sp->polygon.getSegment(i).getLength();
				}
			}

			for (int i = 0; i < 3; i++)
			{
				Point p = getRandomPoint(sp->polygon.getMidPoint(), minimumEdge / 2);
				if (p.distFromPoint(building->getPos()) < maxSightRange + AGENT_RADIUS)
				{
					sources.push_back(p);
				}
			}

			bool found = false;
			for (Point p : sources)
			{
				int angle = absoluteAngle(atan2(building->getY() - p.getY(), building->getX() - p.getX()) * RAD2DEG);
				for (Segment seg : getVisionRays(p, sp->searchMotionless, rayCount, maxSightRange, building, angle - 90, angle + 90))
				{
					if (distanceToSegment(seg, building->getPos()) < AGENT_RADIUS)
					{
						sp->searchPoints.push_back(p);
						found = true;
						break;
					}
				}

				if (found)
				{
					break;
				}
			}
			count += sp->searchPoints.size();

			if (found)
			{
				break;
			}
		}
		LOG(Main, 1) << "[count]allValidSearchPoint:  " << count << endl;
	}
	LOG(Main, 11) << "[end][setSearchPoints]" << endl;
}

void WorldModel::computeSearchAreaAndOtherStuff()
{
	if (world->teamConfig["Search"]["SearchArea"]["Use"].asBool())
	{
		setBuildingsVisionRays();
		setSearchShapes();
		setSearchPoints();
	}
}

Point WorldModel::getCityCenter()
{
	if (cityCenter.getX() == 0 && cityCenter.getY() == 0)
	{
		double avgX = 0, avgY = 0;
		for (MotionlessObject* mo : motionlessObjects)
		{
			avgX += mo->getX();
			avgY += mo->getY();
		}

		cityCenter.setX(avgX / motionlessObjects.size());
		cityCenter.setY(avgY / motionlessObjects.size());
	}

	return cityCenter;
}



