#include "../Connection/OnlineConnection.h"
#include "Objects/Building.h"
#include <netinet/in.h>
#include "Parser.h"
#include "../WorldGraph/WorldGraph.h"
#include "../Utilities/FireEstimator/FireEstimator.h"

#define LOGLEVEL 2
#define BUILDING_IGNITION_POINT 47

using namespace std;
using namespace Types;
using namespace Geometry;

string Parser::analyzeMessage(WorldModel* world, vector<byte> &msg)
{
	int offset = 0;
	string header = Encodings::readString(msg, offset);
	LOG(Main, 1) << "Message header: " << header << endl;
	if (header == Encodings::getMessageType(MT_KA_CONNECT_OK))
	{
		return parseKAConnectOk(world, msg, offset);
	}
	else if (header == Encodings::getMessageType(MT_KA_CONNECT_ERROR))
	{
		return parseKAConnectError(world, msg, offset);
	}
	else if (header == Encodings::getMessageType(MT_KA_SENSE))
	{
		return parseKASense(world, msg, offset);
	}
	else if (header == Encodings::getMessageType(MT_KA_HEAR))
	{
		return parseKAHear(world, msg, offset);
	}
	else
	{
		return "Not a valid header";
	}

	return "";
}

string Parser::parseKAConnectOk(WorldModel* world, vector<byte> &msg, int &offset)
{
	u_int size = Encodings::readInt(msg, offset);
	u_int tempId = Encodings::readInt(msg, offset);
	LOG(Main, 1) << "size:     " << size << endl;
	LOG(Main, 1) << "tmep id:    " << tempId << endl;
	u_int realId = Encodings::readInt(msg, offset);
	world->selfID = realId;
	parseObjectsElementsKAConnectOk(world, msg, offset);
	parseConfig(world, msg, offset);
	return "Receive KA_CONNECT_OK";
}

string Parser::parseKAConnectError(WorldModel* world, vector<byte> &msg, int &offset)
{
	int size = Encodings::readInt(msg, offset);
	int tempId = Encodings::readInt(msg, offset);
	LOG(Main, 1) << "size:     " << size << endl;
	LOG(Main, 1) << "tmep id:    " << tempId << endl;
	string reason = Encodings::readString(msg, offset);
	return reason;
}

string Parser::parseKASense(WorldModel* world, vector<byte> &msg, int &offset)
{
	int size = Encodings::readInt(msg, offset);
	int id = Encodings::readInt(msg, offset);
	LOG(Main, 3) << "size:     " << size << endl;
	LOG(Main, 3) << "id:    " << id << endl;
	int time = Encodings::readInt(msg, offset);
	world->setTime(time);
	parseObjectsElementsKASense(world, msg, offset);
	int zero = Encodings::readInt(msg, offset);
	LOG(Main, 3) << "zero:    " << zero << endl;
	LOG(Main, 3) << "msg.size - offset: " << msg.size() - offset << endl;
	parseRadarMessages(world, msg, offset);
	return "Recive KA_SENSE";
}

string Parser::parseKAHear(WorldModel* world, vector<byte> &msg, int &strat)
{
	return "Recive KA_HEAR";
}

void Parser::parseObjectsElementsKAConnectOk(WorldModel* world, vector<byte> &msg, int &offset)
{
	//numOfObjects is the number of objects
	int numOfObjects = Encodings::readInt(msg, offset);
	for (int i = 0; i < numOfObjects; i++)
	{
		int id;
		string entity;
		entity = Encodings::readString(msg, offset);
		id = Encodings::readInt(msg, offset);
		int len = Encodings::readInt(msg, offset);
		LOG(Main, 1) << "len:  " << len << endl;
		u_int counter = Encodings::readInt(msg, offset);
		parseObjectElements(world, msg, offset, entity, id, counter);
	}
}

void Parser::parseObjectsElementsKASense(WorldModel* world, vector<byte>& msg, int& offset)
{
	//numOfObjects is the number of objects
	int numOfObjects = Encodings::readInt(msg, offset);
	for (int i = 0; i < numOfObjects; i++)
	{
		int id;
		string entity;
		id = Encodings::readInt(msg, offset);
		entity = Encodings::readString(msg, offset);
		u_int counter = Encodings::readInt(msg, offset);
		parseObjectElements(world, msg, offset, entity, id, counter);
	}
}

//this function create objects if they don't exist and if it exist, it use the existed object and send it to parseObjects!!!!!!!!!

void Parser::parseObjectElements(WorldModel* world, vector<byte> &msg, int &offset, string type, int id, int counter)
{
//	cout << "My Id : " << world->selfID << "Got An Object And Entity Is : " << type << " id : " << id << endl;
	if (type == Encodings::getEntityType(ET_BULDING))
	{
		Building *b;
		if (world->objects[id] == nullptr)
		{
			b = new Building(id);
			parseObjects(world, msg, *b, offset, counter);
			b->setLastCycleUpdated(world->getTime());
			b->setLastCycleUpdatedBySense(world->getTime());
			world->buildings.push_back(b);
			world->motionlessObjects.push_back(b);
			world->objects[id] = b;
		}
		else
		{
			b = (Building*) world->objects[id];
			b->setLastCycleUpdated(world->getTime());
			b->setLastCycleUpdatedBySense(world->getTime());

			bool itHasFire = b->hasFire();
			double virtualTemperature = -1;
			if (b->getEstimatedData() != nullptr)
			{
				virtualTemperature = b->getEstimatedData()->getTemperature();
			}

			parseObjects(world, msg, *b, offset, counter);

			if (itHasFire && b->hasFire() == false)
			{
				b->setLastCycleInFire(world->getTime());
			}

			if (b->getEstimatedData() != nullptr)
			{
				if (virtualTemperature > BUILDING_IGNITION_POINT && b->getTemperature() < BUILDING_IGNITION_POINT)
				{
					FireEstimator::resetBuildingTemperature(b);
				}

				LOG(Main, 1) << "Parser: " << b->getId() << ": " << b->getFieryness() << "," << b->getEstimatedData()->getFieryness() << "   " << b->getTemperature() << "," << b->getEstimatedData()->getTemperature() << endl;
			}

		}
	}
	else if (type == Encodings::getEntityType(ET_AMBULANCE_CENTER))
	{
		AmbulanceCenter *ac;
		if (world->objects[id] == nullptr)
		{
			ac = new AmbulanceCenter(id);
			parseObjects(world, msg, *ac, offset, counter);
			ac->setLastCycleUpdated(world->getTime());
			ac->setLastCycleUpdatedBySense(world->getTime());
			world->ambulanceCenters.push_back(ac);
			world->motionlessObjects.push_back(ac);
			world->buildings.push_back(ac);
			world->objects[id] = ac;
		}
		else
		{
			ac = (AmbulanceCenter*) world->objects[id];
			ac->setLastCycleUpdated(world->getTime());
			ac->setLastCycleUpdatedBySense(world->getTime());
			parseObjects(world, msg, *ac, offset, counter);
		}
	}
	else if (type == Encodings::getEntityType(ET_AMBULANCE_TEAM))
	{
		AmbulanceTeam *at;
		if (world->objects[id] == nullptr)
		{
			at = new AmbulanceTeam(id);
			parseObjects(world, msg, *at, offset, counter);
			at->setLastCycleUpdated(world->getTime());
			at->setLastCycleUpdatedBySense(world->getTime());
			world->ambulanceTeams.push_back(at);
			world->platoons.push_back(at);
			world->humans.push_back(at);
			world->objects[id] = at;
		}
		else
		{
			at = (AmbulanceTeam*) world->objects[id];
			at->setLastCycleUpdated(world->getTime());
			at->setLastCycleUpdatedBySense(world->getTime());
			parseObjects(world, msg, *at, offset, counter);
		}
	}
	else if (type == Encodings::getEntityType(ET_CIVILIAN))
	{
		LOG(Main, LOGLEVEL) << "civ parsing : " << endl;
		Civilian *civ;
		civ = new Civilian(id);
		vector<byte> tempMsg = msg;
		int tempOffset = offset;
		parseObjects(world, tempMsg, *civ, tempOffset, counter);

		if (world->getTime() < 3) //Wrong civilian position before cycle 3
		{
			parseObjects(world, msg, *civ, offset, counter);
			return;
		}

		int fakeId = 0;
		if (world->objects[id] != nullptr)
		{
			LOG(Main, LOGLEVEL) << "wada faka" << endl;
			fakeId = ((Civilian*) world->objects[id])->getFakeId();
			LOG(Main, LOGLEVEL) << "moda faka" << endl;
		}
		else
		{
			MotionlessObject* pos = nullptr;
			for (AmbulanceTeam* at : world->ambulanceTeams)
			{
				if (at->getId() == civ->getPosition())
				{
					pos = (MotionlessObject*) world->objects[at->getPosition()];
					break;
				}
			}
			if (civ->getMotionlessObject() == nullptr)
			{
				pos = (MotionlessObject*) world->objects[civ->getPosition()];
			}

			int angle = round(getPolarAngle(civ->getRealY() - pos->getY(), civ->getRealX() - pos->getX()) / angleCoef);
			int distance = round(pos->getPos().distFromPoint(Point(civ->getRealX(), civ->getRealY())) / distanceCoef);
			Civilian* fake = new Civilian(pos->motionlessIndex, angle, distance, ceil(log2l(pos->getMaxDistanceFromCenter() / distanceCoef)));
			fakeId = fake->getFakeId();

			LOG(Main, LOGLEVEL) << "got fake id" << endl;

			civ->setAngle(angle);

			LOG(Main, LOGLEVEL) << "angle setting" << endl;

			civ->setDistanceToCenter(distance);

			LOG(Main, LOGLEVEL) << "distance setting" << endl;

			civ->setDistanceToCenterSize(ceil(log2l(pos->getMaxDistanceFromCenter() / distanceCoef)));

			LOG(Main, LOGLEVEL) << "size setting" << endl;

			delete fake;

			LOG(Main, LOGLEVEL) << "deleting" << endl;
		}

		civ->setFakeId(fakeId);
		if (world->fakeCivilians[fakeId] == nullptr)
		{
			parseObjects(world, msg, *civ, offset, counter);
			civ->setLastCycleUpdated(world->getTime());
			civ->setLastCycleUpdatedBySense(world->getTime());

			civ->humanIndex = world->humans.size();
			civ->civilianIndex = world->civilians.size();

			world->civilians.push_back(civ);
			world->humans.push_back(civ);
			world->fakeCivilians[fakeId] = civ;
		}
		else
		{
			world->fakeCivilians[fakeId]->setId(id);
			world->fakeCivilians[fakeId]->setLastCycleUpdated(world->getTime());
			world->fakeCivilians[fakeId]->setLastCycleUpdatedBySense(world->getTime());
			parseObjects(world, msg, *world->fakeCivilians[fakeId], offset, counter);

			delete civ;
		}

		if (world->objects[id] == nullptr) //Just for storing fakeID. So there is no need to update world->objects more than once.
		{
			world->objects[id] = world->fakeCivilians[fakeId];
		}

		LOG(Main, LOGLEVEL) << "end of civ parsing" << endl;
	}
	else if (type == Encodings::getEntityType(ET_FIRE_BRIGADE))
	{
		FireBrigade *fb;
		if (world->objects[id] == nullptr)
		{
			fb = new FireBrigade(id);
			parseObjects(world, msg, *fb, offset, counter);
			fb->setLastCycleUpdated(world->getTime());
			fb->setLastCycleUpdatedBySense(world->getTime());
			world->fireBrigades.push_back(fb);
			world->platoons.push_back(fb);
			world->humans.push_back(fb);
			world->objects[id] = fb;
		}
		else
		{
			fb = (FireBrigade*) world->objects[id];
			fb->setLastCycleUpdated(world->getTime());
			fb->setLastCycleUpdatedBySense(world->getTime());
			parseObjects(world, msg, *fb, offset, counter);
		}
	}
	else if (type == Encodings::getEntityType(ET_FIRE_STATION))
	{
		FireStation *fs;
		if (world->objects[id] == nullptr)
		{
			fs = new FireStation(id);
			parseObjects(world, msg, *fs, offset, counter);
			fs->setLastCycleUpdated(world->getTime());
			fs->setLastCycleUpdatedBySense(world->getTime());
			world->fireStations.push_back(fs);
			world->motionlessObjects.push_back(fs);
			world->buildings.push_back(fs);
			world->objects[id] = fs;
		}
		else
		{
			fs = (FireStation*) world->objects[id];
			fs->setLastCycleUpdated(world->getTime());
			fs->setLastCycleUpdatedBySense(world->getTime());
			parseObjects(world, msg, *fs, offset, counter);
		}
	}
	else if (type == Encodings::getEntityType(ET_POLICE_FORCE))
	{
		PoliceForce *pf;
		if (world->objects[id] == nullptr)
		{
			pf = new PoliceForce(id);
			parseObjects(world, msg, *pf, offset, counter);
			pf->setLastCycleUpdated(world->getTime());
			pf->setLastCycleUpdatedBySense(world->getTime());
			world->policeForces.push_back(pf);
			world->platoons.push_back(pf);
			world->humans.push_back(pf);
			world->objects[id] = pf;
		}
		else
		{
			pf = (PoliceForce*) world->objects[id];
			pf->setLastCycleUpdated(world->getTime());
			pf->setLastCycleUpdatedBySense(world->getTime());
			parseObjects(world, msg, *pf, offset, counter);
		}
	}
	else if (type == Encodings::getEntityType(ET_POLICE_OFFICE))
	{
		PoliceOffice *po;
		if (world->objects[id] == nullptr)
		{
			po = new PoliceOffice(id);
			parseObjects(world, msg, *po, offset, counter);
			po->setLastCycleUpdated(world->getTime());
			po->setLastCycleUpdatedBySense(world->getTime());
			world->policeOffices.push_back(po);
			world->motionlessObjects.push_back(po);
			world->buildings.push_back(po);
			world->objects[id] = po;
		}
		else
		{
			po = (PoliceOffice*) world->objects[id];
			po->setLastCycleUpdated(world->getTime());
			po->setLastCycleUpdatedBySense(world->getTime());
			parseObjects(world, msg, *po, offset, counter);
		}
	}
	else if (type == Encodings::getEntityType(ET_GAS_STATION))
	{
		GasStation *gasStation;
		if (world->objects[id] == nullptr)
		{
			gasStation = new GasStation(id);
			parseObjects(world, msg, *gasStation, offset, counter);
			gasStation->setLastCycleUpdated(world->getTime());
			gasStation->setLastCycleUpdatedBySense(world->getTime());
			world->gasStations.push_back(gasStation);
			world->buildings.push_back(gasStation);
			world->motionlessObjects.push_back(gasStation);
			world->objects[id] = gasStation;
		}
		else
		{
			gasStation = (GasStation *) world->objects[id];
			gasStation->setLastCycleUpdated(world->getTime());
			gasStation->setLastCycleUpdatedBySense(world->getTime());
			bool itHasFire = gasStation->hasFire();
			parseObjects(world, msg, *gasStation, offset, counter);
			if (itHasFire && gasStation->hasFire() == false)
			{
				gasStation->setLastCycleInFire(world->getTime());
			}
		}
	}
	else if (type == Encodings::getEntityType(ET_REFUGE))
	{
		Refuge *ref;
		if (world->objects[id] == nullptr)
		{
			ref = new Refuge(id);
			parseObjects(world, msg, *ref, offset, counter);
			ref->setLastCycleUpdated(world->getTime());
			ref->setLastCycleUpdatedBySense(world->getTime());
			world->refuges.push_back(ref);
			world->motionlessObjects.push_back(ref);
			world->buildings.push_back(ref);
			world->objects[id] = ref;
		}
		else
		{
			ref = (Refuge*) world->objects[id];
			ref->setLastCycleUpdated(world->getTime());
			ref->setLastCycleUpdatedBySense(world->getTime());
			parseObjects(world, msg, *ref, offset, counter);
		}
	}
	else if (type == Encodings::getEntityType(ET_ROAD))
	{
		Road *road;
		if (world->objects[id] == nullptr)
		{
			road = new Road(id);
			parseObjects(world, msg, *road, offset, counter);
			road->setLastCycleUpdated(world->getTime());
			road->setLastCycleUpdatedBySense(world->getTime());
			world->roads.push_back(road);
			world->motionlessObjects.push_back(road);
			world->objects[id] = road;
		}
		else
		{
			road = (Road*) world->objects[id];
			road->setLastCycleUpdated(world->getTime());
			road->setLastCycleUpdatedBySense(world->getTime());
			road->clearBlockadeIds();
			parseObjects(world, msg, *road, offset, counter);
		}
	}
	else if (type == Encodings::getEntityType(ET_HYDRANT))
	{
		Hydrant *hydrant;
		if (world->objects[id] == nullptr)
		{
			hydrant = new Hydrant(id);
			parseObjects(world, msg, *hydrant, offset, counter);
			hydrant->setLastCycleUpdated(world->getTime());
			hydrant->setLastCycleUpdatedBySense(world->getTime());
			world->hydrants.push_back(hydrant);
			world->roads.push_back(hydrant);
			world->motionlessObjects.push_back(hydrant);
			world->objects[id] = hydrant;
		}
		else
		{
			hydrant = (Hydrant*) world->objects[id];
			hydrant->setLastCycleUpdated(world->getTime());
			hydrant->setLastCycleUpdatedBySense(world->getTime());
			((Road*) hydrant)->clearBlockadeIds();
			parseObjects(world, msg, *hydrant, offset, counter);
		}

	}
	else if (type == Encodings::getEntityType(ET_BLOCKADE))
	{
		Blockade *blockade;
		if (world->objects[id] == nullptr)
		{
			blockade = new Blockade(id);
			parseObjects(world, msg, *blockade, offset, counter);
			blockade->setLastCycleUpdated(world->getTime());
			blockade->setLastCycleUpdatedBySense(world->getTime());
			world->blockades.insert(blockade);
			world->objects[id] = blockade;
			LOG(Main, 11) << "Parser: Blockade nullptr,  " << blockade->getId() << endl;
		}
		else
		{
			blockade = (Blockade*) world->objects[id];
			blockade->clearShape();
			parseObjects(world, msg, *blockade, offset, counter);
			blockade->setLastCycleUpdated(world->getTime());
			blockade->setLastCycleUpdatedBySense(world->getTime());
			LOG(Main, 11) << "Parser: Blockade update,  " << blockade->getId() << endl;
		}
	}
	else
	{
		cout << "unknown entity: " << type << endl;
	}
}

void Parser::parseObjects(WorldModel* world, vector<byte> &msg, RCRObject &obj, int &offset, int counter)
{
	for (int i = 0; i < counter; i++)
	{
		string property = Encodings::readString(msg, offset);
		bool define = Encodings::readBool(msg, offset);
		if (define)
		{
			u_int lengthOfPropertyData = Encodings::readInt(msg, offset);
			if (lengthOfPropertyData > 4)
			{
				int counter2 = Encodings::readInt(msg, offset);
				LOG(Main, 1) << "Counter2:     " << counter2 << endl;
				lengthOfPropertyData -= 4;
			}
			for (; lengthOfPropertyData > 0; lengthOfPropertyData -= 4)
			{
				u_int value = Encodings::readInt(msg, offset);
				obj.setProperties(property, value);
			}
		}
	}
}

void Parser::parseConfig(WorldModel* world, vector<byte>& msg, int& offset)
{
	int numOfConfigData = Encodings::readInt(msg, offset);
	for (int i = 0; i < numOfConfigData; i++)
	{
		string configType = Encodings::readString(msg, offset);
		string value = Encodings::readString(msg, offset);
		world->config.insert(pair<string, string>(configType, value));
	}
}

void Parser::parseRadarMessages(WorldModel* world, vector<byte>& msg, int& offset)
{
	//counter is the number of messages
	int counter = Encodings::readInt(msg, offset);
	LOG(Main, 1) << "number of radar messages: " << counter << endl;
	for (int i = 0; i < counter; i++)
	{
		string speakHeader = Encodings::readString(msg, offset);
		int size = Encodings::readInt(msg, offset);
		LOG(Main, 1) << "size: " << size << endl;
		int sender = Encodings::readInt(msg, offset);
		LOG(Main, 1) << "sender: " << sender << endl;

		//		if (sender == world->selfID)
		//		{
		//			offset += size - 4;
		//			continue;
		//		}

		int time = Encodings::readInt(msg, offset);
		LOG(Main, 1) << "time: " << time << endl;
		int channel = Encodings::readInt(msg, offset);
		LOG(Main, 1) << "channel: " << channel << endl;
		int sizeOfContent = Encodings::readInt(msg, offset);
		LOG(Main, 1) << "sizeOfContent: " << sizeOfContent << endl;
		world->radar->analyzeMessage(msg, offset, sizeOfContent, sender, channel);
		offset += size - 16;
	}

	LOG(Main , 7) << endl << "*******************************************************************" << endl;

}
