#ifndef __WORLDMODEL_H_
#define __WORLDMODEL_H_

#include <iostream>
#include <map>
#include <set>
#include <string>
#include <vector>
#include <list>

#define IS_STATIC 0

#include <polyclipping/clipper.hpp>

#include "Parser.h"
#include "../Utilities/Config.h"
#include "../Utilities/Geometry/Rectangle.h"
#include "../Utilities/Geometry/Segment.h"
#include "../Utilities/Geometry/GeometryFunctions.h"
#include "../Utilities/Types.h"
#include "../WorldGraph/WorldGraph.h"
#include "Objects/AmbulanceCenter.h"
#include "Objects/AmbulanceTeam.h"
#include "Objects/Blockade.h"
#include "Objects/Building.h"
#include "Objects/Civilian.h"
#include "Objects/FireBlock.h"
#include "Objects/FireBrigade.h"
#include "Objects/FireStation.h"
#include "Objects/GasStation.h"
#include "Objects/Hydrant.h"
#include "Objects/PoliceForce.h"
#include "Objects/PoliceOffice.h"
#include "Objects/Refuge.h"
#include "Objects/Edge.h"
#include "Objects/SearchArea.h"
#include "../Radar/AbstractCommunicationradar.h"

#define DEFAULT_RAYS_COUNT 72
#define FLOOR_HEIGHT 7
#define MAX_SAMPLE_DISTANCE 200000
#define IN_RANGE_DISTANCE 50000
#define BLOCKADE_INCREASE_SIZE_L1 (AGENT_RADIUS * 3.0f) //Reachable
#define BLOCKADE_INCREASE_SIZE_L2 (AGENT_RADIUS * 2.0f) //Semi-Reachable

using namespace std;
using namespace Types;
namespace clipper = ClipperLib;

class WorldGraph;
class AbstractCommunicationRadar;
class PoliceForce;
class Cluster;
class Slice;

class WorldModel
{
public:
	WorldModel(Config&);
	std::string init(std::vector<byte> message, Types::EntityType type, bool);
	virtual ~WorldModel();
	RCRObject *self;
	AbstractCommunicationRadar* radar;
	std::vector<AmbulanceCenter *> ambulanceCenters;
	std::vector<PoliceOffice *> policeOffices;
	std::vector<FireStation *> fireStations;
	std::vector<Road *> roads;
	std::vector<Hydrant *> hydrants;
	std::vector<Building *> buildings;
	std::vector<GasStation *> gasStations;
	std::vector<Refuge *> refuges;
	std::vector<Human *> humans;
	std::vector<Platoon *> platoons;
	std::vector<Civilian *> civilians;
	std::vector<AmbulanceTeam *> ambulanceTeams;
	std::vector<FireBrigade *> fireBrigades;
	std::vector<PoliceForce *> policeForces;
	std::vector<MotionlessObject *> motionlessObjects;
	std::vector<FireBlock *> fireBlocks;
	std::set<Blockade *> blockades;
	std::map<int, Civilian*> fakeCivilians;
	std::map<int, RCRObject*> objects;
	std::vector<Segment> visionRays;

	vector<Cluster*> searchClusters;
	vector<Slice*> pizzaSlices;

	std::string update(std::vector<byte> message);
	void updatePositions();
	void updateInnerHumans();
	void updatePassingModesAndRepresentiveNodes();
	void setInitialDistanceFindingObjects();
	void complete(Types::EntityType type, bool);
	int getTime();
	void setTime(int time);
	void setWorldGraph(WorldGraph *worldGraph);
	void setBuildingsInSightPoints();
	void setInSightPointsBuildings();
	void setCanseeBuildings();
	void setAllValidParts();
	void setBuildingsWalls();
	vector<RCRObject*> getInSightObjects(Point, int rayCount = DEFAULT_RAYS_COUNT, bool includeBlockades = true);
	vector<RCRObject*> getInSightObjects(MotionlessObject*, int rayCount = DEFAULT_RAYS_COUNT, bool includeBlockades = true);
	vector<RCRObject*> getInSightObjects();
	vector<RCRObject*> getBoostInSightObjects(Point);
	vector<RCRObject*> getObjectsInRange(Point, float, bool);
	vector<RCRObject*> getObjectsInRangeLessThan50(MotionlessObject*, float, bool);
	vector<Building*> getBuildingsInRange(Point, float);
	vector<Building*> searchBuildingList;
	Rectangle getWorldBound();
	void computeSearchAreaAndOtherStuff();

	vector<Building*> suspiciousBuildings;
	Building* bestSus = nullptr;

	Point clearrrrrpoint;

	vector<Polygon> validParts;
	std::map<std::string, std::string> config;
	Config &teamConfig;
	int selfID;
	bool amIThePreComputer;

	Point getCityCenter();
	Point cityCenter;

	MapName getMapName();
	void setMapName();

	bool isNoComm();
	void setIsNoComm(bool a);
	bool isMapLarge();

	u_int getCityArea();

	Polygon clearArea;

private:
	void computeVisionRays();
	void removeAllBlockades();
	void setLastCylceUpdatesOfCiviliansPlace();
	void setHasBeenSeens();
	void updateBlockades();
	void generateCloserThan();
	void updateAgents();
	void updateCivilians();
	void updateRoads();
	void updateHpAndDamageHistory();
	void updateMotionlessInnerHumans();
	void updateBuildings();
	void updateInSightObjects();
	void updateBuildingsSearchStates();
	void increaseBlockades();
	void setHardToExtinguish(Building * b);
	void sortAndSetObjectsIndexes();
	void setWorldBound();
	void findWallHits(Wall*, u_char*, vector<Building*>&);
	void setBuildingWalls(Building*, bool);
	vector<RCRObject*> getIntersectedObjects(Segment, vector<RCRObject*>, bool);
	Building* getIntersectedBuilding(Segment, vector<Building*>);
	vector<Segment> getVisionRays(Point sourcePoint, MotionlessObject* sourceMotionless, int rayCount, int rayLength, bool ignorInSideBuildingRays);
	vector<Segment> getVisionRays(Point sourcePoint, MotionlessObject* sourceMotionless, int rayCount, int rayLength, Building* target, int minValidAngel, int maxValidAngel);
	vector<SearchArea*> searchAreas;
	void setSearchPoints();
	void setSearchShapes();
	void setBuildingsVisionRays();

	int m_time;
	int rayCount;
	int maxFireTank;
	int maxSightRange;
	u_int cityArea = 0;
	WorldModel *world;
	WorldGraph *worldGraph;
	vector<RCRObject*> inSightObjects;
	Types::MapName mapName;
	bool radarComm;
	Rectangle* worldBound;
};

#endif /* WORLDMODEL_H_ */
