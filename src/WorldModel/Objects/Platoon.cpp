#include "Platoon.h"

using namespace std;

Platoon::Platoon()
{
	positionHistory.clear();
}

Platoon::Platoon(u_int Id) :
		Human(Id)
{
	typeFlag &= ~2;
	canitMove = false;
	lastMoveCommand.clear();
	amIThePreComputer = false;
	needPoliceHelp = false;
}

Platoon::~Platoon()
{
}

void Platoon::setProperties(string type, int value)
{
	Human::setProperties(type, value);
}

void Platoon::setCanMove(int a)
{
	if (a == 0)
		this->canitMove = false;
	else
		this->canitMove = true;
}

void Platoon::setCanMove(bool a)
{
	canitMove = a;
}

void Platoon::setLastMoveCommand(vector<int> moves)
{
	lastMoveCommand = moves;
}

vector<int> &Platoon::getLastMoveCommand()
{
	return lastMoveCommand;
}

bool Platoon::canMove()
{
	return this->canitMove;
}

bool Platoon::dostItMoveEver()
{
	if (positionHistory.size() <= 5)
	{
		return true;
	}

	int thisIndex = positionHistory.size() - 1;

	if (positionHistory[thisIndex]->getId() == positionHistory[thisIndex - 1]->getId() && positionHistory[thisIndex - 1]->getId() == positionHistory[thisIndex - 2]->getId()
			&& positionHistory[thisIndex - 2]->getId() == positionHistory[thisIndex - 3]->getId()
			&& positionHistory[thisIndex - 3]->getId() == positionHistory[thisIndex - 4]->getId())
	{
		return false;
	}

	return true;
}

bool Platoon::isMoved()
{
	if (positionHistory[positionHistory.size() - 1]->getId() == positionHistory[positionHistory.size() - 2]->getId())
	{
		return false;
	}

	return true;
}

