#ifndef __POLICE_FORCE_H_
#define __POLICE_FORCE_H_

#include <string>
#include <vector>
#include "../../Utilities/Clustering/Cluster.h"
#include "Human.h"
#include "Platoon.h"

class PoliceForce: public Platoon
{
public:
	PoliceForce(u_int Id);
	virtual ~PoliceForce();
	void setProperties(std::string type, int value);

	int policeForceIndex;
	Cluster* araroCluster;
	FireBlock* fireBlock;
	Types::PoliceForceJob job;
	Road* aroundFireSearchTarget = nullptr;
	Building* souspiciouseTarget = nullptr;
	vector<Road*> legacySouspiciouseRoads;
	MotionlessObject* searchTarget = nullptr;
	std::vector<Cluster*> searchClusters;
	//bool isSpecialForce = false, isPidgin = false;
	bool buriedAssign = false;
};

#endif
