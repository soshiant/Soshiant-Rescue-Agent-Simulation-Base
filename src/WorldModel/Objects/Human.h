#ifndef __HUMAN_H_
#define __HUMAN_H_

#include <iostream>
#include "RCRObject.h"
#include <vector>
#include "MotionlessObject.h"
using namespace std;

struct DamageDuringTheTime
{
	int time;
	int damage;
};

struct HpDuringTheTime
{
	int time;
	int hp;
};

struct BuriednessDuringTime
{
	int time;
	int buriedness;
};

class Human: public RCRObject
{
public:
	Human(u_int Id);
	Human();
	virtual ~Human();
	u_int getBuriedness() const;
	u_int getDamage() const;
	u_int getHp() const;
	void setBuriedness(u_int buriedness);
	void setDamage(u_int damage);
	void setHp(u_int hp);
	MotionlessObject *getMotionlessObject() const;
	void setMotionlessObject(MotionlessObject *motionlessObject);
	void setProperties(std::string type, int value);
	u_int getDirection() const;
	u_int getPosition() const;
	u_int getPositionExtra() const;
	u_int getPositionHistory() const;
	u_int getStamina() const;
	unsigned long long int getTravelDistaance() const;
	void setDirection(u_int direction);
	void setPosition(u_int position);
	void setPositionExtra(u_int positionExtra);
	void setPositionHistory(u_int positionHistory);
	void setStamina(u_int stamina);
	void setTravelDistance(int value);
	int getRepresentiveNodeIndex();
	int getWithoutBlockadeRepresentiveNodeIndex();
	void setRepresentiveNodeIndex(int);
	void setWithoutBlockadeRepresentiveNodeIndex(int);
	void setFirstTimeMotionless(MotionlessObject * m);
	MotionlessObject * getFirstTimeMotionless();
	void setFirstTimeBuriedness(int b);
	int getFirstTimeBuriedness();

	vector<HpDuringTheTime> & getHpHistory();
	vector<DamageDuringTheTime> & getDamageHistory();
	vector<BuriednessDuringTime> & getBuriednessHistory();
	void addDamageHistory(DamageDuringTheTime dt);
	void setDamageHistory(vector<DamageDuringTheTime> dmgHist);
	void addHpHistory(HpDuringTheTime ht);
	void setHpHistory(vector<HpDuringTheTime> hpHist);
	void addBuriednessHistory(BuriednessDuringTime bt);
	void setBuriednessHistory(vector<BuriednessDuringTime> brHist);

	void clearHpHistory();
	void clearDamageHistory();
	void clearBuriednessHistory();
	void deleteFirstHpHistory();
	void deleteFirstDamageHistory();
	void deleteFirstBuriednessHistory();

	void calculateTimeToDeath(int globalTime);
	void setTimeToDeath(int value);
	int getTimeToDeath();

	int humanIndex;

	bool isAvailable;

	int value;
	int risk;
	bool isSelectedForTarget;

private:
	u_int buriedness;
	u_int damage;
	u_int damageEst;
	u_int hp;
	u_int hpEst;
	u_int direction;
	u_int stamina;
	u_int position;
	u_int positionExtra;
	u_int positionHistory;
	unsigned long long int travelDistance;

	int timeToDeath;

	int firstTimeBuriedness;
	MotionlessObject *motionlessObject;
	MotionlessObject *firstTimeMotionless;
	int representiveNodeIndex;
	int withoutBlockadeRepresentiveNodeIndex;

	vector<HpDuringTheTime> hpHistory;
	vector<DamageDuringTheTime> damageHistory;
	vector<BuriednessDuringTime> buriednessHistory;
};

#endif
