/*
 * Hydrant.h
 *
 *  Created on: Jul 16, 2013
 *      Author: Danial Keshvary
 */

#ifndef HYDRANT_H_
#define HYDRANT_H_

#include "Road.h"

class Hydrant: public Road
{
public:
	Hydrant(u_int id);
	virtual ~Hydrant();
	void setProperties(std::string type, int value);
	int hydrantIndex;
};

#endif /* HYDRANT_H_ */
