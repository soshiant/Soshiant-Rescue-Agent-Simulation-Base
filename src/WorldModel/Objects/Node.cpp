/*
 * Node.cpp
 *
 *  Created on: Nov 28, 2010
 *  Author: Eqbal Sarjami
 *
 *  Edited by Arya Hadi [April 30 2014]
 */

#include <vector>

#include "Node.h"
#include "MotionlessObject.h"

using namespace std;
using namespace Geometry;

Node::Node(int motionlessIndex)
{
	this->motionlessIndex = motionlessIndex;
}

Node::~Node()
{
}

int Node::getMotionlessIndex()
{
	return motionlessIndex;
}

int Node::getFirstMotionlessIndex()
{
	return motionlessIndex;
}

int Node::getSecondMotionlessIndex()
{
	return motionlessIndex;
}

vector<int>& Node::getEdgeIndexes()
{
	return edgeIndexes;
}

void Node::addEdgeIndex(int edgeID)
{
	edgeIndexes.push_back(edgeID);
}

int Node::getEdgeIndex()
{
	return edgeIndex;
}

void Node::setEdgeIndex(int index)
{
	edgeIndex = index;
}

vector<int>& Node::getNeighbours()
{
	return neighbours;
}

void Node::addNeighbour(int index)
{
	neighbours.push_back(index);
}

Point& Node::getRepresentivePoint()
{
	return representivePoint;
}

void Node::setRepresentivePoint(Point point)
{
	representivePoint = point;
}

SimpleNode::SimpleNode()
{
	neighbors.clear();
	isMark = false;
}

SimpleNode::SimpleNode(Point p)
{
	point = p;
	neighbors.clear();
	isMark = false;
}

SimpleNode::~SimpleNode()
{
}

void SimpleNode::setPoint(Point p)
{
	point = p;
}

Point SimpleNode::getPoint()
{
	return point;
}
