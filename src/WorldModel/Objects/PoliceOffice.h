#ifndef __POLICE_OFFICE_H_
#define __POLICE_OFFICE_H_

#include "Building.h"

class PoliceOffice: public Building
{
public:
	PoliceOffice(u_int Id);
	virtual ~PoliceOffice();
	void setProperties(std::string type, int value);
	int policeOfficeIndex;
private:
};

#endif
