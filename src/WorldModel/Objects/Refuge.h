#ifndef __REFUGE_H_
#define __REFUGE_H_

#include "Building.h"

class Refuge: public Building
{
public:
	Refuge(u_int Id);
	virtual ~Refuge();
	void setProperties(std::string type, int value);
	int refugeIndex;
private:
};

#endif
