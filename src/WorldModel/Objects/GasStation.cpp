/*
 * GasStation.cpp
 *
 *  Created on: Jul 15, 2013
 *      Author: Danial Keshvary
 */

#include "GasStation.h"

using namespace std;

GasStation::GasStation(u_int id) :
		Building(id)
{
	typeFlag &= ~4;
}

GasStation::~GasStation()
{
}

void GasStation::setProperties(string type, int value)
{
	Building::setProperties(type, value);
}
