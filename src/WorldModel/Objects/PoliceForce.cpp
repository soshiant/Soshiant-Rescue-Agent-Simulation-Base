#include "PoliceForce.h"

using namespace std;

PoliceForce::PoliceForce(u_int Id) :
		Platoon(Id)
{
	typeFlag |= 4;
	typeFlag &= ~8;
	job = PFJ_NONE;
	araroCluster = nullptr;
	fireBlock = nullptr;
}

PoliceForce::~PoliceForce()
{
}

void PoliceForce::setProperties(string type, int value)
{
	Platoon::setProperties(type, value);
}
