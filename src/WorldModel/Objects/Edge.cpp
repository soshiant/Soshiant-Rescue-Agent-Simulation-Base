/*
 * Edge.cpp
 *
 *  Created on: Nov 28, 2010
 *      Author: Eqbal Sarjami
 */

#include "Edge.h"

using namespace Types;

Edge::Edge(int firstNodeIndex, int secondNodeIndex)
{
	this->firstNodeIndex = firstNodeIndex;
	this->secondNodeIndex = secondNodeIndex;
	this->passingMode = PM_UNKNOWN;
	this->hasToBeSent = -1;
}

Edge::~Edge()
{
}

int Edge::getFirstNodeIndex()
{
	return firstNodeIndex;
}

int Edge::getSecondNodeIndex()
{
	return secondNodeIndex;
}

int Edge::getLength()
{
	return length;
}

int Edge::getMotionlessIndex()
{
	return motionlessIndex;
}

void Edge::setLength(int length)
{
	this->length = length;
}

int Edge::getHasToBeSent()
{
	return hasToBeSent;
}

void Edge::setPassingMode(Types::PassingMode passingMode)
{
	if (isVirtual())
	{
		cout << "Hey yo fuckin BASTARD! It's a virtual edge! WTF R yo doin?!" << endl;
	}

	this->passingMode = passingMode;
}

void Edge::setMotionlessIndex(int motionlessIndex)
{
	this->motionlessIndex = motionlessIndex;
}

void Edge::setHasToBeSent(int time)
{
	hasToBeSent = time;
}

PassingMode Edge::getPassingMode()
{
	if (isVirtual())
	{
		return PM_PASSABLE;
	}

	return passingMode;
}

bool Edge::isVirtual()
{
	return motionlessIndex == -1;
}
