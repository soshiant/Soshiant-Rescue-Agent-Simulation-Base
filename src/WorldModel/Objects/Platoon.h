#ifndef _PLATOON_H
#define	_PLATOON_H

#include "Human.h"

class Platoon: public Human
{
public:
	Platoon();
	Platoon(u_int Id);
	virtual ~Platoon();
	void setProperties(std::string type, int value);
	int platoonIndex;
	Types::CommandType myLastCommand;
	bool canMove();
	void setCanMove(int a);
	void setCanMove(bool a);
	vector<MotionlessObject *> positionHistory;
	bool isMoved();
	bool dostItMoveEver();

	void setLastMoveCommand(vector<int> moves);
	vector<int> &getLastMoveCommand();

	bool needPoliceHelp;
	bool amIThePreComputer;

	int pizzaSlice = -1;

private:

	vector<int> lastMoveCommand;
	bool canitMove;
};

#endif	/* _PLATOON_H */

