#ifndef __RCROBJECT_H_
#define __RCROBJECT_H_

#include <string>
#include "../../Utilities/Types.h"
#include "../../Utilities/Encodings.h"
#include "../../Utilities/Geometry/Point.h"
#include "../../Utilities/Debugger.h"

class RCRObject
{
public:
	RCRObject(u_int Id);
	virtual ~RCRObject();
	virtual void setProperties(std::string type, int value);
	bool operator<(RCRObject &obj);
	bool operator>(RCRObject &obj);
	bool operator ==(const RCRObject &r1);
	u_int getId() const;
	u_int getX() const;
	u_int getY() const;
	u_int getRealX() const;
	u_int getRealY() const;
	Geometry::Point getPos() const;
	void setRealX(u_int x);
	void setRealY(u_int y);
	void setX(u_int x);
	void setY(u_int y);
	void setLastCycleUpdated(int value);
	int getLastCycleUpdated();
	void setLastCycleUpdatedBySense(int value);
	int getLastCycleUpdatedBySense();
	void setLastCycleSentInRadar(int value);
	int getLastCycleSentInRadar();
	void setNumberOfSent(int value);
	int getNumberOfSent();
	int lastTimeSeen;
	bool isHuman();
	bool isMotionlessObject();
	bool isCivilian();
	bool isPlatoon();
	bool isFireBrigade();
	bool isAmbulanceTeam();
	bool isPoliceForce();
	bool isPoliceOffice();
	bool isFireStation();
	bool isAmbulanceCenter();
	bool isCenter();
	bool isBuilding();
	bool isBlockade();
	bool isGasStation();
	bool isRoad();
	bool isHydrant();
	bool isRefuge();
	bool operator ==(RCRObject *);
	bool operator !=(RCRObject *);

protected:
	u_int x;
	u_int y;
	u_int realX;
	u_int realY;
	u_int id;
	int lastCycleUpdated;
	int lastCycleUpdatedBySense;
	int lastCycleSentInRadar;

	int numberOfSent;
	char typeFlag;

};
bool RCRObjectCompare(RCRObject* o1, RCRObject* o2);
#endif
