#ifndef __FIRE_STATION_H_
#define __FIRE_STATION_H_

#include "Building.h"

class FireStation: public Building
{
public:
	FireStation(u_int Id);
	virtual ~FireStation();
	void setProperties(std::string type, int value);
	int fireStationIndex;
private:
};

#endif
