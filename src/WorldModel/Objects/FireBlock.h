#ifndef _FIREBLOCK_H
#define _FIREBLOCK_H

#include <iostream>
#include <vector>
#include "Road.h"
#include "FireBrigade.h"
#include "GasStation.h"
#include "../../WorldGraph/WorldGraph.h"

#define BIG_DANGROUS_BUILDING 1000

using namespace std;
using namespace Geometry;

class WorldModel;
class WorldGraph;
class EstimatedData;

struct PointToBuilding
{
	Point point;
	Building * building;
	bool isMark;
};

struct BuildingByAngle
{
	Building * building;
	double angle;
};

class FireBlock
{
public:
	FireBlock();
	~FireBlock();

	void setAroundBuildingsForPolice(WorldModel*);
	void setAroundBuildings(WorldModel*);
	void setFireBrigades(vector<FireBrigade *>);
	void setCenterPoint();
	void setNeededFireBrigade(int);
	void setNeededPoliceForce(int);
	void setValue(float myvalue);
	void setAllBuildings(vector<Building *>);
	void setAroundRoadsForPoliceForces();
	void setImportantBuildings();
	void setShape();
	void sortBuildingByAngle(vector<Building*>&);
	void setSuspiciousBuildings(WorldModel*);
	void addBuilding(Building*);
	void addFireBrigade(FireBrigade*);
	void addClearingRoads(Road*);
	void addAroundOfConvexHullFiryNeighbors();
	void clearRoadsForClear();
	int getDistanceToPoint(Point point);
	int getGroundArea();
	int getNeededFireBrigade();
	int getNeededPoliceForce();
	int getGroundDistToMotionless(MotionlessObject*, WorldGraph*, Types::GraphMode);
	unsigned long getTotalArea();
	void setExtinguishability(bool);
	bool isExtinguishable();
	float getValue();
	double cross(Point, Point);
	Point getCenterPoint();
	vector<PointToBuilding> convexHull(vector<PointToBuilding>&);
	vector<FireBrigade*>& getFireBrigades();
	vector<Building*>& getAroundBuildings();
	vector<Building*>& getSuspiciousBuildings();
	vector<Building*>& getAllBuildings();
	vector<Road*> &getAroundRoadsForPoliceForces();
	vector<Road*> &getClearingRoads();

	int fireBlockIndex;
	int slice = -1;

private:
	void smoothAroundBuildings(vector<Building*>&, WorldModel*);

	vector<Building*> aroundBuildings;
	vector<Building*> suspiciousBuildings;
	vector<Building*> allBuildings;
	vector<FireBrigade*> fireBrigades;
	vector<Road*> aroundRoadsForPolice;
	vector<Road*> roadsForClear;
	Point centerPoint;
	int neededFireBrigade;
	int neededPoliceForce;
	float value;
	bool extinguishable;
};

#endif //_FIREBLOCKـH
