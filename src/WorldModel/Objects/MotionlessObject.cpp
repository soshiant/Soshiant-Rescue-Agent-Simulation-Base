#include "MotionlessObject.h"

#define LOGLEVEL 1

using namespace std;
using namespace Types;
using namespace Geometry;

MotionlessObject::MotionlessObject(u_int id) :
		RCRObject(id)
{
	typeFlag &= ~1;
	representiveNodeIndex = -1;
	iHaveBeenHere = false;
	maxDistFromCenter = -1;
	isAnyPoliceForceReachAbleToThis = false;
	firstTimeInnerPlatoons.clear();
	lastTimeCheckedInWG = 0;
}

MotionlessObject::MotionlessObject() :
		RCRObject(0)
{
}

MotionlessObject::~MotionlessObject()
{
}

void MotionlessObject::addEdge(int value)
{
	static int tmp = 0;
	static Point p;
	switch (tmp)
	{
	case 0:
	{
		p.setX(value);
		break;
	}
	case 1:
	{
		p.setY(value);
		shape.addVertex(p);
		break;
	}
	case 2:
	{
		break;
	}
	case 3:
	{
		break;
	}
	case 4:
	{
		ids.push_back(value);
		serverIDs.push_back(value);
		break;
	}
	}

	tmp = (tmp + 1) % 5;
}

void MotionlessObject::setProperties(string type, int value)
{
	if (type == Encodings::getPropertyType(PT_EDGES))
	{
		this->addEdge(value);
	}
	else
	{
		RCRObject::setProperties(type, value);
	}
}

Polygon& MotionlessObject::getShape()
{
	return shape;
}

std::vector<int> & MotionlessObject::getIDs()
{
	return ids;
}

std::vector<int> MotionlessObject::getServerIDs()
{
	return serverIDs;
}

void MotionlessObject::setID(int index, int val)
{
	ids[index] = val;
}

std::vector<int>& MotionlessObject::getRelativeNodesIndexes()
{
	return relativeNodesIndexes;
}

void MotionlessObject::addRelativeNodeIndex(int index)
{
	relativeNodesIndexes.push_back(index);
}

std::vector<int>& MotionlessObject::getRelativeEdgesIndexes()
{
	return relativeEdgesIndexes;
}

void MotionlessObject::addRelativeEdgeIndex(int index)
{
	relativeEdgesIndexes.push_back(index);
}

std::vector<int>& MotionlessObject::getNeighbours()
{
	return neighbours;
}

void MotionlessObject::addNeighbour(int index)
{
	neighbours.push_back(index);
}

std::vector<int>& MotionlessObject::getInnerHumansIndexes()
{
	return innerHumansIndexes;
}

void MotionlessObject::setMaxDistanceFromCenter(int maxDistanceFromCenter)
{
	maxDistFromCenter = maxDistanceFromCenter;
}

int MotionlessObject::getMaxDistanceFromCenter()
{
	return maxDistFromCenter;
}

void MotionlessObject::addInnerHumanIndex(int index)
{
	innerHumansIndexes.push_back(index);
}

void MotionlessObject::clearInnerHumans()
{
	innerHumansIndexes.clear();
}

int MotionlessObject::getRepresentiveNodeIndex()
{
	return representiveNodeIndex;
}

int MotionlessObject::getWithoutBlockadeRepresentiveNodeIndex()
{
	return withoutBlockadeRepresentiveNodeIndex;
}

void MotionlessObject::setRepresentiveNodeIndex(int index)
{
	representiveNodeIndex = index;
}

void MotionlessObject::setWithoutBlockadeRepresentiveNodeIndex(int index)
{
	withoutBlockadeRepresentiveNodeIndex = index;
}

void MotionlessObject::addInsideCivilianIndex(int ind)
{
	civilianInsideIndexes.push_back(ind);
}

void MotionlessObject::clearInsideCivilians()
{
	civilianInsideIndexes.clear();
}

std::vector<int> &MotionlessObject::getInsideCivilianIndexes()
{
	return civilianInsideIndexes;
}

void MotionlessObject::clearInsidePlatoons()
{
	platoonsInsideIndexes.clear();
}

std::vector<int> & MotionlessObject::getInsidePlatoonIndexes()
{
	return platoonsInsideIndexes;
}

void MotionlessObject::addInsidePlatoonIndex(int ind)
{
	platoonsInsideIndexes.push_back(ind);
}

std::vector<int> & MotionlessObject::getFirstTimeInnerPlatoonIndexes()
{
	return firstTimeInnerPlatoons;
}

void MotionlessObject::addFirstTimeInnerPlatoonIndex(int ind)
{
	firstTimeInnerPlatoons.push_back(ind);
}
