#include <cmath>
#include "RCRObject.h"
#include "FireStation.h"
#include "AmbulanceTeam.h"
#include "PoliceForce.h"

using namespace std;
using namespace Types;
using namespace Geometry;

RCRObject::RCRObject(u_int id)
{
	this->id = id;
	lastTimeSeen = -1;
	setLastCycleSentInRadar(0); //correct: only in radar after sending object
	setLastCycleUpdated(0); // correct: receive data from object anyway
	setLastCycleUpdatedBySense(0); //correct: wtf it uesed in radar!? just set when receive data directly from server
	setNumberOfSent(0); // correct: any time we send the object on radar it ++
	typeFlag = 0;
}

RCRObject::~RCRObject()
{
	lastTimeSeen = -1;
	setLastCycleSentInRadar(0);
	setLastCycleUpdated(0);
	setLastCycleUpdatedBySense(0);
	typeFlag = 0;
}

u_int RCRObject::getId() const
{
	return id;
}

u_int RCRObject::getX() const
{
	return x;
}

u_int RCRObject::getY() const
{
	return y;
}

Point RCRObject::getPos() const
{
	Point point(x, y);
	return point;
}

void RCRObject::setX(u_int x)
{
	this->x = x;
}

void RCRObject::setY(u_int y)
{
	this->y = y;
}

u_int RCRObject::getRealX() const
{
	return realX;
}

u_int RCRObject::getRealY() const
{
	return realY;
}

void RCRObject::setRealX(u_int x)
{
	this->realX = x;
}

void RCRObject::setRealY(u_int y)
{
	this->realY = y;
}

// We update this object in sense or radar

void RCRObject::setLastCycleUpdated(int value)
{
	this->lastCycleUpdated = value;
}

int RCRObject::getLastCycleUpdated()
{
	return this->lastCycleUpdated;
}

// get last cycle that we send this object information in radar

void RCRObject::setLastCycleSentInRadar(int value)
{
	this->lastCycleSentInRadar = value;
}

int RCRObject::getLastCycleSentInRadar()
{
	return this->lastCycleSentInRadar;
}

// we update this object just in our sense 

void RCRObject::setLastCycleUpdatedBySense(int value)
{
	this->lastCycleUpdatedBySense = value;
}

int RCRObject::getLastCycleUpdatedBySense()
{
	return this->lastCycleUpdatedBySense;
}

void RCRObject::setNumberOfSent(int value)
{
	this->numberOfSent = value;
}

int RCRObject::getNumberOfSent()
{
	return this->numberOfSent;
}

bool RCRObject::operator<(RCRObject &obj)
{
	return obj.getId() > this->getId();
}

bool RCRObject::operator>(RCRObject &obj)
{
	return obj.getId() < this->getId();
}

void RCRObject::setProperties(string type, int value)
{
	if (type == Encodings::getPropertyType(PT_X))
	{
		this->setX(value);

		if (getRealX() == 0)
		{
			this->setRealX(value);
		}
	}
	else if (type == Encodings::getPropertyType(PT_Y))
	{
		this->setY(value);

		if (getRealY() == 0)
		{
			this->setRealY(value);
		}
	}
	else
	{
	}
}

bool RCRObject::operator ==(const RCRObject &r1)
{
	if (r1.getId() == this->getId())
		return true;
	else
		return false;
}

bool RCRObject::isHuman()
{
//	return (typeFlag & 1);
	return (dynamic_cast<Human*>(this) != nullptr);
}

bool RCRObject::isMotionlessObject()
{
//	return !(typeFlag & 1);
	return (dynamic_cast<MotionlessObject*>(this) != nullptr);
}

bool RCRObject::isCivilian()
{
//	return isHuman() && (typeFlag & 2);
	return (dynamic_cast<Civilian*>(this) != nullptr);
}

bool RCRObject::isPlatoon()
{
//	return isHuman() && !(typeFlag & 2);
	return (dynamic_cast<Platoon*>(this) != nullptr);
}

bool RCRObject::isFireBrigade()
{
//	return isPlatoon() && (typeFlag & 4) && (typeFlag & 8);
	return (dynamic_cast<FireBrigade*>(this) != nullptr);
}

bool RCRObject::isAmbulanceTeam()
{
//	return isPlatoon() && !(typeFlag & 4) && !(typeFlag & 8);
	return (dynamic_cast<AmbulanceTeam*>(this) != nullptr);
}

bool RCRObject::isPoliceForce()
{
//	return isPlatoon() && (typeFlag & 4) && !(typeFlag & 8);
	return (dynamic_cast<PoliceForce*>(this) != nullptr);
}

bool RCRObject::isPoliceOffice()
{
	return (dynamic_cast<PoliceOffice*>(this) != nullptr);
}

bool RCRObject::isFireStation()
{
	return (dynamic_cast<FireStation*>(this) != nullptr);
}

bool RCRObject::isAmbulanceCenter()
{
	return (dynamic_cast<AmbulanceCenter*>(this) != nullptr);
}

bool RCRObject::isCenter()
{
	return (isPoliceOffice() || isAmbulanceCenter() || isFireStation());
}

bool RCRObject::isBuilding()
{
//	return isMotionlessObject() && (typeFlag & 2);
	return (dynamic_cast<Building*>(this) != nullptr);
}

bool RCRObject::isBlockade()
{
//	return !isMotionlessObject() && !isHuman();
	return (dynamic_cast<Blockade*>(this) != nullptr);
}

bool RCRObject::isGasStation()
{
//	return isBuilding() && !(typeFlag & 4);
	return (dynamic_cast<GasStation*>(this) != nullptr);
}

bool RCRObject::isRoad()
{
//	return isMotionlessObject() && !(typeFlag & 2);
	return (dynamic_cast<Road*>(this) != nullptr);
}

bool RCRObject::isHydrant()
{
//	return isRoad() && (typeFlag & 4);
	return (dynamic_cast<Hydrant*>(this) != nullptr);
}

bool RCRObject::isRefuge()
{
//	return isBuilding() && (typeFlag & 4);
	return (dynamic_cast<Refuge*>(this) != nullptr);
}

bool RCRObjectCompare(RCRObject* o1, RCRObject* o2)
{
	return o1->getId() < o2->getId();
}

bool RCRObject::operator ==(RCRObject * r)
{
	if (this->getId() == r->getId())
	{
		return true;
	}

	return false;
}

bool RCRObject::operator !=(RCRObject * r)
{
	if (this->getId() == r->getId())
	{
		return false;
	}

	return true;
}
