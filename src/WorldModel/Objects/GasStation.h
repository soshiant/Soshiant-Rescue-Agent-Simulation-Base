/*
 * GasStation.h
 *
 *  Created on: Jul 15, 2013
 *      Author: Danial Keshvary
 */

#ifndef GASSTATION_H_
#define GASSTATION_H_

#include "Building.h"

class GasStation: public Building
{
public:
	GasStation(u_int id);
	virtual ~GasStation();
	void setProperties(std::string type, int value);
	int gasStationIndex;
};

#endif /* GASSTATION_H_ */
