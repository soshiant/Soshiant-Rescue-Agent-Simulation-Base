#include "Refuge.h"

using namespace std;

Refuge::Refuge(u_int Id) :
		Building(Id)
{
	typeFlag |= 4;
}

Refuge::~Refuge()
{
}

void Refuge::setProperties(string type, int value)
{
	Building::setProperties(type, value);
}
