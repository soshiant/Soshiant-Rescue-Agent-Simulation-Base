/*
 * Hydrant.cpp
 *
 *  Created on: Jul 16, 2013
 *      Author: Danial Keshvary
 */

#include "Hydrant.h"

using namespace std;

Hydrant::Hydrant(u_int id) :
		Road(id)
{
	typeFlag |= 4;
}

Hydrant::~Hydrant()
{
}

void Hydrant::setProperties(string type, int value)
{
	Road::setProperties(type, value);
}
