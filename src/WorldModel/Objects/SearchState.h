/*
 * SearchState.h
 *
 *  Created on: Feb 28, 2012
 *      Author: alimohammad
 */

#ifndef SEARCHSTATE_H_
#define SEARCHSTATE_H_

#include "Building.h"
#include "MotionlessObject.h"
#include <vector>
#include <iostream>
#include <algorithm>

class SearchState
{
public:

	SearchState();
	vector<Building *> buildings;

	void setCenter();
	Point getCenter();

	void setPercentage(int p);
	int getPercentage();

	void setValue(int v);
	int getValue();

private:
	Point center;
	int percentage;
	int value;

};

#endif /* SEARCHSTATE_H_ */
