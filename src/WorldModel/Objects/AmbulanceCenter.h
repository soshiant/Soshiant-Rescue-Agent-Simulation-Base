#ifndef __AMBULANCE_CENTER_H_
#define __AMBULANCE_CENTER_H_

#include "Building.h"

class AmbulanceCenter: public Building
{
public:
	AmbulanceCenter(u_int Id);
	virtual ~AmbulanceCenter();
	void setProperties(std::string type, int value);
	int ambulanceCenterIndex;
private:
};

#endif
