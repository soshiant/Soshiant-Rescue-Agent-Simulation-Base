#ifndef __MOTIONLESS_OBJECT_
#define __MOTIONLESS_OBJECT_

#include "RCRObject.h"
#include "../../Utilities/Geometry/Polygon.h"
#include <set>
#include <cmath>

class Node;
class GraphEdge;
class GasStation;

class MotionlessObject: public RCRObject
{
public:
	MotionlessObject(u_int Id);
	MotionlessObject();
	virtual ~MotionlessObject();
	void addEdge(int a);
	void setProperties(std::string type, int value);
	Geometry::Polygon& getShape();
	std::vector<int> & getIDs();
	std::vector<int> getServerIDs();
	void setID(int, int);
	std::vector<int>& getRelativeNodesIndexes();
	void addRelativeNodeIndex(int);
	std::vector<int>& getRelativeEdgesIndexes();
	void addRelativeEdgeIndex(int);
	std::vector<int>& getNeighbours();
	void addNeighbour(int);
	std::vector<int>& getInnerHumansIndexes();
	void addInnerHumanIndex(int);
	void clearInnerHumans();

	std::vector<int> &getInsideCivilianIndexes();
	void addInsideCivilianIndex(int ind);
	void clearInsideCivilians();

	std::vector<int> &getInsidePlatoonIndexes();
	void addInsidePlatoonIndex(int ind);
	void clearInsidePlatoons();

	std::vector<int> &getFirstTimeInnerPlatoonIndexes();
	void addFirstTimeInnerPlatoonIndex(int ind);

	int getRepresentiveNodeIndex();
	int getWithoutBlockadeRepresentiveNodeIndex();
	void setRepresentiveNodeIndex(int);
	void setWithoutBlockadeRepresentiveNodeIndex(int);

	void setMaxDistanceFromCenter(int maxDistanceFromCenter);
	int getMaxDistanceFromCenter();

	int motionlessIndex;
	bool iHaveBeenHere;
	int maxDistFromCenter;
	int lastTimeCheckedInWG;

	std::vector<MotionlessObject *> closerThan30M;
	std::vector<MotionlessObject *> closerThan50M;
	std::vector<MotionlessObject *> closerThan30B;
	std::vector<MotionlessObject *> closerThan50B;
	std::vector<MotionlessObject *> closerThan30R;
	std::vector<MotionlessObject *> closerThan50R;

	bool isAnyPoliceForceReachAbleToThis;

private:

	Geometry::Polygon shape;
	std::vector<int> ids;
	std::vector<int> serverIDs;
	std::vector<int> relativeNodesIndexes;
	std::vector<int> relativeEdgesIndexes;
	std::vector<int> neighbours;
	std::vector<int> innerHumansIndexes;
	std::vector<int> civilianInsideIndexes;
	std::vector<int> platoonsInsideIndexes;
	std::vector<int> firstTimeInnerPlatoons;
	int representiveNodeIndex;
	int withoutBlockadeRepresentiveNodeIndex;
};

#endif
