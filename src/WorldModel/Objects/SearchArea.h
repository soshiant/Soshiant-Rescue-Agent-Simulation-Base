/*
 * SeachArea.h
 *
 *  Created on: Mar 14, 2014
 *      Author: ali
 */

#ifndef SEACHAREA_H_
#define SEACHAREA_H_
#include "Building.h"
#include "MotionlessObject.h"
#include "../../Utilities/Geometry/Point.h"

struct SearchShape;

class SearchArea
{
	public:
		SearchArea();
		virtual ~SearchArea();
		Building* building = nullptr;
		void addSearchShape(MotionlessObject*, Polygon, vector<Point>);
		vector<SearchShape*> seachShapes;
};

struct SearchShape
{
		MotionlessObject* searchMotionless = nullptr;
		Polygon polygon;
		vector<Point> searchPoints;
};

#endif /* SEACHAREA_H_ */
