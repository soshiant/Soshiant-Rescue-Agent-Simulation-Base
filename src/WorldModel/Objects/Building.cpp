#include "Building.h"
#include "FireBlock.h"
#include "../../Utilities/FireEstimator/Wall.h"

#define LOGLEVEL 1

using namespace std;
using namespace Types;

Building::Building(u_int id) :
		MotionlessObject(id)
{
	temperature = 0;
	fieryness = 0;
	typeFlag |= 2;
	civilianProbability = 0;
	setLastCycleInFire(0);
	setLastCycleExtinguished(0);
	hardToExtinguish = false;
	fierynessDuringTime.clear();
	buildingFireSearchValue = 0;
	brokenness = MAX_INT;
	lastTimeSearchedForCivilian = -1;
}

Building::~Building()
{
}

void Building::setLastCycleExtinguished(int t)
{
	lastCycleExtinguished = t;
}

FireBlock* Building::getFireBlock() const
{
	return fireBlock;
}

void Building::setFireBlock(FireBlock* fireBlock)
{
	this->fireBlock = fireBlock;
}

int Building::getLastCycleExtinguished()
{
	return lastCycleExtinguished;
}

void Building::setFieryness(unsigned short int newFieryness)
{
	fieryness = newFieryness;

	if (estimatedData != nullptr)
	{
		estimatedData->setFieryness(newFieryness);
	}
}

void Building::setHardToExtinguish(bool a)
{
	hardToExtinguish = a;
}

bool Building::isHardToExtinguish()
{
	return hardToExtinguish;
}

bool Building::hasFire()
{
	if (this->getFieryness() >= 1 && this->getFieryness() <= 3)
		return true;

	return false;
}

bool Building::operator<(Building a)
{
	return this->getId() < a.getId();
}

void Building::setValue(int bValue)
{
	this->value = bValue;
}

int Building::getValue()
{
	return this->value;
}

bool Building::isUnburned()
{
	if (this->getFieryness() == 0)
		return true;
	return false;
}

bool Building::isPutOut()
{
	if (this->getFieryness() >= 5 && this->getFieryness() <= 7)
	{
		return true;
	}

	return false;
}

u_int Building::getAreaGround() const
{
	return AreaGround;
}

u_int Building::getAreaTotal() const
{
	return AreaTotal;
}

BuildingCode Building::getBuildingCode() const
{
	return buildingCode;
}

unsigned short int Building::getFieryness() const
{
	return fieryness;
}

unsigned short int Building::getFloors() const
{
	return floors;
}

u_int Building::getBrokenness() const
{
	return brokenness;
}

int Building::getLastCycleInFire() const
{
	return lastTimeInFire;
}

int Building::getTemperature() const
{
	return temperature;
}

int Building::getImportance() const
{
	return importance;
}

u_int Building::getAttributes() const
{
	return attributes;
}

EstimatedData* Building::getEstimatedData()
{
	return estimatedData;
}

int Building::getCivilianProbability()
{
	return civilianProbability;
}

void Building::setAreaGround(u_int AreaGround)
{
	this->AreaGround = AreaGround;
}

void Building::setAreaTotal(u_int AreaTotal)
{
	this->AreaTotal = AreaTotal;
}

void Building::setBuildingCode(BuildingCode buildingCode)
{
	this->buildingCode = buildingCode;
}

void Building::setFloors(unsigned short int floors)
{
	this->floors = floors;
}

void Building::setImportance(int value)
{
	this->importance = value;
}

void Building::setBrokenness(u_int brokenness)
{
	this->brokenness = brokenness;
}

void Building::setAttributes(u_int value)
{
	this->attributes = value;
}

void Building::setLastCycleInFire(int t)
{
	this->lastTimeInFire = t;
}

void Building::setTemperature(u_int value)
{
	this->temperature = value;

	if (estimatedData != nullptr)
	{
		estimatedData->setTemperature(value);
	}
}

void Building::setProperties(string type, int value)
{
	if (type == Encodings::getPropertyType(PT_FLOORS))
	{
		this->setFloors(value);
	}
	else if (type == Encodings::getPropertyType(PT_BROKENNESS))
	{
		this->setBrokenness(value);
	}
	else if (type == Encodings::getPropertyType(PT_BUILDING_AREA_GROUND))
	{
		this->setAreaGround(value);
	}
	else if (type == Encodings::getPropertyType(PT_BUILDING_AREA_TOTAL))
	{
		this->setAreaTotal(value);
	}
	else if (type == Encodings::getPropertyType(PT_BUILDING_CODE))
	{
		this->setBuildingCode((BuildingCode) value);
	}
	else if (type == Encodings::getPropertyType(PT_FIERYNESS))
	{
		this->setFieryness(value);
	}
	else if (type == Encodings::getPropertyType(PT_IMPORTANCE))
	{
		this->setImportance(value);
	}
	else if (type == Encodings::getPropertyType(PT_TEMPERATURE))
	{
		this->setTemperature(value);
	}
	else if (type == Encodings::getPropertyType(PT_BUILDING_ATTRIBUTES))
	{
		this->setAttributes(value);
	}
	else
	{
		MotionlessObject::setProperties(type, value);
	}
}

void Building::setNeededFireBrigades(int number)
{
	this->neededFireBrigades = number;
}

void Building::setCivilianProbability(int civilianProbability)
{
	this->civilianProbability = civilianProbability;
}

int Building::getNeededFireBrigades()
{
	return this->neededFireBrigades;
}

vector<InSightBuilding*> Building::getInSightBuildings()
{
	return inSightBuildings;
}

void Building::setWallsLength(double length)
{
	wallsLength = length;
}

double Building::getWallsLength()
{
	return wallsLength;
}

void Building::addSearchStates(Types::BuildingsSearchState bss)
{
	searchStates.push_back(bss);
}

void Building::clearSearchStates()
{
	searchStates.clear();
}

void Building::setWorthlessness(bool worthlessness)
{
	this->worthlessness = worthlessness;
}

bool Building::getWorthlessness()
{
	return this->worthlessness;
}

vector<Types::BuildingsSearchState> Building::getSearchStates()
{
	return searchStates;
}
