/* 
 * File:   Edge.h
 * Author: Eqbal Sarjami
 *
 * Created on November 28, 2010, 1:40 PM
 */

#include "../../Utilities/Types.h"
#include "MotionlessObject.h"
#include "../../Utilities/Geometry/Segment.h"

#ifndef _EDGE_H
#define	_EDGE_H

using namespace Geometry;

class Edge
{
	public:
		Edge(int, int);
		~Edge();
		int getFirstNodeIndex();
		int getSecondNodeIndex();
		int getLength();
		int getMotionlessIndex();
		int getHasToBeSent();
		void setLength(int);
		void setPassingMode(Types::PassingMode);
		void setMotionlessIndex(int motionlessIndex);
		void setHasToBeSent(int time);
		Types::PassingMode getPassingMode();
		bool isVirtual();

		int edgeIndex;

	protected:
		int firstNodeIndex;
		int secondNodeIndex;
		int length;
		int hasToBeSent;
		Types::PassingMode passingMode;
		int motionlessIndex;
};

#endif	/* _EDGE_H */

