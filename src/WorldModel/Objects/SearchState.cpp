/*
 * SearchState.cpp
 *
 *  Created on: Feb 28, 2012
 *      Author: alimohammad
 */

#include "SearchState.h"

SearchState::SearchState()
{
}

void SearchState::setValue(int v)
{
	value = v;
}

int SearchState::getValue()
{
	return value;
}

void SearchState::setPercentage(int p)
{
	percentage = p;
}

int SearchState::getPercentage()
{
	return percentage;
}

void SearchState::setCenter()
{
	int gX = 0, gY = 0;
	for (int i = 0; i < (int) buildings.size(); i++)
	{
		gX = gX + buildings[i]->getPos().getX();
		gY = gY + buildings[i]->getPos().getY();
	}

	gX = gX / buildings.size();
	gY = gY / buildings.size();

	center = Point(gX, gY);
}

Point SearchState::getCenter()
{
	return center;
}
