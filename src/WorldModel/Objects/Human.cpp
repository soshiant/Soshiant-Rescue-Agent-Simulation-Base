#include "Human.h"

#define LOGLEVEL 1

using namespace std;
using namespace Types;

Human::Human(u_int Id) :
		RCRObject(Id)
{
	motionlessObject = nullptr;
	firstTimeMotionless = nullptr;
	typeFlag |= 1;
	representiveNodeIndex = -1;
	damageHistory.clear();
	hpHistory.clear();
	firstTimeBuriedness = -1;
	isAvailable = true;
	buriedness = 0;
	timeToDeath = id;
}

Human::Human() :
		RCRObject(0)
{
}

Human::~Human()
{
}

u_int Human::getBuriedness() const
{
	return buriedness;
}

u_int Human::getDamage() const
{
	return damage;
}

u_int Human::getHp() const
{
	return hp;
}

u_int Human::getDirection() const
{
	return direction;
}

u_int Human::getPosition() const
{
	return position;
}

u_int Human::getPositionExtra() const
{
	return positionExtra;
}

u_int Human::getPositionHistory() const
{
	return positionHistory;
}

u_int Human::getStamina() const
{
	return stamina;
}

int Human::getTimeToDeath()
{
	return this->timeToDeath;
}

void Human::setTimeToDeath(int value)
{
	this->timeToDeath = value;
}

void Human::calculateTimeToDeath(int globalTime)
{
	if (globalTime == this->getLastCycleUpdated())
	{
//		if (hp > MAX_HP || damage > 300)
//		{
//			return;
//		}
//
//		int time = lastCycleUpdated;
//
//		if (!hpHistory.empty() && !damageHistory.empty())
//		{
//			HpDuringTheTime iHp;
//			iHp.time = hpHistory.size() - 1;
//			iHp.hp = this->hp;
//			while (iHp.time > 0 && iHp.hp == hpHistory[iHp.time].hp)
//			{
//				iHp.time--;
//			}
//
//			DamageDuringTheTime iDamage;
//			iDamage.time = damageHistory.size() - 1;
//			iDamage.damage = this->damage;
//			while (iDamage.time > 0 && iDamage.damage == damageHistory[iDamage.time].damage)
//			{
//				iDamage.time--;
//			}
//
//			if (iDamage.time != (int) damageHistory.size() - 1 && iHp.time != (int) hpHistory.size() - 1)
//			{
//				time = max(damageHistory[iDamage.time].time, hpHistory[iHp.time].time);
//			}
//		}

		int time = lastCycleUpdated;
		double hp = this->hp;
		double dmg = this->damage;
		double brokenDmg = dmg * getBuriedness() / 100.;
		double buryDmg = dmg - brokenDmg;

		while (hp > 0)
		{
			buryDmg += buryDmg * buryDmg * 0.000035;
			buryDmg += 0.01;
			brokenDmg += brokenDmg * brokenDmg * 0.00025;
			brokenDmg += 0.01;
			hp -= (buryDmg + brokenDmg);
			time++;

			if (time + globalTime > 1000)
			{
				break;
			}
		}

		if (time - globalTime > 500)
		{
			setTimeToDeath(500);
		}
		else
		{
			setTimeToDeath(time - globalTime);
		}
	}
	else
	{
		this->setTimeToDeath(getTimeToDeath() - 1);
	}
}

void Human::setBuriedness(u_int buriedness)
{
	if (this->firstTimeBuriedness == -1)
	{
		firstTimeBuriedness = buriedness;
	}

	this->buriedness = buriedness;
}

void Human::setDamage(u_int damage)
{
	this->damage = damage;
}

void Human::setHp(u_int hp)
{
	this->hp = hp;
}

void Human::addDamageHistory(DamageDuringTheTime dt)
{
	this->damageHistory.push_back(dt);
}

void Human::addHpHistory(HpDuringTheTime ht)
{
	this->hpHistory.push_back(ht);
}

vector<DamageDuringTheTime>& Human::getDamageHistory()
{
	return this->damageHistory;
}

void Human::setDamageHistory(vector<DamageDuringTheTime> dmgHst)
{
	damageHistory = dmgHst;
}

vector<HpDuringTheTime>& Human::getHpHistory()
{
	return this->hpHistory;
}

void Human::setHpHistory(vector<HpDuringTheTime> hpHst)
{
	hpHistory = hpHst;
}

MotionlessObject* Human::getMotionlessObject() const
{
	return motionlessObject;
}

unsigned long long int Human::getTravelDistaance() const
{
	return this->travelDistance;
}

void Human::setTravelDistance(int value)
{
	this->travelDistance = value;
}

void Human::setMotionlessObject(MotionlessObject *motionlessObject)
{
	if (firstTimeMotionless == nullptr)
	{
		firstTimeMotionless = motionlessObject;
	}
	this->motionlessObject = motionlessObject;
}

void Human::setDirection(u_int direction)
{
	this->direction = direction;
}

void Human::setPosition(u_int position)
{
	this->position = position;
}

void Human::setPositionExtra(u_int positionExtra)
{
	this->positionExtra = positionExtra;
}

void Human::setPositionHistory(u_int positionHistory)
{
	this->positionHistory = positionHistory;
}

void Human::setStamina(u_int stamina)
{
	this->stamina = stamina;
}

void Human::setProperties(string type, int value)
{
	if (type == Encodings::getPropertyType(PT_BURIEDNESS))
	{
		this->setBuriedness(value);
	}
	else if (type == Encodings::getPropertyType(PT_DAMAGE))
	{
		this->setDamage(value);
	}
	else if (type == Encodings::getPropertyType(PT_HP))
	{
		this->setHp(value);
	}
	else if (type == Encodings::getPropertyType(PT_DIRECTION))
	{
		this->setDirection(value);
	}
	else if (type == Encodings::getPropertyType(PT_POSITION))
	{
		this->setPosition(value);
	}
	else if (type == Encodings::getPropertyType(PT_POSITION_HISTORY))
	{
		this->setPositionHistory(value);
	}
	else if (type == Encodings::getPropertyType(PT_STAMINA))
	{
		this->setStamina(value);
	}
	else if (type == Encodings::getPropertyType(PT_TRAVEL_DISTANCE))
	{
		this->setTravelDistance(value);
	}
	else
	{
		RCRObject::setProperties(type, value);
	}
}

int Human::getRepresentiveNodeIndex()
{
	return representiveNodeIndex;
}

int Human::getWithoutBlockadeRepresentiveNodeIndex()
{
	return withoutBlockadeRepresentiveNodeIndex;
}

void Human::setRepresentiveNodeIndex(int index)
{
	representiveNodeIndex = index;
}

void Human::setWithoutBlockadeRepresentiveNodeIndex(int index)
{
	withoutBlockadeRepresentiveNodeIndex = index;
}

void Human::setFirstTimeMotionless(MotionlessObject * m)
{
	firstTimeMotionless = m;
}

MotionlessObject * Human::getFirstTimeMotionless()
{
	return firstTimeMotionless;
}

void Human::setFirstTimeBuriedness(int b)
{
	firstTimeBuriedness = b;
}

int Human::getFirstTimeBuriedness()
{
	return firstTimeBuriedness;
}

void Human::clearDamageHistory()
{
	damageHistory.clear();
}

void Human::clearHpHistory()
{
	hpHistory.clear();
}

void Human::clearBuriednessHistory()
{
	buriednessHistory.clear();
}

vector<BuriednessDuringTime> &Human::getBuriednessHistory()
{
	return buriednessHistory;
}

void Human::setBuriednessHistory(vector<BuriednessDuringTime> brHst)
{
	buriednessHistory = brHst;
}

void Human::addBuriednessHistory(BuriednessDuringTime bt)
{
	buriednessHistory.push_back(bt);
}

void Human::deleteFirstBuriednessHistory()
{
	buriednessHistory.erase(buriednessHistory.begin() + 0);
}

void Human::deleteFirstDamageHistory()
{
	damageHistory.erase(damageHistory.begin() + 0);
}

void Human::deleteFirstHpHistory()
{
	hpHistory.erase(hpHistory.begin() + 0);
}
