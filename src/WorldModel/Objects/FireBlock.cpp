#include "FireBlock.h"

#define LOGLEVEL 1

using namespace std;
using namespace Geometry;

bool myLessPointToBuilding(PointToBuilding a, PointToBuilding b)
{
	if (a.point.getY() != b.point.getY())
	{
		return a.point.getY() < b.point.getY();
	}

	return a.point.getX() < b.point.getX();
}

bool comparing(BuildingByAngle a, BuildingByAngle b)
{
	return a.angle < b.angle;
}

FireBlock::FireBlock()
{
	fireBlockIndex = 0;
	value = 0;
	centerPoint = Point(-1, -1);
	neededFireBrigade = 0;
	slice = -1;
}

FireBlock::~FireBlock()
{
}

void FireBlock::setAroundRoadsForPoliceForces()
{
	setCenterPoint();
	aroundRoadsForPolice.clear();

	for (Building* b : aroundBuildings)
	{
		for (MotionlessObject* road : b->closerThan50R)
		{
			int buildingToCenter = b->getPos().distFromPoint(getCenterPoint());
			int roadToCenter = road->getPos().distFromPoint(getCenterPoint());
			if (roadToCenter > buildingToCenter) //To make sure that we choose the roads outside of the fireblock
			{
				aroundRoadsForPolice.push_back((Road*) road);
			}
		}
	}

	sort(aroundRoadsForPolice.begin(), aroundRoadsForPolice.end());
	aroundRoadsForPolice.resize(unique(aroundRoadsForPolice.begin(), aroundRoadsForPolice.end()) - aroundRoadsForPolice.begin());
}

void FireBlock::addAroundOfConvexHullFiryNeighbors()
{
	vector<Building *> addings;

	for (int i = 0; i < (int) aroundBuildings.size(); i++)
	{
		for (int j = 0; j < (int) aroundBuildings[i]->closerThan30B.size(); j++)
		{
			Building * thisBuilding = (Building *) aroundBuildings[i]->closerThan30B[j];
			if (thisBuilding->hasFire())
			{
				for (int k = 0; k < (int) thisBuilding->closerThan30B.size(); k++)
				{
					if (thisBuilding->closerThan30B[k]->getId() == aroundBuildings[i]->getId())
					{
						continue;
					}

					if (((Building *) thisBuilding->closerThan30B[k])->hasFire() == false)
					{
						addings.push_back((Building *) thisBuilding->closerThan30B[k]);
						break;
					}
				}
			}
		}
	}

	sort(addings.begin(), addings.end());
	addings.resize(unique(addings.begin(), addings.end()) - addings.begin());

	for (int i = 0; i < (int) addings.size(); i++)
	{
		aroundBuildings.push_back(addings[i]);
	}
}

void FireBlock::setAroundBuildings(WorldModel* world)
{
	if (allBuildings.empty())
	{
		LOG(Main, 1) << "[setAroundBuildings] building array is empty" << endl;
		return;
	}

	if (allBuildings.size() == 1 || allBuildings.size() == 2 || allBuildings.size() == 3)
	{
		aroundBuildings = allBuildings;
		return;
	}

	vector<PointToBuilding> allPoints;
	vector<PointToBuilding> firstAnswers;
	vector<PointToBuilding> secondAnswers;
	vector<PointToBuilding> finalAnswers;

	for (int i = 0; i < (int) allBuildings.size(); i++)
	{
		for (int j = 0; j < (int) allBuildings[i]->getShape().getVertices().size(); j++)
		{
			PointToBuilding thisPointToBuilding;
			thisPointToBuilding.building = allBuildings[i];
			thisPointToBuilding.point = allBuildings[i]->getShape().getVertices()[j];

			allPoints.push_back(thisPointToBuilding);
		}
	}

	sort(allPoints.begin(), allPoints.end(), myLessPointToBuilding);
	firstAnswers = convexHull(allPoints);

	for (int i = 0; i < (int) firstAnswers.size() - 1; i++)
	{
		finalAnswers.push_back(firstAnswers[i]);
	}

	reverse(allPoints.begin(), allPoints.end());

	secondAnswers = convexHull(allPoints);

	for (int i = 0; i < (int) secondAnswers.size() - 1; i++)
	{
		finalAnswers.push_back(secondAnswers[i]);
	}

	for (int i = 0; i < (int) finalAnswers.size(); i++)
	{
		aroundBuildings.push_back(finalAnswers[i].building);
//		for (InSightBuilding* isb : finalAnswers[i].building->getInSightBuildings())
//		{
//			if (world->allBuildings[isb->buildingIndex]->getEstimatedData()->getFieryness() == 0)
//			{
//				aroundBuildings.push_back(finalAnswers[i].building);
//				break;
//			}
//		}
	}

	sort(aroundBuildings.begin(), aroundBuildings.end());
	aroundBuildings.resize(unique(aroundBuildings.begin(), aroundBuildings.end()) - aroundBuildings.begin());

	sortBuildingByAngle(aroundBuildings);
}

void FireBlock::setAroundBuildingsForPolice(WorldModel* world)
{
	if (allBuildings.empty())
	{
		LOG(Main, 1) << "[setAroundBuildings] building array is empty" << endl;
		return;
	}

	if (allBuildings.size() == 1 || allBuildings.size() == 2 || allBuildings.size() == 3)
	{
		aroundBuildings = allBuildings;
		return;
	}

	vector<PointToBuilding> allPoints;
	vector<PointToBuilding> firstAnswers;
	vector<PointToBuilding> secondAnswers;
	vector<PointToBuilding> finalAnswers;

	for (int i = 0; i < (int) allBuildings.size(); i++)
	{
		for (int j = 0; j < (int) allBuildings[i]->getShape().getVertices().size(); j++)
		{
			PointToBuilding thisPointToBuilding;
			thisPointToBuilding.building = allBuildings[i];
			thisPointToBuilding.point = allBuildings[i]->getShape().getVertices()[j];

			allPoints.push_back(thisPointToBuilding);
		}
	}

	sort(allPoints.begin(), allPoints.end(), myLessPointToBuilding);
	firstAnswers = convexHull(allPoints);

	for (int i = 0; i < (int) firstAnswers.size() - 1; i++)
	{
		finalAnswers.push_back(firstAnswers[i]);
	}

	reverse(allPoints.begin(), allPoints.end());

	secondAnswers = convexHull(allPoints);

	for (int i = 0; i < (int) secondAnswers.size() - 1; i++)
	{
		finalAnswers.push_back(secondAnswers[i]);
	}

	for (int i = 0; i < (int) finalAnswers.size(); i++)
	{
		aroundBuildings.push_back(finalAnswers[i].building);
	}

	sort(aroundBuildings.begin(), aroundBuildings.end());
	aroundBuildings.resize(unique(aroundBuildings.begin(), aroundBuildings.end()) - aroundBuildings.begin());

	smoothAroundBuildings(aroundBuildings, world);

	sortBuildingByAngle(aroundBuildings);
}

void FireBlock::setSuspiciousBuildings(WorldModel* world)
{
	suspiciousBuildings.clear();
	if (getAroundBuildings().empty())
	{
		LOG(Main, 1) << "[setSuspiciousBuildings] AroundBuildings is empty" << endl;
	}

	for (Building* building : getAroundBuildings())
	{
		for (InSightBuilding* isb : building->getInSightBuildings())
		{
			if (world->buildings[isb->buildingIndex]->getEstimatedData()->getFieryness() == 0)
			{
				suspiciousBuildings.push_back(world->buildings[isb->buildingIndex]);
			}
		}
	}
}

void FireBlock::smoothAroundBuildings(vector<Building*>& around, WorldModel* world)
{
	vector<Building*> tmpAround;

	for (Building* building : around)
	{
		for (MotionlessObject* object : building->closerThan30B)
		{
			Building* neighbour = (Building*) object;
			if (building->getPos().distFromPoint(neighbour->getPos()) > 20000 || !neighbour->getEstimatedData()->hasFire() || neighbour->getEstimatedData()->getFieryness() == 3)
			{
				continue;
			}
			int flag = 0;

			for (InSightBuilding* isb : neighbour->getInSightBuildings())
			{
				if (world->buildings[isb->buildingIndex]->getEstimatedData()->getFieryness() == 0)
				{
					flag++;
				}
			}

			if (flag > 1)
			{
				if (std::find(around.begin(), around.end(), neighbour) == around.end())
				{
					tmpAround.push_back(neighbour);
				}
			}
		}
	}

	for (Building* building : tmpAround)
	{
		around.push_back(building);
	}
}

vector<PointToBuilding> FireBlock::convexHull(vector<PointToBuilding> &points)
{
	vector<int> answersIndex;
	vector<PointToBuilding> answers;

	answersIndex.clear();
	answers.clear();

	for (int i = 0; i < (int) points.size(); i++)
	{
		while ((int) answersIndex.size() > 1
				&& cross(points[i].point - points[answersIndex[answersIndex.size() - 2]].point,
						points[answersIndex[answersIndex.size() - 1]].point - points[answersIndex[answersIndex.size() - 2]].point) <= 0)
		{
			answersIndex.pop_back();
		}

		answersIndex.push_back(i);
	}

	for (int i = 0; i < (int) answersIndex.size(); i++)
		answers.push_back(points[answersIndex[i]]);

	return answers;
}

vector<Road *> &FireBlock::getAroundRoadsForPoliceForces()
{
	return aroundRoadsForPolice;
}

void FireBlock::sortBuildingByAngle(vector<Building*>& buildings)
{
	int gX = 0, gY = 0;
	for (int i = 0; i < (int) buildings.size(); i++)
	{
		gX = gX + buildings[i]->getPos().getX();
		gY = gY + buildings[i]->getPos().getY();
	}
	gX = gX / buildings.size();
	gY = gY / buildings.size();

	vector<BuildingByAngle> tempAngles;
	for (int i = 0; i < (int) buildings.size(); i++)
	{
		BuildingByAngle thisBuildingAndItsAngle;
		thisBuildingAndItsAngle.building = buildings[i];
		thisBuildingAndItsAngle.angle = atan2(buildings[i]->getPos().getY() - gY, buildings[i]->getPos().getX() - gX);
		tempAngles.push_back(thisBuildingAndItsAngle);
	}

	sort(tempAngles.begin(), tempAngles.end(), comparing);

	buildings.clear();

	for (int nn = 0; nn < (int) tempAngles.size(); nn++)
	{
		buildings.push_back(tempAngles[nn].building);
	}
}

void FireBlock::setCenterPoint()
{
	long gX = 0, gY = 0;

	for (int i = 0; i < (int) allBuildings.size(); i++)
	{
		gX = gX + allBuildings[i]->getX();
		gY = gY + allBuildings[i]->getY();
	}

	gX = gX / allBuildings.size();
	gY = gY / allBuildings.size();

	centerPoint = Point(gX, gY);
}

void FireBlock::setFireBrigades(vector<FireBrigade*> setfireBrigades)
{
	this->fireBrigades = setfireBrigades;
}

void FireBlock::setNeededFireBrigade(int numberOfFireBrigade)
{
	this->neededFireBrigade = numberOfFireBrigade;
}

void FireBlock::setValue(float myvalue)
{
	this->value = myvalue;
}

vector<Building *> &FireBlock::getAllBuildings()
{
	return this->allBuildings;
}

vector<Building *> &FireBlock::getAroundBuildings()
{
	return this->aroundBuildings;
}

vector<Building *> &FireBlock::getSuspiciousBuildings()
{
	return this->suspiciousBuildings;
}

void FireBlock::setAllBuildings(vector<Building*> buildings)
{
	this->allBuildings.clear();
	this->allBuildings = buildings;
}

vector<FireBrigade *> &FireBlock::getFireBrigades()
{
	return this->fireBrigades;
}

Point FireBlock::getCenterPoint()
{
	return this->centerPoint;
}

int FireBlock::getDistanceToPoint(Point point)
{
	int bestDist = MAX_INT;

	for (Building* building : allBuildings)
	{
		int dist = distanceBetweenPoints(building->getPos(), point);
		if (dist < bestDist)
		{
			bestDist = dist;
		}
	}

	return bestDist;
}

unsigned long FireBlock::getTotalArea()
{
	unsigned long area = 0;

	for (Building* building : allBuildings)
	{
		area += building->getAreaTotal();
	}

	return area;
}

int FireBlock::getGroundArea()
{
	int area = 0;

	for (Building* building : allBuildings)
	{
		area += building->getAreaGround();
	}

	return area;
}

int FireBlock::getNeededFireBrigade()
{
	return this->neededFireBrigade;
}

void FireBlock::setNeededPoliceForce(int numberOfPoliceForces)
{
	this->neededPoliceForce = numberOfPoliceForces;
}

int FireBlock::getNeededPoliceForce()
{
	return neededPoliceForce;
}

float FireBlock::getValue()
{
	return this->value;
}

void FireBlock::addBuilding(Building* b)
{
	this->allBuildings.push_back(b);
//	b->setFireBlock(this);
}

void FireBlock::addFireBrigade(FireBrigade* fb)
{
	this->fireBrigades.push_back(fb);
}

void FireBlock::addClearingRoads(Road * road)
{
	this->roadsForClear.push_back(road);
}

vector<Road *> &FireBlock::getClearingRoads()
{
	return roadsForClear;
}

void FireBlock::clearRoadsForClear()
{
	roadsForClear.clear();
}

double FireBlock::cross(Point a, Point b)
{
	return (a.getX() * b.getY() - a.getY() * b.getX());
}

int FireBlock::getGroundDistToMotionless(MotionlessObject* motionless, WorldGraph* wg, Types::GraphMode graphMode)
{
	int bestDist = MAX_INT;

	for (Building* building : allBuildings)
	{
		int dist = wg->getDistance(building->getRepresentiveNodeIndex(), motionless->getRepresentiveNodeIndex(), graphMode);
		if (dist < bestDist)
		{
			bestDist = dist;
		}
	}

	return bestDist;
}

void FireBlock::setExtinguishability(bool ex)
{
	extinguishable = ex;
}

bool FireBlock::isExtinguishable()
{
	return extinguishable;
}
