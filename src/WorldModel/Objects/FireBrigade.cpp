#include "FireBrigade.h"

using namespace std;
using namespace Types;

FireBrigade::FireBrigade(u_int Id) :
		Platoon(Id)
{
	hasWaterValue = true;
	typeFlag |= 12;
}

FireBrigade::~FireBrigade()
{
}

void FireBrigade::setWaterQuantity(int waterQuantity)
{
	this->waterQuantity = waterQuantity;
}

int FireBrigade::getWaterQuantity() const
{
	return waterQuantity;
}

void FireBrigade::setHasWaterValue(bool value)
{
	this->hasWaterValue = value;
}

bool FireBrigade::hasWater()
{
	return this->hasWaterValue;
}

void FireBrigade::setProperties(string type, int value)
{
	if (type == Encodings::getPropertyType(PT_WATER_QUANTITY))
	{
		this->setWaterQuantity(value);
	}
	else
	{
		Platoon::setProperties(type, value);
	}

}
