#ifndef _BLOCKADE_H
#define	_BLOCKADE_H

#include "RCRObject.h"
#include "../../Utilities/Geometry/Polygon.h"
#include <array>

class Blockade: public RCRObject
{
public:
	Blockade(u_int id);
	Blockade();
	virtual ~Blockade();
	u_int getPosition() const;
	u_int getRepairCost() const;
	unsigned short int getLastClearTime() const;
	void addVertex(int value);
	void setPosition(u_int value);
	void setRepairCost(u_int value);
	void setProperties(std::string type, int value);
	void setLastClearTime(unsigned short int value);
	Geometry::Polygon& getShape();
	void clearShape();

	std::array<Geometry::Polygon, 2> increasedShapes;

private:
	u_int repairCost;
	u_int position;
	unsigned short int lastClearTime;
	Geometry::Polygon shape;
};

#endif	/* _BLOCKADE_H */

