#include "Civilian.h"
#include "../../Utilities/Geometry/Circle.h"
#include "../../Utilities/Geometry/GeometryFunctions.h"

#define LOGLEVEL 0
#define angleCoef 3
#define distanceCoef 400

using namespace std;
using namespace Geometry;

Civilian::Civilian(u_int Id) :
		Human(Id)
{
	firstDeathSignal = -1;
	typeFlag |= 2;
	fakeId = 0;
	realX = 0;
	realY = 0;
	angle = -1;
	distance = -1;
	distToCenterSize = -1;
}
Civilian::Civilian(int motionlessIndex, int angle, int distFromCenter, int distToCenterSize) :
		Human(1)
{
	firstDeathSignal = -1;
	typeFlag |= 2;
	fakeId = 0;
	realX = 0;
	realY = 0;
	this->distToCenterSize = distToCenterSize;
	this->angle = angle;
	this->distance = distFromCenter;
	initFakeId(motionlessIndex);
}

Civilian::~Civilian()
{
}

void Civilian::setFirstDeathSignal(int firstDeathSignal)
{
	this->firstDeathSignal = firstDeathSignal;
}

int Civilian::getFirstDeathSignal()
{
	return this->firstDeathSignal;
}

void Civilian::setSpotedTime(int value)
{
	this->spotedTime = value;
}

int Civilian::getSpotedTime()
{
	return this->spotedTime;
}

void Civilian::initFakeId(int motionlessIndex)
{
	LOG(Main,1) << "abc-----" << endl;
	int fake = 0;

	LOG(Main , 1) << "angle : " << angle << endl;
	int angleBitSize = ceil(log2l(360 / angleCoef));

	LOG(Main, 1) << "AngleBitSize:   " << angleBitSize << endl;
	LOG(Main, 1) << "Dist2CenterBitSize:   " << distToCenterSize << endl;

	LOG(Main, 1) << "MO before shift:   " << motionlessIndex << endl;
	motionlessIndex = motionlessIndex << (distToCenterSize + angleBitSize);
	LOG(Main, 1) << "MO after shift:   " << motionlessIndex << endl;

	LOG(Main, 1) << "dist before shift:   " << distance << endl;

	int dist = distance << angleBitSize;

	LOG(Main, 1) << "dist after shift:   " << dist << endl;

	fake += motionlessIndex;
	fake += dist;
	fake += angle;

	LOG(Main, 1) << "final_Fake: " << fake << endl;
	this->fakeId = fake;
}

int Civilian::getFakeId()
{
	if (fakeId == 0 && realX != 0 && realY != 0)
	{
		LOG(Main,1) << "abc-----" << endl;

		getDistanceToCenter(); //for initializing distance
		getAngle(); //for initializing angle

		initFakeId(getFirstTimeMotionless()->motionlessIndex);
	}

	return fakeId;
}

void Civilian::setFakeId(u_int fakeId)
{
	this->fakeId = fakeId;
}

void Civilian::setId(u_int id)
{
	this->id = id;
}

int Civilian::getDistanceToCenterSize()
{
	if (getDistanceToCenter() != -1 && getFirstTimeMotionless() != nullptr)
	{
		distToCenterSize = ceil(log2l(getFirstTimeMotionless()->getMaxDistanceFromCenter() / distanceCoef));
	}
	return distToCenterSize;
}

void Civilian::setDistanceToCenterSize(int distanceToCenterSize)
{
	distToCenterSize = distanceToCenterSize;
}

int Civilian::getAngle()
{
	if (angle == -1)
	{
		if (getRealX() != 0 && getRealY() != 0 && getFirstTimeMotionless() != nullptr)
		{
			angle = round(getPolarAngle(getRealY() - getFirstTimeMotionless()->getY(), getRealX() - getFirstTimeMotionless()->getX()) / angleCoef);
		}
	}
	return angle;
}

void Civilian::setAngle(int angle)
{
	this->angle = angle;
}

int Civilian::getDistanceToCenter()
{
	if (distance == -1)
	{
		if (getRealX() != 0 && getRealY() != 0 && getFirstTimeMotionless() != nullptr)
		{
			distance = round(getFirstTimeMotionless()->getPos().distFromPoint(Point(getRealX(), getRealY())) / distanceCoef);
		}
	}

	return distance;
}

void Civilian::setDistanceToCenter(int distanceToCenter)
{
	distance = distanceToCenter;
}

void Civilian::setProperties(string type, int value)
{
	Human::setProperties(type, value);
}
