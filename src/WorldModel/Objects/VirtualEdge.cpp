/*
 * VirtualEdge.cpp
 *
 *  Created on: Apr 30, 2014
 *      Author: arya
 */

#include "VirtualEdge.h"

VirtualEdge::VirtualEdge(int firstNodeIndex, int secondNodeIndex) : Edge(firstNodeIndex, secondNodeIndex)
{
	passingMode = Types::PM_PASSABLE;
}

VirtualEdge::~VirtualEdge()
{
}

