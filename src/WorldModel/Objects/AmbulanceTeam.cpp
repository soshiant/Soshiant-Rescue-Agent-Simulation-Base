#include "AmbulanceTeam.h"

using namespace std;

AmbulanceTeam::AmbulanceTeam(u_int Id) :
		Platoon(Id)
{
	stateEstimator = new AmbulanceTeamStateEstimator(this);
	shouldSendAssignThisCycle = false;
	ambulanceTeamIndex = -1;
	centerTargetPos = nullptr;
	buildingTarget = nullptr;
	sentOnRadarTime = 0;
	centerTarget = nullptr;
	simpleTarget = nullptr;
	target = nullptr;
	assignmentTime = 0;
	isCapitan = false;
	typeFlag &= ~12;
	carry = false;
}

AmbulanceTeam::~AmbulanceTeam()
{
}

void AmbulanceTeam::setProperties(string type, int value)
{
	Platoon::setProperties(type, value);
}

bool AmbulanceTeam::isCarry()
{
	return carry;
}

void AmbulanceTeam::setIsCarry(bool a)
{
	carry = a;
}
