/* 
 * File:   Node.h
 * Author: Eqbal Sarjami
 * Edited by Arya Hadi [April 30 2014]
 *
 * Created on November 28, 2010, 1:37 PM
 */

#ifndef _NODE_H
#define	_NODE_H

#include "../../Utilities/Geometry/Point.h"

class Node
{
	public:
		Node(int);
		~Node();

		int getMotionlessIndex(); // In our new WorldGraph structure, each node has just one motionless index! But I'm very Goshader than these talks to replace whole base!! (Arya hastam!)
		int getFirstMotionlessIndex();
		int getSecondMotionlessIndex();

		std::vector<int>& getEdgeIndexes();
		void addEdgeIndex(int);

		int getEdgeIndex();
		void setEdgeIndex(int);

		std::vector<int>& getNeighbours();
		void addNeighbour(int);

		Geometry::Point& getRepresentivePoint();
		void setRepresentivePoint(Geometry::Point);

		int nodeIndex;

	private:
		int edgeIndex;
		int motionlessIndex;
		std::vector<int> edgeIndexes;
		std::vector<int> neighbours;
		Geometry::Point representivePoint;
};

class SimpleNode
{
	public:
		SimpleNode();
		SimpleNode(Geometry::Point p);
		~SimpleNode();

		void setPoint(Geometry::Point p);
		Geometry::Point getPoint();

		std::vector<SimpleNode *> neighbors;
		bool isMark;
		int pointIndex;
		int groupIndex;

	private:
		Geometry::Point point;
};

#endif	/* _NODE_H */

