#ifndef __ROAD_H_
#define __ROAD_H_

#include "MotionlessObject.h"
#include "Blockade.h"
#include <array>

using namespace Geometry;

class Road: public MotionlessObject
{
public:
	Road(u_int Id);
	virtual ~Road();
	unsigned short int getlength() const;
	float getRepairCost() const;
	u_int getTailId() const;
	unsigned short int getWidth() const;
	std::vector<int>& getBlockadeIds();
	void clearBlockadeIds();
	std::vector<Blockade*>& getBlockades();
	void clearBlockades();
	std::vector<Geometry::Point>& getInnerPoints();
	int getHasToBeSent();
	int getValue();
	void setlength(unsigned short int length);
	void setRepairCost(float repairCost);
	void setWidth(unsigned short int width);
	void addBlockadeId(int value);
	void addBlockade(Blockade *);
	void setProperties(std::string type, int value);
	void setHasToBeSent(int);
	void setValue(int value);
	void setLastCycleBlockades(int bl);
	int getLastCycleBlockades();

	int roadIndex;
	std::array<std::vector<Polygon>, 2> validParts;
	std::vector<int> buildingWithThisRoadCanSee;

private:
	unsigned short int length;
	unsigned short int width;
	std::vector<int> blockadeIds;
	std::vector<Blockade*> blockades;
	int lastCycleBlockades;
	float repairCost;
	int hsaToBeSent;
	int value;

};

#endif
