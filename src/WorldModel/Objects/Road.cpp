/*
 * Road.cpp
 *
 *  Created on: Dec 29, 2009
 *      Author: Alireza Kondori
 */

#include <iostream>
#include "Road.h"
#include "../../Utilities/Geometry/GeometryFunctions.h"
#define LOGLEVEL 1
using namespace std;
using namespace Types;
using namespace Geometry;

Road::Road(u_int Id) :
		MotionlessObject(Id)
{
	blockadeIds.clear();
	typeFlag &= ~2;
	hsaToBeSent = -1;
}

Road::~Road()
{
}

unsigned short int Road::getlength() const
{
	return length;
}

int Road::getValue()
{
	return this->value;
}

float Road::getRepairCost() const
{
	return repairCost;
}

unsigned short int Road::getWidth() const
{
	return width;
}

std::vector<int>& Road::getBlockadeIds()
{
	return blockadeIds;
}

std::vector<Blockade*>& Road::getBlockades()
{
	return blockades;
}

void Road::clearBlockadeIds()
{
	blockadeIds.clear();
}

void Road::clearBlockades()
{
	blockades.clear();
}

void Road::setLastCycleBlockades(int bl)
{
	this->lastCycleBlockades = bl;
}

int Road::getLastCycleBlockades()
{
	return this->lastCycleBlockades;
}

int Road::getHasToBeSent()
{
	return hsaToBeSent;
}

void Road::addBlockadeId(int value)
{
	if (value == 0)
	{
		return;
	}

	blockadeIds.push_back(value);
}

void Road::addBlockade(Blockade *b)
{
	blockades.push_back(b);
}

void Road::setlength(unsigned short int length)
{
	this->length = length;
}

void Road::setValue(int value)
{
	this->value = value;
}

void Road::setRepairCost(float repairCost)
{
	this->repairCost = repairCost;
}

void Road::setWidth(unsigned short int width)
{
	this->width = width;
}

void Road::setProperties(string type, int value)
{
	if (type == Encodings::getPropertyType(PT_LENGTH))
	{
		this->setlength(value);
	}
	else if (type == Encodings::getPropertyType(PT_WIDTH))
	{
		this->setWidth(value);
	}
	else if (type == Encodings::getPropertyType(PT_REPAIR_COST))
	{
		this->setRepairCost((float) value);
	}
	else if (type == Encodings::getPropertyType(PT_BLOCKADES))
	{
		this->addBlockadeId(value);
	}
	else
	{
		MotionlessObject::setProperties(type, value);
	}
}

void Road::setHasToBeSent(int a)
{
	hsaToBeSent = a;
}
