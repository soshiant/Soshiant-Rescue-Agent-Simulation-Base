#ifndef __BUILDING_H_
#define __BUILDING_H_

#include "MotionlessObject.h"
#include "FireBrigade.h"
#include "Civilian.h"
#include "Road.h"
#include "SearchArea.h"
#include "../../Utilities/Geometry/GeometryFunctions.h"
#include "../../Utilities/Geometry/Polygon.h"
#include "../../Utilities/Types.h"
#include <vector>

using namespace Geometry;

class WorldModel;
class FireBlock;
class EstimatedData;
class SearchArea;
class Wall;

struct InSightBuilding
{
	InSightBuilding(float value, int buildingIndex)
	{
		this->value = value;
		this->buildingIndex = buildingIndex;
	}

	float value;
	int buildingIndex;
};

class Building: public MotionlessObject
{
public:
	Building(u_int Id);
	virtual ~Building();
	bool isPutOut();
	bool isUnburned();
	bool hasFire();
	u_int getAreaGround() const;
	u_int getAreaTotal() const;
	Types::BuildingCode getBuildingCode() const;
	unsigned short int getFieryness() const;
	unsigned short int getFloors() const;
	u_int getBrokenness() const;
	u_int getAttributes() const;
	int getTemperature() const;
	int getLastCycleInFire() const;
	int getImportance() const;
	EstimatedData* getEstimatedData();
	int getCivilianProbability();
	void setAreaGround(u_int AreaGround);
	void setAreaTotal(u_int AreaTotal);
	void setBuildingCode(Types::BuildingCode buildingCode);
	void setFieryness(unsigned short int tfieryness);
	void setFloors(unsigned short int floors);
	void setBrokenness(u_int brokenness);
	void setProperties(std::string type, int value);
	void setImportance(int value);
	void setAttributes(u_int value);
	void setTemperature(u_int value);
	void setLastCycleInFire(int t);
	void setCivilianProbability(int civilianProbability);
	bool operator<(Building);

	void addSearchStates(Types::BuildingsSearchState);
	void clearSearchStates();
	vector<Types::BuildingsSearchState> getSearchStates();

	void setWorthlessness(bool);
	bool getWorthlessness();
	void setNeededFireBrigades(int number);
	int getNeededFireBrigades();
	void setValue(int bValue);
	int getValue();
	bool isHardToExtinguish();
	void setHardToExtinguish(bool a);
	void setLastCycleExtinguished(int t);
	int getLastCycleExtinguished();
	FireBlock* getFireBlock() const;
	void setFireBlock(FireBlock* fireBlock);
	vector<InSightBuilding*> getInSightBuildings();

	void setWallsLength(double);
	double getWallsLength();
	void initializeWallValues(WorldModel* world);

	int buildingIndex;
	int buildingFireSearchValue;
	vector<Road *> canSee;
	vector<Wall *> walls;
	vector<InSightBuilding*> inSightBuildings;
	vector<Building *> notIgnitedNeighbor;
	vector<int> fierynessDuringTime;
	EstimatedData* estimatedData = nullptr;
	int lastTimeSearchedForCivilian = -1;
	int searchClusterIndex = -1;
	vector<Segment> visionRays;
	SearchArea* searchArea = nullptr;
	int slice = -1;
	int susSearchValue = -1;
	int lastTimeSearchedByMe = 0;

private:
	u_int attributes;
	unsigned short int fieryness;
	unsigned short int floors;
	u_int AreaTotal;
	u_int AreaGround;
	u_int brokenness;
	int temperature;
	int importance;
	Types::BuildingCode buildingCode;
	int civilianProbability;
	int lastTimeInFire;
	int neededFireBrigades;
	int value;
	bool hardToExtinguish;
	bool worthlessness = false;
	int lastCycleExtinguished;
	double wallsLength = 0;
	FireBlock* fireBlock = nullptr;
	vector<Types::BuildingsSearchState> searchStates;
};

#endif
