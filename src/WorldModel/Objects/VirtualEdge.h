/*
 * VirtualEdge.h
 *
 *  Created on: Apr 30, 2014
 *      Author: arya
 */

#ifndef VIRTUALEDGE_H_
#define VIRTUALEDGE_H_

#include "Edge.h"

class VirtualEdge : public Edge
{
	public:
		VirtualEdge(int firstNodeIndex, int secondNodeIndex);
		virtual ~VirtualEdge();
};

#endif /* VIRTUALEDGE_H_ */
