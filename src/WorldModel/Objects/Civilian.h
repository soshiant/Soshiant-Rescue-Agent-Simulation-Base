#ifndef __CIVILIAN_H_
#define __CIVILIAN_H_

#include "Human.h"
#include "../../Utilities/Geometry/Line.h"
#include "../../Utilities/Geometry/Segment.h"
#include "../../Utilities/Geometry/Point.h"
class Civilian: public Human
{
public:
	Civilian(u_int Id);
	Civilian(int motionlessIndex, int angle, int distFromCenter, int distToCenterSize);
	virtual ~Civilian();
	void setProperties(std::string type, int value);
	void setFirstDeathSignal(int firstDeathSignal);
	int getFirstDeathSignal();
	void setEstimateHp(int value);
	int getEstimateHp();
	void setEstimateDamage(int value);
	int getEstimateDamage();
	void setSpotedTime(int value);
	int getSpotedTime();
	void initFakeId(int motionlessIndex);
	void setFakeId(u_int fakeId);
	void setId(u_int id);
	int getFakeId();
	void setDistanceToCenterSize(int distToCenterSize);
	int getDistanceToCenterSize();
	void setAngle(int angle);
	int getAngle();
	void setDistanceToCenter(int distToCenterSize);
	int getDistanceToCenter();

	int civilianIndex;

private:
	int angle;
	int distance;
	int distToCenterSize;
	int fakeId;
	int firstDeathSignal;
	int spotedTime;
};

#endif
