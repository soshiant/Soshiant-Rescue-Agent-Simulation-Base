#ifndef __AMBULANCE_TEAM_H_
#define __AMBULANCE_TEAM_H_

#include "Human.h"
#include "Building.h"
#include "../../Utilities/AmbulanceTeamStateEstimator.h"
#include <vector>

using namespace std;

class AmbulanceTeam: public Platoon
{
public:
	AmbulanceTeam(u_int Id);
	virtual ~AmbulanceTeam();
	void setProperties(std::string type, int value);
	int ambulanceTeamIndex;

	void setIsCarry(bool a);
	bool isCarry();

	Human* target;
	Human* simpleTarget;
	Human* centerTarget;
	MotionlessObject* centerTargetPos;
	int sentOnRadarTime, assignmentTime;
	Building* buildingTarget;
	bool isCapitan;
	bool shouldSendAssignThisCycle;

	AmbulanceTeamStateEstimator* stateEstimator;

private:
	bool carry;
};

#endif
