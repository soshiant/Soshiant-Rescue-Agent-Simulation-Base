#ifndef __FIRE_BRIGADE_H_
#define __FIRE_BRIGADE_H_

#include "Human.h"
#include "Platoon.h"

#include <iostream>

using namespace std;
using namespace Types;

class Building;
class FireBlock;
class Cluster;

class FireBrigade: public Platoon
{
public:
	FireBrigade(u_int Id);
	virtual ~FireBrigade();
	int getWaterQuantity() const;
	void setWaterQuantity(int waterQuantity);
	void setProperties(std::string type, int value);
	void setHasWaterValue(bool value);
	bool hasWater();

	int fireBrigadeIndex;
	float value = 0;
	bool hasJob = false;
	vector<Building*> targetBuildings;
	FireBlock* targetFireBlock = nullptr;
	int searchCluster = -1;

private:
	bool hasWaterValue;
	int waterQuantity;
};

#endif
