#ifndef _PARSER_H
#define	_PARSER_H

#include <string>
#include <vector>

#include "../Connection/OnlineConnection.h"
#include "../Utilities/Types.h"
#include "Objects/RCRObject.h"
//#include "WorldModel.h"

class WorldModel;

using namespace std;

namespace Parser
{
string analyzeMessage(WorldModel* world, vector<byte> &msg);
std::string parseKAConnectOk(WorldModel* world, std::vector<byte> &msg, int &offset);
std::string parseKAConnectError(WorldModel* world, std::vector<byte> &msg, int &offset);
std::string parseKASense(WorldModel* world, std::vector<byte> &msg, int &offset);
std::string parseKAHear(WorldModel* world, std::vector<byte> &msg, int &offset);
void parseObjects(WorldModel* world, std::vector<byte> &message, RCRObject &object, int &offset, int counter);
void parseObjectElements(WorldModel* world, std::vector<byte> &message, int &offset, std::string type, int id, int counter);
void parseObjectProperties(WorldModel* world, std::vector<byte> &message, int &offset);
void parseObjectsElementsKAConnectOk(WorldModel* world, std::vector<byte> &message, int &offset);
void parseObjectsElementsKASense(WorldModel* world, std::vector<byte> &message, int &offset);
void parseRadarMessages(WorldModel* world, std::vector<byte> &message, int &offset);
void parseConfig(WorldModel* world, std::vector<byte> &message, int &offset);
}

#endif	/* _PARSER_H */

