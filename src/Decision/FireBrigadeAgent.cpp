#include "FireBrigadeAgent.h"

#define LOGLEVEL 1

FireBrigadeAgent::FireBrigadeAgent()
{
	srand(RANDOM_SEED);
}

FireBrigadeAgent::~FireBrigadeAgent()
{
}

void FireBrigadeAgent::precomputation()
{
	maxExtinguishDistance = atoi(world->config["fire.extinguish.max-distance"].c_str());
	maxWaterForExtinguish = atoi(world->config["fire.extinguish.max-sum"].c_str());
	maxSightRange = atoi(world->config["perception.los.max-distance"].c_str());
	maxFireTank = atoi(world->config["fire.tank.maximum"].c_str());

	setFireEstimatorNecessity();

	setAssignStrategy();

	if (isPrecomputeAgent)
	{
		Agent::precomputation();

		world->setCanseeBuildings();
		CLOGLN("[FireBrigadeAgent] CanSee finished")

		world->setInSightPointsBuildings();
		CLOGLN("[FireBrigadeAgent] InSight finished")

#if SEARCH_CLUSTER
		setSearchClusters();
		CLOGLN("[FireBrigadeAgent] SearchClustering finished")
		assignToSearchCluster();
#endif

		fireEstimator = new FireEstimator(world);
		CLOGLN("[FireBrigadeAgent] FireEstimator has Constructed")

		setBuildingsCells();

		exportPreComputeData();
	}
	else
	{
		world->setBuildingsWalls();

		fireEstimator = new FireEstimator(world);
		CLOGLN("[FireBrigadeAgent] FireEstimator has Constructed")

		importPrecomputationData();

		searchClusters = world->searchClusters;
		pizzaSlices = world->pizzaSlices;
	}

	if (assignStrategy == FBAS_PIZZA)
	{
		prepareForPizzaStrategy();
		CLOGLN("[FireBrigadeAgent] I'm prepared for pizza strategy")
	}

	logMapInformation();

	setCityCenter();

	setSegmentBuildingsCount();

	setAverageArea();

	setIgnoredBuildings();

	setHTELimit();

	nearBuilding = world->getWorldBound().getLength() / 2.5;

	CLOGLN("[FireBrigadeAgent] Precomputation has completed");
}

void FireBrigadeAgent::exportPreComputeData()
{
	fstream FBData;
	FBData.open(getPrecomputationFileAddress(type).c_str(), fstream::out);

    Agent::exportPrecomputationData(FBData);

	FBData << endl << Encodings::getPrecomputationTagType(PCDT_CANSEE) << endl << endl;
	for (u_int i = 0; i < world->buildings.size(); i++)
	{
		FBData << world->buildings[i]->canSee.size() << endl;
		for (u_int j = 0; j < world->buildings[i]->canSee.size(); j++)
		{
			FBData << world->buildings[i]->canSee[j]->roadIndex << " ";
		}
		FBData << endl;
	}

	FBData << Encodings::getPrecomputationTagType(PCDT_INSIGHT) << endl;
	for (u_int i = 0; i < world->buildings.size(); i++)
	{
		FBData << world->buildings[i]->getInSightBuildings().size() << endl;
		for (u_int j = 0; j < world->buildings[i]->getInSightBuildings().size(); j++)
		{
			FBData << world->buildings[i]->getInSightBuildings()[j]->buildingIndex << "\n" << std::fixed
					<< world->buildings[i]->getInSightBuildings()[j]->value << "\n";
		}
	}

	FBData << Encodings::getPrecomputationTagType(PCDT_SEARCH_CLUSTERS) << endl;
	FBData << searchClusters.size() << endl;
	for (Cluster* cluster : searchClusters)
	{
		FBData << cluster->getMembers().size() << endl;
		for (MotionlessObject* object : cluster->getMembers())
		{
			FBData << object->motionlessIndex << " ";
		}
		FBData << endl;
	}

	FBData << Encodings::getPrecomputationTagType(PCDT_CELLS) << endl;
	for (Building* building : world->buildings)
	{
		FBData << building->getEstimatedData()->getCells().size() << endl;
		for (array<int, 3> arr : building->getEstimatedData()->getCells())
		{
			for (int n : arr)
			{
				FBData << n;
				FBData << " ";
			}
			FBData << endl;
		}
	}

	FBData << Encodings::getPrecomputationTagType(PCDT_SEARCH_ASSIGN) << endl;
	for (FireBrigade* fireBrigade : world->fireBrigades)
	{
		FBData << fireBrigade->searchCluster << " ";
	}
	FBData << endl;

//	FBData << Encodings::getPreComputeTagType(PCDT_PIZZA_SLICES) << endl;
//	FBData << pizzaSlices.size() << endl;
//	for (Cluster* cluster : pizzaSlices)
//	{
//		FBData << cluster->getMembers().size() << endl;
//		for (MotionlessObject* object : cluster->getMembers())
//		{
//			FBData << object->motionlessIndex << " ";
//		}
//		FBData << endl;
//	}

	FBData << endl;
	FBData << Encodings::getPrecomputationTagType(PCDT_DATA_END);
	FBData.close();
}

void FireBrigadeAgent::actBeforeSense()
{
	if (world->getTime() < 3)
	{
		return;
	}

	if (useFireEstimator)
	{
		if (world->getTime() % FIRE_ESTIMATOR_RESET_TIME == 0)
		{
			fireEstimator->resetEstimatedData();
		}
		LOG(Main, 1) << "Estimate Once" << endl;
		fireEstimator->estimateOnce(); // Estimate this cycle's fiery buildings
	}
}

void FireBrigadeAgent::actBeforeRadar()
{
}

void FireBrigadeAgent::act()
{
	if (world->getTime() < ignoreCommandsUntil)
	{
		return;
	}

	for (RCRObject* object : world->getInSightObjects())
	{
		if (object->isBuilding())
		{
			((Building*) object)->lastTimeSearchedByMe = world->getTime();
		}
	}

	LOG(Main, 1) << "Shasing time: " << shasingTime << endl;

	updateSlices();

	LOG(Main, 1) << "OnFireBuildings: ";
	for (Building* building : world->buildings)
	{
		if (building->getEstimatedData()->hasFire())
		{
			LOG(Main, 1) << building->getId() << " ";
		}
	}
	LOG(Main, 1) << endl;

//	if (SHOW_THE_POWER_OF_SOSHIANT())
//	{
//		return;
//	}

	if (dontShas1())
	{
		return;
	}

	if (self()->getRepresentiveNodeIndex() == -1)
	{
		stuckTime++;
	}
	else
	{
		stuckTime = 0;
	}
	LOG(Main, 1) << "Stuck time: " << stuckTime << endl;

	if (stuckSituation())
	{
		return;
	}

	if (dontShas2() || dontShas3())
	{
		return;
	}

	if (stuckInAreaSituation())
	{
		return;
	}

	if (checkDamage())
	{
		searchTarget = nullptr;
		return;
	}

//	if (assassinVerySuspiciousBuildings())
//	{
//		return;
//	}

	if (fillWaterTank())
	{
		LOG(Main, 1) << "I'm going to fill water tank" << endl;
		return;
	}
	else if (fillWater && self()->getWaterQuantity() == 0)
	{
		search();
		return;
	}

	if (fuckedUpSituation(false))
	{
		shasingTime = 0;
		searchTarget = nullptr;
		return;
	}

	if (normalSituation())
	{
		shasingTime = 0;
		return;
	}

	shasingTime++;
	if (fillWaterTank2())
	{
		LOG(Main, 1) << "I'm shasing... Let's fill water tank" << endl;
		return;
	}

	search();
}

void FireBrigadeAgent::noCommAct()
{
	if (world->getTime() < ignoreCommandsUntil)
	{
		return;
	}

	for (RCRObject* object : world->getInSightObjects())
	{
		if (object->isBuilding())
		{
			((Building*) object)->lastTimeSearchedByMe = world->getTime();
		}
	}

	LOG(Main, 1) << "Shasing time: " << shasingTime << endl;

	LOG(Main, 1) << "OnFireBuildings: ";
	for (Building* building : world->buildings)
	{
		if (building->getEstimatedData()->hasFire())
		{
			LOG(Main, 1) << building->getId() << " ";
		}
	}
	LOG(Main, 1) << endl;

//	if (SHOW_THE_POWER_OF_SOSHIANT())
//	{
//		return;
//	}

	if (dontShas1())
	{
		return;
	}

	if (self()->getRepresentiveNodeIndex() == -1)
	{
		stuckTime++;
	}
	else
	{
		stuckTime = 0;
	}
	LOG(Main, 1) << "Stuck time: " << stuckTime << endl;

	if (stuckSituation())
	{
		return;
	}

	if (dontShas2() /*|| dontShas3()*/)
	{
		return;
	}

	if (stuckInAreaSituation())
	{
		return;
	}

	if (checkDamage())
	{
		searchTarget = nullptr;
		return;
	}

//	if (assassinVerySuspiciousBuildings())
//	{
//		return;
//	}

	resetFireBrigadesInformation();

	if (fillWaterTank())
	{
		LOG(Main, 1) << "I'm going to fill water." << endl;
		return;
	}
	else if (fillWater && self()->getWaterQuantity() == 0)
	{
		search();
		return;
	}

	if (fuckedUpSituation(true))
	{
		shasingTime = 0;
		searchTarget = nullptr;
		return;
	}

	if (normalSituation())
	{
		shasingTime = 0;
		return;
	}

	shasingTime++;
	if (fillWaterTank2())
	{
		LOG(Main, 1) << "I'm shasing... Let's fill water" << endl;
		return;
	}

	search();
}

FireBrigade* FireBrigadeAgent::self()
{
	return (FireBrigade*) world->self;
}

/*** SITUATIONS ***/

bool FireBrigadeAgent::stuckSituation()
{
	if (self()->getRepresentiveNodeIndex() != -1 || self()->getWaterQuantity() == 0)
	{
		return false;
	}
	LOG(Main, 1) << "StuckSituation" << endl;

	if (self()->getMotionlessObject()->isBuilding())
	{
		Building* building = (Building*) self()->getMotionlessObject();
		if (building->hasFire())
		{
			LOG(Main, 1) << "I'm extinguishing myself" << endl;
			command->extinguish(*building, maxWaterForExtinguish);
			return true;
		}
	}

	vector<Building*> candidateBuildings;
	for (Building* building : world->buildings)
	{
		if (world->getTime() - building->getLastCycleUpdated() > 5)
		{
			continue;
		}
		if (self()->getPos().distFromPoint(building->getPos()) > maxExtinguishDistance)
		{
			continue;
		}
		if (!doesNeedToExtinguish(building, true))
		{
			continue;
		}

		candidateBuildings.push_back(building);
	}

	if (candidateBuildings.empty())
	{
		LOG(Main, 1) << "There isn't any candidate target" << endl;
		return false;
	}

	setValue(candidateBuildings, true);

	sort(candidateBuildings.begin(), candidateBuildings.end(), [](Building* b1, Building* b2)
	{
		return b1->getValue() > b2->getValue();
	});

	if (extinguishFromHere(candidateBuildings))
	{
		LOG(Main, 1) << "I'm handling target" << endl;
		return true;
	}
	LOG(Main, 1) << "I can't handle target" << endl;
	return false;
}

bool FireBrigadeAgent::stuckInAreaSituation()
{
	if (!isStuckInArea() || self()->getWaterQuantity() == 0)
	{
		return false;
	}
	LOG(Main, 1) << "StuckInAreaSituation" << endl;

	vector<Building*> candidateBuildings;
	for (Building* building : world->buildings)
	{
		if (world->getTime() - building->getLastCycleUpdated() > 5)
		{
			continue;
		}
		if (!doesNeedToExtinguish(building, true))
		{
			continue;
		}

		candidateBuildings.push_back(building);
	}

	setValue(candidateBuildings, true);

	sort(candidateBuildings.begin(), candidateBuildings.end(), [](Building* b1, Building* b2)
	{
		return b1->getValue() > b2->getValue();
	});

	if (moveAndExtinguish(candidateBuildings, true))
	{
		LOG(Main, 1) << "I'm handling target" << endl;
		return true;
	}
	LOG(Main, 1) << "I can't handle target" << endl;
	return false;
}

bool FireBrigadeAgent::normalSituation()
{
	LOG(Main, 1) << "NormalSituation" << endl;

	if (searchAroundLastTargets())
	{
		return true;
	}

	resetFireBrigadesInformation();

	createFireBlocks();

	assignToFireBlock();

	assignToBuildings();

	if (self()->targetBuildings.empty())
	{
		LOG(Main, 1) << "I don't have any fucking target" << endl;
		return false;
	}

	if (moveAndExtinguish(self()->targetBuildings))
	{
		LOG(Main, 1) << "I'm handling my target" << endl;
		searchTarget = nullptr;
		return true;
	}

	LOG(Main, 1) << "I can't handle my target" << endl;
	return false;
}

bool FireBrigadeAgent::fuckedUpSituation(bool isCommless)
{
	LOG(Main, 1) << "Fucked up situation" << endl;

	int minimumCycles = 0;
	if (world->isMapLarge())
	{
		if (isCommless)
		{
			minimumCycles = 70;
		}
		else
		{
			minimumCycles = 100;
		}
	}
	else
	{
		if (isCommless)
		{
			minimumCycles = 100;
		}
		else
		{
			minimumCycles = 150;
		}
	}

	if (world->getTime() < minimumCycles || !world->isMapLarge())
	{
		return false;
	}

	vector<Building*> candidateBuildings;
	int gohBuildings = 0;
	for (Building* building : world->buildings)
	{
		if (building->getEstimatedData()->getFieryness() > 0)
		{
			gohBuildings++;
		}
		if (building->getEstimatedData()->getTemperature() >= 1 && building->getEstimatedData()->getFieryness() == 0)
		{
			candidateBuildings.push_back(building);
		}
	}
	if (gohBuildings < CITY_FUCKED_LIMIT * world->buildings.size() && !isCommless)
	{
		return false;
	}
	else if (gohBuildings < CITY_FUCKED_LIMIT * (1. / 3) * world->buildings.size() && isCommless)
	{
		return false;
	}

	if (candidateBuildings.empty())
	{
		LOG(Main, 1) << "Fuck" << endl;
		return false;
	}

	setValue2(candidateBuildings, false);

	sort(candidateBuildings.begin(), candidateBuildings.end(), [](Building* b1, Building* b2)
	{
		return b1->getValue() > b2->getValue();
	});

	return moveAndExtinguish(candidateBuildings, false);
}

/*** FIRE BLOCK TOOLS ***/

void FireBrigadeAgent::segmentFireBlocks()
{
	LOG(Main, 1) << "Segment Fire Blocks" << endl;

	vector<FireBlock*> segments;
	for (u_int i = 0; i < world->fireBlocks.size(); i++)
	{
		if (world->fireBlocks[i]->getAllBuildings().size() <= segmentBuildingsCount)
		{
			continue;
		}

		int segmentsCount = min(ceil((double) world->fireBlocks[i]->getAllBuildings().size() / segmentBuildingsCount), 4.);
		if (segmentsCount <= 1)
		{
			continue;
		}

		vector<MotionlessObject*> buildings;
		for (Building* building : world->fireBlocks[i]->getAllBuildings())
		{
			buildings.push_back(building);
		}

		KMean kmean;
		kmean.setNumberOfClusters(segmentsCount);
		kmean.setMembers(buildings);
		kmean.fastCalculate();

		for (Cluster* cluster : kmean.getClusters())
		{
			FireBlock* newSegment = new FireBlock;
			for (MotionlessObject* member : cluster->getMembers())
			{
				newSegment->addBuilding((Building*) member);
			}
			segments.push_back(newSegment);
		}

		delete world->fireBlocks[i];
		world->fireBlocks.erase(world->fireBlocks.begin() + i);
		i--;
	}

	for (FireBlock* fireBlock : segments)
	{
		world->fireBlocks.push_back(fireBlock);
	}
}

void FireBrigadeAgent::createFireBlocks()
{
	LOG(Main, 1) << "Create FireBlocks" << endl;

	for (Building* building : world->buildings)
	{
		building->setFireBlock(nullptr);
	}

	for (u_int i = 0; i < world->fireBlocks.size(); i++)
	{
		delete world->fireBlocks[i];
	}

	world->fireBlocks.clear();

	createFieryFireBlocks();

	segmentFireBlocks();

	setFireBlocksSliceIndex();

	for (FireBlock* fireBlock : world->fireBlocks)
	{
		fireBlock->setCenterPoint();
		fireBlock->setAroundBuildings(world);
	}

	for (u_int i = 0; i < world->fireBlocks.size(); i++)
	{
		LOG(Main, 1) << "FireBlock " << i << "(Slice " << world->fireBlocks[i]->slice << ")" << endl;

		LOG(Main, 1) << " > All Buildings: " << world->fireBlocks[i]->getAllBuildings().size() << " > ";
		for (Building* building : world->fireBlocks[i]->getAllBuildings())
		{
			LOG(Main, 1) << building->getId() << " ";
		}
		LOG(Main, 1) << endl;

		LOG(Main, 1) << " > Around Buildings: " << world->fireBlocks[i]->getAroundBuildings().size() << " > ";
		for (Building* building : world->fireBlocks[i]->getAroundBuildings())
		{
			LOG(Main, 1) << building->getId() << " ";
		}
		LOG(Main, 1) << endl;
	}
}

void FireBrigadeAgent::createFieryFireBlocks()
{
	LOG(Main, 1) << "Create Fiery Fire Blocks" << endl;
	world->fireBlocks.clear();

	for (Building* building : world->buildings)
	{
		if (!doesNeedToExtinguish(building, true))
		{
			continue;
		}
		if (building->getFireBlock() != nullptr)
		{
			continue;
		}

		FireBlock* fireBlock = new FireBlock();

		dfsOnBuildings(building, fireBlock, true);

		world->fireBlocks.push_back(fireBlock);
	}
}

void FireBrigadeAgent::dfsOnBuildings(Building* building, FireBlock* fireblock, bool justOnFire)
{
	fireblock->addBuilding(building);
	building->setFireBlock(fireblock);

	stack<Building*> list;
	list.push(building);

	while (!list.empty())
	{
		Building* top = list.top();
		list.pop();

		for (InSightBuilding* isb : top->getInSightBuildings())
		{
			Building* neighbor = (Building*) world->buildings[isb->buildingIndex];
			if (top->slice != neighbor->slice && neighbor->slice != numeric_limits<int>::infinity())
			{
				continue;
			}
			if (neighbor->getFireBlock() != nullptr)
			{
				continue;
			}
			if (!justOnFire && !doesNeedToExtinguish(neighbor, false))
			{
				continue;
			}
			if (justOnFire && !doesNeedToExtinguish(neighbor, true))
			{
				continue;
			}

			neighbor->setFireBlock(fireblock);
			fireblock->addBuilding(neighbor);
			list.push(neighbor);
		}
	}
}

void FireBrigadeAgent::setExtinguishability(vector<FireBlock*>& fireBlocks, vector<FireBlock*>& outExt, vector<FireBlock*>& outUnExt)
{
	u_int maxBuildingCount = 50;
	if (world->isMapLarge() && world->getMapName() != MN_PARIS)
	{
		maxBuildingCount = 40;
	}

	for (FireBlock* fireBlock : fireBlocks)
	{
		if (fireBlock->getAllBuildings().size() > maxBuildingCount)
		{
			fireBlock->setExtinguishability(false);
			outUnExt.push_back(fireBlock);
		}
		else
		{
			fireBlock->setExtinguishability(true);
			outExt.push_back(fireBlock);
		}
	}
}

void FireBrigadeAgent::setFireBlocksSliceIndex()
{
	for (FireBlock* fireBlock : world->fireBlocks)
	{
		for (Building* building : fireBlock->getAllBuildings())
		{
			if (building->slice == numeric_limits<int>::infinity() || fireBlock->slice == -1)
			{
				fireBlock->slice = building->slice;
			}
		}
	}
}

/*** ASSIGN TOOLS ***/

void FireBrigadeAgent::assignToFireBlocksKilooiy()
{
	LOG(Main, 1) << "HasJob: " << self()->hasJob << endl;
	if (world->fireBlocks.empty())
	{
		return;
	}

	vector<FireBlock*> extinguishableFireBlocks, unExtinguishableFireBlocks;
	setExtinguishability(world->fireBlocks, extinguishableFireBlocks, unExtinguishableFireBlocks);

	setNeededAgents(world->fireBlocks);

	sortFireBlocksKilooiy(extinguishableFireBlocks);
	sortFireBlocksKilooiy(unExtinguishableFireBlocks);

	world->fireBlocks.clear();
	world->fireBlocks.insert(world->fireBlocks.end(), extinguishableFireBlocks.begin(), extinguishableFireBlocks.end());
	world->fireBlocks.insert(world->fireBlocks.end(), unExtinguishableFireBlocks.begin(), unExtinguishableFireBlocks.end());

	if (pizzaSlices.size() > 1)
	{
		LOG(Main, 1) << "Pizza assign" << endl;
		for (FireBlock* fireBlock : world->fireBlocks)
		{
			assignNearestAgentsToFireBlock(fireBlock);
		}
	}
	else
	{
		LOG(Main, 1) << "Best assign" << endl;
		world->fireBlocks[0]->addFireBrigade(self());
		self()->targetFireBlock = world->fireBlocks[0];
		self()->hasJob = true;
	}
}

void FireBrigadeAgent::assignToFireBlocksHelper()
{
	Slice* mySlice = pizzaSlices[self()->pizzaSlice];
	int myIndex = find(mySlice->getAssignedAgents().begin(), mySlice->getAssignedAgents().end(), self())
			- mySlice->getAssignedAgents().begin();
	bool isMyObligation = true;

	if (myIndex >= SLICE_FREE_AGENTS * (double) mySlice->getAssignedAgents().size())
	{
		isMyObligation = false;
		LOG(Main, 1) << "It's not my obligation, I'll do it just for REZAYE KHODA" << endl;
	}

	vector<FireBlock*> validFireBlocks;
	for (FireBlock* fireBlock : world->fireBlocks)
	{
		if (fireBlock->slice == self()->pizzaSlice)
		{
			continue;
		}
		if (!isMyObligation && fireBlock->getDistanceToPoint(self()->getPos()) * 2 > nearBuilding)
		{
			continue;
		}
		validFireBlocks.push_back(fireBlock);
	}

	if (validFireBlocks.empty())
	{
		LOG(Main, 1) << "I don't have any valid fire block" << endl;
		return;
	}

	sortFireBlocksKilooiy(validFireBlocks);

	self()->targetFireBlock = validFireBlocks[0];
	self()->hasJob = true;
	validFireBlocks[0]->addFireBrigade(self());
}

void FireBrigadeAgent::assignToFireBlock()
{
	if (world->fireBlocks.empty())
	{
		return;
	}

	assignToFireBlocksKilooiy();

	if (self()->targetFireBlock == nullptr)
	{
		LOG(Main, 1) << "I don't have any fireblock target" << endl;
	}
	else
	{
		LOG(Main, 1) << "I have a fireblock target" << endl;
	}
}

void FireBrigadeAgent::assignNearestAgentsToFireBlock(FireBlock* fireBlock)
{
	// TODO: hatman hatman check konimesh!!!!!!!!

	vector<FireBrigade*> ableAgents;
	for (FireBrigade* fireBrigade : world->fireBrigades)
	{
//		if (fireBrigade->pizzaSlice != fireBlock->slice && fireBlock->slice != numeric_limits<int>::infinity())
//		{
//			continue;
//		}
		if (fireBrigade->hasJob)
		{
			continue;
		}
		if (!isReachableToFireBlock(fireBrigade, fireBlock))
		{
			continue;
		}

		fireBrigade->value = -fireBlock->getDistanceToPoint(fireBrigade->getPos());
		ableAgents.push_back(fireBrigade);
	}

	sort(ableAgents.begin(), ableAgents.end(), [](FireBrigade* f1, FireBrigade* f2)
	{
		return f1->value > f2->value;
	});

	for (int i = 0; i < (int) min((double) fireBlock->getNeededFireBrigade(), (double) ableAgents.size()); i++)
	{
		fireBlock->addFireBrigade(ableAgents[i]);
		ableAgents[i]->targetFireBlock = fireBlock;
		ableAgents[i]->hasJob = true;
	}
}

void FireBrigadeAgent::assignToBuildingsKilooyi()
{
	for (Building* building : self()->targetFireBlock->getAroundBuildings())
	{
		if (position(building).isSuccess || radar->getRadarMode() == RM_NO_COMMUNICATION || radar->getRadarMode() == RM_LOW)
		{
			self()->targetBuildings.push_back(building);
		}
	}

	setValue(self()->targetBuildings, false);

	sort(self()->targetBuildings.begin(), self()->targetBuildings.end(), [](Building* b1, Building* b2)
	{
		return b1->getValue() > b2->getValue();
	});
}

void FireBrigadeAgent::assignToNearBuildings()
{
	LOG(Main, 1) << "Near Buildings!" << endl;

	vector<Building*> candidateBuildings;
	for (Building* building : world->buildings)
	{
		if (!doesNeedToExtinguish(building, true))
		{
			continue;
		}

		PositioningResult positionRes = position(building);
		if (positionRes.motionless != nullptr)
		{
			float graphDistance = worldGraph->getDistance(positionRes.motionless->getRepresentiveNodeIndex(),
					self()->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED);
			if (graphDistance > nearBuilding)
			{
				continue;
			}
		}

		if (positionRes.isSuccess || radar->getRadarMode() == RM_NO_COMMUNICATION || radar->getRadarMode() == RM_LOW)
		{
			candidateBuildings.push_back(building);
		}
	}

	setValue(candidateBuildings, false);

	sort(candidateBuildings.begin(), candidateBuildings.end(), [](Building* b1, Building* b2)
	{
		return b1->getValue() > b2->getValue();
	});

	self()->targetBuildings = candidateBuildings;
}

void FireBrigadeAgent::assignToBuildings()
{
	self()->targetBuildings.clear();

	if (self()->targetFireBlock != nullptr)
	{
		assignToBuildingsKilooyi();
	}
	else
	{
		assignToNearBuildings();
	}
}

void FireBrigadeAgent::setValue2(vector<Building*>& buildings, bool stuckMode)
{
	const float distanceToAgentCoeff = -1 * (stuckMode ? 50.f : 45.f) / DISTANCE_SCALE;
//	const float distanceToGasStationCoeff = -7. / DISTANCE_SCALE;
	const float cityCenterCoeff = -13.f / DISTANCE_SCALE;
	const float volumeCoeff = 3;
	const float innerHumanCoeff = 2;

	for (Building* building : buildings)
	{
		float value = 0;

		PositioningResult positionRes = position(building);
		if (positionRes.motionless != nullptr)
		{
			float graphDistance = worldGraph->getDistance(positionRes.motionless->getRepresentiveNodeIndex(),
					self()->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED);
			value += graphDistance * distanceToAgentCoeff;
		}
		else
		{
			value += self()->getPos().distFromPoint(building->getPos()) * 2. * distanceToAgentCoeff;
		}

//		value += building->getPos().distFromPoint(getNearestGasStationTo(building->getPos())->getPos()) * distanceToGasStationCoeff;
		value += building->getInnerHumansIndexes().size() * innerHumanCoeff;
		value += (building->getAreaGround() * building->getFloors() * FLOOR_HEIGHT) * volumeCoeff;
		value += building->getPos().distFromPoint(cityCenter->getPos()) * cityCenterCoeff;

		building->setValue(value);

		LOG(Main, 1) << "Building " << building->getId() << " " << building->getValue() << endl;
	}
}

void FireBrigadeAgent::setValue(vector<Building*>& buildings, bool stuckMode)
{
	const float distanceToAgentCoeff = -1 * (stuckMode ? 50.f : 45.f) / DISTANCE_SCALE;
//	const float distanceToGasStationCoeff = -7. / DISTANCE_SCALE;
	const float temperatureCoeff = 0.3;
	const float fireBlockCenterCoeff = 10.f / DISTANCE_SCALE;
	const float cityCenterCoeff = -13.f / DISTANCE_SCALE;
	const float volumeCoeff = -0.5;
	const float innerHumanCoeff = 0;

	for (Building* building : buildings)
	{
		float value = 0;

		PositioningResult positionRes = position(building);
		if (positionRes.motionless != nullptr)
		{
			float graphDistance = worldGraph->getDistance(positionRes.motionless->getRepresentiveNodeIndex(),
					self()->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED);
			value += graphDistance * distanceToAgentCoeff;
		}
		else
		{
//			if (!positionRes.isSuccess)
			value += self()->getPos().distFromPoint(building->getPos()) * 2. * distanceToAgentCoeff;
		}

//		value += building->getPos().distFromPoint(getNearestGasStationTo(building->getPos())->getPos()) * distanceToGasStationCoeff;
		value += building->getInnerHumansIndexes().size() * innerHumanCoeff;
		value += (building->getAreaGround() * building->getFloors() * FLOOR_HEIGHT) * volumeCoeff;

		if (!building->getEstimatedData()->hasFire())
		{
			value += building->getEstimatedData()->getTemperature() * temperatureCoeff;
		}

		if (building->getFireBlock() != nullptr)
		{
			value += building->getPos().distFromPoint(building->getFireBlock()->getCenterPoint()) * fireBlockCenterCoeff;
		}
//		else
//		{
		value += building->getPos().distFromPoint(cityCenter->getPos()) * cityCenterCoeff;
//		}

		if (!building->getEstimatedData()->hasFire() && building->getEstimatedData()->getFieryness() != 8)
		{
			value -= 5000;
		}

		if (building->isHardToExtinguish())
		{
			value -= 3000;
		}

		if (building->getEstimatedData()->getFieryness() == 3)
		{
			value -= 300;
		}
		if (building->getEstimatedData()->getFieryness() == 2)
		{
			value -= 500;
		}

		building->setValue(value);

		LOG(Main, 1) << "Building " << building->getId() << " " << building->getValue() << endl;
	}
}

void FireBrigadeAgent::sortFireBlocksKilooiy(vector<FireBlock*>& fireBlocks)
{
	int aroundBuildingWeight = -8, allBuildingWeight = -6, distanceToCenter = -15, distanceToMe = -100, aroundCiviliansWeight = 10,
			sliceWeight = -60;
	if (world->isMapLarge())
	{
		distanceToMe = -130;
		distanceToCenter = -20;
	}

	for (FireBlock* fireBlock : fireBlocks)
	{
		int value = 0;
		value += fireBlock->getAroundBuildings().size() * aroundBuildingWeight;
		value += fireBlock->getAllBuildings().size() * allBuildingWeight;
		value += fireBlock->getDistanceToPoint(cityCenter->getPos()) / DISTANCE_SCALE * distanceToCenter;
		value += fireBlock->getDistanceToPoint(self()->getPos()) / DISTANCE_SCALE * distanceToMe;

		int aroundCivilians = 0;
		for (Building* building : fireBlock->getAroundBuildings())
		{
			for (InSightBuilding* isb : building->getInSightBuildings())
			{
				Building* neighbor = world->buildings[isb->buildingIndex];
				aroundCivilians += neighbor->getInsideCivilianIndexes().size();
			}
		}
		value += aroundCivilians * aroundCiviliansWeight;

		if (fireBlock->slice != numeric_limits<int>::infinity())
		{
			value += abs(fireBlock->slice - self()->pizzaSlice) * sliceWeight;
		}

		fireBlock->setValue(value);
	}

	sort(fireBlocks.begin(), fireBlocks.end(), [](FireBlock* f1, FireBlock* f2)
	{
		return f1->getValue() > f2->getValue();
	});
}

void FireBrigadeAgent::sortFireBlocksTeamWork(vector<FireBlock*>& fireBlocks)
{
	const float AREA_COEFF = -100 / (DISTANCE_SCALE * DISTANCE_SCALE);
	const float CIVILIAN_COEFF = 20;
	const float DISTANCE_TO_CENTER_COEFF = -30 / DISTANCE_SCALE;
	const float DISTANCE_TO_GAS_STATION_COEFF = -80 / DISTANCE_SCALE;

	for (FireBlock* fireBlock : fireBlocks)
	{
		float value = 0;
		value += fireBlock->getTotalArea() * AREA_COEFF;
		value += fireBlock->getCenterPoint().distFromPoint(cityCenter->getPos()) * DISTANCE_TO_CENTER_COEFF;
		value += getDistanceToGasStations(fireBlock) * DISTANCE_TO_GAS_STATION_COEFF;

		int aroundCivilians = 0;
		for (Building* building : fireBlock->getAroundBuildings())
		{
			for (MotionlessObject* neighbor : building->closerThan50B)
			{
				aroundCivilians += neighbor->getInsideCivilianIndexes().size();
			}
		}
		value += aroundCivilians * CIVILIAN_COEFF;

		fireBlock->setValue(value);
	}

	sort(fireBlocks.begin(), fireBlocks.end(), [](FireBlock* f1, FireBlock* f2)
	{
		return f1->getValue() > f2->getValue();
	});
}

void FireBrigadeAgent::setNeededAgents(vector<FireBlock*>& fireBlocks)
{
	bool shitHappens = false;
	if (world->isMapLarge() && world->getMapName() != MN_PARIS)
	{
		shitHappens = true;
	}

	for (FireBlock* fireBlock : fireBlocks)
	{
		unsigned long long totalArea = 0;
		for (Building* building : fireBlock->getAllBuildings())
		{
			totalArea += building->getAreaTotal();
		}
		int buildingsCount = totalArea / averageArea;

		int neededAgents = 0;
		if (shitHappens)
		{
			if (buildingsCount >= 30)
			{
				neededAgents = 25;
			}
			else if (buildingsCount >= 20)
			{
				neededAgents = 18;
			}
			else if (buildingsCount >= 10)
			{
				neededAgents = 14;
			}
			else if (buildingsCount >= 6)
			{
				neededAgents = 10;
			}
			else
			{
				neededAgents = 7;
			}
		}
		else
		{
			if (buildingsCount >= 55)
			{
				neededAgents = 25;
			}
			else if (buildingsCount >= 45)
			{
				neededAgents = 18;
			}
			else if (buildingsCount >= 30)
			{
				neededAgents = 13;
			}
			else if (buildingsCount >= 15)
			{
				neededAgents = 10;
			}
			else
			{
				neededAgents = 7;
			}
		}
		fireBlock->setNeededFireBrigade(neededAgents);
	}
}

Building* FireBrigadeAgent::getBestBuilding(vector<Building*>& buildings, bool stuckMode)
{
	if (buildings.empty())
	{
		return nullptr;
	}

	setValue(buildings, stuckMode);

	sort(self()->targetBuildings.begin(), self()->targetBuildings.end(), [](Building* b1, Building* b2)
	{
		return b1->getValue() > b2->getValue();
	});

	return buildings[0];
}

/*** SEARCH TOOLS ***/

bool FireBrigadeAgent::searchAroundLastTargets()
{
	LOG(Main, 1) << "[searchAroundLastTargets]" << endl;
	if (self()->targetFireBlock == nullptr)
	{
		LOG(Main, 1) << "I don't have any targetFireBlock" << endl;
		aroundSearchTargets.clear();
		aroundSearchTargets.shrink_to_fit();
		lastCycleAroundSet = numeric_limits<int>::max();
		return false;
	}
	else if (aroundSearchTargets.empty())
	{
		LOG(Main, 1) << "It's empty, I'll add new things" << endl;
		for (Building* building : self()->targetFireBlock->getAllBuildings())
		{
			for (InSightBuilding* isb : building->getInSightBuildings())
			{
				if (world->getTime() - world->buildings[isb->buildingIndex]->getLastCycleUpdated() < 5)
				{
					continue;
				}

				if (world->buildings[isb->buildingIndex]->getEstimatedData()->getFieryness() > 0)
				{
					continue;
				}

				if (world->buildings[isb->buildingIndex]->getPos().distFromPoint(self()->getPos()) > AVERAGE_AGENT_SPEED * 3)
				{
					continue;
				}

				aroundSearchTargets.push_back(world->buildings[isb->buildingIndex]);
			}
		}

		if (!aroundSearchTargets.empty())
		{
			LOG(Main, 1) << "OK, set lastCycleAroundSet" << endl;
			lastCycleAroundSet = world->getTime();
			aroundSearchTargets.resize(unique(aroundSearchTargets.begin(), aroundSearchTargets.end()) - aroundSearchTargets.begin());
		}
	}

	// Erase seen targets
	for (int i = 0; i < (int) aroundSearchTargets.size(); i++)
	{
		if (aroundSearchTargets[i]->getLastCycleUpdated() >= lastCycleAroundSet)
		{
			aroundSearchTargets.erase(aroundSearchTargets.begin() + i);
			i--;
		}
	}

	if (aroundSearchTargets.empty())
	{
		LOG(Main, 1) << "aroundSearchTargets is empty" << endl;
		return false;
	}

	// Check if FireBlock has fire
	for (Building* building : self()->targetFireBlock->getAllBuildings())
	{
		if (doesNeedToExtinguish(building, true))
		{
			LOG(Main, 1) << "It's fiery" << endl;
			return false;
		}

		for (InSightBuilding* isb : building->getInSightBuildings())
		{
			if (doesNeedToExtinguish(world->buildings[isb->buildingIndex], true))
			{
				LOG(Main, 1) << "It has a fiery neighbor [type1]" << endl;
				return false;
			}
		}
		for (MotionlessObject* neighborMo : building->closerThan50B)
		{
			Building* neighbor = (Building*) neighborMo;
			if (doesNeedToExtinguish(neighbor, true))
			{
				LOG(Main, 1) << "It has a fiery neighbor [type2]" << endl;
				return false;
			}
		}
	}

	// Find nearest road for search
	Building* bestBuilding = nullptr;
	Road* nearestRoad = nullptr;
	int nearestDistance = numeric_limits<int>::max();
	for (Building* building : aroundSearchTargets)
	{
		for (Road* road : building->canSee)
		{
			if (!worldGraph->isReachable(self()->getRepresentiveNodeIndex(), road->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED))
			{
				continue;
			}
			if (self()->getMotionlessObject()->motionlessIndex == road->motionlessIndex)
			{
				continue;
			}

			int distance = worldGraph->getDistance(self()->getRepresentiveNodeIndex(), road->getRepresentiveNodeIndex(),
					GM_DEFAULT_NOT_BLOCKED);
			if (distance < nearestDistance)
			{
				nearestDistance = distance;
				nearestRoad = road;
				bestBuilding = building;
			}
		}
	}
	world->bestSus = bestBuilding;

	if (nearestRoad != nullptr)
	{
		LOG(Main, 1) << "[Suspicious Search 1] I'm going to suspicious building, " << bestBuilding->getId() << endl;
		command->moveToMotionless(nearestRoad->motionlessIndex);
		return true;
	}
	return false;
}

bool FireBrigadeAgent::isBuildingReachableForSearch(Building* building, FireBrigade* agent)
{
	for (Road* road : building->canSee)
	{
		if (worldGraph->isReachable(road->getRepresentiveNodeIndex(), agent->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED))
		{
			return true;
		}
	}
	return false;
}

bool FireBrigadeAgent::isBuildingClusterReachableForSearch(Cluster cluster, FireBrigade* agent)
{
	for (MotionlessObject* mo : cluster.getMembers())
	{
		if (isBuildingReachableForSearch((Building*) mo, agent))
		{
			return true;
		}
	}
	return false;
}

bool FireBrigadeAgent::searchSuspiciousBuildings()
{
	LOG(Main, 1) << "searchSuspiciousBuildings" << endl;

	if (!canISearchSuspiciousBuildings())
	{
		return false;
	}

	vector<Building*> suspiciousBuildings = getSuspiciousBuildings();
	suspiciousBuildings.resize(unique(suspiciousBuildings.begin(), suspiciousBuildings.end()) - suspiciousBuildings.begin());
	world->suspiciousBuildings = suspiciousBuildings;

	LOG(Main, 1) << "SUS SEARCH: ";
	for (Building* building : suspiciousBuildings)
	{
		LOG(Main, 1) << building->getId() << " ";
	}
	LOG(Main, 1) << endl;

	if (suspiciousBuildings.empty())
	{
		LOG(Main, 1) << "There isn't any suspicious building" << endl;
		return false;
	}

	Road* nearestRoad = nullptr;
	Building* bestBuilding = nullptr;
	for (Building* building : suspiciousBuildings)
	{
		int nearestDistance = numeric_limits<int>::max();
		for (Road* road : building->canSee)
		{
			if (!worldGraph->isReachable(self()->getRepresentiveNodeIndex(), road->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED))
			{
				continue;
			}
			if (world->getTime() - road->getLastCycleUpdatedBySense() < 20)
			{
				continue;
			}

			int distance = worldGraph->getDistance(self()->getRepresentiveNodeIndex(), road->getRepresentiveNodeIndex(),
					GM_DEFAULT_NOT_BLOCKED);
			if (distance < nearestDistance)
			{
				nearestDistance = distance;
				nearestRoad = road;
				bestBuilding = building;
			}
		}

		if (nearestRoad != nullptr)
		{
			break;
		}
	}

	world->bestSus = bestBuilding;

	if (nearestRoad != nullptr)
	{
		LOG(Main, 1) << "[Suspicious Search 1] I'm going to suspicious building, " << bestBuilding->getId() << endl;
		command->moveToMotionless(nearestRoad->motionlessIndex);
		return true;
	}

	return false;
}

bool FireBrigadeAgent::doesNeedSuspicousSearch(Building* building)
{
	if (building->hasFire())
	{
		return false;
	}
	if (building->getWorthlessness())
	{
		return false;
	}
	if (building->getFieryness() == 8)
	{
		return false;
	}
	if (building->getLastCycleUpdated() == world->getTime())
	{
		return false;
	}

	bool flag = false;
	for (InSightBuilding* isb : building->getInSightBuildings())
	{
		Building* neighbor = world->buildings[isb->buildingIndex];
		if (neighbor->getEstimatedData()->hasFire())
		{
			return false;
		}
		if (neighbor->getEstimatedData()->getFieryness() > 3 && neighbor->getEstimatedData()->getFieryness() != 8)
		{
			flag = true;
		}
	}

	return flag;
}

bool FireBrigadeAgent::setNearRoadSearchTarget()
{
	int bestBuildingIndex = -1;
	int minDistance = numeric_limits<int>::max(), tempDistance;

	for (u_int i = 0; i < world->buildings.size(); i++)
	{
		Building* thisBuilding = world->buildings[i];

		if (thisBuilding->lastTimeSeen != -1)
		{
			continue;
		}

		if (thisBuilding->hasFire() || thisBuilding->getFieryness() == 8 || thisBuilding->getEstimatedData()->getFieryness() == 8)
		{
			continue;
		}

		if (!worldGraph->isReachable(self()->getRepresentiveNodeIndex(), thisBuilding->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED))
		{
			continue;
		}

		tempDistance = worldGraph->getDistance(self()->getRepresentiveNodeIndex(), thisBuilding->getRepresentiveNodeIndex(),
				GM_DEFAULT_NOT_BLOCKED);
		if (minDistance > tempDistance)
		{
			minDistance = tempDistance;
			bestBuildingIndex = i;
		}
	}

	if (bestBuildingIndex != -1)
	{
		searchTarget = world->buildings[bestBuildingIndex];
		command->moveToMotionless(searchTarget->motionlessIndex);
		return true;
	}
	return false;
}

bool FireBrigadeAgent::setRandomSearchTarget()
{
	LOG(Main, 1) << "Random Search" << endl;

	vector<Road*> reachableRoads;
	if (assignStrategy == FBAS_PIZZA && pizzaSlices.size() > 1)
	{
		for (Building* building : pizzaSlices[self()->pizzaSlice]->getBuildings())
		{
			for (Road* road : building->canSee)
			{
				if (world->getTime() - road->getLastCycleUpdated() > 10 || road->getLastCycleUpdated() <= 0)
				{
					if (worldGraph->isReachable(self()->getRepresentiveNodeIndex(), road->getRepresentiveNodeIndex(),
							GM_DEFAULT_NOT_BLOCKED))
					{
						reachableRoads.push_back(road);
						break;
					}
				}
			}
		}
	}
	else
	{
#if SEARCH_CLUSTER
		for (MotionlessObject* motionless : searchClusters[self()->searchCluster]->getMembers())
		{
			Road* road = (Road*) motionless;
			if (!road->buildingWithThisRoadCanSee.empty())
			{
				if (world->getTime() - road->getLastCycleUpdated() > 10 || road->getLastCycleUpdated() <= 0)
				{
					if (worldGraph->isReachable(self()->getRepresentiveNodeIndex(), road->getRepresentiveNodeIndex(),
							GM_DEFAULT_NOT_BLOCKED))
					{
						reachableRoads.push_back(road);
					}
				}
			}
		}
#else
		for (MotionlessObject* motionless : world->roads)
		{
			Road* road = (Road*) motionless;
			if (!road->buildingWithThisRoadCanSee.empty())
			{
				if (world->getTime() - road->getLastCycleUpdated() > 10 || road->getLastCycleUpdated() <= 0)
				{
					if (worldGraph->isReachable(self()->getRepresentiveNodeIndex(), road->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED))
					{
						reachableRoads.push_back(road);
						break;
					}
				}
			}
		}
#endif
	}

	if (reachableRoads.empty())
	{
		LOG(Main, 1) << "No reachable road for searching" << endl;
		return false;
	}
	else
	{
		searchTarget = reachableRoads[rand() % reachableRoads.size()];
		return true;
	}
}

bool FireBrigadeAgent::setSuspiciousSearchTarget()
{
	LOG(Main, 1) << "Suspicious Search" << endl;

	vector<Building*> suspiciousBuildings;
	for (Building* building : world->buildings)
	{
		if (doesNeedSuspicousSearch(building))
		{
			suspiciousBuildings.push_back(building);
		}
	}

	LOG(Main, 1) << "Suspicious buildings --> " << suspiciousBuildings.size() << endl;

	Road* nearestRoad = nullptr;
	double nearestDistance = numeric_limits<double>::max();
	for (Building* building : suspiciousBuildings)
	{
		for (Road* road : building->canSee)
		{
			if (!worldGraph->isReachable(self()->getRepresentiveNodeIndex(), road->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED))
			{
				continue;
			}
			if (self()->getPos().distFromPoint(road->getPos()) * 1.5 > nearBuilding)
			{
				continue;
			}
			if (road->getLastCycleUpdatedBySense() - world->getTime() < 8)
			{
				continue;
			}

			double distance = self()->getPos().distFromPoint(road->getPos());
			if (distance < nearestDistance)
			{
				nearestDistance = distance;
				nearestRoad = road;
			}
		}
	}

	if (nearestRoad != nullptr)
	{
		searchTarget = nearestRoad;
		return true;
	}
	return false;
}

bool FireBrigadeAgent::canISearchSuspiciousBuildings()
{
//	if (self()->targetFireBlock != nullptr)
//	{
//		if (self()->targetFireBlock->getDistanceToPoint(self()->getPos()) * 1.5 < nearBuilding * 2 / 3)
//		{
//			return false;
//		}
//	}

	for (Building* building : self()->targetBuildings)
	{
		if (building->getPos().distFromPoint(self()->getPos()) * 1.5 < nearBuilding * 2 / 3)
		{
			return false;
		}
	}
	return true;

	/*	for (Building* building : self()->targetFireBlock->getAllBuildings())
	 {
	 if (building->getEstimatedData()->hasFire())
	 {
	 return false;
	 }

	 for (InSightBuilding* isb : building->getInSightBuildings())
	 {
	 Building* neighbor = world->buildings[isb->buildingIndex];

	 if (neighbor->getEstimatedData()->hasFire())
	 {
	 return false;
	 }
	 }
	 }*/
//	return true;
}

vector<Building*> FireBrigadeAgent::getSuspiciousBuildings()
{
	vector<Building*> suspiciousBuildings;

	for (Building* building : world->buildings)
	{
//		if (building->getEstimatedData()->hasFire() || building->getEstimatedData()->getTemperature() >= 1 || building->hasFire() || building->getTemperature() > 0)
		if (isVerySuspiciousBuilding(building))
		{
//			if (building->getEstimatedData()->getFieryness() >= 5 && building->getEstimatedData()->getFieryness() <= 8)
//			{
//				continue;
//			}

			LOG(Main, 1) << building->getId() << " -- " << building->getEstimatedData()->hasFire() << " "
					<< building->getEstimatedData()->getTemperature() << " " << building->hasFire() << " " << building->getTemperature()
					<< endl;
			for (InSightBuilding* isb : building->getInSightBuildings())
			{
				Building* neighbor = world->buildings[isb->buildingIndex];

				if (isb->value < 0.02)
				{
					continue;
				}

				if (neighbor->getEstimatedData()->hasFire() || neighbor->getEstimatedData()->getFieryness() == 8)
				{
					continue;
				}

				if (world->getTime() - neighbor->lastTimeSearchedByMe < 60)
				{
					continue;
				}

				if (!isBuildingReachableForSearch(neighbor, self()))
				{
					continue;
				}

				int bestDistance = numeric_limits<int>::max();
				for (Road* cansee : neighbor->canSee)
				{
					int distance = worldGraph->getDistance(self()->getRepresentiveNodeIndex(), cansee->getRepresentiveNodeIndex(),
							GM_DEFAULT_NOT_BLOCKED);
					if (distance < bestDistance)
					{
						bestDistance = distance;
					}
				}
				if (bestDistance > SUSPICIOUS_SEARCH_NEAR_BUILDING)
				{
					continue;
				}

				neighbor->susSearchValue = 0;
				neighbor->susSearchValue += neighbor->getTemperature() / 5 + neighbor->getEstimatedData()->getTemperature() / 5;
				neighbor->susSearchValue += (world->getTime() - neighbor->getLastCycleUpdated()) * 80;
				neighbor->susSearchValue -= neighbor->getPos().distFromPoint(cityCenter->getPos()) / DISTANCE_SCALE;
				neighbor->susSearchValue -= bestDistance / DISTANCE_SCALE;

				suspiciousBuildings.push_back(neighbor);
			}
		}
	}

	sort(suspiciousBuildings.begin(), suspiciousBuildings.end(), [](Building* b1, Building* b2)
	{
		return b1->susSearchValue > b2->susSearchValue;
	});

	return suspiciousBuildings;
}

void FireBrigadeAgent::search()
{
	LOG(Main, 1) << "Search Mode: " << searchMode << endl;

	if (searchTarget != nullptr)
	{
		LOG(Main, 1) << "I have search target" << endl;
		if (searchTarget->getId() == self()->getMotionlessObject()->getId())
		{
			searchTarget = nullptr;
		}
		else if (searchTarget->isBuilding() && ((Building*) searchTarget)->getEstimatedData()->hasFire())
		{
			searchTarget = nullptr;
		}
		else if (!worldGraph->isReachable(self()->getRepresentiveNodeIndex(), searchTarget->getRepresentiveNodeIndex(),
				GM_DEFAULT_NOT_BLOCKED))
		{
			searchTarget = nullptr;
		}
		else if (world->getTime() - searchTarget->getLastCycleUpdated() <= SEEN_SEARCH_TARGET)
		{
			searchTarget = nullptr;
		}

		if (searchTarget != nullptr)
		{
			LOG(Main, 1) << "I'm going to my last target, " << searchTarget->getId() << endl;
			command->moveToMotionless(searchTarget->motionlessIndex);
			return;
		}
	}
	if (searchTarget == nullptr)
	{
		searchMode = FSM_NONE;
	}

	if (self()->fireBrigadeIndex <= NEAR_SEARCH_LIMIT * world->fireBrigades.size())
	{
		if (isAnyOneCloseToMe())
		{
			return;
		}

		if (setNearRoadSearchTarget())
		{
			LOG(Main, 1) << "I'm going to near places, " << searchTarget->getId() << endl;
			searchMode = FSM_NEAR_ROAD;
			command->moveToMotionless(searchTarget->motionlessIndex);
			return;
		}
	}

	if (setRandomSearchTarget())
	{
		LOG(Main, 1) << "I'm going to random places, " << searchTarget->getId() << endl;
		searchMode = FSM_RANDOM_SEARCH;
		command->moveToMotionless(searchTarget->motionlessIndex);
		return;
	}

	randomWalk();

	return;
}

bool FireBrigadeAgent::isAnyOneCloseToMe()
{
	vector<FireBrigade *> myAroundFireBrigades;
	for (FireBrigade* fb : ableAgents)
	{
		if (fb->getLastCycleUpdatedBySense() == world->getTime() && self()->getPos().distFromPoint(fb->getPos()) < 20000)
		{
			myAroundFireBrigades.push_back(fb);
			LOG(Main, 1) << fb->getId() << " is close to me!" << endl;
		}
	}

	if (myAroundFireBrigades.size() <= 1)
	{
		return false;
	}

	int myIndexInOurGroup = 0;
	int gX = 0, gY = 0;

	vector<Road*> aroundRoads;
	for (u_int fCounter = 0; fCounter < myAroundFireBrigades.size(); fCounter++)
	{
		if (self()->getId() == myAroundFireBrigades[fCounter]->getId())
		{
			myIndexInOurGroup = fCounter;
		}

		for (u_int bCounter = 0; bCounter < myAroundFireBrigades[fCounter]->getMotionlessObject()->closerThan50R.size(); bCounter++)
		{
			Road* thisRoad = ((Road*) myAroundFireBrigades[fCounter]->getMotionlessObject()->closerThan50R[bCounter]);
			aroundRoads.push_back(thisRoad);

			gX = gX + thisRoad->getPos().getX();
			gY = gY + thisRoad->getPos().getY();
		}
	}

	if (aroundRoads.empty())
	{
		return false;
	}

	if (world->getTime() % 5 == 1)
	{
		myIndexInOurGroup = (myAroundFireBrigades.size() - 1) - myIndexInOurGroup;
	}

	gX = gX / aroundRoads.size();
	gY = gY / aroundRoads.size();
	Point g(gX, gY);

	int myMinAngle = (360 / myAroundFireBrigades.size()) * (myIndexInOurGroup);
	int myMaxAngle = (360 / myAroundFireBrigades.size()) * (myIndexInOurGroup + 1);
	vector<Road*> myAroundRoads;

	for (Road* road : myAroundRoads)
	{
		Segment thisSegment(g, road->getPos());
		if (isBetween(thisSegment.getTheta(), myMinAngle, myMaxAngle))
		{
			myAroundRoads.push_back(road);
		}
	}

	if (myAroundRoads.empty())
	{
		return false;
	}

	int maxUpdateTime = -1, bestIndex = -1;
	for (u_int i = 0; i < myAroundRoads.size(); i++)
	{
		if (world->getTime() - myAroundRoads[i]->getLastCycleUpdated() > maxUpdateTime
				&& myAroundRoads[i]->getId() != self()->getMotionlessObject()->getId())
		{
			if (!worldGraph->isReachable(self()->getRepresentiveNodeIndex(), myAroundRoads[i]->getRepresentiveNodeIndex(),
					GM_DEFAULT_NOT_BLOCKED))
			{
				continue;
			}

			maxUpdateTime = world->getTime() - myAroundRoads[i]->getLastCycleUpdated();
			bestIndex = i;
		}
	}

	if (bestIndex != -1)
	{
		searchTarget = myAroundRoads[bestIndex];
		command->moveToMotionless(searchTarget->motionlessIndex);
		return true;
	}
	else
	{
		int bestIndex1 = -1, bestIndex2 = -1;
		for (u_int i = 0; i < myAroundRoads.size(); i++)
		{
			for (u_int j = 0; j < myAroundRoads[i]->closerThan50R.size(); j++)
			{
				if (worldGraph->isReachable(self()->getRepresentiveNodeIndex(),
						myAroundRoads[i]->closerThan50R[j]->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED))
				{
					bestIndex1 = i;
					bestIndex2 = j;
					break;
				}
			}
		}

		if (bestIndex1 != -1)
		{
			command->moveToMotionless(myAroundRoads[bestIndex1]->closerThan50R[bestIndex2]->motionlessIndex);
			return true;
		}
	}

	command->rest();
	return true;
}

void FireBrigadeAgent::randomWalk()
{
	LOG(Main , 1) << "[RandomWalk] start!" << endl;
	vector<MotionlessObject*> roads;

	for (Road* road : world->roads)
	{
		if (worldGraph->isReachable(self()->getRepresentiveNodeIndex(), road->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED))
		{
			roads.push_back(road);
		}
	}
	LOG(Main, 1) << "[RandomWalk] reachable roads: " << roads.size() << endl;

	if (roads.empty())
	{
		vector<int> tempPath;
		tempPath.push_back(self()->getMotionlessObject()->getId());
		return command->move(tempPath, -1, -1);
	}

	int maxUpdate = -1, bestIndex = 0;
	for (u_int i = 0; i < roads.size(); i++)
	{
		if (world->getTime() - roads[i]->getLastCycleUpdatedBySense() > maxUpdate)
		{
			maxUpdate = world->getTime() - roads[i]->getLastCycleUpdatedBySense();
			bestIndex = i;
		}
	}

	command->moveToMotionless(roads[bestIndex]->motionlessIndex);
}

void FireBrigadeAgent::assignToSearchCluster()
{
	vector<vector<int>> costMatrix;
	costMatrix.resize(world->fireBrigades.size());

	for (u_int y = 0; y < costMatrix.size(); y++)
	{
		costMatrix[y].resize(costMatrix.size());
		for (u_int x = 0; x < costMatrix[y].size(); x++)
		{
//			costMatrix[y][x % searchClusters.size()] = -searchClusters[x % searchClusters.size()]->groundDistToEntity(world->fireBrigades[y]->getMotionlessObject(), worldGraph, GM_DEFAULT_NOT_BLOCKED);
			costMatrix[y][x % searchClusters.size()] = -searchClusters[x % searchClusters.size()]->airDistToEntity(
					world->fireBrigades[y]->getMotionlessObject());
		}
	}

	HungarianAssignment hungarianAss(costMatrix);

	vector<vector<bool>> assignment = hungarianAss.hungarian();
	for (u_int y = 0; y < assignment.size(); y++)
	{
		for (u_int x = 0; x < assignment[y].size(); x++)
		{
			if (assignment[y][x])
			{
				world->fireBrigades[y]->searchCluster = x % searchClusters.size();
				break;
			}
		}
	}
}

void FireBrigadeAgent::setSearchClusters()
{
	searchClusters.clear();

	vector<MotionlessObject*> roads;
	for (Road* road : world->roads)
	{
		if (!road->buildingWithThisRoadCanSee.empty())
		{
			roads.push_back(road);
		}
	}

	KMean kmean;
	kmean.setNumberOfClusters(int(max(1., world->fireBrigades.size() / 2.)));
	kmean.setMembers(roads);
	kmean.fastCalculate();

	for (Cluster* cluster : kmean.getClusters())
	{
		Cluster* newCluster = new Cluster;
		newCluster->setMembers(cluster->getMembers());
		newCluster->setCenteralPoint();
		searchClusters.push_back(newCluster);
	}

	world->searchClusters = searchClusters;
}

/*** POSITIONING ***/

Road* FireBrigadeAgent::getBestRoadForPosition(vector<Road*>& roads)
{
	const float DISTANCE_TO_AGENT = -1000. / DISTANCE_SCALE;
	const float HYDRANT = 200000.;

	Road* bestRoad = nullptr;
	float bestValue = numeric_limits<float>::lowest();

	for (Road* road : roads)
	{
		float value = 0;
		value += worldGraph->getDistance(self()->getRepresentiveNodeIndex(), road->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED)
				* DISTANCE_TO_AGENT;
		if (road->isHydrant())
		{
			value += HYDRANT;

			for (int index : road->getInsidePlatoonIndexes())
			{
				if (world->platoons[index]->isFireBrigade())
				{
					value -= HYDRANT;
					break;
				}
			}
		}

		if (value > bestValue)
		{
			bestRoad = road;
			bestValue = value;
		}
	}

	return bestRoad;
}

PositioningResult FireBrigadeAgent::position(Building* building)
{
	PositioningResult visibilityPosition = visibilityPositioning(building);
	PositioningResult approximatedPosition = approximatePositioning(building);

	if (approximatedPosition.isSuccess && useFireEstimator)
	{
		if (world->getTime() - building->getLastCycleUpdated() <= min(2, world->getTime()))
		{
			return approximatedPosition;
		}
	}

	if (visibilityPosition.isSuccess)
	{
		// I'm in right location
		if (visibilityPosition.motionless == nullptr)
		{
			return visibilityPosition;
		}

		// WTF?!
		if (approximatedPosition.motionless == nullptr && !approximatedPosition.isSuccess)
		{
			CLOGLN("WTF!?!?")
			return visibilityPosition;
		}

		// If visibility takes much time, so use fucking approximated position
		int visibilityDistance = worldGraph->getDistance(self()->getRepresentiveNodeIndex(),
				visibilityPosition.motionless->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED);
		int approximateDistance = 0;
		if (approximatedPosition.motionless != nullptr)
		{
			approximateDistance = worldGraph->getDistance(self()->getRepresentiveNodeIndex(),
					approximatedPosition.motionless->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED);
		}

		if ((world->isMapLarge() && visibilityDistance - approximateDistance > POSITIONINGS_DIFFERENCE_BIG)
				|| (!world->isMapLarge() && visibilityDistance - approximateDistance > POSITIONINGS_DIFFERENCE_SMALL))
		{
			return approximatedPosition;
		}
		else
		{
			return visibilityPosition;
		}
	}

	if (approximatedPosition.isSuccess)
	{
		if ((!useFireEstimator && world->getTime() - building->getLastCycleUpdated() <= min(APPROXIMATE_POSITIONING_LCU_1, world->getTime()))
				|| (useFireEstimator
						&& world->getTime() - building->getLastCycleUpdated() <= min(APPROXIMATE_POSITIONING_LCU_2, world->getTime())))
		{
			return approximatedPosition;
		}
	}

	return PositioningResult(nullptr, false);
}

PositioningResult FireBrigadeAgent::visibilityPositioning(Building* building)
{
	if (self()->getPos().distFromPoint(building->getPos()) < maxExtinguishDistance
			&& building->getLastCycleUpdatedBySense() == world->getTime())
	{
//		LOG(Main, 1) << "I can see this building" << endl;
		return PositioningResult(nullptr, true);
	}

	vector<Road*> cansees;
	for (Road* road : building->canSee)
	{
		if (worldGraph->isReachable(self()->getRepresentiveNodeIndex(), road->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED)
				&& building->getPos().distFromPoint(road->getPos()) < maxExtinguishDistance)
		{
			cansees.push_back(road);
		}
	}

//	LOG(Main, 1) << cansees.size() << " Reachable in range cansee road" << endl;
	if (cansees.empty())
	{
		return PositioningResult(nullptr, false);
	}

	for (Road* road : cansees)
	{
		LOG(Main, 1 ) << road->getId() << " ";
	}
	LOG(Main, 1) << endl;

	Road* bestRoad = getBestRoadForPosition(cansees);
	if (self()->getMotionlessObject()->getId() != bestRoad->getId())
	{
		return PositioningResult(bestRoad, true);
	}
	else
	{
		return PositioningResult(self()->getMotionlessObject(), true);
	}
}

PositioningResult FireBrigadeAgent::approximatePositioning(Building* building)
{
	if (self()->getPos().distFromPoint(building->getPos()) < maxExtinguishDistance)
	{
//		LOG(Main, 1) << "I'm near to this building" << endl;
		return PositioningResult(nullptr, true);
	}

	vector<Road*> nearRoads;
	for (Road* road : world->roads)
	{
		if (building->getPos().distFromPoint(road->getPos()) < maxExtinguishDistance
				&& worldGraph->isReachable(self()->getRepresentiveNodeIndex(), road->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED))
		{
			nearRoads.push_back(road);
		}
	}

//	LOG(Main, 1) << nearRoads.size() << " Reachable roads" << endl;
	if (nearRoads.empty())
	{
		return PositioningResult(nullptr, false);
	}

	Road* bestRoad = getBestRoadForPosition(nearRoads);
	if (self()->getMotionlessObject()->getId() != bestRoad->getId())
	{
		return PositioningResult(bestRoad, true);
	}
	else
	{
		return PositioningResult(self()->getMotionlessObject(), true);
	}
}

bool FireBrigadeAgent::canPosition(FireBrigade* fireBrigade, Building* building)
{
	if (fireBrigade->getPos().distFromPoint(building->getPos()) < maxExtinguishDistance - 1000.f)
	{
		return true;
	}

	for (Road* road : building->canSee)
	{
		if (worldGraph->isReachable(fireBrigade->getRepresentiveNodeIndex(), road->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED))
		{
			return true;
		}
	}

	for (Road* road : world->roads)
	{
		if (road->getPos().distFromPoint(building->getPos()) < maxExtinguishDistance
				&& worldGraph->isReachable(fireBrigade->getRepresentiveNodeIndex(), road->getRepresentiveNodeIndex(),
						GM_DEFAULT_NOT_BLOCKED))
		{
			return true;
		}
	}

	return false;
}

bool FireBrigadeAgent::moveAndExtinguish(Building* building, bool tryToBlock)
{
	LOG(Main, 1) << "[moveAndExtinguish] building: " << building->getId() << endl;

	if (self()->getWaterQuantity() == 0)
	{
		LOG(Main, 1) << "I can't extinguish, Water quantity equals to zero" << endl;
		return false;
	}

	if (building->isHardToExtinguish())
	{
		hardToExtinguishPlan(building);
		return true;
	}

	PositioningResult positioningResult = position(building);

	if (positioningResult.isSuccess)
	{
		if (positioningResult.motionless == nullptr)
		{
			int waterAmount = min(getNeededWaterAmount(building), min(maxWaterForExtinguish, self()->getWaterQuantity()));
			if (useFireEstimator)
			{
				fireEstimator->extinguishBuilding(building, waterAmount * getTeamMatesCount(building));
			}
			command->extinguish(*building, waterAmount);
			LOG(Main, 1) << "I'm extinguishing target [" << building->getId() << "]" << endl;
			return true;
		}
		else
		{
			LOG(Main, 1) << "I'm going to reach target [" << building->getId() << "]" << endl;
			command->moveToMotionless(positioningResult.motionless->motionlessIndex);
			return true;
		}
	}
	else
	{
		LOG(Main, 1) << "I can't reach target [" << building->getId() << "]." << endl;
		if (tryToBlock)
		{
			LOG(Main, 1) << "I'll try to block this building" << endl;
			vector<Building*> nearBuildings = getNearExtinguishableBuildings(building);

			setValue(nearBuildings, false);
			sort(nearBuildings.begin(), nearBuildings.end(), [](Building* b1, Building* b2)
			{
				return b1->getValue() > b2->getValue();
			});

			if (moveAndExtinguish(nearBuildings, false))
			{
				return true;
			}
		}
		return false;
	}
}

bool FireBrigadeAgent::moveAndExtinguish(vector<Building*>& buildings, bool tryToBlock)
{
	for (Building* building : buildings)
	{
		if (moveAndExtinguish(building, tryToBlock))
		{
			return true;
		}
	}
	return false;
}

bool FireBrigadeAgent::extinguishFromHere(Building* building, bool tryToBlock)
{
	if (self()->getWaterQuantity() == 0)
	{
		LOG(Main, 1) << "I can't extinguish, Water quantity equals to zero" << endl;
		return false;
	}

//	if (tryToBlock)
//	{
//		LOG(Main, 1) << "I'll try to block this building" << endl;
//		vector<Building*> nearBuildings = getNearExtinguishableBuildings(building);
//
//		setValue(nearBuildings, false);
//		sort(nearBuildings.begin(), nearBuildings.end(), [](Building* b1, Building* b2)
//		{
//			return b1->getValue() > b2->getValue();
//		});
//
//		if (moveAndExtinguish(nearBuildings, false))
//		{
//			return true;
//		}
//	}

	if (self()->getPos().distFromPoint(building->getPos()) > maxExtinguishDistance)
	{
		LOG(Main, 1) << "I can't extinguish, It's too far" << endl;
		return false;
	}

	int water = getNeededWaterAmount(building);

	if (useFireEstimator)
	{
		fireEstimator->extinguishBuilding(building, water * getTeamMatesCount(building));
	}
	command->extinguish(*building, water);
	return true;
}

bool FireBrigadeAgent::extinguishFromHere(vector<Building*>& buildings, bool tryToBlock)
{
	for (Building* building : buildings)
	{
		if (extinguishFromHere(building, tryToBlock))
		{
			return true;
		}
	}
	return false;
}

void FireBrigadeAgent::hardToExtinguishPlan(Building* building)
{
	int minHighRiskTemperature = 15;

	vector<Building*> aroundsOfMyMainTarget;
	vector<Building*> aroundsHighRiskOfMyMainTarget;

	for (InSightBuilding* isb : building->getInSightBuildings())
	{
		Building* thisBuilding = (Building*) world->buildings[isb->buildingIndex];

		if (self()->getPos().distFromPoint(thisBuilding->getPos()) >= maxExtinguishDistance)
		{
			continue;
		}

		if (!thisBuilding->getEstimatedData()->isFlammable())
		{
			continue;
		}

		if (thisBuilding->getEstimatedData()->hasFire() && !thisBuilding->isHardToExtinguish())
		{
			aroundsOfMyMainTarget.push_back(thisBuilding);
		}

		if (!thisBuilding->getEstimatedData()->hasFire() && thisBuilding->getEstimatedData()->getTemperature() > minHighRiskTemperature)
		{
			aroundsHighRiskOfMyMainTarget.push_back(thisBuilding);
		}
	}

	if (!aroundsOfMyMainTarget.empty())
	{
		Building* bestBuilding = getBestBuilding(aroundsOfMyMainTarget, false);
		command->extinguish(*bestBuilding, getNeededWaterAmount(bestBuilding));
		return;
	}

	if (!aroundsHighRiskOfMyMainTarget.empty())
	{
		Building* bestBuilding = getBestBuilding(aroundsHighRiskOfMyMainTarget, false);
		command->extinguish(*bestBuilding, getNeededWaterAmount(bestBuilding));
		return;
	}

	if (scanAroundHereForExtinguishingCloseFire())
	{
		return;
	}

	if (takeAWalkAround(building))
	{
		return;
	}

	command->rest();
	return;
}

/*** UTILITIES ***/

vector<Building*> FireBrigadeAgent::getNearExtinguishableBuildings(Building* building)
{
	vector<Building*> nearBuildings;
	for (InSightBuilding* isb : building->getInSightBuildings())
	{
		if (doesNeedToExtinguish(world->buildings[isb->buildingIndex],
				false) && !world->buildings[isb->buildingIndex]->isHardToExtinguish() && world->buildings[isb->buildingIndex]->getAreaTotal() >= averageArea * BUILDING_AREA_FOR_BLOCK)
		{
			nearBuildings.push_back(world->buildings[isb->buildingIndex]);
		}
	}
	return nearBuildings;
}

vector<Building*> FireBrigadeAgent::getNearExtinguishableBuildingsForBlock(Building* building)
{
	vector<Building*> nearBuildings;
	for (InSightBuilding* isb : building->getInSightBuildings())
	{
		if (world->buildings[isb->buildingIndex]->getEstimatedData()->getFieryness() != 0)
		{
			continue;
		}
		if (!doesNeedToExtinguish(world->buildings[isb->buildingIndex], false))
		{
			continue;
		}

		nearBuildings.push_back(world->buildings[isb->buildingIndex]);
	}
	return nearBuildings;
}

GasStation* FireBrigadeAgent::getNearestGasStationTo(Point point)
{
	double minDistance = numeric_limits<double>::max();
	GasStation* nearest = nullptr;

	for (GasStation* gasStation : world->gasStations)
	{
		double distance = point.distFromPoint(gasStation->getPos());
		if (distance < minDistance)
		{
			nearest = gasStation;
			minDistance = distance;
		}
	}

	return nearest;
}

int FireBrigadeAgent::getDistanceToGasStations(FireBlock* fireBlock)
{
	int totalDistance = -1;

	for (GasStation* gasStation : world->gasStations)
	{
		double minDistance = numeric_limits<double>::max();
		for (Building* building : fireBlock->getAllBuildings())
		{
			int distance = building->getPos().distFromPoint(gasStation->getPos());
			if (distance < minDistance)
			{
				minDistance = distance;
			}
		}

		totalDistance += minDistance;
	}

	return totalDistance;
}

int FireBrigadeAgent::getNeededWaterAmount(Building* building)
{
	// TODO
	if (!building->getEstimatedData()->hasFire())
	{
		return maxWaterForExtinguish / 2;
	}
	if (building->getAreaTotal() < averageArea / 2)
	{
		return maxWaterForExtinguish / 2;
	}
	return maxWaterForExtinguish;
}

int FireBrigadeAgent::getTeamMatesCount(Building* building)
{
	int count = 0;

	for (FireBrigade* fb : world->fireBrigades)
	{
		if (fb->getPos().distFromPoint(self()->getPos()) < TEAM_MATES_DISTANCE
				&& building->getPos().distFromPoint(fb->getPos()) < maxExtinguishDistance)
		{
			count++;
		}
	}

	return count;
}

bool FireBrigadeAgent::SHOW_THE_POWER_OF_SOSHIANT()
{
	if (world->getTime() < 4 && self()->getPos().distFromPoint(cityCenter->getPos()) > maxExtinguishDistance)
	{
		command->extinguish(*cityCenter, 1);
		return true;
	}

	return false;
}

bool FireBrigadeAgent::assassinVerySuspiciousBuildings()
{
	LOG(Main, 1) << "Assassin very suspicious buildings" << endl;

	if (!self()->targetBuildings.empty())
	{
		for (Building* building : self()->targetBuildings)
		{
			if (building->getPos().distFromPoint(self()->getPos()) <= nearBuilding * 1 / 2)
			{
				return false;
			}
		}
	}

	vector<Building*> verySuspiciousBuildings;
	for (Building* building : world->buildings)
	{
		if (isVerySuspiciousBuilding(building))
		{
			verySuspiciousBuildings.push_back(building);
		}
	}

	if (verySuspiciousBuildings.empty())
	{
		return false;
	}

	// Create suspicious groups using DFS
	vector<Cluster> suspiciousGroups;
	vector<bool> mark(world->buildings.size(), false);
	for (Building* building : verySuspiciousBuildings)
	{
		if (mark[building->buildingIndex])
		{
			continue;
		}

		Cluster susGroup;
		stack<Building*> list;
		list.push(building);
		mark[building->buildingIndex] = true;

		while (!list.empty())
		{
			Building* top = list.top();
			list.pop();

			for (InSightBuilding* isb : top->getInSightBuildings())
			{
				Building* building = world->buildings[isb->buildingIndex];
				if (mark[building->buildingIndex]
						|| find(verySuspiciousBuildings.begin(), verySuspiciousBuildings.end(), building) == verySuspiciousBuildings.end())
				{
					continue;
				}
				mark[building->buildingIndex] = true;
				susGroup.addMember((MotionlessObject*) building);
				list.push(building);
			}
		}
		suspiciousGroups.push_back(susGroup);
	}
	LOG(Main, 1) << "Suspicious groups: " << suspiciousGroups.size() << endl;

	// Choose my cluster
	Cluster nearestCluster;
	int minDistance = numeric_limits<int>::max();
	for (Cluster susGroup : suspiciousGroups)
	{
		if (!isBuildingClusterReachableForSearch(susGroup, self()))
		{
			continue;
		}

		int distance = susGroup.airDistToEntity(self()->getMotionlessObject());
		if (distance < minDistance && distance * 1.5 < nearBuilding * 1 / 3)
		{
			minDistance = distance;
			nearestCluster = susGroup;
		}
	}

	if (nearestCluster.getMembers().empty())
	{
		return false;
	}

	// Find nearest road
	vector<Building*> buildingsInCluster;
	buildingsInCluster.reserve(nearestCluster.getMembers().size());

	Road* nearestRoad = nullptr;
	double nearestDistance = numeric_limits<double>::max();
	for (MotionlessObject* mo : nearestCluster.getMembers())
	{
		buildingsInCluster.push_back((Building*) mo);

		for (InSightBuilding* isb : ((Building*) mo)->getInSightBuildings())
		{
			Building* building = world->buildings[isb->buildingIndex];
			for (Road* road : building->canSee)
			{
				if (!worldGraph->isReachable(self()->getRepresentiveNodeIndex(), road->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED))
				{
					continue;
				}
				if (road->getLastCycleUpdatedBySense() - world->getTime() < min(5, world->getTime()))
				{
					continue;
				}

				double distance = self()->getPos().distFromPoint(road->getPos());
				if (distance < nearestDistance)
				{
					nearestDistance = distance;
					nearestRoad = road;
				}
			}
		}
	}

	if (nearestRoad != nullptr)
	{
		LOG(Main, 1) << "I'm going to check very suspicious building" << endl;
		command->moveToMotionless(nearestRoad->motionlessIndex);
		return true;
	}

	// Extinguish
	for (u_int i = 0; i < buildingsInCluster.size(); i++)
	{
		if (!buildingsInCluster[i]->getEstimatedData()->isFlammable())
		{
			buildingsInCluster.erase(buildingsInCluster.begin() + i);
			i--;
		}
	}

	if (buildingsInCluster.empty())
	{
		LOG(Main, 1) << "No flammable building for extinguish" << endl;
		return false;
	}

	setValue(buildingsInCluster, false);

	if (moveAndExtinguish(buildingsInCluster))
	{
		LOG(Main, 1) << "I'm going to extinguish very suspicious building" << endl;
		return true;
	}

	return false;
}

bool FireBrigadeAgent::isVerySuspiciousBuilding(Building* building)
{
	if (building->getWorthlessness())
	{
		return false;
	}

	if (building->getEstimatedData()->getFieryness() > 0 && building->getEstimatedData()->getFieryness() != 4)
	{
		return false;
	}

	if (building->getEstimatedData()->getTemperature() < 1)
	{
		return false;
	}

	for (InSightBuilding* isb : building->getInSightBuildings())
	{
		Building* neighbor = (Building*) world->buildings[isb->buildingIndex];
		if (neighbor->getEstimatedData()->getFieryness() > 0 && neighbor->getEstimatedData()->getFieryness() != 4)
		{
			return false;
		}
	}

	return true;
}

bool FireBrigadeAgent::isStuckInArea()
{
	int count = 0;
	for (Road* road : world->roads)
	{
		if (worldGraph->isReachable(self()->getRepresentiveNodeIndex(), road->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED))
		{
			count++;
		}
	}
	LOG(Main, 1) << "[isStuckInArea] Reachable roads: " << count << endl;

	if (count >= (int) min((double) STUCK_IN_AREA, (double) world->roads.size()))
	{
		return false;
	}
	return true;
}

bool FireBrigadeAgent::isReachableToFireBlock(FireBrigade* fireBrigade, FireBlock* fireBlock)
{
	for (Building* building : fireBlock->getAroundBuildings())
	{
		if (canPosition(fireBrigade, building))
		{
			return true;
		}
	}

	return false;
}

bool FireBrigadeAgent::doesNeedToExtinguish(Building* building, bool justFiery)
{
	if (!building->getEstimatedData()->isFlammable())
	{
		return false;
	}

	if (building->getEstimatedData()->getFieryness() == 8 || building->getFieryness() == 8)
	{
		return false;
	}

	if (building->getWorthlessness())
	{
		return false;
	}

	if (justFiery && !building->getEstimatedData()->hasFire())
	{
		return false;
	}

	if (building->getEstimatedData()->getFieryness() == 3 && !building->isHardToExtinguish())
	{
		return false;
	}

	if (!justFiery && building->getEstimatedData()->getTemperature() < BLOCK_TEMPERATURE_LIMIT)
	{
		return false;
	}

	if (building->getEstimatedData()->getFieryness() == 3)
	{
		bool flag = false;
		for (InSightBuilding* isb : building->getInSightBuildings())
		{
			Building* neighbor = world->buildings[isb->buildingIndex];
			if (neighbor->getEstimatedData()->getFieryness() == 0 || neighbor->getEstimatedData()->getFieryness() == 4)
			{
				flag = true;
				break;
			}
		}
		if (!flag)
		{
			return false;
		}
	}

	return true;
}

bool FireBrigadeAgent::dontShas1()
{
	LOG(Main, 1) << "Don't Shas 1" << endl;

	if (self()->getMotionlessObject()->isBuilding())
	{
		if (self()->myLastCommand == CT_MOVE && !self()->dostItMoveEver())
		{
			vector<int> path;
			path.push_back(self()->getMotionlessObject()->getId());

			int randomNeighbor = world->getTime() % self()->getMotionlessObject()->getNeighbours().size();
			path.push_back(world->motionlessObjects[self()->getMotionlessObject()->getNeighbours()[randomNeighbor]]->getId());

			command->move(path, -1, -1);
			return true;
		}
	}

	return false;
}

bool FireBrigadeAgent::dontShas2()
{
	LOG(Main, 1) << "Don't Shas 2, " << self()->getRepresentiveNodeIndex() << endl;

	if (self()->getRepresentiveNodeIndex() != -1)
	{
		return false;
	}

	if (stuckTime < 4 && !self()->getLastMoveCommand().empty())
	{
		vector<int> lastMove = self()->getLastMoveCommand();
		while (!lastMove.empty() && (u_int) lastMove[0] != self()->getMotionlessObject()->getId())
		{
			lastMove.erase(lastMove.begin());
		}
		if (lastMove.size() >= 2)
		{
			lastMove.erase(lastMove.begin());
			command->move(lastMove);
			return true;
		}
	}

	vector<int> path;
	int randomNeighbor = world->getTime() % self()->getMotionlessObject()->getNeighbours().size();
	path.push_back(self()->getMotionlessObject()->getId());
	path.push_back(world->motionlessObjects[self()->getMotionlessObject()->getNeighbours()[randomNeighbor]]->getId());
	command->move(path, -1, -1);
	return true;
}

bool FireBrigadeAgent::dontShas3()
{
	return false;
	LOG(Main, 1) << "Don't Shas 3, " << self()->getRepresentiveNodeIndex() << endl;

	if (self()->dostItMoveEver() && self()->myLastCommand == CT_MOVE)
	{
		vector<int> lastMove = self()->getLastMoveCommand();
		while (!lastMove.empty() && (u_int) lastMove[0] != self()->getMotionlessObject()->getId())
		{
			lastMove.erase(lastMove.begin());
		}
		if (lastMove.size() >= 2)
		{
			lastMove.erase(lastMove.begin());
			command->move(lastMove);
			return true;
		}
	}

	vector<int> path;
	int randomNeighbor = world->getTime() % self()->getMotionlessObject()->getNeighbours().size();
	path.push_back(self()->getMotionlessObject()->getId());
	path.push_back(world->motionlessObjects[self()->getMotionlessObject()->getNeighbours()[randomNeighbor]]->getId());
	command->move(path, -1, -1);
	return true;
}

bool FireBrigadeAgent::checkDamage()
{
	bool isDamaged = false;
	if (self()->getDamage() > 0)
	{
		isDamaged = true;
	}

	int hSize = self()->getHpHistory().size() - 1;
	if (hSize > 2)
	{
		if (self()->getHpHistory()[hSize].hp != self()->getHpHistory()[hSize - 1].hp)
		{
			isDamaged = true;
		}
		else if (self()->getHpHistory()[hSize - 1].hp != self()->getHpHistory()[hSize - 2].hp)
		{
			isDamaged = true;
		}
		else if (self()->getHpHistory()[hSize - 2].hp != self()->getHpHistory()[hSize - 3].hp)
		{
			isDamaged = true;
		}
	}

	if (isDamaged)
	{
		if (self()->getMotionlessObject()->isRefuge())
		{
			command->rest();
			return true;
		}
		else if (moveToRefuge())
		{
			return true;
		}
	}

	return false;
}

bool FireBrigadeAgent::moveToRefuge()
{
	if (self()->getMotionlessObject()->isRefuge())
	{
		command->rest();
		return true;
	}

	if (world->refuges.empty())
	{
		LOG(Main, 1) << "[moveToRefuge] There's no refuge in map" << endl;
		return false;
	}

	Refuge* nearestRefuge = nullptr;
	int nearestDist = MAX_INT, dist;

	for (Refuge* refuge : world->refuges)
	{
		dist = worldGraph->getDistance(self()->getRepresentiveNodeIndex(), refuge->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED);
		if (dist != INF && nearestDist > dist)
		{
			nearestDist = dist;
			nearestRefuge = refuge;
		}
	}

	if (nearestRefuge != nullptr)
	{
		command->moveToMotionless(nearestRefuge->motionlessIndex);
		return true;
	}

	return false;
}

bool FireBrigadeAgent::moveToHydrant()
{
	if (self()->getMotionlessObject()->isHydrant())
	{
		command->rest();
		return true;
	}

	if (world->hydrants.empty())
	{
		LOG(Main, 1) << "[moveToHydrant] There's no hydrant in map" << endl;
		return false;
	}

	Hydrant* nearestHydrant = nullptr;
	int nearestDist = MAX_INT, dist;

	for (Hydrant* hydrant : world->hydrants)
	{
		dist = worldGraph->getDistance(self()->getRepresentiveNodeIndex(), hydrant->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED);
		if (dist != INF && nearestDist > dist)
		{
			nearestDist = dist;
			nearestHydrant = hydrant;
		}
	}

	if (nearestHydrant != nullptr)
	{
		command->moveToMotionless(nearestHydrant->motionlessIndex);
		return true;
	}

	return false;
}

bool FireBrigadeAgent::fillWaterTank()
{
	if (self()->getWaterQuantity() == 0)
	{
		fillWater = true;
	}
	if (self()->getWaterQuantity() == maxFireTank)
	{
		fillWater = false;
	}

	if (fillWater)
	{
		if (getAvailableRefuges().empty())
		{
			LOG(Main, 1) << "I need water, there's no available refuge!!" << endl;

			if (getAvailableHydrants().empty())
			{
				LOG(Main, 1) << "I need water, there's no available hydrant!!" << endl;
			}
			else
			{
				if (moveToHydrant())
				{
					return true;
				}
			}
		}
		else
		{
			if (moveToRefuge())
			{
				return true;
			}
		}
	}

	return false;
}

bool FireBrigadeAgent::fillWaterTank2()
{
	if (self()->getWaterQuantity() < maxFireTank * 3 / 4 && shasingTime >= 15)
	{
		fillWaterForFun = true;
	}
	if (self()->getWaterQuantity() == maxFireTank)
	{
		fillWaterForFun = false;
	}

	if (fillWaterForFun)
	{
		if (getAvailableRefuges().empty())
		{
			LOG(Main, 1) << "I need water, there's no available refuge!!" << endl;
			return false;
		}

		if (self()->getMotionlessObject()->isRefuge())
		{
			command->rest();
			return true;
		}
		else
		{
			if (moveToRefuge())
			{
				return true;
			}
		}
	}

	return false;
}

bool FireBrigadeAgent::isHardToExtinguish(Building* building)
{
	if (building->getAreaTotal() * building->getEstimatedData()->getTemperature() < hardToExtinguishLimit)
	{
		return false;
	}
	return true;
}

bool FireBrigadeAgent::isWorthlessSlice(Slice* slice)
{
	u_int worthlessBuildings = 0;

	for (Building* building : slice->getBuildings())
	{
		if (building->getEstimatedData()->getFieryness() == 0)
		{
			continue;
		}

		if (!doesNeedToExtinguish(building, true))
		{
			worthlessBuildings++;
		}
	}

	if (worthlessBuildings >= slice->getBuildings().size() * WORTHLESS_SLICE_MAGIC_NUMBER)
	{
		return true;
	}

	return false;
}

bool FireBrigadeAgent::scanAroundHereForExtinguishingCloseFire()
{
	int maxCycleForAttentionFromOthers = 6;

	vector<Building*> aroundBuildingsInFire;

	for (int i = 0; i < (int) self()->getMotionlessObject()->closerThan50B.size(); i++)
	{
		Building* thisBuilding = (Building*) self()->getMotionlessObject()->closerThan50B[i];

		if (!thisBuilding->getEstimatedData()->isFlammable())
		{
			continue;
		}
		if (self()->getPos().distFromPoint(thisBuilding->getPos()) > maxExtinguishDistance)
		{
			continue;
		}
		if (world->getTime() - thisBuilding->getLastCycleUpdated() > maxCycleForAttentionFromOthers)
		{
			continue;
		}
		if (thisBuilding->hasFire() && !thisBuilding->isHardToExtinguish())
		{
			aroundBuildingsInFire.push_back(thisBuilding);
		}
	}

	if (aroundBuildingsInFire.empty())
	{
		return false;
	}

	Building* bestBuilding = getBestBuilding(aroundBuildingsInFire, false);
	command->extinguish(*bestBuilding, getNeededWaterAmount(bestBuilding));
	return true;
}

bool FireBrigadeAgent::takeAWalkAround(Building* building)
{
	int maxUpdate = numeric_limits<int>::min(), tempUpdate = -1;
	int bestMotionlessIndex = -1;

	for (Road* road : building->canSee)
	{
		if (!worldGraph->isReachable(self()->getRepresentiveNodeIndex(), road->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED))
		{
			continue;
		}

		tempUpdate = world->getTime() - road->getLastCycleUpdatedBySense();
		if (tempUpdate > maxUpdate)
		{
			maxUpdate = tempUpdate;
			bestMotionlessIndex = road->motionlessIndex;
		}
	}

	if (bestMotionlessIndex == -1)
	{
		return false;
	}

	command->moveToMotionless(world->motionlessObjects[bestMotionlessIndex]->motionlessIndex);
	return true;
}

bool FireBrigadeAgent::takeCareOfCivilian()
{
	vector<Civilian*> nearCivilians;
	for (Civilian* civilian : world->civilians)
	{
		if (civilian->isAvailable)
		{
			int dist = worldGraph->getDistance(self()->getRepresentiveNodeIndex(), civilian->getRepresentiveNodeIndex(),
					GM_DEFAULT_NOT_BLOCKED);
			if (dist / AVERAGE_AGENT_SPEED <= 4)
			{
				nearCivilians.push_back(civilian);
			}
		}
	}

	if (nearCivilians.empty())
	{
		return false;
	}

	vector<Civilian*> niceCivilians;
	for (Civilian* civilian : nearCivilians)
	{
		for (AmbulanceTeam* agent : world->ambulanceTeams)
		{
			if (agent->isAvailable && !agent->getMotionlessObject()->isRefuge() && agent->getBuriedness() == 0
					&& civilian->getMotionlessObject()->getId() == agent->getMotionlessObject()->getId())
			{
				niceCivilians.push_back(civilian);
				break;
			}
		}
	}

	if (niceCivilians.empty())
	{
		return false;
	}

	Civilian* target = nullptr;
	for (Civilian* civilian : niceCivilians)
	{
		int minDistance = MAX_INT, tempDistance;
		FireBrigade* bestAgent = nullptr;

		for (FireBrigade* fb : ableAgents)
		{
			if (fb->hasJob)
			{
				continue;
			}

			tempDistance = worldGraph->getDistance(self()->getRepresentiveNodeIndex(),
					civilian->getMotionlessObject()->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED);
			if (tempDistance == INF)
			{
				continue;
			}

			if (minDistance > tempDistance)
			{
				minDistance = tempDistance;
				bestAgent = fb;
			}
		}

		if (bestAgent != nullptr)
		{
			if (bestAgent->getId() == self()->getId())
			{
				target = civilian;
				break;
			}
			else
			{
				bestAgent->hasJob = true;
			}
		}
	}

	if (target == nullptr)
	{
		return false;
	}

	vector<Building*> aroundOfTarget;
	for (MotionlessObject* mo : target->getMotionlessObject()->closerThan50B)
	{
		Building* thisBuilding = (Building*) mo;
		if (world->getTime() - thisBuilding->lastTimeSeen >= MIN_CYCLE_FOR_CIVILIAN_UPDATE * 1.5
				&& worldGraph->isReachable(self()->getRepresentiveNodeIndex(), thisBuilding->getRepresentiveNodeIndex(),
						GM_DEFAULT_NOT_BLOCKED))
		{
			aroundOfTarget.push_back(thisBuilding);
		}
	}

	if (aroundOfTarget.empty())
	{
		return false;
	}

	Building* bestBuilding = nullptr;
	int maxCycleForNotUpdating = -1;
	for (Building* building : aroundOfTarget)
	{
		if (world->getTime() - building->lastTimeSeen > maxCycleForNotUpdating)
		{
			maxCycleForNotUpdating = world->getTime() - building->lastTimeSeen;
			bestBuilding = building;
		}
	}

	command->moveToMotionless(bestBuilding->motionlessIndex);
	return true;
}

vector<Refuge*> FireBrigadeAgent::getAvailableRefuges()
{
	vector<Refuge*> availableRefuges;

	for (Refuge* refuge : world->refuges)
	{
		if (worldGraph->isReachable(self()->getRepresentiveNodeIndex(), refuge->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED))
		{
			availableRefuges.push_back(refuge);
		}
	}

	return availableRefuges;
}

vector<Hydrant*> FireBrigadeAgent::getAvailableHydrants()
{
	vector<Hydrant*> availableHydrant;

	for (Hydrant* hydrant : world->hydrants)
	{
		if (worldGraph->isReachable(self()->getRepresentiveNodeIndex(), hydrant->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED))
		{
			availableHydrant.push_back(hydrant);
		}
	}

	return availableHydrant;
}

void FireBrigadeAgent::updateSlices()
{
	if (assignStrategy == FBAS_PIZZA)
	{
		vector<Slice*> validSlices;
		vector<Slice*> worthlessSlices;
		for (Slice* slice : pizzaSlices)
		{
			if (isWorthlessSlice(slice))
			{
				worthlessSlices.push_back(slice);
			}
			else
			{
				validSlices.push_back(slice);
			}
		}
		LOG(Main, 1) << "ValidSlices: " << validSlices.size() << endl;
		LOG(Main, 1) << "WorthlessSlices: " << worthlessSlices.size() << endl;

		if (validSlices.empty())
		{
			LOG(Main, 1) << "[changeSliceIfNeeded] There isn't any valid slice!!! set strategy to kilooiy mode" << endl;
			assignStrategy = FBAS_KILOOIY;
			for (Slice* slice : pizzaSlices)
			{
				slice->getAssignedAgents().clear();
			}
			for (FireBrigade* fb : world->fireBrigades)
			{
				fb->pizzaSlice = -1;
			}
			return;
		}

		if (validSlices.size() == pizzaSlices.size())
		{
			return;
		}

		vector<FireBrigade*> freedAgents;
		for (Slice* slice : worthlessSlices)
		{
			for (Platoon* platoon : slice->getAssignedAgents())
			{
				freedAgents.push_back((FireBrigade*) platoon);
				((FireBrigade*) platoon)->pizzaSlice = -1;
			}
			slice->getAssignedAgents().clear();
		}

		// Find best slice
		sort(validSlices.begin(), validSlices.end(), [](Slice* s1, Slice* s2)
		{
			return s1->getTotalSliceBuildingsArea() > s2->getTotalSliceBuildingsArea();
		});

		for (u_int i = 0; i < freedAgents.size(); i++)
		{
			validSlices[i % validSlices.size()]->assignAgentToSlice(freedAgents[i]);
		}
	}
	LOG(Main, 1) << "Slice: " << self()->pizzaSlice << endl;
}

void FireBrigadeAgent::prepareForPizzaStrategy()
{
	int availableAgents = 0;
	for (FireBrigade* fb : world->fireBrigades)
	{
		if (!fb->getMotionlessObject()->isBuilding() || fb->getMotionlessObject()->isRefuge())
		{
			availableAgents++;
		}
	}

	int slices = -1;
	if (radar->getRadarMode() == RM_NO_COMMUNICATION || radar->getRadarMode() == RM_LOW)
	{
		slices = 1;
	}
	else if (!world->isMapLarge() && availableAgents >= 10)
	{
		slices = 3;
	}
	else if (world->isMapLarge() && availableAgents >= 10)
	{
		slices = 5;
	}
	else
	{
		slices = 1;
	}

	PizzaClustering<Building> pc(world->buildings, slices);
	pc.calculate();

	for (Cluster* cluster : pc.getClusters())
	{
		Slice* slice = new Slice;
		slice->setIndex(pizzaSlices.size());
		for (MotionlessObject* mo : cluster->getMembers())
		{
			slice->addBuilding((Building*) mo);
		}
		pizzaSlices.push_back(slice);
	}

	world->pizzaSlices = pizzaSlices;

	addCenterToSlices();

	assignToPizzaSlices();

	LOG(Main, 1) << "Slices: " << pizzaSlices.size() << endl;
	LOG(Main, 1) << "My Slice: " << self()->pizzaSlice << endl;
}

void FireBrigadeAgent::addCenterToSlices()
{
	if (pizzaSlices.size() == 1)
	{
		return;
	}

	Point center(0, 0);
	for (Building* building : world->buildings)
	{
		center.setX(building->getPos().getX() + center.getX());
		center.setY(building->getPos().getY() + center.getY());
	}
	center.setX(center.getX() / world->buildings.size());
	center.setY(center.getY() / world->buildings.size());

	for (Building* building : world->buildings)
	{
		if (building->getPos().distFromPoint(center) <= PIZZA_CENTER_RADIUS)
		{
			for (Slice* slice : pizzaSlices)
			{
				if (find(slice->getBuildings().begin(), slice->getBuildings().end(), building) == slice->getBuildings().end())
				{
					slice->addBuilding(building);
				}
			}
			building->slice = numeric_limits<int>::infinity();
		}
	}
}

void FireBrigadeAgent::assignToPizzaSlices()
{
//	for (FireBrigade* fireBrigade : world->fireBrigades)
//	{
//		fireBrigade->pizzaSlice = -1;
//	}
//
//	for (FireBrigade* fireBrigade : world->fireBrigades)
//	{
//		if (fireBrigade->pizzaSlice == -1)
//		{
//			continue;
//		}
//
//		double minDistance = numeric_limits<double>::max();
//		Slice* nearestSlice = nullptr;
//		for (Slice* slice : pizzaSlices)
//		{
//			double distance = slice->getAirDistanceToEntity(fireBrigade->getPos());
//			if (distance < minDistance)
//			{
//				nearestSlice = slice;
//				minDistance = distance;
//			}
//		}
//
//		if (nearestSlice != nullptr)
//		{
//			nearestSlice->assignAgentToSlice(fireBrigade);
//		}
//	}

	vector<vector<int>> costMatrix;
	costMatrix.resize(world->fireBrigades.size());

	for (u_int y = 0; y < costMatrix.size(); y++)
	{
		costMatrix[y].resize(costMatrix.size());
		for (u_int x = 0; x < costMatrix[y].size(); x++)
		{
			costMatrix[y][x % pizzaSlices.size()] = -pizzaSlices[x % pizzaSlices.size()]->getGroundDistanceToEntity(
					world->fireBrigades[y]->getMotionlessObject(), worldGraph, GM_DEFAULT_NOT_BLOCKED);
		}
	}

	HungarianAssignment hungarianAss(costMatrix);

	vector<vector<bool>> assignment = hungarianAss.hungarian();
	for (u_int y = 0; y < assignment.size(); y++)
	{
		for (u_int x = 0; x < assignment[y].size(); x++)
		{
			if (assignment[y][x])
			{
				pizzaSlices[x % pizzaSlices.size()]->assignAgentToSlice(world->fireBrigades[y]);
			}
		}
	}
}

void FireBrigadeAgent::setIgnoredBuildings()
{
	vector<int> ignoredBuildings;

	if (world->getMapName() == MN_BERLIN)
	{
		ignoredBuildings.resize(27);

		ignoredBuildings[0] = 50798;
		ignoredBuildings[1] = 48896;
		ignoredBuildings[2] = 52432;
		ignoredBuildings[3] = 57657;
		ignoredBuildings[4] = 57660;
		ignoredBuildings[5] = 57659;
		ignoredBuildings[6] = 30864;
		ignoredBuildings[7] = 33759;
		ignoredBuildings[8] = 38303;
		ignoredBuildings[9] = 57666;
		ignoredBuildings[10] = 57665;
		ignoredBuildings[11] = 54099;
		ignoredBuildings[12] = 23997;
		ignoredBuildings[13] = 36063;
		ignoredBuildings[14] = 23976;
		ignoredBuildings[15] = 44217;
		ignoredBuildings[16] = 54098;
		ignoredBuildings[17] = 49704;
		ignoredBuildings[18] = 27909;
		ignoredBuildings[19] = 38735;
		ignoredBuildings[20] = 27502;
		ignoredBuildings[21] = 54096;
		ignoredBuildings[22] = 57663;
		ignoredBuildings[23] = 57662;
		ignoredBuildings[24] = 37993;
		ignoredBuildings[25] = 35389;
		ignoredBuildings[26] = 36875;
	}
	else if (world->getMapName() == MN_ISTANBUL)
	{
		ignoredBuildings.resize(2);

		ignoredBuildings[0] = 35709;
		ignoredBuildings[1] = 49717;
	}
	else if (world->getMapName() == MN_PARIS)
	{
		ignoredBuildings.resize(2);

		ignoredBuildings[0] = 38043;
		ignoredBuildings[1] = 47094;
	}

	for (Building* building : world->buildings)
	{
		if (find(ignoredBuildings.begin(), ignoredBuildings.end(), building->getId()) != ignoredBuildings.end())
		{
			building->setWorthlessness(true);
		}
	}
}

void FireBrigadeAgent::setSegmentBuildingsCount()
{
	switch (world->getMapName())
	{
		case MN_KOBE:
		case MN_VC:
		case MN_PARIS:
			segmentBuildingsCount = 45;
			break;
		case MN_BERLIN:
		case MN_ISTANBUL:
		case MN_EINDHOVEN:
		case MN_MEXICO:
		case MN_TEHRAN:
			segmentBuildingsCount = 40;
			break;
		case MN_TEST:
			segmentBuildingsCount = 40;
			break;
		default:
			segmentBuildingsCount = 45;
			break;
	}
}

void FireBrigadeAgent::setAssignStrategy()
{
//	int availableAgents = 0;
//	for (FireBrigade* fb : world->fireBrigades)
//	{
//		if (!fb->getMotionlessObject()->isBuilding() || fb->getMotionlessObject()->isRefuge())
//		{
//			availableAgents++;
//		}
//	}

	assignStrategy = FBAS_PIZZA;
	LOG(Main, 1) << "Pizza" << endl;
}

void FireBrigadeAgent::resetFireBrigadesInformation()
{
	ableAgents.clear();
	for (FireBrigade* fireBrigade : world->fireBrigades)
	{
		fireBrigade->targetBuildings.clear();
		fireBrigade->targetFireBlock = nullptr;
		fireBrigade->hasJob = false;
		fireBrigade->value = 0;

		if (fireBrigade->getBuriedness() == 0)
		{
			ableAgents.push_back(fireBrigade);
		}
	}
}

void FireBrigadeAgent::setCityCenter()
{
	long long gX = 0, gY = 0;

	for (int i = 0; i < (int) world->buildings.size(); i++)
	{
		gX = gX + world->buildings[i]->getPos().getX();
		gY = gY + world->buildings[i]->getPos().getY();
	}

	gX = gX / world->buildings.size();
	gY = gY / world->buildings.size();
	Point g(gX, gY);

	int minDistance = numeric_limits<int>::max(), tempDistance = -1, bestIndex = 0;
	for (u_int i = 0; i < world->buildings.size(); i++)
	{
		tempDistance = g.distFromPoint(world->buildings[i]->getPos());
		if (minDistance > tempDistance)
		{
			minDistance = tempDistance;
			bestIndex = i;
		}
	}

	cityCenter = world->buildings[bestIndex];

	LOG(Main , 1) << "City Center is: " << cityCenter->getId() << endl;
}

void FireBrigadeAgent::logMapInformation()
{
	LOG(Main, 1) << "Map: " << world->getMapName() << " [";
	if (world->isMapLarge())
	{
		LOG(Main, 1) << "Large";
	}
	else
	{
		LOG(Main, 1) << "Small";
	}
	LOG(Main, 1) << "]" << endl;
}

void FireBrigadeAgent::setBuildingsCells()
{
	for (Building* building : world->buildings)
	{
		fireEstimator->simulatedWorld->setBuildingCells(building);
	}
}

void FireBrigadeAgent::setFireEstimatorNecessity()
{
	LOG(Main, 1) << "Radar: " << radar->getRadarMode() << endl;
#if USE_FIRE_SIMULATOR
// TODO XXX Check Noise
	if (radar->getRadarMode() == RM_HIGH || radar->getRadarMode() == RM_MEDIUM)
	{
		useFireEstimator = true;
	}
	else
	{
		useFireEstimator = false;
	}
#else
	useFireSimulator = false;
#endif
	LOG(Main, 1) << "FE: " << boolalpha << useFireEstimator << endl;
}

void FireBrigadeAgent::setAverageArea()
{
	unsigned long long totalArea = 0;
	for (Building* building : world->buildings)
	{
		totalArea += building->getAreaTotal();
	}
	averageArea = totalArea / world->buildings.size();
	LOG(Main, 1) << "Average Area: " << averageArea << endl;
}

void FireBrigadeAgent::setHTELimit()
{
	hardToExtinguishLimit = averageArea * 600;
	if (world->getMapName() == MN_KOBE || world->getMapName() == MN_VC)
	{
		hardToExtinguishLimit *= 2;
	}
	LOG(Main, 1) << "HTE Limit: " << hardToExtinguishLimit << endl;
}

