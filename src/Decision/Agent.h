#ifndef AGENT_H_
#define AGENT_H_

#include "../WorldModel/WorldModel.h"
#include "../Connection/OnlineConnection.h"
#include "../Utilities/Viewer/OptimusViewer.h"
#include "../Utilities/Logger.h"
#include "../Utilities/Config.h"
#include "../Utilities/Debugger.h"
#include "../Connection/Message.h"
#include "../WorldGraph/WorldGraph.h"
#include "../Radar/HighBWCommunicationRadar.h"
#include "../Radar/MediumBWCommunicationRadar.h"
#include "../Radar/LowBWCommunicationRadar.h"
#include "../Connection/OfflineConnection.h"
#include "../Utilities/Basics.h"
#include "Command.h"
#include <tr1/memory>
#include <sys/stat.h>
#include <algorithm>
#include <fstream>
#include <iostream>
#include <boost/thread.hpp>
#include <chrono>
#include <thread>

#define OPEN_VIEWER 0
#define IS_CHALLENGE 0
#define GAS_STATION_RADIUS 50000

class AbstractCommunicationRadar;

using namespace std;
using namespace basics;
using namespace Types;

class Agent
{
public:
	Agent();
	virtual ~Agent();
	void run(double coeff);
	int id;
	Types::EntityType type;
	void initData();
	void setConnectionParameters(std::string ip, int port);
	int initConnection(Types::EntityType, int);

	bool isPrecomputeAgent;

protected:

	virtual void actBeforeSense();
	virtual void actBeforeRadar();
	virtual void act();
	virtual void globalPrecomputation();
	virtual void precomputation();
	virtual void exportPrecomputationData(std::fstream &);
	virtual void importPrecomputationData();
	virtual void noCommAct();
	virtual RCRObject* self();

	void setRadar();
	void exportWorldGraphInit(std::fstream&);
	void exportWorldGraphOthers(std::fstream&);
	void exportWorldGraphUpdate(std::fstream&);
	void exportBFSAndDijkstra(bool, bool, bool, bool, std::fstream&);
	void exportCloserThan(std::fstream&);
	void exportNotRelatedEdges(std::fstream&);
	void exportJustACloserThan(vector<MotionlessObject*>, Types::PrecomputationDataTag, std::fstream&);

	std::string parsePrecomputationDataByTag(std::string, std::fstream &);
	std::string getPrecomputationFileAddress(Types::EntityType);
	std::string getDistanceFindingConfigAddress(Types::EntityType);

	Logger logger;
	WorldModel *world;
	WorldGraph *worldGraph;
	Config config;
	AbstractConnection *connection;
	Command *command;
	AbstractCommunicationRadar* radar;

	int agentThinkTime;
	int ignoreCommandsUntil;
	int startupConnectTime;
	int damagePerception;
	int hpPerception;

	double optimizedCoeff;

private:
	void initializeLogger(Types::EntityType et, int id);
	void initializeConfig();
	void sendAKAcknowledge();
	bool shouldOpenViewer();
#if IS_STATIC
	viewer::OptimusViewer* optimusViewer;
	boost::thread* viewerThread;
#endif
	std::string getOfflineFileDir(Types::EntityType, int);
	std::ofstream offline;
	bool doOffline;
	vector<int> fbViewer;
	vector<int> atViewer;
	vector<int> pfViewer;
};

#endif /* AGENT_H_ */
