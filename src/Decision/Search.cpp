#include <iostream>
#include <algorithm>
#include "Search.h"

#define LOGLEVEL 1
#define SEE_IT 10
#define SCALE 10000
#define VISION_RADIUS 30000

using namespace std;
using namespace Types;
using namespace Geometry;

Search::Search(WorldModel* tworldModel, WorldGraph* tworldGraph, SearchMode tmode)
{
	LOG(Main, 1) << "Search Constructor!" << endl;
	this->world = tworldModel;
	this->worldGraph = tworldGraph;
	this->mode = tmode;
	srand(RANDOM_SEED);
}

//----------------------------------------------------------Initialization & creating Global Path-------------------------------------------------------------//

void Search::init(bool createPath)
{
	air = world->teamConfig["Search"]["TableDistance"]["Air"].asBool();
	if (createPath) //()false // createPath
	{
		if (air)
		{
			calculateDistanceTables();
		}
		else
		{
			FloydWarshal();
			//			creatGraphDistanceTable();
		}

		createNearestPath();

		LOG(Main, 1) << "SimulatedAnnealing" << endl;

		simulatedAnnealing();
	}
	else
	{
		searchBuildingList = world->searchBuildingList;
	}

	if (world->teamConfig["Search"]["SearchClustering"]["Kmeans"].asBool())
	{
		setKmeansClusters();
	}
	else
	{
		creatPathClusters();
	}

	//	randomAssignment();

	LOG(Main, 1) << "Search Path :::>  " << searchBuildingList.size() << endl;
}

void Search::createNearestPath()
{
	LOG(Main, 1) << "createNearestPath()" << endl;

	if (air)
	{
		LOG(Main, 1) << "Air Calculation" << endl;
	}
	else
	{
		LOG(Main, 1) << "Graph Calculation" << endl;
	}

	vector<Building*> nearestNeighbors(world->buildings.size(), nullptr);
	for (Building* b1 : world->buildings)
	{
		int dist = MAX_INT, localDist;

		if (nearestNeighbors[b1->buildingIndex] != nullptr)
		{
			continue;
		}

		for (Building* b2 : world->buildings)
		{
			if (air)
			{
				localDist = airDistanceTable[b1->buildingIndex][b2->buildingIndex];
			}
			else
			{
				localDist = graphDistanceTable[b1->motionlessIndex][b2->motionlessIndex];
			}

			if (localDist < dist)
			{
				nearestNeighbors[b1->buildingIndex] = b2;
				nearestNeighbors[b2->buildingIndex] = b1;
				dist = localDist;
			}
		}
	}

	int min = MAX_INT;
	//	for (Building* first : world->buildings)
	Building* first = world->buildings[0];
	{
		vector<bool> mark(world->buildings.size(), false);
		vector<Building*> list;
		list.push_back(first);
		int dist = 0;
		int localDist;
		while (list.size() != world->buildings.size())
		{
			localDist = MAX_INT;
			Building* closest = nullptr;
			first = list[list.size() - 1];

			if (!mark[nearestNeighbors[first->buildingIndex]->buildingIndex]) //Using NearestNeighbors Table !
			{
				closest = nearestNeighbors[first->buildingIndex];

				if (air)
				{
					localDist = airDistanceTable[first->buildingIndex][closest->buildingIndex];
				}
				else
				{
					localDist = graphDistanceTable[first->motionlessIndex][closest->motionlessIndex];
				}
			}
			else
			{
				for (Building* build : world->buildings)
				{
					if (!mark[build->buildingIndex])
					{
						int distance;

						if (first == build)
						{
							distance = 0;
						}
						else
						{
							if (air)
							{
								distance = airDistanceTable[first->buildingIndex][build->buildingIndex];
							}
							else
							{
								distance = graphDistanceTable[first->motionlessIndex][build->motionlessIndex];
							}
						}

						if (distance < localDist)
						{
							closest = build;
							localDist = distance;
						}
					}
				}
			}

			dist += localDist;
			list.push_back(closest);
			mark[closest->buildingIndex] = true;
		}

		if (dist < min)
		{
			searchBuildingList = list;
			min = dist;
		}
	}
	LOG(Main, 1) << "SourceBID: " << searchBuildingList[0]->getId() << endl;
}

void Search::creatGraphDistanceTable()
{
	//	LOG(Main, 1) << "creatGraphDistanceTable()" << endl;
	//
	//	graphDistanceTable.resize(world->buildings.size(), vector<int>(world->buildings.size(), -1));
	//
	//	Building* b1, *b2;
	//	for (u_int i = 0; i < world->buildings.size(); ++i)
	//	{
	//		b1 = world->buildings[i];
	//		for (u_int j = i + 1; j < world->buildings.size() - 1; ++j)
	//		{
	//			b2 = world->buildings[j];
	//
	//			graphDistanceTable[b1->buildingIndex][b2->buildingIndex] = worldGraph->getOnlineDistance(b1->getWithoutBlockadeRepresentiveNodeIndex(),
	//					b2->getWithoutBlockadeRepresentiveNodeIndex(), GM_WITHOUT_BLOCKADE);
	//			graphDistanceTable[b1->buildingIndex][b2->buildingIndex] = graphDistanceTable[b2->buildingIndex][b1->buildingIndex];
	//
	//		}
	//	}
	//
	//	LOG(Main, 1) << "creatGraphDistanceTable() END" << endl;
}

void Search::FloydWarshal()
{
	LOG(Main, 1) << "starting FloydWarshal()" << endl;
	//	graphDistanceTable.resize(worldGraph->getNodes().size(), vector<int>(worldGraph->getNodes().size(), MAX_INT));
	graphDistanceTable.resize(world->motionlessObjects.size(), vector<int>(world->motionlessObjects.size(), MAX_INT));
	LOG(Main, 1) << "Table Size: " << graphDistanceTable.size() << "x" << graphDistanceTable.size() << " : " << graphDistanceTable.size() * graphDistanceTable.size() << endl;
	LOG(Main, 1) << "Table Size: " << worldGraph->getNodes().size() << "x" << worldGraph->getNodes().size() << " : "
			<< worldGraph->getNodes().size() * worldGraph->getNodes().size() << endl;
	int tmp = -1;

	for (u_int i = 0; i < world->motionlessObjects.size(); i++)
	{
		graphDistanceTable[i][i] = 0;
		for (u_int j = 0; j < world->motionlessObjects[i]->getNeighbours().size(); j++)
		{
			graphDistanceTable[i][world->motionlessObjects[i]->getNeighbours()[j]] = world->motionlessObjects[i]->getPos().distFromPoint(
					world->motionlessObjects[world->motionlessObjects[i]->getNeighbours()[j]]->getPos());
		}
	}

	LOG(Main, 1) << "Tables Has created !!!" << endl;

	for (u_int k = 0; k < graphDistanceTable.size(); k++)
	{

		for (u_int i = 0; i < graphDistanceTable.size(); i++)
		{

			if (graphDistanceTable[i][k] == MAX_INT)
			{
				continue;
			}

			for (u_int j = 0; j < graphDistanceTable.size(); j++)
			{
				if (graphDistanceTable[k][j] == MAX_INT)
				{
					continue;
				}
				//				if (graphDistanceTable[i][j] > (tmp = graphDistanceTable[i][k] + graphDistanceTable[k][j]))
				//				{
				//					graphDistanceTable[i][j] = tmp;
				//				}
				if (graphDistanceTable[i][j] > (tmp = (graphDistanceTable[i][k] + graphDistanceTable[k][j])))
				{
					graphDistanceTable[i][j] = tmp;
				}

			}
		}
	}
	LOG(Main, 1) << "FloydWarshal end" << endl;
}

void Search::calculateDistanceTables()
{
	LOG(Main, 1) << "calculating distance tables " << endl;

	airDistanceTable.resize(world->buildings.size(), vector<int>(world->buildings.size(), -1));
	for (Building* b : world->buildings)
	{
		for (Building* b2 : world->buildings)
		{
			if (b->buildingIndex == b2->buildingIndex)
			{
				airDistanceTable[b->buildingIndex][b2->buildingIndex] = 0;
			}
			else
			{
				if (b->buildingIndex > b2->buildingIndex)
				{
					airDistanceTable[b->buildingIndex][b2->buildingIndex] = airDistanceTable[b2->buildingIndex][b->buildingIndex];
				}
				else
				{
					airDistanceTable[b->buildingIndex][b2->buildingIndex] = b->getPos().distFromPoint(b2->getPos());
				}
			}
		}
	}

	LOG(Main, 1) << endl;
	LOG(Main, 1) << "tables calculation finished " << endl;
}

int Search::getTotalDistance(vector<Building*>& permutation, bool airDistance)
{
	int sum = 0;

	for (u_int i = 0; i < permutation.size() - 1; i++)
	{
		int dist;

		if (airDistance)
			dist = airDistanceTable[permutation[i + 1]->buildingIndex][permutation[i]->buildingIndex];
		else
			dist = graphDistanceTable[permutation[i + 1]->motionlessIndex][permutation[i]->motionlessIndex];

		sum = sum + dist;
	}

	return sum;
}

//---------------------------------------------------------------Clusters Initializing--------------------------------------------------------------------------//

void Search::creatPathClusters()
{
	clusters.clear();
	for (int i = 0, j = -1; i < (int) searchBuildingList.size(); i++)
	{
		if (i % CLUSTER_SIZE == 0)
		{
			if (j > -1)
			{
				clusters[j]->setCenteralPoint();
			}

			j++;

			clusters.push_back(new Cluster());
			clusters[j]->ID = j;
		}

		clusters[j]->addMember(searchBuildingList[i]);
		searchBuildingList[i]->searchClusterIndex = j;
	}

	LOG(Main, 1) << "Search clusters.size: " << clusters.size() << endl;
	for (Cluster* c : clusters)
	{
		LOG(Main, 1) << "Cluster: " << c->ID << " : " << c->getMembers().size() << endl;
	}
}

void Search::setKmeansClusters()
{
	cout << "setKmeansClusters()" << endl;
	LOG(Main,1) << "setKmeansClusters()" << endl;
	vector<MotionlessObject*> roads(world->roads.size());
	for (u_int i = 0; i < world->roads.size(); i++)
	{
		roads[i] = world->roads[i];
	}

	KMean kmeans(roads, world->buildings.size() / CLUSTER_SIZE);
	kmeans.fastCalculate();
	//	clusters = vector<Cluster*>(kmeans.getClusters().size(), new Cluster());
	clusters.resize(kmeans.getClusters().size());
	vector<Cluster*> roadsClusters = kmeans.getClusters();
	LOG(Main,1) << "fgtttttttttttttttttttttttttttttttttttt" << endl;
	for (u_int i = 0; i < clusters.size(); ++i)
	{
		clusters[i] = new Cluster();
		Cluster* cluster = roadsClusters[i];

		for (u_int j = 0; j < roadsClusters[i]->getMembers().size(); ++j)
		{
			MotionlessObject* mo = cluster->getMembers()[j];
			for (u_int k = 0; k < mo->getNeighbours().size(); ++k)
			{
				MotionlessObject* neighbor = (MotionlessObject*) world->motionlessObjects[mo->getNeighbours()[k]];
				if (neighbor->isBuilding())
				{
					((Building*) neighbor)->searchClusterIndex = i;
					clusters[i]->addMember(neighbor);
				}
			}
		}
	}
}

//------------------------------------------------------------------Cluster Assignment--------------------------------------------------------------------------//
void Search::greedySearchClusterToPolicForceAssignment()
{
	myClusters().clear();
	vector<PoliceForce*> availblePoliceForces;

	for (PoliceForce* pf : world->policeForces)
	{
		pf->searchClusters.clear();
		if (pf->getBuriedness() != 0)
		{
			continue;
		}

		if (availblePoliceForces.size() == clusters.size())
		{
			break;
		}

		availblePoliceForces.push_back(pf);
	}

	int counter = availblePoliceForces.size(), firstSize = availblePoliceForces.size();
	while (availblePoliceForces.size() < clusters.size())
	{
		availblePoliceForces.push_back(availblePoliceForces[(counter++) % firstSize]);
	}

	for (Cluster* cluster: clusters)
	{
		int dist = MAX_INT;
		int best = -1;
		for (int i = 0; i < availblePoliceForces.size(); ++i)
		{
			PoliceForce* pf = availblePoliceForces[i];
			if (cluster->airDistToEntity(pf->getMotionlessObject()) < dist)
			{
				dist = cluster->airDistToEntity(pf->getMotionlessObject());
				best = i;
			}
		}

		availblePoliceForces[best]->searchClusters.push_back(cluster);
		availblePoliceForces.erase(availblePoliceForces.begin() + best);
	}

}

void Search::hungarianAssignment()
{
	LOG(Main, 1) << "setMyClusters()" << endl;

	myClusters().clear();
	vector<vector<int>> matrixCost(clusters.size(), vector<int>(clusters.size(), (-1 * MAX_INT)));
	vector<PoliceForce*> availblePoliceForces;

	for (PoliceForce* pf : world->policeForces)
	{
		if (pf->getBuriedness() != 0)
		{
			continue;
		}

		if (availblePoliceForces.size() == clusters.size())
		{
			break;
		}

		availblePoliceForces.push_back(pf);
	}

	int counter = availblePoliceForces.size(), firstSize = availblePoliceForces.size();
	while (availblePoliceForces.size() < clusters.size())
	{
		availblePoliceForces.push_back(availblePoliceForces[(counter++) % firstSize]);
	}

	for (u_int i = 0; i < availblePoliceForces.size(); i++)
	{
		for (u_int j = 0; j < clusters.size(); j++)
		{
			matrixCost[i][j] = (-1
					* worldGraph->getDistance(availblePoliceForces[i]->getFirstTimeMotionless()->getWithoutBlockadeRepresentiveNodeIndex(),
							clusters[j]->getMembers()[0]->getWithoutBlockadeRepresentiveNodeIndex(), GM_WITHOUT_BLOCKADE));
		}
	}

	HungarianAssignment hg(matrixCost);
	vector<vector<bool>> result = hg.hungarian();
	for (u_int i = 0; i < result.size(); ++i)
	{
		for (u_int j = 0; j < result.size(); ++j)
		{
			if (result[i][j])
			{
				availblePoliceForces[i]->searchClusters.push_back(clusters[j]);
				LOG(Main, 1) << "PF:  " << availblePoliceForces[i]->getId() << " : " << " C: " << clusters[j]->ID << endl;
				break;
			}
		}
	}

	for (int i = 0; i < (int) myClusters().size(); i++)
		LOG(Main, 1) << "Myclusters : " << myClusters()[i]->ID << " : " << myClusters()[i]->getMembers().size() << endl;
	LOG(Main, 1) << "myClustersSize: " << myClusters().size() << endl;
}

void Search::randomAssignment()
{
	if (!myClusters().empty())
		return;

	LOG(Main, 1) << "randomAssignment()" << endl;

	vector<PoliceForce*> availblePoliceForces;
	for (PoliceForce* pf : world->policeForces)
	{
		if (pf->getBuriedness() > 0)
		{
			continue;
		}

		if (availblePoliceForces.size() == clusters.size())
		{
			break;
		}

		availblePoliceForces.push_back(pf);
	}

	int counter = availblePoliceForces.size(), firstSize = availblePoliceForces.size();

	while (availblePoliceForces.size() < clusters.size())
	{
		availblePoliceForces.push_back(availblePoliceForces[counter % firstSize]);
		counter++;
	}

	myClusters().clear();
	vector<int> list;
	int randIndex = 0;

	for (u_int i = 0; i < clusters.size(); i++)
		list.push_back(i);

	for (PoliceForce* pf : availblePoliceForces)
	{
		int neededClusters = ((int) (list.size() / availblePoliceForces.size())) + 1;
		LOG(Main, 1) << "neededCluster: " << neededClusters << endl;
		LOG(Main, 1) << "list.size: " << list.size() << endl;
		while (neededClusters != 0 && list.empty() == false)
		{
			randIndex = rand() % list.size();
			pf->searchClusters.push_back(clusters[list[randIndex]]);

			if (self()->getId() == pf->getId())
				myClusters().push_back(clusters[list[randIndex]]);

			list.erase(list.begin() + randIndex);
			neededClusters--;
		}
	}

	if (myClusters().empty())
	{
		int index1 = rand() % clusters.size(), index2 = rand() % clusters.size();

		while (index1 == index2)
		{
			index2 = rand() % clusters.size();
		}
		myClusters().push_back(clusters[index1]);
		myClusters().push_back(clusters[index2]);
	}

	LOG(Main, 1) << "myClustersSize: " << myClusters().size() << endl;
}

//----------------------------------------------------------------Choosing BestCluster--------------------------------------------------------------------------//

bool Search::setBestCluster()
{
	LOG(Main,1) << "setBestCluster()" << endl;
	if (bestCluster != nullptr && isValid(bestCluster) && !fullySearched(bestCluster))
	{
		LOG(Main, 1) << "no need to change bestCluster !!!" << endl;
		return false;
	}

	for (Cluster* sc : myClusters())
	{
		setScore(sc);
	}

	sort(myClusters().begin(), myClusters().end(), compareSearchCluster);

	bool changed = false;
	for (u_int i = 0; i < myClusters().size(); i++)
	{
		if (isValid(myClusters()[i]))
		{
			LOG(Main, 1) << "bestCluster is changing !!!" << endl;
			bestCluster = myClusters()[i];
			self()->searchTarget = nullptr;
			return true;
		}
	}

	/*All of getMyClusters() members are inValid.
	 * In this case best cluster will be chosen from all clusters.*/

	if (changed == false && isValid(bestCluster) == false)
	{
		myClusters().clear();
		int bestScore = 0;
		for (Cluster* cluster : clusters)
		{
			setScore(cluster);
			if (cluster->score > bestScore)
			{
				bestScore = cluster->score;
				bestCluster = cluster;
				self()->searchTarget = nullptr;
				changed = true;
			}
		}
		myClusters().push_back(bestCluster);
	}

	return changed;
}

bool Search::fullySearched(Cluster* sc)
{
	for (MotionlessObject* mo : sc->getMembers())
	{
		if (((Building*) mo)->lastTimeSearchedForCivilian == -1 && isValid((Building*) mo))
			return false;
	}
	return true;
}

bool Search::isValid(Cluster* sc)
{
	if (sc->getMembers().empty())
	{
		return false;
	}

	for (MotionlessObject* mo : sc->getMembers())
	{
		if (isValid((Building*) mo))
		{
			return true;
		}
	}

	return false;
}

//---------------------------------------------------------------------Score Functions--------------------------------------------------------------------------//

void Search::setScore(Cluster* sc)
{
	int total = 0;
	if (isValid(sc) == false)
	{
		sc->score = numeric_limits<int>::min();
		return;
	}

	int notSearched = 0;
	for (MotionlessObject* mo : sc->getMembers())
	{
		Building* building = (Building*) mo;

		for (Types::BuildingsSearchState bst : building->getSearchStates())
		{
			switch (bst)
			{
				case Types::BSS_CLOSE_2_CIVILIANS_SHOUT:
					total += CLOSE_2_CIVILIANS_SHOUT_VALU;
					break;

				case Types::BSS_CLOSE_2_FIRE:
					total += CLOSE_2_FIRE_VALUE;
					break;

				case Types::BSS_NOT_SEARCHED:
					notSearched++;
					break;

				case Types::BSS_SEARCHED:

					if (building->getInsideCivilianIndexes().empty())
					{
						total -= SEARCHED_EMPTY_VALUE;
					}

					break;

				case Types::BSS_HAS_FIRE:
					total += HAS_FIRE_VALU;
					break;
			}
		}
	}

	total -= sc->getCenteralPoint().distFromPoint(self()->getPos()) / SCALE * DISTANCE_COFF;

	total += (double) notSearched / (double) sc->getMembers().size() * NOT_SEARCHED_VALUE;

	if (notSearched <= CLUSTER_SIZE / 10)
	{
		total -= ((double) sc->getMembers().size() - notSearched) * NOT_SEARCHED_VALUE;
	}

	sc->score = total;

	setNeighborsScore(sc);
}

void Search::setNeighborsScore(Cluster* sc)
{
	int total = 0;

	for (MotionlessObject* mo : sc->getMembers())
	{
		Building* b = (Building*) mo;
		for (MotionlessObject* neighbor : b->closerThan30B)
		{
			Building* neighborBuilding = (Building*) neighbor;
			if (neighborBuilding->searchClusterIndex != b->searchClusterIndex)
			{
				if (neighborBuilding->isRefuge())
				{
					total += REFUGE_COFF;
				}

				if (neighborBuilding->lastTimeSearchedForCivilian != -1)
				{
					total -= NEIGHBOR_SEEN_CYCLE;
				}

				for (Types::BuildingsSearchState bst : neighborBuilding->getSearchStates())
				{
					switch (bst)
					{
						case Types::BSS_CLOSE_2_CIVILIANS_SHOUT:
							total += CLOSE_2_CIVILIANS_SHOUT_VALU;
							break;

						case Types::BSS_CLOSE_2_FIRE:
							total += CLOSE_2_FIRE_VALUE;
							break;

						case Types::BSS_HAS_FIRE:
							total += HAS_FIRE_VALU;
							break;

						default:
							break;
					}
				}
			}
		}
	}

	sc->score += total;
}

bool Search::compareSearchCluster(Cluster* sc1, Cluster* sc2)
{
	return (sc1->score > sc2->score);
}

//------------------------------------------------------------Target Choosing Functions-------------------------------------------------------------------------//

bool Search::setMyTarget()
{
	if (self()->searchTarget != nullptr && isValid((Building*) self()->searchTarget))
		return true;

	if (bestCluster == nullptr)
	{
		return false;
	}

	LOG(Main, 1) << "starting: setTarget !!!" << endl;

	if (world->teamConfig["Search"]["SearchClustering"]["Kmeans"].asBool())
	{
		LOG(Main, 1) << "1" << endl;
		Building* tmp = getNearestValidBuildingInBestCluster();

		if (tmp == nullptr)
		{
			self()->searchTarget = self()->getMotionlessObject();
			self()->searchTarget = nullptr;
			LOG(Main, 1) << "2" << endl;
			return false;
		}
		else
		{
			self()->searchTarget = tmp;
			LOG(Main, 1) << "3" << endl;
			return true;
		}
		LOG(Main, 1) << "4" << endl;
	}
	else
	{
		for (MotionlessObject* mo : bestCluster->getMembers())
		{
			Building* building = (Building*) mo;
			//		LOG(Main, 1) << "bIndex: " << building->buildingIndex << " isValid" << (isValid(building) == true) << endl;
			if (isValid(building) && ((PoliceForce*) world->self)->getMotionlessObject()->motionlessIndex != building->motionlessIndex)
			{
				LOG(Main, 1) << "ChangingTarget !!!" << endl;
				self()->searchTarget = building;
				return true;
			}
		}

		self()->searchTarget = self()->getMotionlessObject();
		self()->searchTarget = nullptr;
		return false;
	}

	return false;
}

Building* Search::getNearestValidBuildingInBestCluster()
{
	int minDist = MAX_INT, dist;
	Building* nearest = nullptr;
	for (MotionlessObject* mo : bestCluster->getMembers())
	{
		Building* b = (Building*) mo;
		if (isValid(b)
				&& (dist = worldGraph->getDistance(self()->getWithoutBlockadeRepresentiveNodeIndex(), b->getWithoutBlockadeRepresentiveNodeIndex(), GM_WITHOUT_BLOCKADE)) < minDist)
		{
			minDist = dist;
			nearest = b;
		}
	}
	return nearest;
}

bool Search::isValid(Building* building)
{
	if (building == nullptr || building == nullptr)
	{
		return false;
	}

	if (building->getInsidePlatoonIndexes().empty() == false)
	{
		return false;
	}

	if (building->getFieryness() > 0 || building->getTemperature() >= 45)
	{
		return false;
	}

	if (building->isRefuge())
	{
		return false;
	}

	if (building->getBrokenness() == 0)
	{
		return false;
	}

	if (building->lastTimeSearchedForCivilian > 0 && (world->getTime() - building->lastTimeSearchedForCivilian) < INVALID_UPDATE_CYCLE)
	{
		return false;
	}

	if (building->lastTimeSearchedForCivilian > 0 && building->getInsideCivilianIndexes().empty())
	{
		return false;
	}

	if (building->lastTimeSearchedForCivilian > 0 && building->getInsideCivilianIndexes().empty() == false)
	{
		for (int index : building->getInsideCivilianIndexes())
		{
			if (world->civilians[index]->getLastCycleUpdatedBySense() > 0 && world->civilians[index]->getHp() > 0)
			{
				return true;
			}
		}

		return false;
	}

	return true;
}

bool Search::isValidForCount(Building* building)
{
	if (building->isRefuge())
	{
		return false;
	}

	if (building->getBrokenness() == 0)
	{
		return false;
	}

	return true;
}

//------------------------------------------------------------SimulatedAnnealing---------------------------------------------------------------------------------//

void Search::simulatedAnnealing()
{
	LOG(Main, 1) << "simulatedAnnealing() !!!" << endl;
	vector<Building *> order = searchBuildingList;
	double temperature = 100000.0;
	double deltaDistance = 0;
	double coolingRate = 0.9999;
	double absoluteTemperature = 0.00001;
	int distance = getTotalDistance(order, air);
	vector<Building*> nextOrder;
	int bestHistory = distance;

	while (temperature > absoluteTemperature)
	{
		nextOrder = getNewOrder(order);

		int tempDistance = getTotalDistance(nextOrder, air);
		deltaDistance = tempDistance - distance;

		double randomNum = (rand() % 10000000) / 100000000;
		if (bestHistory > tempDistance)
		{
			bestHistory = tempDistance;
		}

		if ((deltaDistance < 0) || (distance > 0 && exp(-deltaDistance / temperature) < randomNum))
		{
			order.clear();
			order = nextOrder;

			distance = deltaDistance + distance;
		}

		temperature *= coolingRate;
	}

	searchBuildingList = order;
	LOG(Main, 1) << "end of SimulatedAnnealing" << endl;
}

vector<Building *> Search::getNewOrder(const vector<Building*>& permutation)
{
	int first = rand() % permutation.size();
	vector<Building*> vec;

	vec = permutation;
	Building* a = vec[first];
	if (first < (int) permutation.size() - 2)
	{
		vec[first] = vec[first + 2];
		vec[first + 2] = a;
	}
	else
	{
		vec[first] = vec[first - 2];
		vec[first - 2] = a;
	}

	return vec;
}

//-----------------------------------------------------------------Getters & Other Functions----------------------------------------------------------------------//

PoliceForce* Search::self()
{
	return ((PoliceForce *) world->self);
}

vector<Cluster*>& Search::myClusters()
{
	return self()->searchClusters;
}

vector<Building*> & Search::getSearchBuildingList()
{
	return searchBuildingList;
}

bool Search::calculate()
{
	int inMyCluster = 0, valids = 0, searched = 0, validsAndNotSearched = 0, validsAndSearched = 0;

	for (Cluster* sc : myClusters())
	{
		for (MotionlessObject* mo : sc->getMembers())
		{
			Building* b = (Building*) mo;
			if (isValidForCount(b))
			{
				valids++;

				if (b->lastTimeSearchedForCivilian == -1)
				{
					validsAndNotSearched++;
				}

				if (b->lastTimeSearchedForCivilian > 0)
				{
					validsAndSearched++;
				}
			}

			if (b->lastTimeSearchedForCivilian > 0)
			{
				searched++;
			}
		}
	}

	LOG(Main , 1) << "SearchedBuildings: " << searched << endl;
	LOG(Main , 1) << "validBuildings: " << valids << endl;
	LOG(Main , 1) << "validsAndNotSearched: " << validsAndNotSearched << endl;
	LOG(Main , 1) << "validsAndSearched: " << validsAndSearched << endl;
	LOG(Main , 1) << "validsAndSearched/validBuildings*100: " << (((double) validsAndSearched) / ((double) valids)) * 100 << endl;
	LOG(Main , 1) << "Number of available buildings for search in myCluster: " << inMyCluster << endl;

	if (self()->searchTarget->isBuilding())
	{
		if (isValid((Building*) self()->searchTarget))
		{
			LOG(Main , 1) << "myTarget is valid" << endl;
			return true;
		}
	}

	if (myClusters().empty())
	{
		return false;
	}

	setBestCluster();

	return setMyTarget();
}

//----------------------------------------------------------------------------Fire Finder Search---------------------------------------------------------------------------------------//

void Search::test()
{
//	world->sousTargetLegacyRoads = find4LegacyRoads(world->buildings[world->getTime() % world->buildings.size()]);
//	int count = 0;
//	for (Building* building : world->buildings)
//	{
//		count += (int) isItSouspiciouseBuilding(building);
//	}
//	LOG(Main, 1) << "Number of SouspiciouseBuildings" << count << endl;
}

bool Search::isItSouspiciouseBuilding(Building* building)
{
	if (building->getTemperature() < 1)
	{
		return false;
	}

	if (building->getLastCycleUpdatedBySense() != world->getTime())
	{
		return false;
	}

	if (building->hasFire())
	{
		return false;
	}

	if (building->getFieryness() == 8)
	{
		return false;
	}

	int count = 0;
	for (InSightBuilding* b : building->inSightBuildings)
	{
		count += (int) isItValidSouspiciouseNeighbor(world->buildings[b->buildingIndex]);
	}
	cout << "neighbors count : " << count << endl;
	return (count >= MINIMUM_VALID_NEIGHBORS);

	return true;
}

bool Search::isItValidSouspiciouseNeighbor(Building* neighbor)
{
	if (neighbor->hasFire())
	{
		return false;
	}

	if (neighbor->getFieryness() == 8)
	{
		return false;
	}

	if (world->getTime() - neighbor->getLastCycleUpdatedBySense() < 30 && neighbor->getLastCycleUpdatedBySense() == 0)
	{
		return false;
	}

	//TODO Check Real Tempreture !!!

	return true;
}

bool Search::setSouspiciouseClusters()
{
	vector<Building*> suspiciousBuildings;
	for (Building* building : world->buildings)
	{
		if (isItSouspiciouseBuilding(building))
		{
			suspiciousBuildings.push_back(building);
		}
	}

	if (suspiciousBuildings.empty())
	{
		return false;
	}

	vector<bool> mark(world->buildings.size(), false);
	for (Building* building : suspiciousBuildings)
	{
		if (mark[building->buildingIndex])
		{
			continue;
		}

		Cluster* susGroup = new Cluster();
		stack<Building*> list;
		list.push(building);
		mark[building->buildingIndex] = true;

		while (!list.empty())
		{
			Building* top = list.top();
			list.pop();

			for (Building* building : suspiciousBuildings)
			{
				if (mark[building->buildingIndex] || building->getPos().distFromPoint(top->getPos()) > 80000)
				{
					continue;
				}
				mark[building->buildingIndex] = true;
				susGroup->addMember((MotionlessObject*) building);
				list.push(building);
			}
		}
		souspiciousClusters.push_back(susGroup);
	}

	return (souspiciousClusters.empty() == false);
}

bool Search::setMySouspiciouseCluster()
{
	vector<PoliceForce*> markedAgents;
	for (Cluster* souspiciouseCluster : souspiciousClusters)
	{
		PoliceForce* bestAgent = nullptr;
		int minDistace = numeric_limits<int>::max();

		for (PoliceForce* policeForce : world->policeForces)
		{
			if (find(markedAgents.begin(), markedAgents.end(), policeForce) != markedAgents.end() || policeForce->isAvailable == false || policeForce->getHp() < 10)
			{
				continue;
			}

			int distance = souspiciouseCluster->airDistToEntity(policeForce->getMotionlessObject());
			if (distance < minDistace)
			{
				minDistace = distance;
				bestAgent = policeForce;
			}
		}

		if (bestAgent != nullptr)
		{
			markedAgents.push_back(bestAgent);
			if (self() == bestAgent)
			{
				mySouspiciouseCluster = souspiciouseCluster;
				break;
			}
		}
	}

	return (mySouspiciouseCluster != nullptr);
}

bool Search::setMySouspiciouseTarget()
{
	if (mySouspiciouseCluster == nullptr)
	{
		return false;
	}

	self()->souspiciouseTarget = nullptr;
	int minDist = MAX_INT;
	for (MotionlessObject* motionless : mySouspiciouseCluster->getMembers())
	{
		Building* building = (Building*) motionless;
		if (building->getPos().distFromPoint(self()->getPos()) < minDist)
		{
			minDist = building->getPos().distFromPoint(self()->getPos());
			self()->souspiciouseTarget = building;
		}
	}
	self()->legacySouspiciouseRoads = findFourLegacyRoads(self()->souspiciouseTarget);
	return (self()->legacySouspiciouseRoads.empty() == false);
}

bool canSeeComparator(Road* r1, Road* r2)
{
	return (r1->buildingWithThisRoadCanSee.size() > r2->buildingWithThisRoadCanSee.size());
}

vector<Road*> Search::findFourLegacyRoads(Building* target)
{
	if (target == nullptr)
	{
		return vector<Road*>(0);
	}

	int maxDist = 0;
	Road* tmp = nullptr;
	for (int i = 1; i < 4; ++i)
	{
		maxDist = 0;
		for (MotionlessObject* mo : target->closerThan30R)
		{
			int localDist = 0;
			if (sousTargetLegacyRoads.empty())
			{
				localDist = mo->getPos().distFromPoint(target->getPos());
			}
			else
			{
				for (Road* r : sousTargetLegacyRoads)
				{
					LOG(Main ,1) << (r == nullptr) << endl;
					localDist += r->getPos().distFromPoint(mo->getPos());
				}
			}

			if (localDist > maxDist && find(sousTargetLegacyRoads.begin(), sousTargetLegacyRoads.end(), tmp) == sousTargetLegacyRoads.end())
			{
				maxDist = localDist;
				tmp = (Road*) mo;
			}
		}
		sousTargetLegacyRoads.push_back(tmp);
	}
	if (sousTargetLegacyRoads.empty() == false)
	{
		return sousTargetLegacyRoads;
	}
	else
	{
		LOG(Main, 1) << "Generating None Legacy Roads." << endl;
		for (InSightBuilding* isb : target->inSightBuildings)
		{
			sousTargetLegacyRoads.insert(sousTargetLegacyRoads.end(), world->buildings[isb->buildingIndex]->canSee.begin(), world->buildings[isb->buildingIndex]->canSee.end());
		}
		sort(sousTargetLegacyRoads.begin(), sousTargetLegacyRoads.end(), canSeeComparator);
		return sousTargetLegacyRoads;
	}
}

bool Search::prepareForSouspeciouseSearch()
{
	if (isItSouspiciouseBuilding(self()->souspiciouseTarget) == false)
	{
		if (setSouspiciouseClusters())
		{
			if (setMySouspiciouseCluster())
			{
				return setMySouspiciouseTarget();
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	else
	{
		return true;
	}
}

//------------------------------------------------------------------- FastFagetSearch !!! -------------------------------------------------------------------------------//

bool Search::fastFagetSearch()
{
	int maxTime = 0;
	Building* best = nullptr;
	for (Building* building : world->buildings)
	{
		if (building->getPos().distFromPoint(self()->getPos()) < 2 * AVERAGE_AGENT_SPEED)
		{
			if (isValid(building) && world->getTime() - building->lastTimeSearchedForCivilian > maxTime)
			{
				maxTime = world->getTime() - building->lastTimeSearchedForCivilian;
				best = building;
			}
		}
	}

	self()->searchTarget = best;
	return (best == nullptr);
}

//-------------------------------از این جا به پایین قرار نیست استفاده بشه !!!! !!!!!!----------------------------------------------------------------------------------------------------------------------------------------------//

//---------------------------------------------------------------------AroundFireSearch------------------------------------------------------------------------------//

bool Search::doAroundFireSearch()
{
	if (isMyAroundFireTargetValid() == false)
	{
		LOG(Main, 1) << "My AroundFire target is Not Valid :(((" << endl;
		if (deleteAroundFireClusters())
		{
			LOG(Main, 1) << "Around Fire Clusters are empty after cleaning data !!!" << endl;
			if (setAroundFireClusters())
			{
				if (setMyAroundFireCluster())
				{
					return setMyAroundFireTarget();
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
	}
	else
	{
		LOG(Main, 1) << "My Around Fire 5target is Valid :))" << endl;
		return true;
	}

	return false;
}

bool Search::setAroundFireClusters()
{
	vector<Building*> aroundFireBuildings;
	for (Building* building : world->buildings)
	{
		if (isItAroundFireBuilding(building))
		{
			aroundFireBuildings.push_back(building);
		}
	}

	if (aroundFireBuildings.empty())
	{
		return false;
	}

	vector<bool> mark(world->buildings.size(), false);
	for (Building* building : aroundFireBuildings)
	{
		if (mark[building->buildingIndex])
		{
			continue;
		}

		Cluster* aroundCluster = new Cluster();
		stack<Building*> list;
		list.push(building);
		mark[building->buildingIndex] = true;

		while (!list.empty())
		{
			Building* top = list.top();
			list.pop();

			for (Building* building : aroundFireBuildings)
			{
				if (mark[building->buildingIndex] || building->getPos().distFromPoint(top->getPos()) > 80000)
				{
					continue;
				}
				mark[building->buildingIndex] = true;
				aroundCluster->addMember((MotionlessObject*) building);
				list.push(building);
			}
		}
		aroundFireClusters.push_back(aroundCluster);
	}

	return true;
}

bool Search::deleteAroundFireClusters()
{
	for (u_int i = 0; i < aroundFireClusters.size(); ++i)
	{
		{
			delete aroundFireClusters[i];
		}
	}
	myAroundFireCluster = nullptr;
	aroundFireClusters.clear();
	return aroundFireClusters.empty();
}

bool Search::setMyAroundFireCluster()
{
	vector<PoliceForce*> markedAgents;
	for (Cluster* aroundFireCluster : aroundFireClusters)
	{
		PoliceForce* bestAgent = nullptr;
		int minDistace = numeric_limits<int>::max();

		for (int i = 0; i < 2; ++i)
		{
			for (PoliceForce* policeForce : world->policeForces)
			{
				LOG(Main, 1) << "PF: " << policeForce->policeForceIndex << endl;
				LOG(Main, 1) << "Marked: " << ((find(markedAgents.begin(), markedAgents.end(), policeForce) != markedAgents.end()) == true) << endl;
				LOG(Main, 1) << "isAvailble: " << policeForce->isAvailable << endl;
				LOG(Main, 1) << "PF_JOBE: " << (policeForce->job != PFJ_NONE) << endl;

				if (find(markedAgents.begin(), markedAgents.end(), policeForce) != markedAgents.end() || policeForce->isAvailable == false || policeForce->job != PFJ_NONE)
				{
					LOG(Main, 1) << "PF isInValid" << endl;
					continue;
				}

				LOG(Main, 1) << "PF isValid" << endl;

				int distance = aroundFireCluster->getAirDistanceToNearestEntity(policeForce->getMotionlessObject());
				if (distance < minDistace)
				{
					minDistace = distance;
					bestAgent = policeForce;
				}
			}
		}

		if (bestAgent != nullptr)
		{
			markedAgents.push_back(bestAgent);
			if (self() == bestAgent)
			{
				myAroundFireCluster = aroundFireCluster;
				break;
			}
		}
	}

	return (myAroundFireCluster != nullptr);
}

bool Search::setMyAroundFireTarget()
{
	LOG(Main, 1) << "myAroundFireCluster.size  --> " << myAroundFireCluster->getMembers().size() << endl;

	if (myAroundFireCluster->getMembers().empty())
	{
		return false;
	}

	Road* bestRoad = nullptr;
	double nearestDistance = numeric_limits<double>::max();
	for (MotionlessObject* mo : myAroundFireCluster->getMembers())
	{
		for (Road* road : ((Building*) mo)->canSee)
		{
			if (world->getTime() - road->getLastCycleUpdated() > 20)
			{
				//double distance = self()->getPos().distFromPoint(road->getPos());

				double distance = 0;

				if (air)
				{
					distance = self()->getPos().distFromPoint(road->getPos());
				}
				else
				{
					distance = graphDistanceTable[self()->getMotionlessObject()->motionlessIndex][road->motionlessIndex];
				}

				if (distance < nearestDistance)
				{
					nearestDistance = distance;
					bestRoad = road;
				}
			}
		}
	}

	self()->aroundFireSearchTarget = bestRoad;

	return (self()->aroundFireSearchTarget != nullptr);
}

bool Search::isMyAroundFireTargetValid()
{
	if (self()->aroundFireSearchTarget == nullptr)
	{
		return false;
	}

	for (int buildingIndex : self()->aroundFireSearchTarget->buildingWithThisRoadCanSee)
	{
		if (isItAroundFireBuilding(world->buildings[buildingIndex]))
		{
			return true;
		}

		return false;
	}

	return false;
}

bool Search::isItAroundFireBuilding(Building* building)
{
	if (building->hasFire())
	{
		return false;
	}

	if (building->getFieryness() == 8)
	{
		return false;
	}

	if (building->getLastCycleUpdated() == world->getTime())
	{
		return false;
	}

	if (building->getEstimatedData()->getTemperature() >= 1)
	{
		return true;
	}

	for (InSightBuilding* isb : building->getInSightBuildings())
	{
		Building* neighbor = world->buildings[isb->buildingIndex];

		if (neighbor->getEstimatedData()->hasFire() || (neighbor->getEstimatedData()->getTemperature() >= 1 && neighbor->getFieryness() != 8))
		{
			return true;
		}
	}

	return false;
}

bool Search::isAroundFireClusterValid(Cluster* cluster)
{
	if (cluster == nullptr)
	{
		return false;
	}

	for (MotionlessObject* mo : cluster->getMembers())
	{
		if (isItAroundFireBuilding((Building*) mo))
		{
			return true;
		}

		return false;
	}
	return false;

}
