#include "Agent.h"
#include "../Utilities/Clustering/Slice.h"

#define LOGLEVEL 2

using namespace std;
using namespace Types;
using namespace Encodings;

void Agent::globalPrecomputation()
{
	worldGraph = new WorldGraph(world);
	command = new Command(worldGraph, world, connection);

	setRadar();

	world->setWorldGraph(worldGraph);
	world->radar = radar;

	for (MotionlessObject* motionless : world->motionlessObjects)
		for (Point vertex : motionless->getShape().getVertices())
			if (motionless->getPos().distFromPoint(vertex) > motionless->getMaxDistanceFromCenter())
				motionless->setMaxDistanceFromCenter(motionless->getPos().distFromPoint(vertex));
}

void Agent::precomputation()
{
	LOG(Main, 1) << "Precomputation started!" << endl;
	cout << "********************************* Soshiant ********************************* " << endl;
	cout << "Precomputation Started!	" << world->selfID << endl;

	cout << "WorldGraph Creating Started!!" << endl;
	worldGraph->init();
	cout << "WorldGraph Creating Finished!!" << endl;

	world->setInitialDistanceFindingObjects();
	world->updateInnerHumans();

	if (!self()->isBuilding())
		worldGraph->update();

	LOG(Main, 1) << "Precomputation finished!" << endl;
}

std::string Agent::getPrecomputationFileAddress(Types::EntityType et)
{
	if (et == ET_FIRE_BRIGADE)
	{
		return "./Config/PreComputeData/FB-PreCompute-Data";
	}
	else if (et == ET_AMBULANCE_TEAM)
	{
		return "./Config/PreComputeData/AT-PreCompute-Data";
	}
	else if (et == ET_POLICE_FORCE)
	{
		return "./Config/PreComputeData/PF-PreCompute-Data";
	}
	else
	{
		LOG(Main, 1) << "Invalid Request" << endl;
		return "";
	}
}

std::string Agent::getDistanceFindingConfigAddress(Types::EntityType et)
{
	if (et == ET_FIRE_BRIGADE)
	{
		return "FireBrigadesDistanceFinding";
	}
	else if (et == ET_AMBULANCE_TEAM)
	{
		return "AmbulanceTeamsDistanceFinding";
	}
	else if (et == ET_POLICE_FORCE)
	{
		return "PoliceForcesDistanceFinding";
	}
	else
	{
		LOG(Main, 1) << "Invalid Request" << endl;
		return "";
	}
}

void Agent::exportPrecomputationData(fstream &PCData)
{
	LOG(Main, 1) << "Export Precompute started!" << endl;
	exportWorldGraphInit(PCData);
	exportWorldGraphOthers(PCData);
	exportWorldGraphUpdate(PCData);
	exportBFSAndDijkstra((world->teamConfig["MoveAndWorldGraphSettings"][getDistanceFindingConfigAddress(type)]["UseWithoutBlockade"].asBool()),
			(world->teamConfig["MoveAndWorldGraphSettings"][getDistanceFindingConfigAddress(type)]["UseDefaultBlocked"].asBool()),
			(world->teamConfig["MoveAndWorldGraphSettings"][getDistanceFindingConfigAddress(type)]["UseDefaultNotBlocked"].asBool()),
			(world->teamConfig["MoveAndWorldGraphSettings"]["GlobalSettings"]["UseAirDistances"].asInt() != 2), PCData);
	exportNotRelatedEdges(PCData);
	exportCloserThan(PCData);
	LOG(Main, 1) << "Export Precompute finished!" << endl;
}

void Agent::exportWorldGraphInit(fstream& PCData)
{
	PCData << getPrecomputationTagType(PCDT_NODES) << endl;
	PCData << worldGraph->getNodes().size() << endl;
	for (u_int i = 0; i < worldGraph->getNodes().size(); i++)
	{
		Node *node = worldGraph->getNodes()[i];
		PCData << node->nodeIndex << " " << node->getMotionlessIndex() << endl;
		PCData << node->getEdgeIndexes().size() << " ";
		for (u_int j = 0; j < node->getEdgeIndexes().size(); j++)
		{
			PCData << node->getEdgeIndexes()[j] << " ";
		}
		PCData << endl;
		PCData << node->getEdgeIndex() << endl;
		PCData << node->getNeighbours().size() << " ";
		for (u_int j = 0; j < node->getNeighbours().size(); j++)
		{
			PCData << node->getNeighbours()[j] << " ";
		}
		PCData << endl;
		PCData << std::fixed << node->getRepresentivePoint().getX() << " " << node->getRepresentivePoint().getY() << endl;
	}

	PCData << endl << getPrecomputationTagType(PCDT_EDGES) << endl;

	PCData << worldGraph->getEdges().size() << endl;

	for (u_int i = 0; i < worldGraph->getEdges().size(); i++)
	{
		Edge* edge = worldGraph->getEdges()[i];
		PCData << edge->edgeIndex << " " << edge->getFirstNodeIndex() << " " << edge->getSecondNodeIndex() << endl;
		PCData << edge->getMotionlessIndex() << " " << edge->getLength() << endl;
	}

	PCData << endl << getPrecomputationTagType(PCDT_REP_NODES) << endl;

	for (int i = 0; i < (int) world->motionlessObjects.size(); i++)
	{
		PCData << world->motionlessObjects[i]->getRepresentiveNodeIndex() << " " << world->motionlessObjects[i]->getWithoutBlockadeRepresentiveNodeIndex() << endl;
	}
	for (int i = 0; i < (int) world->humans.size(); i++)
	{
		PCData << world->humans[i]->getRepresentiveNodeIndex() << " " << world->humans[i]->getWithoutBlockadeRepresentiveNodeIndex() << endl;
	}
	PCData << endl;
}

void Agent::exportWorldGraphOthers(fstream& PCData)
{
	PCData << endl << getPrecomputationTagType(PCDT_DIS_FINDINGS) << endl;
	PCData << worldGraph->getDistanceFindingHumanIndexes().size() << endl;
	for (u_int i = 0; i < worldGraph->getDistanceFindingHumanIndexes().size(); i++)
	{
		PCData << worldGraph->getDistanceFindingHumanIndexes()[i] << " ";
	}
	PCData << endl << worldGraph->getDistanceFindingMotionlessIndexes().size() << endl;
	for (u_int i = 0; i < worldGraph->getDistanceFindingMotionlessIndexes().size(); i++)
	{
		PCData << worldGraph->getDistanceFindingMotionlessIndexes()[i] << " ";
	}

	PCData << endl << endl;

	PCData << getPrecomputationTagType(PCDT_INNER_HUMANS) << endl;
	for (u_int i = 0; i < world->motionlessObjects.size(); i++)
	{
		//		PCData << world->motionlessObjects[i]->motionlessIndex << endl;
		PCData << world->motionlessObjects[i]->getInnerHumansIndexes().size() << endl;
		for (u_int j = 0; j < world->motionlessObjects[i]->getInnerHumansIndexes().size(); j++)
		{
			PCData << world->motionlessObjects[i]->getInnerHumansIndexes()[j] << " ";
		}
	}

	PCData << endl << endl;
}

void Agent::exportWorldGraphUpdate(fstream& PCData)
{
	PCData << getPrecomputationTagType(PCDT_DIS_FINDING_NODES) << endl;
	PCData << worldGraph->getWithBlockadeDistanceFindingNodesIndexes().size() << endl;
	for (u_int i = 0; i < worldGraph->getWithBlockadeDistanceFindingNodesIndexes().size(); i++)
	{
		int wbDistance = worldGraph->getWithBlockadeDistanceFindingNodesIndexes()[i];
		PCData << wbDistance << " ";
	}

	PCData << endl << worldGraph->getWithoutBlockadeDistanceFindingNodesIndexes().size() << endl;

	for (u_int i = 0; i < worldGraph->getWithoutBlockadeDistanceFindingNodesIndexes().size(); i++)
	{
		int wobDistance = worldGraph->getWithoutBlockadeDistanceFindingNodesIndexes()[i];
		PCData << wobDistance << " ";
	}

	PCData << endl << endl;

	PCData << getPrecomputationTagType(PCDT_COMPONENTS) << endl;

	PCData << worldGraph->getDefaultBlockedModeComponents().size() << endl;
	for (u_int i = 0; i < worldGraph->getDefaultBlockedModeComponents().size(); i++)
	{
		PCData << worldGraph->getDefaultBlockedModeComponents()[i] << endl;
	}

	PCData << endl << worldGraph->getDefaultNotBlockedModeComponents().size() << endl;

	for (u_int i = 0; i < worldGraph->getDefaultNotBlockedModeComponents().size(); i++)
	{
		PCData << worldGraph->getDefaultNotBlockedModeComponents()[i] << endl;
	}

	PCData << endl;
}

void Agent::exportBFSAndDijkstra(bool wb, bool db, bool dnb, bool doBFS, fstream& PCData)
{
	PCData << getPrecomputationTagType(PCDT_DIS_TABLES) << endl;
	if (wb == true)
	{
		PCData << getPrecomputationTagType(PCDT_NO_BLOCK) << endl;
		PCData << getPrecomputationTagType(PCDT_DIJKSTRA) << endl;
		PCData << worldGraph->getWithoutBlockadeDijkstraDistanceTable().size() << endl;
		for (u_int i = 0; i < worldGraph->getWithoutBlockadeDijkstraDistanceTable().size(); i++)
		{
			PCData << worldGraph->getWithoutBlockadeDijkstraDistanceTable()[i].distance << " " << worldGraph->getWithoutBlockadeDijkstraDistanceTable()[i].parentNodeIndex << endl;
		}

		PCData << endl;

		if (doBFS == true)
		{
			PCData << getPrecomputationTagType(PCDT_BFS) << endl;
			PCData << worldGraph->getWithoutBlockadeDistanceTable().size() << endl;
			for (u_int i = 0; i < worldGraph->getWithoutBlockadeDistanceTable().size(); i++)
			{
				PCData << worldGraph->getWithoutBlockadeDistanceTable()[i].size() << endl;
				for (u_int j = 0; j < worldGraph->getWithoutBlockadeDistanceTable()[i].size(); j++)
				{
					PCData << worldGraph->getWithoutBlockadeDistanceTable()[i][j].distance << " " << worldGraph->getWithoutBlockadeDistanceTable()[i][j].parentNodeIndex << endl;
				}
			}
		}

		PCData << endl;
	}
	if (db == true)
	{
		PCData << getPrecomputationTagType(PCDT_DEF_BLOCKED) << endl;
		PCData << getPrecomputationTagType(PCDT_DIJKSTRA) << endl;
		PCData << worldGraph->getDefaultBlockedDijkstraDistanceTable().size() << endl;
		for (u_int i = 0; i < worldGraph->getDefaultBlockedDijkstraDistanceTable().size(); i++)
		{
			PCData << worldGraph->getDefaultBlockedDijkstraDistanceTable()[i].distance << " " << worldGraph->getDefaultBlockedDijkstraDistanceTable()[i].parentNodeIndex << endl;
		}

		PCData << endl;

		if (doBFS == true)
		{
			PCData << getPrecomputationTagType(PCDT_BFS) << endl;
			PCData << worldGraph->getDefaultBlockedDistanceTable().size() << endl;
			for (u_int i = 0; i < worldGraph->getDefaultBlockedDistanceTable().size(); i++)
			{
				PCData << worldGraph->getDefaultBlockedDistanceTable()[i].size() << endl;
				for (u_int j = 0; j < worldGraph->getDefaultBlockedDistanceTable()[i].size(); j++)
				{
					PCData << worldGraph->getDefaultBlockedDistanceTable()[i][j].distance << " " << worldGraph->getDefaultBlockedDistanceTable()[i][j].parentNodeIndex << endl;
				}
			}
		}

		PCData << endl;

	}
	if (dnb == true)
	{
		PCData << getPrecomputationTagType(PCDT_DEF_NOT_BLOCKED) << endl;
		PCData << getPrecomputationTagType(PCDT_DIJKSTRA) << endl;
		PCData << worldGraph->getDefaultNotBlockedDijkstraDistanceTable().size() << endl;
		for (u_int i = 0; i < worldGraph->getDefaultNotBlockedDijkstraDistanceTable().size(); i++)
		{
			PCData << worldGraph->getDefaultNotBlockedDijkstraDistanceTable()[i].distance << " " << worldGraph->getDefaultNotBlockedDijkstraDistanceTable()[i].parentNodeIndex
					<< endl;
		}

		PCData << endl;

		if (doBFS == true)
		{
			PCData << getPrecomputationTagType(PCDT_BFS) << endl;
			PCData << worldGraph->getDefaultNotBlockedDistanceTable().size() << endl;
			for (u_int i = 0; i < worldGraph->getDefaultNotBlockedDistanceTable().size(); i++)
			{
				PCData << worldGraph->getDefaultNotBlockedDistanceTable()[i].size() << endl;
				for (u_int j = 0; j < worldGraph->getDefaultNotBlockedDistanceTable()[i].size(); j++)
				{
					PCData << worldGraph->getDefaultNotBlockedDistanceTable()[i][j].distance << " " << worldGraph->getDefaultNotBlockedDistanceTable()[i][j].parentNodeIndex
							<< endl;
				}
			}
		}

		PCData << endl;
	}
}

void Agent::exportCloserThan(fstream& PCData)
{
	PCData << getPrecomputationTagType(PCDT_CLOSER_THAN) << endl;
	for (u_int i = 0; i < world->motionlessObjects.size(); i++)
	{
		exportJustACloserThan(world->motionlessObjects[i]->closerThan30B, PCDT_CLOSER_THAN_30B, PCData);
		exportJustACloserThan(world->motionlessObjects[i]->closerThan50B, PCDT_CLOSER_THAN_50B, PCData);
		exportJustACloserThan(world->motionlessObjects[i]->closerThan30M, PCDT_CLOSER_THAN_30M, PCData);
		exportJustACloserThan(world->motionlessObjects[i]->closerThan50M, PCDT_CLOSER_THAN_50M, PCData);
		exportJustACloserThan(world->motionlessObjects[i]->closerThan30R, PCDT_CLOSER_THAN_30R, PCData);
		exportJustACloserThan(world->motionlessObjects[i]->closerThan50R, PCDT_CLOSER_THAN_50R, PCData);
	}
}

void Agent::exportNotRelatedEdges(fstream& PCData)
{
	PCData << getPrecomputationTagType(PCDT_NOT_RELATED_TO_ROAD_EDGES) << endl;
	PCData << worldGraph->notRelatedToRoadEdges.size() << endl;
	for (Edge* edge : worldGraph->notRelatedToRoadEdges)
	{
		PCData << edge->edgeIndex << " ";
	}
	PCData << endl;
}

void Agent::exportJustACloserThan(vector<MotionlessObject*> closerThan, Types::PrecomputationDataTag tag, std::fstream& PCData)
{
	PCData << endl << getPrecomputationTagType(tag) << endl;
	PCData << closerThan.size() << endl;
	for (u_int j = 0; j < closerThan.size(); j++)
	{
		PCData << closerThan[j]->motionlessIndex << " ";
	}
	PCData << endl;
}

void Agent::importPrecomputationData()
{
	fstream PCData;
	PCData.open(getPrecomputationFileAddress(type).c_str(), fstream::in);
	string tag = "";
	PCData >> tag;

	while (tag != getPrecomputationTagType(PCDT_DATA_END))
	{
		LOG(Main, 1) << "Importing : " << tag << " Is Starting" << endl;
		tag = parsePrecomputationDataByTag(tag, PCData);
	}

	PCData.close();

	worldGraph->withoutPrecomputationInit();
}

bool isTag(string data)
{
	if (data[0] == '[')
	{
		return true;
	}

	return false;
}

string Agent::parsePrecomputationDataByTag(string tag, fstream &PCData)
{
	if (tag == getPrecomputationTagType(PCDT_NODES))
	{
		int nodesSize;
		PCData >> nodesSize;
		for (int i = 0; i < nodesSize; i++)
		{
			int nodeIndex;
			PCData >> nodeIndex;

			int motionlessIndex;
			PCData >> motionlessIndex;

			Node* newNode = new Node(motionlessIndex);
			world->motionlessObjects[motionlessIndex]->addRelativeNodeIndex(nodeIndex);
			newNode->nodeIndex = nodeIndex;

			int edgesSize, edgeIndex;
			PCData >> edgesSize;
			for (int j = 0; j < edgesSize; j++)
			{
				PCData >> edgeIndex;
				newNode->addEdgeIndex(edgeIndex);
			}

			int edgeInMotionless;
			PCData >> edgeInMotionless;
			newNode->setEdgeIndex(edgeInMotionless);

			int neighborsSize, neighborIndex;
			PCData >> neighborsSize;
			for (int j = 0; j < neighborsSize; j++)
			{
				PCData >> neighborIndex;
				newNode->addNeighbour(neighborIndex);
			}

			double repPointX, repPointY;
			PCData >> repPointX >> repPointY;
			newNode->setRepresentivePoint(Point(repPointX, repPointY));
			worldGraph->addNode(newNode);
		}
	}
	else if (tag == getPrecomputationTagType(PCDT_EDGES))
	{
		int edgesSize;
		PCData >> edgesSize;
		for (int i = 0; i < edgesSize; i++)
		{
			int edgeIndex, firstNodeIndex, secondNodeIndex, lenght, motionlessIndex;
			PCData >> edgeIndex >> firstNodeIndex >> secondNodeIndex >> motionlessIndex >> lenght;

			Edge* newEdge = new Edge(firstNodeIndex, secondNodeIndex);
			newEdge->setMotionlessIndex(motionlessIndex);
			newEdge->setLength(lenght);
			newEdge->edgeIndex = edgeIndex;
			if (motionlessIndex != -1)
			{
				world->motionlessObjects[motionlessIndex]->addRelativeEdgeIndex(edgeIndex);
			}
			worldGraph->addEdge(newEdge);
		}
	}
	else if (tag == getPrecomputationTagType(PCDT_REP_NODES))
	{
		LOG(Main, 1) << "Start Importing Rep Nodes !" << endl;
		int withBlockadeRepNodeIndex, withOutBlockadeRepNodeIndex;
		for (u_int i = 0; i < world->motionlessObjects.size(); i++)
		{
			PCData >> withBlockadeRepNodeIndex >> withOutBlockadeRepNodeIndex;
			world->motionlessObjects[i]->setRepresentiveNodeIndex(withBlockadeRepNodeIndex);
			world->motionlessObjects[i]->setWithoutBlockadeRepresentiveNodeIndex(withOutBlockadeRepNodeIndex);
		}

		for (u_int i = 0; i < world->humans.size(); i++)
		{
			PCData >> withBlockadeRepNodeIndex >> withOutBlockadeRepNodeIndex;
			world->humans[i]->setRepresentiveNodeIndex(withBlockadeRepNodeIndex);
			world->humans[i]->setWithoutBlockadeRepresentiveNodeIndex(withOutBlockadeRepNodeIndex);
		}
		LOG(Main, 1) << "Finish Importing Rep Nodes !" << endl;
	}
	else if (tag == getPrecomputationTagType(PCDT_DIS_FINDINGS))
	{
		int dfHumansSize, dfmLSize, index;
		PCData >> dfHumansSize;
		for (int i = 0; i < dfHumansSize; i++)
		{
			PCData >> index;
			worldGraph->addDistanceFindingHumanIndex(index);
		}

		PCData >> dfmLSize;
		for (int i = 0; i < dfmLSize; i++)
		{
			PCData >> index;
			worldGraph->addDistanceFindingMotiolessIndex(index);
		}

	}
	else if (tag == getPrecomputationTagType(PCDT_INNER_HUMANS))
	{
		for (u_int i = 0; i < world->motionlessObjects.size(); i++)
		{
			int innersSize, index;
			PCData >> innersSize;
			for (int j = 0; j < innersSize; j++)
			{
				PCData >> index;
				world->motionlessObjects[i]->addInnerHumanIndex(index);
			}
		}
	}
	else if (tag == getPrecomputationTagType(PCDT_DIS_FINDING_NODES))
	{
		int wbNIndexSize, wobNIndexSize, index;
		PCData >> wbNIndexSize;
		for (int i = 0; i < wbNIndexSize; i++)
		{
			PCData >> index;
			worldGraph->addWithBlockadeDistanceFindingNodesIndex(index);
		}
		PCData >> wobNIndexSize;
		for (int i = 0; i < wobNIndexSize; i++)
		{
			PCData >> index;
			worldGraph->addWithoutBlockadeDistanceFindingNodesIndex(index);
		}
	}
	else if (tag == getPrecomputationTagType(PCDT_COMPONENTS))
	{
		int dbComponentsSize, dnbComponentsSize, index;
		vector<int> dbComponents, dnbComponents;
		PCData >> dbComponentsSize;
		for (int i = 0; i < dbComponentsSize; i++)
		{
			PCData >> index;
			dbComponents.push_back(index);
		}
		worldGraph->setDefaultBlockedModeComponents(dbComponents);
		PCData >> dnbComponentsSize;
		for (int i = 0; i < dnbComponentsSize; i++)
		{
			PCData >> index;
			dnbComponents.push_back(index);
		}
		worldGraph->setDefaultNotBlockedModeComponents(dnbComponents);
	}
	else if (tag == getPrecomputationTagType(PCDT_DIS_TABLES))
	{
		string graphMode;
		PCData >> graphMode;
		if (graphMode == getPrecomputationTagType(PCDT_NO_BLOCK))
		{
			string subTag;
			PCData >> subTag;
			if (subTag == getPrecomputationTagType(PCDT_DIJKSTRA))
			{
				vector<DistanceTableEntry> dijkstraTable;
				int dijkstraTableSize;
				PCData >> dijkstraTableSize;
				int parentIndex, distance;
				for (int i = 0; i < dijkstraTableSize; i++)
				{
					DistanceTableEntry entry = DistanceTableEntry();
					PCData >> distance >> parentIndex;
					entry.distance = distance;
					entry.parentNodeIndex = parentIndex;
					dijkstraTable.push_back(entry);
				}
				worldGraph->setWithoutBlockadeDijkstraDistanceTable(dijkstraTable);
			}
			PCData >> subTag;
			if (subTag == getPrecomputationTagType(PCDT_BFS))
			{
				vector<vector<DistanceTableEntry> > BFSTable;
				int BFSTableSize, BFSTableSubSize, parentIndex, distance;
				PCData >> BFSTableSize;
				BFSTable.resize(BFSTableSize);
				for (int i = 0; i < BFSTableSize; i++)
				{
					PCData >> BFSTableSubSize;
					for (int j = 0; j < BFSTableSubSize; j++)
					{
						DistanceTableEntry entry = DistanceTableEntry();
						PCData >> distance >> parentIndex;
						entry.parentNodeIndex = parentIndex;
						entry.distance = distance;
						BFSTable[i].push_back(entry);
					}
				}
				worldGraph->setWithoutBlockadeDistanceTable(BFSTable);
				PCData >> graphMode;
			}
			else
			{
				graphMode = subTag;
			}
		}
		if (graphMode == getPrecomputationTagType(PCDT_DEF_BLOCKED))
		{
			string subTag;
			PCData >> subTag;
			if (subTag == getPrecomputationTagType(PCDT_DIJKSTRA))
			{
				vector<DistanceTableEntry> dijkstraTable;
				int dijkstraTableSize;
				PCData >> dijkstraTableSize;
				int parentIndex, distance;
				for (int i = 0; i < dijkstraTableSize; i++)
				{
					DistanceTableEntry entry = DistanceTableEntry();
					PCData >> distance >> parentIndex;
					entry.distance = distance;
					entry.parentNodeIndex = parentIndex;
					dijkstraTable.push_back(entry);
				}
				worldGraph->setDefaultBlockedDijkstraDistanceTable(dijkstraTable);
			}
			PCData >> subTag;
			if (subTag == getPrecomputationTagType(PCDT_BFS))
			{
				vector<vector<DistanceTableEntry> > BFSTable;
				int BFSTableSize, BFSTableSubSize, parentIndex, distance;
				PCData >> BFSTableSize;
				BFSTable.resize(BFSTableSize);
				for (int i = 0; i < BFSTableSize; i++)
				{
					PCData >> BFSTableSubSize;
					for (int j = 0; j < BFSTableSubSize; j++)
					{
						DistanceTableEntry entry = DistanceTableEntry();
						PCData >> distance >> parentIndex;
						entry.parentNodeIndex = parentIndex;
						entry.distance = distance;
						BFSTable[i].push_back(entry);
					}
				}
				worldGraph->setDefaultBlockedDistanceTable(BFSTable);
				PCData >> graphMode;
			}
			else
			{
				graphMode = subTag;
			}
		}
		if (graphMode == getPrecomputationTagType(PCDT_DEF_NOT_BLOCKED))
		{
			string subTag;
			PCData >> subTag;
			if (subTag == getPrecomputationTagType(PCDT_DIJKSTRA))
			{
				vector<DistanceTableEntry> dijkstraTable;
				int dijkstraTableSize;
				PCData >> dijkstraTableSize;
				int parentIndex, distance;
				for (int i = 0; i < dijkstraTableSize; i++)
				{
					DistanceTableEntry entry = DistanceTableEntry();
					PCData >> distance >> parentIndex;
					entry.distance = distance;
					entry.parentNodeIndex = parentIndex;
					dijkstraTable.push_back(entry);
				}
				worldGraph->setDefaultNotBlockedDijkstraDistanceTable(dijkstraTable);
			}
			PCData >> subTag;
			if (subTag == getPrecomputationTagType(PCDT_BFS))
			{
				vector<vector<DistanceTableEntry> > BFSTable;
				int BFSTableSize, BFSTableSubSize, parentIndex, distance;
				PCData >> BFSTableSize;
				BFSTable.resize(BFSTableSize);
				for (int i = 0; i < BFSTableSize; i++)
				{
					PCData >> BFSTableSubSize;
					for (int j = 0; j < BFSTableSubSize; j++)
					{
						DistanceTableEntry entry = DistanceTableEntry();
						PCData >> distance >> parentIndex;
						entry.parentNodeIndex = parentIndex;
						entry.distance = distance;
						BFSTable[i].push_back(entry);
					}
				}
				worldGraph->setDefaultNotBlockedDistanceTable(BFSTable);
				PCData >> graphMode;
			}
			else
			{
				graphMode = subTag;
				//				LOG(Main, 1) << "Valid Added Tag Reading" << endl;
				//				return graphMode;
			}
		}
		return graphMode;
	}
	else if (tag == getPrecomputationTagType(PCDT_CANSEE))
	{
		for (u_int i = 0; i < world->buildings.size(); i++)
		{
			int canSeeSize, roadIndex;
			PCData >> canSeeSize;
			for (int j = 0; j < canSeeSize; j++)
			{
				PCData >> roadIndex;
				world->buildings[i]->canSee.push_back(world->roads[roadIndex]);
				world->roads[roadIndex]->buildingWithThisRoadCanSee.push_back(i);
				world->roads[roadIndex]->buildingWithThisRoadCanSee.push_back(i);
			}
		}
	}
	else if (tag == getPrecomputationTagType(PCDT_SEARCH_CLUSTERS))
	{
		int clusterSize;
		PCData >> clusterSize;
		for (int i = 0; i < clusterSize; i++)
		{
			int buildingsSize;
			PCData >> buildingsSize;
			Cluster* cluster = new Cluster;
			for (int j = 0; j < buildingsSize; j++)
			{
				int index;
				PCData >> index;
				cluster->addMember(world->motionlessObjects[index]);
			}
			world->searchClusters.push_back(cluster);
		}
	}
	else if (tag == getPrecomputationTagType(PCDT_SEARCH_ASSIGN))
	{
		for (FireBrigade* fireBrigade : world->fireBrigades)
		{
			PCData >> fireBrigade->searchCluster;
		}
	}
	else if (tag == getPrecomputationTagType(PCDT_SEARCH_PATH))
	{
		int index = 0;
		for (u_int i = 0; i < world->buildings.size(); ++i)
		{
			PCData >> index;
			world->searchBuildingList.push_back(world->buildings[index]);
		}
	}
	else if (tag == getPrecomputationTagType(PCDT_SEARCH_AREA))
	{
		LOG(Main, 11) << "PCDT_SEARCH_AREA" << endl;
		int buidlingIndex = 0, searchShapeSize = 0, motionlessIndex = -1, numeberOfSearchPointsInSearchShape = 0;
		double X = -1, Y = -1;
		for (u_int i = 0; i < world->buildings.size(); i++)
		{
			PCData >> buidlingIndex;
			PCData >> searchShapeSize;

			LOG(Main, 11) << "buildingIndex: " << buidlingIndex << endl;
			LOG(Main, 11) << "searchShapeSize: " << searchShapeSize << endl;

			world->buildings[buidlingIndex]->searchArea = new SearchArea();

			for (int k = 0; k < searchShapeSize; k++)
			{
				PCData >> motionlessIndex;
				PCData >> numeberOfSearchPointsInSearchShape;
				LOG(Main, 11) << "motionlessIndex: " << motionlessIndex << endl;
				LOG(Main, 11) << "numeberOfSearchPointsInSearchShape: " << numeberOfSearchPointsInSearchShape << endl;
				SearchShape* sp = new SearchShape();
				sp->searchMotionless = world->motionlessObjects[motionlessIndex];

				for (int j = 0; j < numeberOfSearchPointsInSearchShape; j++)
				{
					PCData >> X;
					PCData >> Y;
					LOG(Main, 11) << "searchPoint: " << Point(X, Y) << endl;
					sp->searchPoints.push_back(Point(X, Y));
				}

				world->buildings[buidlingIndex]->searchArea->seachShapes.push_back(sp);
			}
		}
	}
	else if (tag == getPrecomputationTagType(PCDT_INSIGHT))
	{
		for (u_int i = 0; i < world->buildings.size(); i++)
		{
			int inSightSize, buildingIndex;
			float value;
			PCData >> inSightSize;
			for (int j = 0; j < inSightSize; j++)
			{
				PCData >> buildingIndex;
				PCData >> value;
				world->buildings[i]->inSightBuildings.push_back(new InSightBuilding(value, buildingIndex));
			}
		}
	}
	else if (tag == getPrecomputationTagType(PCDT_CELLS))
	{
		for (u_int i = 0; i < world->buildings.size(); i++)
		{
			int cellsSize;
			PCData >> cellsSize;
			for (int j = 0; j < cellsSize; j++)
			{
				array<int, 3> num;
				PCData >> num[0];
				PCData >> num[1];
				PCData >> num[2];
				world->buildings[i]->getEstimatedData()->getCells().push_back(num);
			}
		}
	}
	else if (tag == getPrecomputationTagType(PCDT_NOT_RELATED_TO_ROAD_EDGES))
	{
		int notRelatedEdgeSize;
		PCData >> notRelatedEdgeSize;
		for (int i = 0; i < notRelatedEdgeSize; i++)
		{
			int edgeIndex;
			PCData >> edgeIndex;
			worldGraph->notRelatedToRoadEdges.push_back(worldGraph->getEdges()[edgeIndex]);
		}
	}
	else if (tag == getPrecomputationTagType(PCDT_CLOSER_THAN))
	{
		// XXX: SubTag Is Too Risky
		// TODO: Functional

		string subTag;
		for (u_int i = 0; i < world->motionlessObjects.size(); i++)
		{
			PCData >> subTag;
			u_int size;
			if (subTag == getPrecomputationTagType(PCDT_CLOSER_THAN_30B))
			{
				PCData >> size;
				for (u_int j = 0; j < size; j++)
				{
					u_int index;
					PCData >> index;
					world->motionlessObjects[i]->closerThan30B.push_back(world->motionlessObjects[index]);
				}
			}
			PCData >> subTag;
			if (subTag == getPrecomputationTagType(PCDT_CLOSER_THAN_50B))
			{
				PCData >> size;
				for (u_int j = 0; j < size; j++)
				{
					u_int index;
					PCData >> index;
					world->motionlessObjects[i]->closerThan50B.push_back(world->motionlessObjects[index]);
				}
			}
			PCData >> subTag;
			if (subTag == getPrecomputationTagType(PCDT_CLOSER_THAN_30M))
			{
				PCData >> size;
				for (u_int j = 0; j < size; j++)
				{
					u_int index;
					PCData >> index;
					world->motionlessObjects[i]->closerThan30M.push_back(world->motionlessObjects[index]);
				}
			}
			PCData >> subTag;
			if (subTag == getPrecomputationTagType(PCDT_CLOSER_THAN_50M))
			{
				PCData >> size;
				for (u_int j = 0; j < size; j++)
				{
					u_int index;
					PCData >> index;
					world->motionlessObjects[i]->closerThan50M.push_back(world->motionlessObjects[index]);
				}
			}
			PCData >> subTag;
			if (subTag == getPrecomputationTagType(PCDT_CLOSER_THAN_30R))
			{
				PCData >> size;
				for (u_int j = 0; j < size; j++)
				{
					u_int index;
					PCData >> index;
					world->motionlessObjects[i]->closerThan30R.push_back(world->motionlessObjects[index]);
				}
			}
			PCData >> subTag;
			if (subTag == getPrecomputationTagType(PCDT_CLOSER_THAN_50R))
			{
				PCData >> size;
				for (u_int j = 0; j < size; j++)
				{
					u_int index;
					PCData >> index;
					world->motionlessObjects[i]->closerThan50R.push_back(world->motionlessObjects[index]);
				}
			}
		}
	}
	else if (tag == getPrecomputationTagType(PCDT_PIZZA_SLICES))
	{
		int size = 0;
		PCData >> size;
		for (int i = 0; i < size; i++)
		{
			Slice* slice = new Slice();
			int sliceSize = 0;
			PCData >> sliceSize;
			for (int j = 0 ;j < sliceSize; j++)
			{
				int motionlessIndex = 0;
				PCData >> motionlessIndex;
				slice->addBuilding((Building*) world->motionlessObjects[motionlessIndex]);
			}
			world->pizzaSlices.push_back(slice);
		}
	}

	string ret;
	PCData >> ret;
	return ret;
}

void Agent::setRadar()
{
	vector<RadioChannel*> radioChannels;
	int numOfSubscribeForCenters = atoi(world->config[Encodings::getConfigType(CT_CENTER_MAX_CHANNELS)].c_str());
	int numOfSubscribeForPlatoons = atoi(world->config[Encodings::getConfigType(CT_PLATOON_MAX_CHANNELS)].c_str());
	int numOfChannels = atoi(world->config[Encodings::getConfigType(CT_NUM_OF_CHANNELS)].c_str());
	for (int i = 0; i < numOfChannels; i++)
	{
		std::stringstream num;
		num << i;
		string typeKey = Encodings::getConfigType(CT_COMMUNICATION_PREFIX) + num.str() + Encodings::getConfigType(CT_TYPE_SUFFIX);
		if (world->config[typeKey] == Encodings::getConfigType(CT_TYPE_RADAR))
		{
			string bandwidthKey = Encodings::getConfigType(CT_COMMUNICATION_PREFIX) + num.str() + Encodings::getConfigType(CT_BANDWIDTH_SUFIX);
			int bandwidth = atoi(world->config[bandwidthKey].c_str());
			RadioChannel *newChannel = new RadioChannel("radio", i, bandwidth);
			radioChannels.push_back(newChannel);
		}
		else if (world->config[typeKey] == Encodings::getConfigType(CT_TYPE_VOICE))
		{
			LOG(Main , 1) << "i : " << i << " is voice!" << endl;
			continue;
		}
	}

	sort(radioChannels.begin(), radioChannels.end(), AbstractCommunicationRadar::channelComparator);

	float bandwidthPerPlatoon = 0;
	for (int i = 0; i < numOfSubscribeForPlatoons; i++)
	{
		bandwidthPerPlatoon += radioChannels[i]->getBandWidth();
	}

	bandwidthPerPlatoon *= 8; //convert bandwidth to bits
	int centers = ceil((world->ambulanceCenters.size() + world->fireStations.size() + world->policeOffices.size()) / 2.);
	bandwidthPerPlatoon /= (world->platoons.size() + centers);
	if (bandwidthPerPlatoon >= 80)
	{
		LOG(Main , 1) << "initializing radar in high bandwidth mode, bandwidth per platoon : " << bandwidthPerPlatoon << endl;
		radar = new HighBWCommunicationRadar(world, worldGraph, command);
		radar->setRadarMode(RM_HIGH);
	}
	else if (bandwidthPerPlatoon >= 20)
	{
		LOG(Main , 1) << "initializing radar in medium bandwidth mode, bandwidth per platoon : " << bandwidthPerPlatoon << endl;
		radar = new MediumBWCommunicationRadar(world, worldGraph, command);
		radar->setRadarMode(RM_MEDIUM);
	}
	else if (bandwidthPerPlatoon > 0)
	{
		LOG(Main , 1) << "initializing radar in low bandwidth mode, bandwidth per platoon : " << bandwidthPerPlatoon << endl;
		radar = new LowBWCommunicationRadar(world, worldGraph, command);
		radar->setRadarMode(RM_LOW);
	}
	else
	{
		LOG(Main , 1) << "initializing radar in NO bandwidth mode, bandwidth per platoon : " << bandwidthPerPlatoon << endl;
		radar = new LowBWCommunicationRadar(world, worldGraph, command);
		radar->setRadarMode(RM_NO_COMMUNICATION);
	}
}
