#ifndef _POLICEOFFICEAGENT_H
#define _POLICEOFFICEAGENT_H
#include "Agent.h"

class PoliceOfficeAgent: public Agent
{
public:
	PoliceOfficeAgent();
	virtual ~PoliceOfficeAgent();

protected:
	void actBeforeSense();
	void actBeforeRadar();
	void act();
	void precomputation();
	void noCommAct();
};

#endif	/* _POLICEOFFICEAGENT_H */

