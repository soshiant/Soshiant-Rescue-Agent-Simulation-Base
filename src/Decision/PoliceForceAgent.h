#ifndef _POLICEFORCEAGENT_H
#define	_POLICEFORCEAGENT_H

#include <string>
#include <cmath>
#include <vector>
#include <queue>
#include <algorithm>
#include <stack>
#include <utility>

#include "Agent.h"
#include "Search.h"

#include "../WorldModel/Objects/FireBlock.h"
#include "../WorldModel/Objects/PoliceForce.h"

#include "../Utilities/Geometry/Point.h"
#include "../Utilities/Clustering/KMean.h"
#include "../Utilities/HungarianAssignment.h"
#include "../Utilities/FireEstimator/FireEstimator.h"
#include "../Utilities/Clustering/PizzaClustering.h"
#include "../Utilities/Clustering/Cluster.h"
#include "../Utilities/Clustering/Slice.h"
#include "../Utilities/Debugger.h"
#include "../Utilities/Geometry/GeometryFunctions.h"
#include "../Utilities/Clustering/Hierarchical.h"
#include "../Utilities/Geometry/Circle.h"
#include <polyclipping/clipper.hpp>

struct StuckAgent
{
	StuckAgent(Platoon* agent, MotionlessObject* position);

	Platoon* agent;
	MotionlessObject* position;
};

class PoliceForceAgent: public Agent
{
public:

	PoliceForceAgent();
	virtual ~PoliceForceAgent();

protected:
	void actBeforeSense();
	void actBeforeRadar();
	void act();
	void precomputation();
	void exportPreComputeData();
	PoliceForce* self();
	void noCommAct();

private:
	bool specialWorks();
	bool normalWorks();

	vector<PoliceForce *> availablePolices;
	bool setAvailablePoliceForces();

	void fillTheBlacklists();

	void orderPizza();
	void assignToSlices(vector<PoliceForce*>, vector<Slice*>);
	bool isInSlice(MotionlessObject*, Slice*);
	vector<Slice*> buriedPizzaSlices, healthyPizzaSlices;
	Slice* mySlice;

	void chooseSpecialForces(vector<PoliceForce*>);
	vector<PoliceForce*> specialForces;

	bool pidgin();
	void createPidginPath();
	queue<MotionlessObject*> pidginPath;

	void updateJobs();
	void preDecision();
	void globalDecision();

	bool isStuck(MotionlessObject* target);
	bool isStuck(int nodeIndex);
	void updateStuckAgents();
	void clusterStuckAgents(int clusterCount);
	bool clearThisCluster(Cluster* araroCluster);
	bool openBestAgent();
	bool openOnTheWayHuman();
	vector<Cluster*> araroClusters;
	vector<StuckAgent*> araroTargets;
	vector<Platoon*> agentBlacklist;

	bool updateFire();
	void setAroundBuildings();
	void dfsOnBuildings(Building* building, FireBlock* fireBlock);
	void createFireBlocks();
	bool clearThisFireBlock(FireBlock* fb);
	bool isThisFireBlockOk(FireBlock* fb);
	void dfsOnFireBrigades(vector<FireBrigade*> &group, FireBrigade* fb);
	bool isReachableToFireGroup(PoliceForce* pf, vector<FireBrigade*> group);
	bool fireBlockToFireBrigade();
	bool checkSuspiciousBuildings();
	bool isSuspicious(Building*);
	bool openBestRefuge();
	PoliceForce* nearestPoliceToFireGroup(vector<FireBrigade*> group, vector<PoliceForce*> blacklist);
	FireEstimator* fireEstimator;
	vector<FireBlock*> fireBlocks;
	vector<Building *> onFireBuildings;
	vector<Point> threeCycleHistory;
	vector<Refuge*> refugeBlacklist;

	bool isValidForUpdate(Civilian* civ);
	bool isValidForClearing(Human* human);
	bool updateCivilians();
	bool clearCivilians();

	bool clearStuckFireBrigadesAroundMe();
	bool clearOtherStucksAroundMe();

	Building * mySearchTarget;
	Search * advanceSearch;
	bool fastSearch();
	bool warmBuildingSearch();
	bool search();
	bool suspiciousSearch();
	vector<Building*> searchTerritory;
	void initializeSearchTerritory();
	void beTheDomGuy();

	int maxClearDistance;
	int clearWidth;
	int repairRate;

	bool dontShas();
	bool iAmStucking();
	bool clearNearestBlockade();
	void updateLastRepNodeIndex();
	std::array<int, 3> lastRepNodeIndex;

	bool checkDamage();
	bool moveToRefuge();

	void inversionRectifier(vector<int>&);
	Point getClearPoint(Point);
	Point getClearPoint(vector<int>, Point);
	Segment getMutualEdge(MotionlessObject* first, MotionlessObject* second);
	Polygon getClearArea(Point target);
	bool isAnyBlockadeInClearArea(Polygon clearArea);
	Point getNextClearPoint(Segment mutualEdge);
	void clearAndGoToTarget(MotionlessObject* target);
	bool clear(MotionlessObject * target, bool setCommand = true);
	void moveAndClear(MotionlessObject*);
	void moveAndClear(MotionlessObject*, Point);
	void clearAndGo(MotionlessObject* target);
	void clearAndGo(MotionlessObject* target, Point point);
	bool makeReachable(MotionlessObject*, Point);
	void clearAndGoToPoint(MotionlessObject*, Point);
	bool doSuspiciousSearch();

	bool useFireEstimator = false;
	void setFireEstimatorNecessity();

	bool superTof();
	bool tof();

	int inRangeDistance = 0;
};

#endif	/* _POLICEFORCEAGENT_H */
