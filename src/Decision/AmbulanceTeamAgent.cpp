/*
 * File:   AmbulanceTeamAgent.cpp
 * Author: Pedram Taheri
 *
 * Created on September 24, 2010, 3:55 PM
 */

#include "AmbulanceTeamAgent.h"

#define LOGLEVEL 1

bool humanScoreComparator(Human * h1, Human * h2)
{
	return h1->value > h2->value;
}

bool humanRiskComparator(Human* h1, Human* h2)
{
	return h2->risk > h1->risk;
}

AmbulanceTeamAgent::AmbulanceTeamAgent()
{
	centerDecisionMaker = nullptr;
	bestRefuge = nullptr;

	srand(RANDOM_SEED);
}

AmbulanceTeamAgent::~AmbulanceTeamAgent()
{
}

/********************** Precompute **********************/

void AmbulanceTeamAgent::precomputation()
{
	if (isPrecomputeAgent)
	{
		Agent::precomputation();
		exportPreComputeData();
	}
	else
	{
		importPrecomputationData();
	}

	if (radar->getRadarMode() == RM_HIGH || radar->getRadarMode() == RM_MEDIUM)
	{
		world->ambulanceTeams[0]->isCapitan = true;
		if (self()->isCapitan)
		{
			centerDecisionMaker = new AmbulanceCenterDecision(world, worldGraph, hpPerception, ignoreCommandsUntil);
		}
	}

	//Constructing Estimated Datas
	for (Building* b : world->buildings)
	{
		b->estimatedData = new EstimatedData(b, nullptr);
	}

	//Initializing search places
	if (IS_CHALLENGE == false)
	{
		searchTerritory = world->buildings;
	}
	else
	{
		initializeSearchTerritory();
	}
}

void AmbulanceTeamAgent::globalPrecomputation()
{
	Agent::globalPrecomputation();
}

void AmbulanceTeamAgent::exportPreComputeData()
{
	fstream ATData;
	ATData.open(getPrecomputationFileAddress(type).c_str(), fstream::out);
    Agent::exportPrecomputationData(ATData);
	ATData << endl;
	ATData << Encodings::getPrecomputationTagType(PCDT_DATA_END);
	ATData.close();
}

/********************** Decision **********************/

void AmbulanceTeamAgent::actBeforeSense()
{

}

void AmbulanceTeamAgent::actBeforeRadar()
{
	if (world->getTime() < ignoreCommandsUntil || world->isNoComm())
	{
		return;
	}

	checkCaptain();
	if (self()->isCapitan)
	{
		LOG(Main, 1) << "i'm captain" << endl;
		centerDecisionMaker->act();
	}
}

void AmbulanceTeamAgent::act()
{
	//TODO fire death time
	//TODO if i loaded a civilian i can rescue another person till the refuge gets reachable to me.
	//
	if (world->getTime() < ignoreCommandsUntil || self()->getBuriedness() != 0)
	{
		return;
	}

	LOG(Main, 1) << (isLoadingInjured(self()) ? "i'm full" : "i'm empty") << endl;

	if (dontShas())
	{
		return;
	}

	setBestRefuge();
	if (escapeFromGasStation())
	{
		return;
	}

	if (checkDamage())
	{
		return;
	}

	if (doUnloadDuty())
	{
		return;
	}

	updateInformation();

	assign();

	if (isValid(self()->target) && rescueTarget(self()->target))
	{
		return;
	}

	if (isValid(self()->simpleTarget) && rescueTarget(self()->simpleTarget))
	{
		return;
	}

	search();
}

void AmbulanceTeamAgent::updateInformation()
{
	setOnFireBuildings();
	LOG(Main, 1) << "On fire building setting finished, on fire quantity : " << onFireBuildings.size() << endl;

	setAvailableAmbulanceTeams();
	LOG(Main, 1) << "Available ambulance setting finished, available quantity : " << availableAmbulanceTeams.size() << endl;

	setValidHumans();
	sort(validHumans.begin(), validHumans.end(), humanScoreComparator);
	LOG(Main, 1) << "Valid human setting finished, valid quantity : " << validHumans.size() << endl;

	LOG(Main, 1) << "Valid Humans : " << endl;
	for (Human* hm : validHumans)
	{
		LOG(Main, 1) << hm->getId() << " " << hm->getHp() << " " << hm->getDamage() << " " << hm->getBuriedness() << " " << endl;
	}
}

bool AmbulanceTeamAgent::checkCaptain()
{
	if (radar->getRadarMode() == RM_LOW || radar->getRadarMode() == RM_NO_COMMUNICATION)
	{
		return false;
	}

	bool didCaptainChange = false;
	AmbulanceTeam* captain = nullptr;
	for (AmbulanceTeam* at : world->ambulanceTeams)
	{
		if (at->isCapitan)
		{
			captain = at;
			break;
		}
	}

	if (captain)
	{
		u_int index = captain->ambulanceTeamIndex;
		while (!captain->isAvailable)
		{
			index++;
			if (index >= world->ambulanceTeams.size())
			{
				index = 0;
			}

			didCaptainChange = true;
			captain = world->ambulanceTeams[index];
		}
	}

	return didCaptainChange;
}

void AmbulanceTeamAgent::setValidHumans()
{
	validHumans.clear();
	for (Human* hm : world->humans)
	{
		if (isValid(hm))// && find(myWorkspace->members.begin(), myWorkspace->members.end(), hm->getMotionlessObject()) != myWorkspace->members.end())
		{
			setRisk(hm);
			setScore(hm);
			validHumans.push_back(hm);
		}
	}
}

void AmbulanceTeamAgent::assign()
{
	if (isValid(self()->target))
	{
		return;
	}

	LOG (Main , 1) << "===============--------:::::::::ASSIGN:::::::::--------===============" << endl;

	bool center = false;
	for (AmbulanceTeam* at : world->ambulanceTeams)
	{
		if (at->isCapitan)
		{
			center = true;
			break;
		}
	}

	if (center)
	{
		for (AmbulanceTeam* at : availableAmbulanceTeams)
		{
			if (!isValid(at->target))
			{
				if (isValid(at->centerTarget)) //Centralized decision
				{
					at->target = at->centerTarget;
				}
				else if (IS_CHALLENGE == false) //Simple nearest decision
				{
					int minDist = MAX_INT;
					for (Human* hm : validHumans)
					{
						if (worldGraph->isReachable(self()->getRepresentiveNodeIndex(), hm->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED))
						{
							int dist = worldGraph->getDistance(self()->getRepresentiveNodeIndex(), hm->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED);
							if (dist < minDist)
							{
								self()->simpleTarget = hm;
								minDist = dist;
							}
						}
					}
				}
			}
		}
	}
	else //Greedy decision
	{
		vector<AmbulanceTeam *> assigned;
		for (u_int i = 0; i < validHumans.size() && assigned.size() < availableAmbulanceTeams.size(); i++)
		{
			int ambCountForRescue = neededAmbulanceCount(validHumans[i], true);
			if (validHumans[i]->isPlatoon())
			{
				ambCountForRescue = suggestedAmbulanceCount(validHumans[i], true);
			}

			if (ambCountForRescue > availableAmbulanceTeams.size() || ambCountForRescue == -1)
			{
				continue;
			}

			vector<AmbulanceTeam *> nearAmbulances = getNearestInWorkspaceAmbulances(assigned, validHumans[i], ambCountForRescue);
			LOG (Main , 1) << "near ambulances size: " << nearAmbulances.size() << endl;
			if ((int) nearAmbulances.size() < neededAmbulanceCount(validHumans[i], true))
			{
				continue;
			}

			for (u_int j = 0; j < nearAmbulances.size(); j++)
			{
				assigned.push_back(nearAmbulances[j]);
				nearAmbulances[j]->target = validHumans[i];
			}
			LOG (Main , 1) << "ASSIGN APPLIED " << assigned.size() << endl;
		}

		LOG (Main , 1) << "===============--------:::::::::END OF ASSIGN:::::::::--------===============" << endl;

		if ((radar->getRadarMode() == RM_LOW || radar->getRadarMode() == RM_NO_COMMUNICATION) == false)
		{
			reAssign();
		}
	}

	if (isValid(self()->target) == false)
	{
		if (self()->target != nullptr)
		{
			self()->target->isAvailable = false;
			self()->target = nullptr;
		}
	}

	if (isValid(self()->simpleTarget) == false)
	{
		if (self()->simpleTarget != nullptr)
		{
			self()->simpleTarget->isAvailable = false;
			self()->simpleTarget = nullptr;
		}
	}
}

void AmbulanceTeamAgent::reAssign()
{
	LOG(Main , 1) << "reassignment" << endl;
	vector<Human*> chosenHumans;
	vector<AmbulanceTeam*> velgardAmbulances;
	for (u_int i = 0; i < availableAmbulanceTeams.size(); i++)
	{
		if (isValid(availableAmbulanceTeams[i]->target))
		{
			chosenHumans.push_back(availableAmbulanceTeams[i]->target);
		}
		else if (!isRescuing(availableAmbulanceTeams[i]) && !isLoadingInjured(availableAmbulanceTeams[i]))
		{
			velgardAmbulances.push_back(availableAmbulanceTeams[i]);
		}
	}

	LOG(Main , 1) << "sorting by risk value" << endl;
	sort(chosenHumans.begin(), chosenHumans.end(), humanRiskComparator); //TODO رو کل ولید هیومن ها بندازیم اینو فک کنم بهتر باشه
	LOG(Main , 1) << "sorted by risk value" << endl;
	for (AmbulanceTeam* at : velgardAmbulances)
	{
		int minDist = MAX_INT;
		for (Human* hm : chosenHumans)
		{
			if (!worldGraph->isReachable(at->getRepresentiveNodeIndex(), hm->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED))
			{
				continue;
			}
			int dist = worldGraph->getDistance(at->getRepresentiveNodeIndex(), hm->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED);
			if (dist < minDist)
			{
				minDist = dist;
				at->target = hm;
				LOG(Main , 1) << "guess who just got a target!!!" << endl;
			}
		}
	}
}

vector<AmbulanceTeam *> AmbulanceTeamAgent::getNearestInWorkspaceAmbulances(vector<AmbulanceTeam *> blackList, Human* human, int count)
{
	vector<AmbulanceTeam*> nearAmbulances;
	if (availableAmbulanceTeams.size() <= blackList.size()) //Return an empty vector
	{
		return nearAmbulances;
	}

	while ((int) nearAmbulances.size() < count)
	{
		int bestIndex = -1;
		int minDist = MAX_INT;
		for (int i = 0; i < (int) availableAmbulanceTeams.size(); i++)
		{
			if (find(nearAmbulances.begin(), nearAmbulances.end(), availableAmbulanceTeams[i]) != nearAmbulances.end()) //has been assigned before
			{
				continue;
			}
			if (find(blackList.begin(), blackList.end(), availableAmbulanceTeams[i]) != blackList.end()) //has been assigned before
			{
				continue;
			}
			if (isLoadingInjured(availableAmbulanceTeams[i]))
			{
				continue;
			}
			if (!worldGraph->isReachable(availableAmbulanceTeams[i]->getRepresentiveNodeIndex(), human->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED))
			{
				continue;
			}
			int dist = worldGraph->getDistance(availableAmbulanceTeams[i]->getRepresentiveNodeIndex(), human->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED);
			if (dist < minDist)
			{
				minDist = dist;
				bestIndex = i;
			}
		}

		if (bestIndex != -1)
		{
			nearAmbulances.push_back(availableAmbulanceTeams[bestIndex]);
		}
		else
		{
			break;
		}
	}

	return nearAmbulances;
}

/********************** Search **********************/

void AmbulanceTeamAgent::initializeSearchTerritory()
{
	vector<MotionlessObject*> objects;
	for (Building* b : world->buildings)
	{
		objects.push_back(b);
	}

	KMean k(objects, world->ambulanceTeams.size());
	k.fastCalculate();

	vector<vector<int>> cost(world->ambulanceTeams.size());
	for (u_int i = 0; i < world->ambulanceTeams.size(); i++)
	{
		for (Cluster* c : k.getClusters())
		{
			cost[i].push_back(-1 * c->groundDistToEntity(world->ambulanceTeams[i]->getMotionlessObject(), worldGraph, GM_DEFAULT_NOT_BLOCKED));
		}
	}

	HungarianAssignment hgAssign(cost);
	vector<vector<bool>> result = hgAssign.hungarian();

	for (u_int i = 0; i < result[self()->ambulanceTeamIndex].size(); i++)
	{
		if (result[self()->ambulanceTeamIndex][i])
		{
			for (MotionlessObject* motionless : k.getClusters()[i]->getMembers())
			{
				searchTerritory.push_back((Building*) motionless);
			}

			break;
		}
	}

	LOG(Main, 1) << "Search cluster: " << endl;
	for (Building* b : searchTerritory)
	{
		LOG(Main, 1) << b->getId() << " ";
	}
	LOG(Main, 1) << endl;
}

void AmbulanceTeamAgent::search()
{
	vector<Building*> validToSearch;
	vector<Building*> validToCheck;
	for (Building* b : searchTerritory)
	{
		if (worldGraph->isReachable(self()->getRepresentiveNodeIndex(), b->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED) == false)
		{
			continue;
		}

		if (b->isRefuge())
		{
			continue;
		}

		if (b->hasFire() || b->getFieryness() == 8)
		{
			continue;
		}

		bool nextContinue = false;
		for (MotionlessObject* close : b->closerThan50B)
		{
			if (((Building*) close)->hasFire())
			{
				nextContinue = true;
				break;
			}
		}
		if (nextContinue)
		{
			continue;
		}

		if (b->getBrokenness() == 0)
		{
			validToCheck.push_back(b);
			continue;
		}

		if (b->lastTimeSearchedForCivilian != -1)
		{
			validToCheck.push_back(b);
		}
		else
		{
			validToSearch.push_back(b);
		}
	}

	MotionlessObject* target = nullptr;
	if (validToSearch.empty())
	{
		if (searchTerritory != world->buildings)  //Finished my own search territory
		{
			searchTerritory = world->buildings;
		}

		int minimumLTS = MAX_INT;
		for (Building* b : validToCheck)
		{
			if (b->lastTimeSearchedForCivilian < minimumLTS)
			{
				minimumLTS = b->lastTimeSearchedForCivilian;
				target = b;
			}
		}
	}
	else
	{
		int minimumDistance = MAX_INT;
		for (Building* b : validToSearch)
		{
			int distance = worldGraph->getDistance(self()->getRepresentiveNodeIndex(), b->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED);
			if (distance < minimumDistance)
			{
				minimumDistance = distance;
				target = b;
			}
		}
	}

	if (target == nullptr)
	{
		LOG(Main, 1) << "No target found to search " << validToSearch.size() << " search valid : " << validToCheck.size() << endl;
		moveToRoad();
		//randomWalk();
		return;
	}

	LOG(Main, 1) << "Going to search : " << target->getId() << endl;
	command->moveToMotionless(target->motionlessIndex);
}

void AmbulanceTeamAgent::randomWalk()
{
	LOG(Main , 1) << "[RandomWalk] start!" << endl;
	vector<MotionlessObject *> aroundOfMe;

	for (MotionlessObject* around : world->roads)
	{
		if (worldGraph->isReachable(self()->getRepresentiveNodeIndex(), around->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED))
		{
			aroundOfMe.push_back(around);
			LOG(Main , 1) << around->getId() << ",";
		}
	}
	LOG(Main , 1) << " hamina bud!! " << endl;

	if (aroundOfMe.empty())
	{
		vector<int> tempPath;
		tempPath.push_back(self()->getMotionlessObject()->getId());
		command->move(tempPath, -1, -1);
		return;
	}

	int minimumUpdate = MAX_INT, bestIndex = 0;
	for (int i = 0; i < (int) aroundOfMe.size(); i++)
	{
		if (minimumUpdate < aroundOfMe[i]->getLastCycleUpdatedBySense())
		{
			minimumUpdate = aroundOfMe[i]->getLastCycleUpdatedBySense();
			bestIndex = i;
		}
	}

	command->moveToMotionless(aroundOfMe[bestIndex]->motionlessIndex);
}

/********************** Rescue **********************/

bool AmbulanceTeamAgent::rescueTarget(Human* target)
{
	if (isValid(target) == false)
	{
		return false;
	}

	if (doRescueDuty(target))
	{
		return true;
	}

	if (target->isCivilian())
	{
		if (doLoadDuty((Civilian*) target))
		{
			return true;
		}

		if (doUnloadDuty())
		{
			return true;
		}
	}

	return false;
}

bool AmbulanceTeamAgent::doRescueDuty(Human* target)
{
	if (target->getMotionlessObject() == self()->getMotionlessObject())
	{
		if (target->getBuriedness() > 0)
		{
			LOG (Main , 1) << "RESCUING MY TARGET " << target->getBuriedness() << " " << world->getTime() << " " << target->getMotionlessObject()->getId() << " " << self()->getMotionlessObject()->getId() << "  " << target->getLastCycleUpdatedBySense() << " " << (target->isAvailable ? "true " : "false ") << (target->isPlatoon() ? "agent" : "civ") << endl;

			if (target->isCivilian())
			{
				LOG(Main , 1) << ((Civilian*) target)->getFakeId() << endl;
			}
			else
			{
				LOG(Main , 1) << endl;
			}

			command->rescue(*target);

			return true;
		}
		else if (target->getBuriedness() == 0)
		{
			return false;
		}
	}
	else
	{
		command->moveToMotionless(target->getMotionlessObject()->motionlessIndex);

		return true;
	}

	return false;
}

bool AmbulanceTeamAgent::doLoadDuty(Civilian* target)
{
	if (self()->getMotionlessObject() != target->getMotionlessObject() || target->getBuriedness() != 0 || isLoadingInjured(self()))
	{
		return false;
	}

	if (radar->getRadarMode() == RM_LOW || radar->getRadarMode() == RM_NO_COMMUNICATION || target == self()->simpleTarget)
	{
		command->load(*target);
		return true;
	}
	else
	{
		AmbulanceTeam* loader = getLoaderAmbulance(target);
		if (loader == nullptr)
		{
			LOG(Main, 1) << "Null loader, it probably is a bug!" << endl;
			return false;
		}
		else if (loader == self())
		{
			command->load(*target);
			return true;
		}
		else
		{
			LOG(Main, 1) << "This is the chosen ambulance : " << loader->getId() << endl;
			return false;
		}
	}
}

bool AmbulanceTeamAgent::doUnloadDuty()
{
	if (isLoadingInjured(self()) == false)
	{
		return false;
	}

	Civilian* inMe = nullptr;
	for (Civilian* civ : world->civilians)
	{
		if (civ->getPosition() == self()->getId())
		{
			inMe = civ;
			break;
		}
	}

	if (bestRefuge) //An accessible refuge is available
	{
		if (isReallyAlive(inMe) == false)
		{
			command->unload();
			return true;
		}

		if (moveToRefuge())
		{
			return true;
		}
		if (self()->getMotionlessObject()->isRefuge())
		{
			command->unload();
			return true;
		}

		return false; //The code shall not reach to this line!
	}
	else if (world->refuges.empty()) //No refuge map
	{
		if (isReallyAlive(inMe) == false)
		{
			command->unload();
			return true;
		}

		if (moveToRoad())
		{
			return true;
		}
		if (self()->getMotionlessObject()->isRoad())
		{
			command->unload();
			return true;
		}

		return false; //Not a single accessible road
	}
	else //No reachable refuges
	{
		return false;
	}
}

/********************** Scoring **********************/

int AmbulanceTeamAgent::setScore(Human* human)
{
	human->value = numeric_limits<int>::min();
	if (!isValid(human))
	{
		return human->value;
	}

	int value = 0;

	if (human->isFireBrigade())
		value += 2000;
	else if (human->isPoliceForce())
		value += 1600;
	else if (human->isAmbulanceTeam())
		value += 1300;

	LOG(Main , 1) << "start point score : " << value << endl;

	value += getFireScore(human);

	LOG (Main, 1) << "fire score : " << getFireScore(human) << endl;

	value += getGasStationScore(human);

	LOG (Main, 1) << "gas station score : " << getGasStationScore(human) << endl;

	value += getTimeToRescueScore(human);

	LOG (Main, 1) << "time to rescue score : " << getTimeToRescueScore(human) << endl;

	value += getTimeToDeathScore(human);

	LOG (Main, 1) << "death time score : " << getTimeToDeathScore(human) << endl;

	value += getAroundCiviliansScore(human);

	LOG (Main, 1) << "around civilian score : " << getAroundCiviliansScore(human) << endl;

	value += getDistanceToAmbulancesScore(human);

	LOG (Main, 1) << "ambulance distance score : " << getDistanceToAmbulancesScore(human) << endl;

	human->value = value;

	LOG (Main, 1) << "final score : " << value << endl << endl;

	return value;
}

int AmbulanceTeamAgent::getFireScore(Human* human)
{
	if (onFireBuildings.empty())
	{
		return 0;
	}

	int minDist = MAX_INT;
	for (int i = 0; i < (int) onFireBuildings.size(); i++)
	{
		int dist = human->getPos().distFromPoint(onFireBuildings[i]->getPos());
		if (dist < minDist)
		{
			minDist = dist;
		}
	}

	int scale = SCALE / 3;

	int value = -1 * ((minDist / scale - 27.5 * SCALE / scale) * (minDist / scale - 27.5 * SCALE / scale) + 3) / 8;

	if (value < -1000)
	{
		value = -1000;
	}

	return value;
}

int AmbulanceTeamAgent::getGasStationScore(Human* human)
{
	if (world->gasStations.empty())
	{
		return 0;
	}

	int distanceToGasStationWeight = -6;
	if (world->isMapLarge())
	{
		distanceToGasStationWeight = -11;
	}

	int distToNearestGasStation = MAX_INT;
	for (int i = 0; i < (int) world->gasStations.size(); i++)
	{
		int dist = worldGraph->getAirDistance(human->getRepresentiveNodeIndex(), world->gasStations[i]->getRepresentiveNodeIndex());
		if (dist < distToNearestGasStation)
		{
			distToNearestGasStation = dist;
		}
	}

	distToNearestGasStation = distToNearestGasStation / SCALE;

	if (distToNearestGasStation > 20)
	{
		return 0;
	}

	return distToNearestGasStation * distanceToGasStationWeight;
}

int AmbulanceTeamAgent::getTimeToRescueScore(Human* human)
{
	int TimeToRescueValue = -12;
	if (world->isMapLarge())
	{
		TimeToRescueValue = -15;
	}

	int timeToRefuge = MAX_INT;
	if (human->isCivilian())
	{
		if (!world->refuges.empty())
		{
			for (int i = 0; i < (int) world->refuges.size(); i++)
			{
				if (!worldGraph->isReachable(human->getRepresentiveNodeIndex(), world->refuges[i]->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED))
				{
					continue;
				}

				int dist = worldGraph->getDistance(human->getRepresentiveNodeIndex(), world->refuges[i]->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED);
				if (dist < timeToRefuge)
				{
					timeToRefuge = dist;
				}
			}
		}
		if (timeToRefuge == MAX_INT)
		{
			return -100;
		}

		timeToRefuge /= (AVERAGE_AGENT_SPEED / 10);
	}
	else
	{
		timeToRefuge = 0;
	}

	int counter = 0;
	int timeToReach = 0;
	for (int i = 0; i < (int) availableAmbulanceTeams.size(); i++)
	{
		if (isRescuing(availableAmbulanceTeams[i]))
		{
			continue;
		}

		if (!worldGraph->isReachable(availableAmbulanceTeams[i]->getRepresentiveNodeIndex(), human->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED))
		{
			continue;
		}

		timeToReach += round(worldGraph->getDistance(availableAmbulanceTeams[i]->getRepresentiveNodeIndex(), human->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED) / AVERAGE_AGENT_SPEED);
		counter++;
	}

	if (counter == 0)
	{
		timeToReach = MAX_INT;
//		return -1 * MAX_INT;
		return numeric_limits<int>::min();
	}

	timeToReach = round(timeToReach / counter);


	int TTR = timeToReach + human->getFirstTimeBuriedness() + timeToRefuge; //Time To Rescue
	return TTR * TimeToRescueValue;
}

int AmbulanceTeamAgent::getTimeToDeathScore(Human* human)
{
	if (human->getTimeToDeath() > 510)
	{
		return 0;
	}

	float timeToDeathWeight = -0.5;
	if (world->isMapLarge())
	{
		timeToDeathWeight = -5;
	}
	if (world->refuges.empty())
	{
		timeToDeathWeight *= -12;
	}

	return human->getTimeToDeath() * timeToDeathWeight;
}

int AmbulanceTeamAgent::getAroundCiviliansScore(Human* human)
{
	int aroundCivilianWeight = 20;
	int numberOfCivilianNeighbor = 0;

	for (int i = 0; i < (int) human->getMotionlessObject()->closerThan50B.size(); i++)
	{
		for (int j = 0; j < (int) human->getMotionlessObject()->closerThan50B[i]->getInsideCivilianIndexes().size(); j++) //Valid Civilians Around
		{
			int thisIndex = human->getMotionlessObject()->closerThan50B[i]->getInsideCivilianIndexes()[j];

			if (isValid(world->civilians[thisIndex]))
			{
				numberOfCivilianNeighbor++;
			}
		}

		for (int j = 0; j < (int) human->getMotionlessObject()->closerThan50B[i]->getInsidePlatoonIndexes().size(); j++) //Buried Agents Around
		{
			int thisIndex = human->getMotionlessObject()->closerThan50B[i]->getInsidePlatoonIndexes()[j];

			if (isValid(world->platoons[thisIndex]))
			{
				numberOfCivilianNeighbor++;
			}
		}
	}

	return numberOfCivilianNeighbor * aroundCivilianWeight;
}

int AmbulanceTeamAgent::getDistanceToAmbulancesScore(Human* human)
{
	int distance = worldGraph->getDistance(self()->getRepresentiveNodeIndex(), human->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED);

	if (radar->getRadarMode() == RM_LOW || radar->getRadarMode() == RM_NO_COMMUNICATION)
	{
		return -1 * (distance / SCALE);
	}
	else
	{
		return -1 * (distance / (SCALE / 5));
	}
}

int AmbulanceTeamAgent::setRisk(Human* human)
{
	human->risk = MAX_INT;
	if (!isValid(human))
	{
		LOG(Main , 1) << "shit was not valid" << endl;
		return human->risk;
	}

	LOG(Main , 1) << "starting the risk calculation" << endl;
	int needed = neededAmbulanceCount(human, true);
	int assigned = 0;
	for (u_int i = 0; i < availableAmbulanceTeams.size(); i++)
	{
		if (availableAmbulanceTeams[i]->target && human->isCivilian())
		{
			if (availableAmbulanceTeams[i]->target->isCivilian() && ((Civilian*) availableAmbulanceTeams[i]->target)->getFakeId() == ((Civilian*) human)->getFakeId())
			{

			}
			else if (availableAmbulanceTeams[i]->target == human)
			{
				assigned++;
			}
		}
	}

	if (assigned > 0)
	{
		needed = assigned;
	}

	if (needed == 0)
	{
		human->risk = MAX_INT;
		return human->risk;
	}

	int buriedness = human->getBuriedness();
	int TOR = 0; //Time of Reaching to target
	int total = 0;
	int availableCount = 0;
	for (u_int i = 0; i < availableAmbulanceTeams.size(); i++)
	{
		if (isRescuing(availableAmbulanceTeams[i]))
		{
			continue;
		}
		if (!worldGraph->isReachable(availableAmbulanceTeams[i]->getRepresentiveNodeIndex(), human->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED))
		{
			continue;
		}

		availableCount++;
		total += round(worldGraph->getDistance(availableAmbulanceTeams[i]->getRepresentiveNodeIndex(), human->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED) / AVERAGE_AGENT_SPEED);
	}

	if (availableCount == 0)
	{
		human->risk = MAX_INT;
		return human->risk;
	}
	else
	{
		TOR += round(total / availableCount);
	}

	LOG(Main , 1) << " ambulances average move time setted " << TOR << " going to set time to refuge" << endl;

	TOR += 2; //Load and Unload

	int distToRefuge = MAX_INT;
	if (!world->refuges.empty())
	{
		for (int i = 0; i < (int) world->refuges.size(); i++)
		{
			if (!worldGraph->isReachable(human->getRepresentiveNodeIndex(), world->refuges[i]->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED))
			{
				continue;
			}

			int dist = worldGraph->getDistance(human->getRepresentiveNodeIndex(), world->refuges[i]->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED);
			if (dist < distToRefuge)
			{
				distToRefuge = dist;
			}
		}
	}

	if (distToRefuge != MAX_INT)
	{
		TOR += (distToRefuge / AVERAGE_AGENT_SPEED);
	}

	LOG(Main , 1) << "time to refuge setted to " << TOR << " going to calculate final risk" << endl;

	TOR += max((hpPerception / 100), 5); //Error Cancellation (محاسبه ی خطا!)
	TOR += (world->getTime() - human->getLastCycleUpdated()) / 8; //Error Cancellation (محاسبه ی خطا!)

	human->risk = human->getTimeToDeath() - (TOR + (buriedness / needed));

	LOG(Main , 1) << "risk setted for human : " << human->getId() << " the risk value is : " << human->risk << endl;

	return human->risk;
}

/********************** Utilities **********************/

bool AmbulanceTeamAgent::isValid(Human* human)
{
	if (!human)
	{
		return false;
	}
	if (!human->isAvailable)
	{
		return false;
	}
	if (human->getLastCycleUpdatedBySense() != 0 && human->getHp() == 0)
	{
		return false;
	}
	if (human->isPlatoon() && human->getBuriedness() == 0)
	{
		return false;
	}
	if (human->isCivilian() && (isLoaded((Civilian*) human) || human->getTimeToDeath() > 510 || human->getMotionlessObject()->isRefuge()))
	{
		return false;
	}
	if (human->getMotionlessObject()->isBuilding())
	{
		Building * humanPos = ((Building *) human->getMotionlessObject());
		if (humanPos->hasFire() && human->getBuriedness() > 0)
		{
			return false;
		}

		//HP >= [FIX] Kobe4 Civilian with 10400 HP
		if (human->getLastCycleUpdatedBySense() != 0 && !humanPos->hasFire() && human->getBuriedness() == 0 && !isGoingToLoad((Civilian*) human) && human->getDamage() == 0 && human->getHp() >= MAX_HP)
		{
			return false;
		}
	}
	else
	{
		if (human->getBuriedness() == 0 && human->getDamage() == 0)
		{
			return false;
		}
	}

	if (worldGraph->isReachable(self()->getRepresentiveNodeIndex(), human->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED) == false)
	{
		return false;
	}

	return true;
}

bool AmbulanceTeamAgent::isGoingToLoad(Civilian* civ)
{
	int bSize = civ->getBuriednessHistory().size();
	if (bSize >= 2 && self()->getMotionlessObject() == civ->getMotionlessObject() && civ->getBuriedness() == 0 && civ->getBuriednessHistory()[bSize - 1].buriedness != civ->getBuriednessHistory()[bSize - 2].buriedness)
	{
		return true;
	}
	return false;
}

bool AmbulanceTeamAgent::isLoaded(Civilian * civ)
{
	for (int i = 0; i < (int) world->ambulanceTeams.size(); i++)
	{
		if (civ->getPosition() == world->ambulanceTeams[i]->getId())
		{
			return true;
		}
	}

	return false;
}

bool AmbulanceTeamAgent::isReallyAlive(Civilian* civ)
{
	if (civ->getHp() > 0)
	{
		return true;
	}
	if (civ->getDamage() == 0)
	{
		return true;
	}

	LOG(Main, 1) << "time from death : " << world->getTime() - civ->getFirstDeathSignal() << endl;
	LOG(Main, 1) << "perception / damage: " << ceil(hpPerception / (double) civ->getDamage()) << endl;

	if (ceil(hpPerception / (double) civ->getDamage()) < (world->getTime() - civ->getFirstDeathSignal()))
	{
		cout << "civilian ke goor migerefti hame omr didi ke chegune goor civilian gereft?!?" << endl;

		return false;
	}

	return true;
}

bool AmbulanceTeamAgent::isRescuing(AmbulanceTeam* at)
{
	if (!at->getMotionlessObject() || at == self()) //XXX at == self() tof tof
	{
		return false;
	}

	if (at->positionHistory[at->positionHistory.size() - 1] == at->positionHistory[at->positionHistory.size() - 2])
	{
		for (u_int i = 0; i < validHumans.size(); i++)
		{
			if (validHumans[i]->getBuriedness() != 0 && validHumans[i]->getMotionlessObject() == at->getMotionlessObject())
			{
				return true;
			}
		}
	}

	return false;
}

bool AmbulanceTeamAgent::isLoadingInjured(AmbulanceTeam* at)
{
	if (at != self() && at->isCarry())
	{
		return true;
	}

	for (int i = 0; i < (int) world->civilians.size(); i++)
	{
		if (world->civilians[i]->getPosition() == at->getId())
		{
			return true;
		}
	}

	return false;
}

bool AmbulanceTeamAgent::moveToRoad()
{
	if (self()->getMotionlessObject()->isRoad())
	{
		return false;
	}

	int minDist = MAX_INT;
	Road* bestRoad = nullptr;

	for (u_int i = 0; i < world->roads.size(); i++)
	{
		if (worldGraph->isReachable(self()->getRepresentiveNodeIndex(), world->roads[i]->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED))
		{
			int dist = worldGraph->getDistance(self()->getRepresentiveNodeIndex(), world->roads[i]->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED);

			if (dist < minDist)
			{
				minDist = dist;
				bestRoad = world->roads[i];
			}
		}
	}

	if (bestRoad)
	{
		command->moveToMotionless(bestRoad->motionlessIndex);
		return true;
	}

	return false;
}

bool AmbulanceTeamAgent::moveToRefuge()
{
	if (self()->getMotionlessObject()->isRefuge())
	{
		return false;
	}

	if (bestRefuge)
	{
		command->moveToMotionless(bestRefuge->motionlessIndex);
		return true;
	}
	else
	{
		return false;
	}
}

void AmbulanceTeamAgent::setOnFireBuildings()
{
	onFireBuildings.clear();

	for (int i = 0; i < (int) world->buildings.size(); i++)
	{
		if (world->buildings[i]->hasFire())
		{
			onFireBuildings.push_back(world->buildings[i]);
		}
	}
}

void AmbulanceTeamAgent::setAvailableAmbulanceTeams()
{
	availableAmbulanceTeams.clear();

	for (int i = 0; i < (int) world->ambulanceTeams.size(); i++)
	{
		if (world->ambulanceTeams[i]->getBuriedness() != 0)
		{
			continue;
		}
		if (world->ambulanceTeams[i]->isAvailable == false)
		{
			continue;
		}
		if (world->ambulanceTeams[i]->isCarry())
		{
			continue;
		}
		if (world->ambulanceTeams[i]->getRepresentiveNodeIndex() == -1)
		{
			continue;
		}
//		if ((radar->getRadarMode() == RM_NO_COMMUNICATION || radar->getRadarMode() == RM_LOW) && find(myWorkspace->agents.begin(), myWorkspace->agents.end(), world->ambulanceTeams[i]) == myWorkspace->agents.end()) //Removing not same workspace ambs
//		{
//			continue;
//		}

		availableAmbulanceTeams.push_back(world->ambulanceTeams[i]);
	}

	LOG(Main, 1) << "Available ambs: " << endl;
	for (AmbulanceTeam* at : availableAmbulanceTeams)
	{
		LOG(Main, 1) << at->getId() << ", ";
	}
	LOG(Main, 1) << endl;
}

int AmbulanceTeamAgent::neededAmbulanceCount(Human* human, bool isAgentDecisioning)
{
	if (!isValid(human))
	{
		return -1;
	}

	int TOR = 0; //Time of Reaching to target
	if (isAgentDecisioning)
	{
		if (availableAmbulanceTeams.empty())
		{
			TOR = 5;
			if (world->isMapLarge())
			{
				TOR = 20;
			}
		}
		else
		{
			int total = 0;
			vector<AmbulanceTeam*> ambulances;
			for (u_int i = 0; i < availableAmbulanceTeams.size(); i++)
			{
				if (isRescuing(availableAmbulanceTeams[i]))
				{
					continue;
				}
				if (!worldGraph->isReachable(availableAmbulanceTeams[i]->getRepresentiveNodeIndex(), human->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED))
				{
					continue;
				}

				total += round(worldGraph->getDistance(availableAmbulanceTeams[i]->getRepresentiveNodeIndex(), human->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED) / AVERAGE_AGENT_SPEED);
			}

			TOR += round(total / availableAmbulanceTeams.size());
		}
	}

	TOR += 2; //Load and Unload

	int distToRefuge = MAX_INT;
	if (!world->refuges.empty())
	{
		for (int i = 0; i < (int) world->refuges.size(); i++)
		{
			if (!worldGraph->isReachable(human->getRepresentiveNodeIndex(), world->refuges[i]->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED))
			{
				continue;
			}

			int dist = worldGraph->getDistance(human->getRepresentiveNodeIndex(), world->refuges[i]->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED);
			if (dist < distToRefuge)
			{
				distToRefuge = dist;
			}
		}
	}

	if (distToRefuge != MAX_INT)
	{
		TOR += (distToRefuge / AVERAGE_AGENT_SPEED);
	}

	LOG (Main , 1) << "DIS IS ORIGINAL TOR : " << TOR << endl;

	if (isAgentDecisioning)
	{
		TOR += max((hpPerception / 100), 5); //Error Cancellation (محاسبه ی خطا!)
		TOR += (world->getTime() - human->getLastCycleUpdated()) / 8; //Error Cancellation (محاسبه ی خطا!)
	}

	LOG (Main , 1) << "DIS IS FINAL TOR : " << TOR << endl;

	int CTD = human->getTimeToDeath();

	if (TOR >= CTD)
	{
		return -1;
	}

	int count = 1;
	while (TOR + ceil(human->getBuriedness() / count) > CTD)
	{
		count++;
	}

	return count;
}

int AmbulanceTeamAgent::suggestedAmbulanceCount(Human* human, bool isAgentDecisioning)
{
	if (!isValid(human))
	{
		return -1;
	}

	int suggested = 4 - abs((int) ((60 - human->getBuriedness()) / 20));
	int needed = neededAmbulanceCount(human, isAgentDecisioning);
	if (needed != -1 && human->getBuriedness() == 0)
	{
		return 1;
	}
	else if (needed == -1)
	{
		return -1;
	}

	if (suggested < needed)
	{
		return needed;
	}
	else
	{
		return suggested;
	}
}

void AmbulanceTeamAgent::setBestRefuge()
{
	if (world->refuges.empty())
	{
		return;
	}

	bestRefuge = nullptr;
	int minDist = MAX_INT;
	for (u_int i = 0; i < world->refuges.size(); i++)
	{
		if (!worldGraph->isReachable(world->refuges[i]->getRepresentiveNodeIndex(), self()->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED)) //Reachability Check
		{
			continue;
		}

		int dist = worldGraph->getDistance(world->refuges[i]->getRepresentiveNodeIndex(), self()->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED);
		if (dist < minDist)
		{
			minDist = dist;
			bestRefuge = world->refuges[i];
		}
	}
}

AmbulanceTeam * AmbulanceTeamAgent::getLoaderAmbulance(Human * human)
{
	vector<AmbulanceTeam*> coWorkers;
	for (AmbulanceTeam* at : availableAmbulanceTeams)
	{
		if (at->target == nullptr || isLoadingInjured(at))
		{
			continue;
		}

		if (at->getMotionlessObject() == human->getMotionlessObject())
		{
			if (human->isCivilian() && at->target->isCivilian())
			{
				if (((Civilian*) human)->getFakeId() == ((Civilian*) at->target)->getFakeId())
				{
					coWorkers.push_back(at);
				}
			}
			else if (human->isPlatoon() && at->target->isPlatoon())
			{
				if (human->getId() == at->target->getId())
				{
					coWorkers.push_back(at);
				}
			}
		}
	}

	if (coWorkers.empty())
	{
		return nullptr;
	}

	sort(coWorkers.begin(), coWorkers.end());
	return coWorkers[0];
}

/********************** Escape **********************/

bool AmbulanceTeamAgent::checkDamage()
{
	bool isDamaged = false;
	if (self()->getDamage() > 0)
	{
		isDamaged = true;
	}
	else
	{
		int hSize = self()->getHpHistory().size() - 1;
		if (hSize > 2)
		{
			if (self()->getHpHistory()[hSize].hp != self()->getHpHistory()[hSize - 1].hp)
			{
				isDamaged = true;
			}
			else if (self()->getHpHistory()[hSize - 1].hp != self()->getHpHistory()[hSize - 2].hp)
			{
				isDamaged = true;
			}
			else if (self()->getHpHistory()[hSize - 2].hp != self()->getHpHistory()[hSize - 3].hp)
			{
				isDamaged = true;
			}
		}
	}

	if (isDamaged)
	{
		if (moveToRefuge())
		{
			return true;
		}
	}

	return false;
}

bool AmbulanceTeamAgent::escapeFromGasStation()
{
	if (world->gasStations.empty())
	{
		return false;
	}

	int minDist = MAX_INT;
	for (u_int i = 0; i < world->gasStations.size(); i++)
	{
		if (world->gasStations[i]->getTemperature() >= 30 && (world->gasStations[i]->getFieryness() == 0 || world->gasStations[i]->getFieryness() == 4))
		{
			int dist = worldGraph->getAirDistance(self()->getRepresentiveNodeIndex(), world->gasStations[i]->getRepresentiveNodeIndex());
			if (dist < minDist)
			{
				minDist = dist;
			}
		}
	}

	if (minDist <= GAS_STATION_RADIUS)
	{
		waterIsClosed(); //RUN FORREST RUN!!
		return true;
	}

	return false;
}

bool AmbulanceTeamAgent::waterIsClosed() //Run away from gas station! //TODO Test the shit
{
	vector<GasStation*> gasStations;
	for (GasStation* gasStation : world->gasStations)
	{
		if (gasStation->getTemperature() >= 30 && (gasStation->getFieryness() == 0 || gasStation->getFieryness() == 4))
		{
			gasStations.push_back(gasStation);
		}
	}
	if (gasStations.empty())
	{
		for (GasStation* gasStation : world->gasStations)
			gasStations.push_back(gasStation);

		if (gasStations.empty())
			return false;
	}

	vector<Road*> targetsForEscape;
	for (Road* road : world->roads)
	{
		if (!worldGraph->isReachable(self()->getRepresentiveNodeIndex(), road->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED))
		{
			continue;
		}
		bool isValid = true;
		for (GasStation* gs : gasStations)
		{
			if (gs->getPos().distFromPoint(road->getPos()) <= GAS_STATION_RADIUS)
			{
				isValid = false;
				break;
			}
		}

		if (isValid)
		{
			targetsForEscape.push_back(road);
		}
	}
	if (targetsForEscape.empty())
	{
		while (!gasStations.empty() && targetsForEscape.empty())
		{
			int minTemp = MAX_INT;
			GasStation* toBeRemoved = nullptr;
			for (GasStation* gs : gasStations)
			{
				if (gs->getTemperature() < minTemp)
				{
					minTemp = gs->getTemperature();
					toBeRemoved = gs;
				}
			}

			if (toBeRemoved)
			{
				gasStations.erase(find(gasStations.begin(), gasStations.end(), toBeRemoved));
			}
			else
			{
				break;
			}

			for (Road* road : world->roads)
			{
				if (!worldGraph->isReachable(self()->getRepresentiveNodeIndex(), road->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED))
				{
					continue;
				}
				bool isValid = true;
				for (GasStation* gs : gasStations)
				{
					if (gs->getPos().distFromPoint(road->getPos()) <= GAS_STATION_RADIUS)
					{
						isValid = false;
						break;
					}
				}

				if (isValid)
				{
					targetsForEscape.push_back(road);
				}
			}
		}
		if (targetsForEscape.empty())
		{
			LOG(Main , 1) << "aslan rah nadare az in dar berim :|" << endl;
			return false;
		}
	}

	LOG(Main , 1) << "gas station targets size : " << targetsForEscape.size() << " motionless size : " << world->motionlessObjects.size() << endl;

	Road* target = nullptr;
	int minDist = MAX_INT;
	for (Road* motionless : targetsForEscape)
	{
		int dist = worldGraph->getDistance(self()->getRepresentiveNodeIndex(), motionless->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED);
		if (dist < minDist)
		{
			minDist = dist;
			target = motionless;
		}
	}

	if (target)
	{
		if (self()->getMotionlessObject() == target)
		{
			return false;
		}

		command->moveToMotionless(target->motionlessIndex);
		return true;
	}

	return false;
}

/********************* Don't Shas ********************/

bool AmbulanceTeamAgent::dontShas()
{
	if (world->getTime() <= 6)
	{
		return false;
	}

	if (self()->getRepresentiveNodeIndex() == -1)
	{
		int randomNeighbour = world->getTime() % self()->getMotionlessObject()->getNeighbours().size();
		vector<int> path;
		path.push_back(self()->getMotionlessObject()->getId());
		path.push_back(world->motionlessObjects[self()->getMotionlessObject()->getNeighbours()[randomNeighbour]]->getId());
		LOG(Main, 1) << "dontShasWhenIAmIsNotReachable " << world->motionlessObjects[self()->getMotionlessObject()->getNeighbours()[randomNeighbour]]->getId() << endl;
		command->move(path, -1, -1);
		return true;
	}

	int index = self()->positionHistory.size() - 1;
	if (self()->myLastCommand == CT_MOVE && world->getTime() % 3 == 0)
	{
		if (self()->getMotionlessObject()->getId() == self()->positionHistory[index]->getId() && self()->positionHistory[index]->getId() == self()->positionHistory[index - 1]->getId() && self()->positionHistory[index - 1]->getId() == self()->positionHistory[index - 2]->getId())
		{
			vector<int> path;
			path.push_back(self()->getMotionlessObject()->getId());
			command->move(path, -1, -1);
			//			randomWalk();
			return true;
		}

		if (index >= 4)
		{
			if (self()->getMotionlessObject()->getId() == self()->positionHistory[index]->getId() && self()->positionHistory[index]->getId() == self()->positionHistory[index - 2]->getId() && self()->positionHistory[index - 2]->getId() == self()->positionHistory[index - 4]->getId())
			{
				if (world->getTime() % 3 == 0)
				{
					randomWalk();
					return true;
				}

				vector<int> path;
				path.push_back(self()->getMotionlessObject()->getId());
				command->move(path, -1, -1);
//				randomWalk();
				return true;
			}
		}
	}

	return false;
}

void AmbulanceTeamAgent::noCommAct()
{
	if (world->getTime() < ignoreCommandsUntil)
	{
		return;
	}

	act();
}

AmbulanceTeam* AmbulanceTeamAgent::self()
{
	return (AmbulanceTeam*) world->self;
}
