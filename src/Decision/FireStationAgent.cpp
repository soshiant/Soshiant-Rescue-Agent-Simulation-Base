#include "FireStationAgent.h"

#define LOGLEVEL 1

FireStationAgent::FireStationAgent()
{
}

FireStationAgent::~FireStationAgent()
{
}

void FireStationAgent::precomputation()
{
	Agent::precomputation();
}

void FireStationAgent::actBeforeSense()
{

}

void FireStationAgent::actBeforeRadar()
{
}

void FireStationAgent::act()
{
}

void FireStationAgent::noCommAct()
{
}

FireStation* FireStationAgent::self()
{
	return (FireStation *) world->self;
}
