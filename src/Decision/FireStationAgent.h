#ifndef _FIRESTATIONAGENT_H
#define _FIRESTATIONAGENT_H

#include "Agent.h"
#include "../Utilities/HungarianAssignment.h"
#include "../Utilities/Debugger.h"

class FireStationAgent: public Agent
{
public:
	FireStationAgent();
	virtual ~FireStationAgent();

protected:
	void actBeforeSense();
	void actBeforeRadar();
	void act();
	void precomputation();
	void noCommAct();
	FireStation* self();
};

#endif	/* _FIRESTATIONAGENT_H */

