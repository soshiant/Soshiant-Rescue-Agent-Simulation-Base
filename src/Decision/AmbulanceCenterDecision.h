/*
 * AmbulanceCenterDecision.h
 *
 *  Created on: Dec 16, 2013
 *      Author: Pedram
 */

#include "AmbulanceTeamAgent.h"
#include "Agent.h"
#include "../Utilities/Debugger.h"
#include "../Utilities/HungarianAssignment.h"
#include "../WorldModel/Objects/Human.h"

#ifndef AMBULANCECENTERDECISION_H_
#define AMBULANCECENTERDECISION_H_

using namespace std;

class AmbulanceCenterDecision
{
public:
	AmbulanceCenterDecision(WorldModel* world, WorldGraph* worldGraph, int hpPerception, int ignoreCommandsUntil);
	virtual ~AmbulanceCenterDecision();
	void act();

private:
	void checkPastAssignments();
	void setOnFireBuildings();
	void setFreeAmbulances();
	void setValidHumans();
	void filterHumans();

	int setScore(Human* human);
	int getFireScore(Human* human);
	int getGasStationScore(Human* human);
	int getTimeToDeathScore(Human* human);
	int getTimeToRescueScore(Human* human);
	int getAroundCiviliansScore(Human* human);
	int getDistanceToAmbulancesScore(Human* human);

	void assign();
	vector<vector<int> > createMatrixCost();
	void applyAssign(vector<vector<bool> > assignment);

	void progressStates();
	bool isValid(Human* human);
	bool shouldAssignThisCycle();
	bool isLoaded(Civilian* civ);
	int getFreeAmbulancesLimit();
	int neededAmbulanceCount(Human* human);
	int suggestedAmbulanceCount(Human* human);

	int setRisk(Human* human);

	vector<Human*> validHumans;
	vector<AmbulanceTeam*> freeAmbulances;
	vector<pair<AmbulanceTeam*, Human*>> assignment;

	vector<Building*> onFireBuildings;

	WorldGraph* worldGraph;
	AmbulanceTeam* self;
	WorldModel* world;

	int ignoreCommandsUntil;
	int hpPerception;
};

#endif /* AMBULANCECENTERDECISION_H_ */
