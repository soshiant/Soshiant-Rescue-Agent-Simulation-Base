/*
 * File:   AmbulanceTeamAgent.h
 * Author: Pedram Taheri
 *
 * Created on September 24, 2010, 3:55 PM
 */

#ifndef _AMBULANCETEAMAGENT_H
#define	_AMBULANCETEAMAGENT_H

#include <vector>
#include "../Utilities/FireEstimator/FireEstimator.h"
#include "../Utilities/Geometry/GeometryFunctions.h"
#include "../WorldModel/Objects/AmbulanceTeam.h"
#include "../WorldModel/Objects/Building.h"
#include "../WorldModel/Objects/Civilian.h"
#include "../Utilities/Clustering/KMean.h"
#include "../Utilities/Geometry/Circle.h"
#include "AmbulanceCenterDecision.h"
#include "../Utilities/Debugger.h"

#define NO_THING 2500
#define SCALE 3000

using namespace std;
using namespace Types;

class AmbulanceTeamAgent: public Agent
{
public:
	AmbulanceTeamAgent();
	virtual ~AmbulanceTeamAgent();

protected:
	void actBeforeSense();
	void actBeforeRadar();
	void act();
	void precomputation();
	void globalPrecomputation();
	void exportPreComputeData();
	void noCommAct();
	AmbulanceTeam* self();

private:
	bool checkCaptain();

	bool isLoadingInjured(AmbulanceTeam*);
	bool isReallyAlive(Civilian*);
	bool isGoingToLoad(Civilian*);
	bool isLoaded(Civilian*);

	vector<Human *> validHumans;

	void updateInformation();

	void assign();
	void reAssign();
	void setValidHumans();

	bool rescueTarget(Human* target);
	bool doRescueDuty(Human* target);
	bool doLoadDuty(Civilian* target);
	bool doUnloadDuty();

	int neededAmbulanceCount(Human*, bool);
	int suggestedAmbulanceCount(Human*, bool);

	int setScore(Human*);
	int getFireScore(Human*);
	int getRefugeScore(Human*);
	int getGasStationScore(Human*);
	int getTimeToDeathScore(Human*);
	int getTimeToRescueScore(Human*);
	int getAroundCiviliansScore(Human*);
	int getDistanceToAmbulancesScore(Human*);

	int setRisk(Human* human);

	bool isRescuing(AmbulanceTeam*);
	bool escapeFromGasStation();
	bool isValid(Human*);
	bool waterIsClosed(); //!آب قطع می‌باشد
	bool checkDamage();

	bool moveToRefuge();
	bool moveToRoad();

	vector<AmbulanceTeam *> getNearestInWorkspaceAmbulances(vector<AmbulanceTeam *>, Human*, int);

	void randomWalk();
	void search();
	void initializeSearchTerritory();
	vector<Building*> searchTerritory;

	vector<AmbulanceTeam *> availableAmbulanceTeams;
	vector<Building *> onFireBuildings;
	void setAvailableAmbulanceTeams();
	void setOnFireBuildings();

	AmbulanceTeam * getLoaderAmbulance(Human*);
	void setBestRefuge();
	Refuge* bestRefuge;

	bool dontShas();

	AmbulanceCenterDecision* centerDecisionMaker;
};

#endif	/* _AMBULANCETEAMAGENT_H */
