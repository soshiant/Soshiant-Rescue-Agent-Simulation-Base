#include "Command.h"

#define LOGLEVEL 1

using namespace std;
using namespace Types;
using namespace Geometry;

Command::Command(WorldGraph* worldGraph, WorldModel* world, AbstractConnection *connection)
{
	this->worldGraph = worldGraph;
	this->world = world;
	this->connection = connection;
}

Command::~Command()
{
}

void Command::load(Civilian &target)
{
	load(target, world->getTime());
}

void Command::load(Civilian &target, int time)
{
	LOG(Main, 1) << "Load " << target.getId() << endl;
	((Platoon*) world->self)->myLastCommand = CT_LOAD;
	cm.setAKLoad(world->selfID, time, target.getId());
	message = cm.getMessage();
	connection->sendMessage(message);
}

void Command::unload()
{
	unload(world->getTime());
}

void Command::unload(int time)
{
	LOG(Main, 1) << "Unload" << endl;
	((Platoon*) world->self)->myLastCommand = CT_UNLOAD;
	cm.setAKUnload(world->selfID, time);
	message = cm.getMessage();
	connection->sendMessage(message);
}

void Command::rescue(const Human &target)
{
	rescue(target, world->getTime());
}

void Command::rescue(const Human &target, int time)
{
	LOG(Main, 1) << "Rescue " << target.getId() << endl;
	((Platoon*) world->self)->myLastCommand = CT_RESCUE;
	cm.setAKRescue(world->selfID, time, target.getId());
	message = cm.getMessage();
	connection->sendMessage(message);
}

void Command::rest()
{
	rest(world->getTime());
}

void Command::rest(int time)
{
	LOG(Main, 1) << "Rest" << endl;
	((Platoon*) world->self)->myLastCommand = CT_REST;
	cm.setAKRest(world->selfID, time);
	message = cm.getMessage();
	connection->sendMessage(message);
}

void Command::extinguish(const Building& target, int water)
{
	extinguish(target, water, world->getTime());
}

void Command::extinguish(const Building &target, int water, int time)
{
	LOG(Main, 1) << "Extinguish " << target.getId() << " Fieryness: " << target.getFieryness() << " Temperature: " << target.getTemperature() << " Water: " << water << " Distance: " << world->self->getPos().distFromPoint(target.getPos()) << endl;

	world->buildings[target.buildingIndex]->setLastCycleExtinguished(world->getTime());
	((Platoon*) world->self)->myLastCommand = CT_EXTINGUISH;

	if (world->self->isFireBrigade() && ((FireBrigade *) world->self)->getWaterQuantity() > 0)
	{
		if (water > ((FireBrigade *) world->self)->getWaterQuantity())
		{
			water = ((FireBrigade *) world->self)->getWaterQuantity();
		}
		LOG(Main, 1) << "Command's Water: " << water << endl;
		cm.setAKExtinguish(world->selfID, time, target.getId(), water);
		message = cm.getMessage();
		connection->sendMessage(message);
	}
	else
	{
		rest();
	}
}

void Command::clear(Blockade& target)
{
	clear(target, world->getTime());
}

void Command::clear(Blockade &target, int time)
{
	LOG(Main, 1) << "Clear " << target.getId() << endl;
	((Platoon*) world->self)->myLastCommand = CT_CLEAR;
	target.setLastClearTime(world->getTime());
	cm.setAKClear(world->selfID, time, target.getId());
	message = cm.getMessage();
	connection->sendMessage(message);
}

void Command::clear(int x, int y, int time)
{
	LOG(Main, 1) << "New Clear: [" << x << "]" << "[" << y << "]" << endl;
	((Platoon*) world->self)->myLastCommand = CT_CLEAR;
	cm.setAKClearArea(world->selfID, time, x, y);
	message = cm.getMessage();
	connection->sendMessage(message);
}

void Command::clear(int x, int y)
{
	clear(x, y, world->getTime());
}

void Command::clear(Point target)
{
	clear(target.getX(), target.getY(), world->getTime());
}

void Command::move(std::vector<int>& path)
{
	move(path, -1, -1, world->getTime());
}

void Command::move(std::vector<int>& path, int time)
{
	move(path, -1, -1, time);
}

void Command::move(std::vector<int>& path, int destinationX, int destinationY)
{
	move(path, destinationX, destinationY, world->getTime());
}

void Command::move(vector<int> &path, int destinationX, int destinationY, int time)
{
	LOG(Main, 1) << "Move ";
	for (int i = 0; i < (int) path.size(); i++)
	{
		LOG(Main, 1) << path[i] << ",";
	}
	LOG(Main, 1) << "(" << destinationX << ", " << destinationY << ")" << endl;

	((Platoon*) world->self)->myLastCommand = CT_MOVE;
	((Platoon*) world->self)->setLastMoveCommand(path);
	cm.setAKMove(world->selfID, time, path, destinationX, destinationY);
	message = cm.getMessage();
	connection->sendMessage(message);
}

void Command::speak(int channel, vector<byte> data)
{
	cm.setAKSpeak(world->selfID, world->getTime(), channel, data);
	message = cm.getMessage();
	connection->sendMessage(message);
}

void Command::subscribe(vector<int> channels)
{
	cm.setAKSubscribe(world->selfID, world->getTime(), channels);
	message = cm.getMessage();
	connection->sendMessage(message);
}

void Command::moveToMotionless(int motionlessIndex)
{
	vector<int> path;

	GraphMode graphMode;
	if (world->self->isPoliceForce())
	{
		graphMode = GM_WITHOUT_BLOCKADE;
	}
	else
	{
		graphMode = GM_DEFAULT_NOT_BLOCKED;
	}

	if (graphMode == GM_WITHOUT_BLOCKADE)
	{
		path = getPathToNode(world->motionlessObjects[motionlessIndex]->getWithoutBlockadeRepresentiveNodeIndex(), graphMode);
	}
	else
	{
		path = getPathToNode(world->motionlessObjects[motionlessIndex]->getRepresentiveNodeIndex(), graphMode);
	}

	if (path.size() == (int) 0 || path.back() != (int) world->motionlessObjects[motionlessIndex]->getId())
	{
		path.push_back(world->motionlessObjects[motionlessIndex]->getId());
	}

	move(path);
}

void Command::moveToHuman(int humanIndex, Types::GraphMode graphMode)
{
	vector<int> path;
	if (graphMode == GM_WITHOUT_BLOCKADE)
	{
		if (world->humans[humanIndex]->getWithoutBlockadeRepresentiveNodeIndex() == -1)
		{
			LOG(Main, 1) << "Human " << humanIndex << "'s place is not known or it's not reachable." << endl;
			return;
		}
		path = getPathToNode(world->humans[humanIndex]->getWithoutBlockadeRepresentiveNodeIndex(), graphMode);
	}
	else
	{
		if (world->humans[humanIndex]->getRepresentiveNodeIndex() == -1)
		{
			LOG(Main, 1) << "Human " << humanIndex << "'s place is not known or it's not reachable." << endl;
			return;
		}
		path = getPathToNode(world->humans[humanIndex]->getRepresentiveNodeIndex(), graphMode);
	}
	if (path.size() == (int) 0 || path.back() != (int) world->humans[humanIndex]->getMotionlessObject()->getId())
	{
		path.push_back(world->humans[humanIndex]->getMotionlessObject()->getId());
	}
	move(path, (int) world->humans[humanIndex]->getPos().getX(), (int) world->humans[humanIndex]->getPos().getY());
}

void Command::moveToPoint(int motionlessIndex, Point p, Types::GraphMode graphMode)
{
	vector<int> path;
	if (graphMode == GM_WITHOUT_BLOCKADE)
	{
		int repNode;
		repNode = world->motionlessObjects[motionlessIndex]->getWithoutBlockadeRepresentiveNodeIndex();
		path = getPathToNode(repNode, graphMode);
	}
	else
	{
		int repNode;
		repNode = world->motionlessObjects[motionlessIndex]->getRepresentiveNodeIndex();
		path = getPathToNode(repNode, graphMode);
	}
	if (path.size() == (int) 0 || path.back() != (int) world->motionlessObjects[motionlessIndex]->getId())
	{
		path.push_back(world->motionlessObjects[motionlessIndex]->getId());
	}
	move(path, (int) p.getX(), (int) p.getY());
}

vector<int> Command::getPathToNode(int nodeIndex, Types::GraphMode graphMode)
{
	vector<int> path;
	if (graphMode == GM_WITHOUT_BLOCKADE)
	{
		path = worldGraph->getPath(((Human*) world->self)->getWithoutBlockadeRepresentiveNodeIndex(), nodeIndex, graphMode);
	}
	else
	{
		path = worldGraph->getPath(((Human*) world->self)->getRepresentiveNodeIndex(), nodeIndex, graphMode);
	}
	if (path.size() > 0)
	{
		path = worldGraph->getIDPath(path);
		return path;
	}
	else
	{
		LOG(Main, 1) << "Node " << nodeIndex << " is not reachable." << endl;
		return path;
	}
}
