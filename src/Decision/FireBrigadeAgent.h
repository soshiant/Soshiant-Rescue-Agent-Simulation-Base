#ifndef _FIREBRIGADEAGENT_H
#define _FIREBRIGADEAGENT_H

#include "Agent.h"
#include "../Utilities/FireEstimator/FireEstimator.h"
#include "../Utilities/Debugger.h"
#include "../Utilities/Geometry/Circle.h"
#include "../Utilities/Types.h"
#include "../Utilities/Geometry/GeometryFunctions.h"
#include "../Utilities/Clustering/KMean.h"
#include "../Utilities/Clustering/PizzaClustering.h"
#include "../Utilities/Clustering/Slice.h"
#include "../WorldModel/Objects/Building.h"
#include "../Utilities/HungarianAssignment.h"
#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <cmath>
#include <fstream>
#include <chrono>

#define CLOGLN(text) LOG(Main, 1) << text << endl; cout << text << endl;
#define USE_FIRE_SIMULATOR 1
#define SEGMENT_FIRE_BLOCKS 1
#define SEARCH_CLUSTER 1
#define DISTANCE_SCALE 1000
#define APPROXIMATE_POSITIONING_LCU_1 4
#define APPROXIMATE_POSITIONING_LCU_2 6
#define SEEN_SEARCH_TARGET 4
#define CONTROLABLE_BUILDINGS_BY_AGENT 2
#define BLOCK_TEMPERATURE_LIMIT 10
#define BUILDING_AREA_FOR_BLOCK 0.6
#define TEAM_MATES_DISTANCE 40000
#define STUCK_IN_AREA 15
#define SLICE_FREE_AGENTS 0.75
//#define NEAR_BUILDINGS (AVERAGE_AGENT_SPEED * 10)
#define SUSPICIOUS_SEARCH_NEAR_BUILDING nearBuilding
#define WORTHLESS_SLICE_MAGIC_NUMBER 0.95
#define PIZZA_CENTER_RADIUS 250000
#define CITY_FUCKED_LIMIT 0.5
#define MIN_CYCLE_FOR_CIVILIAN_UPDATE 7
#define FIRE_ESTIMATOR_RESET_TIME 30
#define NEAR_SEARCH_LIMIT 0.4
#define POSITIONINGS_DIFFERENCE_BIG AVERAGE_AGENT_SPEED * 6
#define POSITIONINGS_DIFFERENCE_SMALL AVERAGE_AGENT_SPEED * 8

using namespace std;
using namespace Types;
using namespace Geometry;

struct PositioningResult
{
		MotionlessObject* motionless = nullptr;
		bool isSuccess = false;

		PositioningResult(MotionlessObject* m, bool success) :
				motionless(m), isSuccess(success)
		{
		}
};

class FireBrigadeAgent : public Agent
{
	public:
		FireBrigadeAgent();
		virtual ~FireBrigadeAgent();

	protected:
		void exportPreComputeData();
		void actBeforeSense();
		void actBeforeRadar();
		void precomputation();
		FireBrigade* self();
		void noCommAct();
		void act();

	private:
		/*** SITUATIONs ***/
		bool stuckSituation();
		bool stuckInAreaSituation();
		bool normalSituation();
		bool fuckedUpSituation(bool);

		/*** FIRE BLOCK TOOLS ***/
		void segmentFireBlocks();
		void createFireBlocks();
		void createFieryFireBlocks();
		void dfsOnBuildings(Building*, FireBlock*, bool);
		void setExtinguishability(vector<FireBlock*>&, vector<FireBlock*>&, vector<FireBlock*>&);
		void setFireBlocksSliceIndex();

		/*** ASSIGN TOOLS ***/
		void assignToFireBlocksKilooiy();
		void assignToFireBlocksHelper();
		void assignToFireBlock();
		void assignNearestAgentsToFireBlock(FireBlock*);
		void assignToBuildingsKilooyi();
		void assignToNearBuildings();
		void assignToBuildings();
		void setValue(vector<Building*>&, bool);
		void setValue2(vector<Building*>&, bool);
		void sortFireBlocksKilooiy(vector<FireBlock*>&);
		void sortFireBlocksTeamWork(vector<FireBlock*>&);
		void setNeededAgents(vector<FireBlock*>&);
		Building* getBestBuilding(vector<Building*>&, bool);

		/*** SEARCH TOOLS ***/
		bool searchAroundLastTargets();
		bool isBuildingReachableForSearch(Building*, FireBrigade*);
		bool isBuildingClusterReachableForSearch(Cluster cl, FireBrigade*);
		bool searchSuspiciousBuildings();
		bool setNearRoadSearchTarget();
		bool setRandomSearchTarget();
		bool setSuspiciousSearchTarget();
		bool doesNeedSuspicousSearch(Building*);
		bool isAnyOneCloseToMe();
		bool canISearchSuspiciousBuildings();
		vector<Building*> getSuspiciousBuildings();
		void search();
		void randomWalk();
		void setSearchClusters();
		void assignToSearchCluster();

		/*** POSITIONING ***/
		Road* getBestRoadForPosition(vector<Road*>&);
		PositioningResult position(Building*);
		PositioningResult visibilityPositioning(Building*);
		PositioningResult approximatePositioning(Building*);
		bool canPosition(FireBrigade*, Building*);
		bool moveAndExtinguish(Building*, bool tryToBlock = true);
		bool moveAndExtinguish(vector<Building*>&, bool tryToBlock = true);
		bool extinguishFromHere(Building*, bool tryToBlock = true);
		bool extinguishFromHere(vector<Building*>&, bool tryToBlock = true);
		void hardToExtinguishPlan(Building*);

		/*** UTILITIES ***/
		vector<Building*> getNearExtinguishableBuildings(Building*);
		vector<Building*> getNearExtinguishableBuildingsForBlock(Building*);
		vector<Hydrant*> getAvailableHydrants();
		vector<Refuge*> getAvailableRefuges();
		GasStation* getNearestGasStationTo(Point);
		int getNeededWaterAmount(Building*);
		int getDistanceToGasStations(FireBlock*);
		int getTeamMatesCount(Building*);
		bool SHOW_THE_POWER_OF_SOSHIANT();
		bool assassinVerySuspiciousBuildings();
		bool isVerySuspiciousBuilding(Building*);
		bool isStuckInArea();
		bool isReachableToFireBlock(FireBrigade*, FireBlock*);
		bool doesNeedToExtinguish(Building*, bool);
		bool dontShas1();
		bool dontShas2();
		bool dontShas3();
		bool checkDamage();
		bool fillWaterTank();
		bool fillWaterTank2();
		bool moveToHydrant();
		bool moveToRefuge();
		bool isHardToExtinguish(Building*);
		bool isWorthlessSlice(Slice*);
		bool scanAroundHereForExtinguishingCloseFire();
		bool takeAWalkAround(Building* building);
		bool takeCareOfCivilian();
		void addCenterToSlices();
		void updateSlices();
		void prepareForPizzaStrategy();
		void setIgnoredBuildings();
		void setAverageArea();
		void setSegmentBuildingsCount();
		void setAssignStrategy();
		void assignToPizzaSlices();
		void resetFireBrigadesInformation();
		void setCityCenter();
		void logMapInformation();
		void setBuildingsCells();
		void setFireEstimatorNecessity();
		void setHTELimit();

		/*** VARIABLES ***/
		FireBrigadeAssignStrategy assignStrategy = FBAS_KILOOIY;
		FireSearchMode searchMode = FSM_NONE;
		int maxFireTank = -1, maxWaterForExtinguish = -1, maxSightRange = -1, maxExtinguishDistance = -1;
		int stuckTime = 0;
		int shasingTime = -1;
		int averageArea = 0;
		int nearBuilding = 0;
		int lastCycleAroundSet = numeric_limits<int>::max();
		u_int segmentBuildingsCount = 0;
		u_int hardToExtinguishLimit = 0;
		bool useFireEstimator = false;
		bool fillWater = false;
		bool fillWaterForFun = false;
		Building* cityCenter = nullptr;
		FireEstimator* fireEstimator = nullptr;
		MotionlessObject* searchTarget = nullptr;
		vector<Building*> aroundSearchTargets;
		vector<FireBrigade*> ableAgents;
		vector<Cluster*> searchClusters;
		vector<Slice*> pizzaSlices;
		vector<FireBlock*> extinguishedFireblocks;
		vector<FireBlock*> unSearchedExtinguishedFireblocks;
};

#endif	/* _FIREBRIGADEAGENT_H */
