#ifndef SEARCH_H
#define	SEARCH_H
#include "../WorldModel/WorldModel.h"
#include "../Utilities/Types.h"
#include "../Utilities/Clustering/Cluster.h"
#include "../Utilities/Clustering/KMean.h"
#include "../Utilities/HungarianAssignment.h"
#include <limits.h>
#include <stack>
#define CLUSTER_SIZE 35
#define INVALID_UPDATE_CYCLE 15
#define MINIMUM_VALID_NEIGHBORS 2


using namespace std;
using namespace Types;

const double NEIGHBOR_SEEN_CYCLE = 2;
const double REFUGE_COFF = 1;
const double DISTANCE_COFF = 1;
const double HAS_FIRE_VALU = 5;
const double NEED_UPDATE_VALUE = 7;
const double CLOSE_2_FIRE_VALUE = 5;
const double NOT_SEARCHED_VALUE = 20;
const double SEARCHED_EMPTY_VALUE = 3;
const double INSIDE_CIVILIAN_INDEXES_COFF = 0.5;
const double CLOSE_2_CIVILIANS_SHOUT_VALU = 1.5;

class Search
{
	public:
		Search(WorldModel * worldModel, WorldGraph * worldGraph, SearchMode mode);
		void init(bool createPath);
		bool calculate();
		std::vector<Building*> &getSearchBuildingList();
		bool isValid(Building*);
		bool isValidForCount(Building*);
		bool doAroundFireSearch();
		vector<Road*> findFourLegacyRoads(Building*);
		bool prepareForSouspeciouseSearch();
		void hungarianAssignment();
		void greedySearchClusterToPolicForceAssignment();

		void test();
	private:
		bool air = false;
		WorldModel * world;
		WorldGraph * worldGraph;
		SearchMode mode;

		vector<Building *> searchBuildingList;
		vector<Cluster*> clusters;
		vector<Cluster*> aroundFireClusters;
		vector<Cluster*> souspiciousClusters;

		Cluster* bestCluster = nullptr;
		Cluster* myAroundFireCluster = nullptr;
		Cluster* mySouspiciouseCluster = nullptr;
		vector<Road*> sousTargetLegacyRoads;

		PoliceForce * self();
		vector<Cluster*>& myClusters();
		vector<vector<int>> graphDistanceTable;
		vector<vector<int>> airDistanceTable;

		// Simulated Annealing
		void simulatedAnnealing();
		vector<Building*> getNewOrder(const vector<Building*> & permutation);
		int getTotalDistance(vector<Building*>& permutation, bool airDistance);

		//Path & TableDistance
		void createNearestPath();
		void FloydWarshal();
		void creatGraphDistanceTable();

		//AroundFireSearch // قرار نیست از این استفاده بشه !!!!! :@@@@@@@ #!@@@@@@@@@@@@@@@@@@@@@@@@
		bool setMyAroundFireTarget();
		bool setAroundFireClusters();
		bool setMyAroundFireCluster();
		bool deleteAroundFireClusters();
		bool isMyAroundFireTargetValid();
		bool isItAroundFireBuilding(Building*);
		bool isAroundFireClusterValid(Cluster*);


		//SouspiciouseSearch
		bool isItSouspiciouseBuilding(Building*);
		bool isItValidSouspiciouseNeighbor(Building*);
		bool setSouspiciouseClusters();
		bool deleteSouspociouseClusters();
		bool setMySouspiciouseCluster();
		bool setMySouspiciouseTarget();

		//Fast Faget Search
		bool fastFagetSearch();

		// Actions
		bool setMyTarget();
		void creatPathClusters();
		void setKmeansClusters();
		void randomAssignment();
		bool setBestCluster();
		void calculateDistanceTables();
		bool isValid(Cluster*);
		bool fullySearched(Cluster* sc);
		void setScore(Cluster*);
		void setNeighborsScore(Cluster* sc);
		Building* getNearestValidBuildingInBestCluster();
		static bool compareSearchCluster(Cluster* sc1, Cluster* sc2);
};

#endif	/* SEARCH_H */
