#include "Agent.h"

#define LOGLEVEL 1

Agent::Agent()
{
	startupConnectTime = 180000;
	isPrecomputeAgent = false;
	ignoreCommandsUntil = 3;
	damagePerception = 100;
	agentThinkTime = 1000;
	hpPerception = 1000;
#if IS_STATIC
	optimusViewer = nullptr;
	viewerThread = nullptr;
#endif
	doOffline = false;
	worldGraph = nullptr;
	type = ET_UNKNOWN;
	connection = nullptr;
	command = nullptr;
	world = nullptr;
	radar = nullptr;
	id = -1;
//	fbViewer.push_back(0);
//	atViewer.push_back(0);
	pfViewer.push_back(0);
}

Agent::~Agent()
{
#if IS_STATIC
	viewerThread->interrupt();
	delete viewerThread;
	delete optimusViewer;
#endif
}

void Agent::initializeConfig()
{
	string configTopic;
	string fileCounterValue;
	config.add("Config_FileAdresses");
	config["Config_FileAdresses"].setByFile("./Config/Config_FileAddresses.conf");
	int setFileCounter = config["Config_FileAdresses"]["DefaultSet"]["ConfigurationDataFileCount"][0].as<float>();
	int getFileCounter = setFileCounter;

	for (int fileCounter = 1; fileCounter <= getFileCounter; fileCounter++)
	{
		fileCounterValue = Str(fileCounter).as<string>();
		configTopic = config["Config_FileAdresses"]["ConfigurationDataFileName"][fileCounterValue][0].as<string>();
		config.add(configTopic);
		string file = "./Config/" + configTopic + ".conf";
		config[configTopic].setByFile("./Config/" + configTopic + ".conf");
		WarnLoadingTrue(" Loading Config_FileAdresses File: " + file);
	}
}

void Agent::initializeLogger(EntityType type, int id)
{
	string agent;

	switch (type)
	{
		case ET_FIRE_BRIGADE:
			agent = "FireBrigade";
			break;
		case ET_FIRE_STATION:
			agent = "FireStation";
			break;
		case ET_AMBULANCE_TEAM:
			agent = "AmbulanceTeam";
			break;
		case ET_AMBULANCE_CENTER:
			agent = "AmbulanceCenter";
			break;
		case ET_POLICE_FORCE:
			agent = "PoliceForce";
			break;
		case ET_POLICE_OFFICE:
			agent = "PoliceOffice";
			break;
		default:
			agent = "Agent";
			break;
	}

	agent += "-";

	const Timer * mTimer = new OnlineTimer();
	string logDir = config["LoggerInitialize"]["Logger"]["Directory"].asString();

	if (*logDir.rbegin() != '/')
		logDir += '/';

	if (config["LoggerInitialize"]["Logger"]["LogMain"].asBool())
	{
		logger.add("Main", new LogFile(logDir + agent + string(Str(id).as<string>()) + ".log", mTimer));
		mkdir(logDir.c_str(), 16877);
	}
	else
		logger.add("Main", new LogDevNull());

	if (config["LoggerInitialize"]["Logger"]["Offline"].asBool() && !config["ServerInitialize"]["Server"]["OfflinePlaying"].asBool() && doOffline)
		logger.add("MainOff", new LogFile(logDir + "/Offline/" + agent + string(Str(id).as<string>()) + ".log.offline", mTimer), false);
	else
		logger.add("MainOff", new LogDevNull(), false);
}

void Agent::setConnectionParameters(string ip, int port)
{
	if (ip.length() > 3)
	{
		config["ServerInitialize"]["Connection"]["HostName"].setString(ip);
	}

	if (port != 0)
	{
		config["ServerInitialize"]["Connection"]["Port"].setInt(port);
	}
}

bool Agent::shouldOpenViewer()
{
//	return true;
	if (self()->isFireBrigade())
	{
		if (find(fbViewer.begin(), fbViewer.end(), ((FireBrigade*) self())->fireBrigadeIndex) != fbViewer.end()
				|| find(fbViewer.begin(), fbViewer.end(), self()->getId()) != fbViewer.end())
		{
			return true;
		}
	}
	else if (self()->isAmbulanceTeam())
	{
		if (find(atViewer.begin(), atViewer.end(), ((AmbulanceTeam*) self())->ambulanceTeamIndex) != atViewer.end()
				|| find(atViewer.begin(), atViewer.end(), self()->getId()) != atViewer.end())
		{
			return true;
		}
	}
	else if (self()->isPoliceForce())
	{
		if (find(pfViewer.begin(), pfViewer.end(), ((PoliceForce*) self())->policeForceIndex) != pfViewer.end()
				|| find(pfViewer.begin(), pfViewer.end(), self()->getId()) != pfViewer.end())
		{
			return true;
		}
	}
	else if (self()->isAmbulanceCenter())
	{
		return false;
	}

	return false;
}

void Agent::run(double coeff)
{
	// implementation in child classes
	cout << "Agent Running With ID = " << this->id << endl;
	LOG(Main, 1) << "Soshiant Agent started with id: " << this->id << endl;

	this->agentThinkTime = atoi(world->config[Encodings::getConfigType(CT_THINK_TIME)].c_str());
	this->ignoreCommandsUntil = atoi(world->config[Encodings::getConfigType(CT_IGNORE_UNTIL)].c_str());
	this->damagePerception = atoi(world->config[Encodings::getConfigType(CT_DAMAGE_PERCEPTION)].c_str());
	this->hpPerception = atoi(world->config[Encodings::getConfigType(CT_HP_PERCEPTION)].c_str());
	this->optimizedCoeff = coeff;

	globalPrecomputation();
	precomputation();
	radar->init();
	sendAKAcknowledge();
	LOG(Main, 1) << "after AK Acknowledge! " << endl;
	vector<byte> message;

#if OPEN_VIEWER && IS_STATIC
	if (shouldOpenViewer())
	{
		optimusViewer = new viewer::OptimusViewer(world, worldGraph);
		viewerThread = new boost::thread(boost::bind(&viewer::OptimusViewer::start, optimusViewer));
	}
#endif

	while (true)
	{
		auto start = chrono::system_clock::now();
		message.clear();

		if (!connection->getMessage(message))
		{
			cout << "Agent With ID " << id << " Disconnected" << endl;
			LOG(Main, 1) << "I'm Disconnected !" << endl;
			connection->done();
			break;
		}

		auto connectionTime = chrono::system_clock::now();

		if (doOffline)
		{
			LOGL(MainOff)<< message.size() << endl;
			for (int i = 0; i < (int) message.size(); i++)
			{
				LOGL(MainOff) << message[i];
			}
			LOGL(MainOff) << flush;
		}

		if (config["LoggerInitialize"]["Logger"]["LogMain"].asBool())
		{
			((Timer*) (LOGL(Main).timer()))->resetCycleTime();
		}

		LOG(Main, 1) << endl << "[" << world->getTime() << "]" << endl;

#if OPEN_VIEWER && IS_STATIC
		if (optimusViewer != nullptr)
		{
			optimusViewer->mutex.lock();
		}
#endif

		auto actBeforeSenseTime = chrono::system_clock::now();
		actBeforeSense();
		chrono::duration<double> elapsedActBeforeSense = chrono::system_clock::now() - actBeforeSenseTime;
		LOG(Main, 1) << "Act before sense: " << elapsedActBeforeSense.count() << "s" << endl;

		auto worldModel = chrono::system_clock::now();
		string result = world->update(message);
		LOG(Main, 1) << "WorldModel updated!" << endl;

		if (world->self->isHuman() && ((Human *) world->self)->getHp() == 0 && world->getTime() > ignoreCommandsUntil + 5)
		{
			LOG(Main, 1) << "I am fucking dead! :'( fuck this world" << endl;
			int * killme = nullptr;
			cout << "I am fucking dead! :'( fuck this world" << endl;
			cout << "GoOd ByE!" << endl;
			cout << *killme << endl;
		}
		chrono::duration<double> elapsedWorldModel = chrono::system_clock::now() - worldModel;
		LOG(Main, 1) << "WorldModel: " << elapsedWorldModel.count() << "s" << endl;

		if (self()->isBuilding() == false)
		{
			auto worldgraph = chrono::system_clock::now();
			worldGraph->update();
			LOG(Main, 1) << "worldGraph updated!" << endl;
			chrono::duration<double> elapsedWorldGraph = chrono::system_clock::now() - worldgraph;
			LOG(Main, 1) << "WorldGraph: " << elapsedWorldGraph.count() << "s" << endl;
		}

		actBeforeRadar();

		auto radarTime = chrono::system_clock::now();
		if (!message.empty() && world->getTime() > ignoreCommandsUntil)
		{
			radar->subscribe();
			LOG(Main, 1) << "before share sense " << endl;
			radar->shareSenseWorldAndSendOrders();
			LOG(Main, 1) << "after share sense " << endl;
		}
		chrono::duration<double> elapsedRadar = chrono::system_clock::now() - radarTime;
		LOG(Main, 1) << "Radar: " << elapsedRadar.count() << "s" << endl;

		auto actTime = chrono::system_clock::now();
		if (radar->getRadarMode() == RM_NO_COMMUNICATION || radar->getRadarMode() == RM_LOW)
		{
			noCommAct();
		}
		else
		{
			act();
		}
		chrono::duration<double> elapsedAct = chrono::system_clock::now() - actTime;
		LOG(Main, 1) << "Act: " << elapsedAct.count() << "s" << endl;

		if (world->getTime() > ignoreCommandsUntil)
		{
			radar->shareSenseWorldAfterDecision();
		}

#if OPEN_VIEWER && IS_STATIC
		if (optimusViewer != nullptr)
		{
			optimusViewer->mutex.unlock();
		}
#endif

		chrono::duration<double> elapsed = chrono::system_clock::now() - start;
		LOG(Main, 1) << "WholeCycle: " << elapsed.count() << "s" << endl;

		chrono::duration<double> elapsedTeam = chrono::system_clock::now() - connectionTime;
		LOG(Main, 1) << "TeamWholeCycle: " << elapsedTeam.count() << "s" << endl;

//		std::this_thread::sleep_for(std::chrono::milliseconds(100));
//		if (world->getTime() >= 95 && world->getTime() < 100)
//		{
//			std::this_thread::sleep_for(std::chrono::seconds(5));
//		}
	}
}

void Agent::actBeforeSense()
{
}

void Agent::actBeforeRadar()
{
}

void Agent::act()
{
}

void Agent::noCommAct()
{
}

void Agent::sendAKAcknowledge()
{
	Message connectionMessage;
	vector<byte> message;
	connectionMessage.setAKACKnowledge(1, world->selfID);
	message = connectionMessage.getMessage();
	connection->sendMessage(message);
}

int Agent::initConnection(EntityType type, int offlineId)
{
	if (offlineId == -1)
	{
		connection = new OnlineConnection(config["ServerInitialize"]["Connection"]["HostName"].asString(), config["ServerInitialize"]["Connection"]["Port"].asInt());
		doOffline = true;
	}
	else
	{
		doOffline = false;
		connection = new OfflineConnection(getOfflineFileDir(type, offlineId));
	}

	if (!connection->init())
	{
		cerr << "Connection Error;" << endl;
		this->id = -2;
		return -2;
	}

	Message connectionMessage;
	vector<byte> message;
	connectionMessage.setAKConnect(type, 1);
	message = connectionMessage.getMessage();
	connection->sendMessage(message);
	connection->getMessage(message);

	world = new WorldModel(config);
	string analyzeResult = world->init(message, type, isPrecomputeAgent);
	if (analyzeResult == "Receive KA_CONNECT_OK")
	{
		this->id = world->selfID;
		initializeLogger(type, this->id);
		if (doOffline)
		{
			LOGL(MainOff)<< message.size() << endl;

			for (int i = 0; i < (int) message.size(); i++)
			{
				LOGL(MainOff) << message[i];
			}

			LOGL(MainOff) << flush;
		}

		this->type = type;
		this->startupConnectTime = atoi(world->config[Encodings::getConfigType(CT_STARTUP_CONNECT_TIME)].c_str());

		return 0;
	}
	else
	{
		this->id = -3;
		return -3;
	}
}

string Agent::getOfflineFileDir(EntityType et, int id)
{
	string agent;
	string logDir = config["LoggerInitialize"]["Logger"]["Directory"].asString();

	agent = logDir + "/Offline/";

	switch (et)
	{
		case ET_FIRE_BRIGADE:
			agent += "FireBrigade";
			break;
		case ET_FIRE_STATION:
			agent += "FireStation";
			break;
		case ET_AMBULANCE_TEAM:
			agent += "AmbulanceTeam";
			break;
		case ET_AMBULANCE_CENTER:
			agent += "AmbulanceCenter";
			break;
		case ET_POLICE_FORCE:
			agent += "PoliceForce";
			break;
		case ET_POLICE_OFFICE:
			agent += "PoliceOffice";
			break;
		default:
			agent += "Agent";
			break;
	}

	agent += "-" + string(Str(id).as<string>()) + ".log.offline";

	return agent;
}
// return value: 0=ok, -2=connection error, -3=no more agents

void Agent::initData()
{
	initializeConfig();
}

RCRObject* Agent::self()
{
	return world->self;
}
