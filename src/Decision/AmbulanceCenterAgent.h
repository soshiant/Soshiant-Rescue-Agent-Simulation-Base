#ifndef _AMBULANCECENTERAGENT_H
#define _AMBULANCECENTERAGENT_H

#include "Agent.h"
#include "../Utilities/Types.h"
#include "../Utilities/HungarianAssignment.h"
#include "../Utilities/FireEstimator/FireEstimator.h"

class AmbulanceCenterAgent: public Agent
{
public:
	AmbulanceCenterAgent();
	virtual ~AmbulanceCenterAgent();
protected:
	void actBeforeSense();
	void actBeforeRadar();
	void act();
	void precomputation();
	void noCommAct();
	AmbulanceCenter* self();

private:
};

#endif	/* _AMBULANCECENTERAGENT_H */

