#include "AmbulanceCenterAgent.h"

#define LOGLEVEL 1

#define SCALE 3000

AmbulanceCenterAgent::AmbulanceCenterAgent()
{
}

AmbulanceCenterAgent::~AmbulanceCenterAgent()
{
}

/********************** Precompute **********************/

void AmbulanceCenterAgent::precomputation()
{
	Agent::precomputation();
}

/********************** Decision **********************/

void AmbulanceCenterAgent::actBeforeSense()
{

}

void AmbulanceCenterAgent::actBeforeRadar()
{
}

void AmbulanceCenterAgent::noCommAct() //You have no power here :-|
{
}

void AmbulanceCenterAgent::act()
{
}

AmbulanceCenter* AmbulanceCenterAgent::self()
{
	return (AmbulanceCenter*) world->self;
}
