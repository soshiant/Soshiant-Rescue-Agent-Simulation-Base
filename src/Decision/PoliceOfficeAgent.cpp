#include "PoliceOfficeAgent.h"
#include "PoliceForceAgent.h"

#define LOGLEVEL 1

PoliceOfficeAgent::PoliceOfficeAgent()
{
}

PoliceOfficeAgent::~PoliceOfficeAgent()
{
}

void PoliceOfficeAgent::actBeforeSense()
{

}

void PoliceOfficeAgent::actBeforeRadar()
{
}

void PoliceOfficeAgent::act()
{
	LOG(Main , 1) << "I am police office!" << endl;
	if (world->getTime() < ignoreCommandsUntil)
	{
		return;
	}
}

void PoliceOfficeAgent::noCommAct()
{
}

void PoliceOfficeAgent::precomputation()
{
	Agent::precomputation();
}
