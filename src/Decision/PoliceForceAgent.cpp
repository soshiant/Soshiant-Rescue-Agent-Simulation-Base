/*
 * File:   PoliceForceAgent.cpp
 * Author: Pedram
 *
 * Created on روزی که ادیتور اکلیپس نبوده!
 */

#include "PoliceForceAgent.h"

#define LOGLEVEL 1
#define STUCK_REFRESH_INTERVAL 50
#define DECISION_CYCLE_TIME 30
#define NEAR_TARGET_DISTANCE (2 * AVERAGE_AGENT_SPEED)
#define MAX_POLICES_FOR_A_FIRE_BLOCK 7
#define AROUND_ROADS_FOR_EACH_POLICE 10

//fire block clearing
#define FIRE_BLOCK_CLEARING_ROADS_DISTANCE_COEFFICIENT (-1 / 10000.)
#define ARARO_ASSIGNMENT_COEFFICIENT (2 / 3.)
#define HYDRANT_SCORE 50
#define FIRE_BLOCK_CLEARING_CANSEE_COEFFICIENT 20

// Move & Clear
#define NEAR_BLOCKADES_TO_CLEAR_AREA_BIG_MAP 35000
#define NEAR_BLOCKADES_TO_CLEAR_AREA_SMALL_MAP 25000
#define ACCEPTABLE_ANGLE_VARIANCE_1 (PI / 5.f)
#define ACCEPTABLE_ANGLE_VARIANCE_2 (PI / 6.f)

using namespace Types;
using namespace std;
using namespace Geometry;

bool intComparator(int a, int b)
{
	return a > b;
}

bool fireBlockValueComparator(FireBlock * a, FireBlock * b)
{
	return a->getValue() > b->getValue();
}

bool buildingValueComparator(Building* a, Building* b)
{
	return a->getValue() > b->getValue();
}

PoliceForceAgent::PoliceForceAgent()
{
	maxClearDistance = 10000;
	mySearchTarget = nullptr;
	advanceSearch = nullptr;
	fireEstimator = nullptr;
	clearWidth = 1250;
	repairRate = 15;

	lastRepNodeIndex[0] = -1;
	lastRepNodeIndex[1] = -1;
	lastRepNodeIndex[2] = -1;

	srand(RANDOM_SEED);
}

PoliceForceAgent::~PoliceForceAgent()
{
	delete mySlice;
}

void PoliceForceAgent::precomputation()
{
	if (isPrecomputeAgent)
	{
		Agent::precomputation();

		LOG(Main, 1) << "CanSee started(PF)" << endl;
		cout << "CanSee started!(PF)" << endl;
		world->setCanseeBuildings();
		cout << "CanSee finished!(PF)" << endl;
		LOG(Main, 1) << "CanSee finished (PF)" << endl;

		LOG(Main, 1) << "InSight started" << endl;
		cout << "InSight started!" << endl;
		world->setInSightPointsBuildings();
		cout << "InSight finished!" << endl;
		LOG(Main, 1) << "InSight finished" << endl;

		fireEstimator = new FireEstimator(world);
		for (Building* building : world->buildings)
		{
			fireEstimator->simulatedWorld->setBuildingCells(building);
		}

		advanceSearch = new Search(world, worldGraph, SM_POLICE_FORCE);
		advanceSearch->init(true);
		world->computeSearchAreaAndOtherStuff();
		exportPreComputeData();
	}
	else
	{
		world->setBuildingsWalls();

		fireEstimator = new FireEstimator(world);

		importPrecomputationData();
	}

	maxClearDistance = atoi(world->config[Encodings::getConfigType(CT_CLEAR_DISTANCE)].c_str());
	repairRate = atoi(world->config[Encodings::getConfigType(CT_REPAIR_RATE)].c_str());
	clearWidth = atoi(world->config[Encodings::getConfigType(CT_CLEAR_RADIUS)].c_str());

	// FIXME Ha?!??
	LOG(Main, 1) << "First Time Motionlesses!" << endl;
	for (int i = 0; i < (int) world->platoons.size(); i++)
	{
		world->platoons[i]->getMotionlessObject()->addFirstTimeInnerPlatoonIndex(world->platoons[i]->platoonIndex);
		world->platoons[i]->setFirstTimeMotionless(world->platoons[i]->getMotionlessObject());

		world->platoons[i]->positionHistory.clear();

		LOG(Main , 1) << world->platoons[i]->getMotionlessObject()->getId() << ",";
	}
	LOG(Main, 1) << endl;

	if (radar->getRadarMode() == RM_LOW || radar->getRadarMode() == RM_NO_COMMUNICATION)
	{
		int pidginCount = world->isMapLarge() ? 2 : 1;
		if (world->getMapName() == MN_EINDHOVEN || world->getMapName() == MN_MEXICO || world->getMapName() == MN_BERLIN)
		{
			pidginCount++;
		}

		u_int i = 0, j = 0;
		while (j < pidginCount && i < world->policeForces.size())
		{
			i++;
			if (world->policeForces[i]->getMotionlessObject()->isBuilding())
			{
				continue;
			}

			world->policeForces[i]->job = PFJ_PIDGIN;
			j++;
		}

		if (self()->job == PFJ_PIDGIN)
		{
			LOG(Main, 1) << "I'm pidgin and I know it" << endl;
			createPidginPath();
		}

		for (Platoon* p : world->platoons)
		{
			p->setRepresentiveNodeIndex(-1);
		}
	}

	orderPizza();

	if (world->isMapLarge())
	{
		inRangeDistance = hypot(world->getWorldBound().getWidth(), world->getWorldBound().getLength()) / 6;
	}
	else
	{
		inRangeDistance = hypot(world->getWorldBound().getWidth(), world->getWorldBound().getLength()) / 5;
	}

	LOG(Main, 1) << "Precomputation has completed (PF)" << endl;
	cout << "Precomputation has completed (PF)" << endl;
}

void PoliceForceAgent::exportPreComputeData()
{
	fstream PFData;
	PFData.open(getPrecomputationFileAddress(type).c_str(), fstream::out);
    Agent::exportPrecomputationData(PFData);
	PFData << endl;
	PFData << endl << Encodings::getPrecomputationTagType(PCDT_CANSEE) << endl << endl;
	for (u_int i = 0; i < world->buildings.size(); i++)
	{
		PFData << world->buildings[i]->canSee.size() << endl;
		for (u_int j = 0; j < world->buildings[i]->canSee.size(); j++)
		{
			PFData << world->buildings[i]->canSee[j]->roadIndex << " ";
		}

		PFData << endl;
	}

	PFData << Encodings::getPrecomputationTagType(PCDT_INSIGHT) << endl;
	for (u_int i = 0; i < world->buildings.size(); i++)
	{
		PFData << world->buildings[i]->getInSightBuildings().size() << endl;
		for (u_int j = 0; j < world->buildings[i]->getInSightBuildings().size(); j++)
		{
			PFData << world->buildings[i]->getInSightBuildings()[j]->buildingIndex << "\n" << std::fixed
					<< world->buildings[i]->getInSightBuildings()[j]->value << "\n";
		}
	}

	PFData << Encodings::getPrecomputationTagType(PCDT_CELLS) << endl;
	for (Building* building : world->buildings)
	{
		PFData << building->getEstimatedData()->getCells().size() << endl;
		for (array<int, 3> arr : building->getEstimatedData()->getCells())
		{
			for (int n : arr)
			{
				PFData << n << " ";
			}
			PFData << "\n";
		}
	}

	PFData << Encodings::getPrecomputationTagType(PCDT_SEARCH_PATH) << endl;
	for (Building* building : advanceSearch->getSearchBuildingList())
	{
		PFData << building->buildingIndex << " ";
	}
	PFData << endl;

	if (world->buildings[0]->searchArea != nullptr)
	{
		PFData << Encodings::getPrecomputationTagType(PCDT_SEARCH_AREA) << endl;
		for (Building* building : world->buildings)
		{
			PFData << building->buildingIndex << " ";
			PFData << building->searchArea->seachShapes.size() << " ";
			for (SearchShape* sp : building->searchArea->seachShapes)
			{
				PFData << sp->searchMotionless->motionlessIndex << " ";
				PFData << sp->searchPoints.size() << " ";
				for (Point p : sp->searchPoints)
				{
					PFData << p.getX() << " " << p.getY() << " ";
				}
			}
			PFData << endl;
		}

		PFData << endl;
	}

	PFData << Encodings::getPrecomputationTagType(PCDT_DATA_END);
	PFData.close();
}

void PoliceForceAgent::actBeforeSense()
{
	if (useFireEstimator)
	{
		LOG(Main, 1) << "Estimate Once" << endl;
		fireEstimator->estimateOnce(); // Estimate this cycle's fiery buildings
	}
}

void PoliceForceAgent::actBeforeRadar()
{
}

void PoliceForceAgent::act()
{
	LOG(Main , 1) << "I am policeforce!" << endl;

	updateLastRepNodeIndex();

	if (world->getTime() < ignoreCommandsUntil)
	{
		return;
	}

	fillTheBlacklists();

	if (dontShas())
	{
		LOG(Main, 1) << "I'm shasing like a bitch :|" << endl;
		return;
	}

	if (checkDamage())
	{
		LOG(Main, 1) << "I'm damaged like a raped girl!!" << endl;
		return;
	}

	if (pidgin())
	{
		LOG(Main, 1) << "I'm pidgin!!" << endl;
		return;
	}

	updateStuckAgents();

	if (self()->job == PFJ_SPECIAL_FORCE)
	{
		LOG(Main, 1) << "I'm a special man!" << endl;
		if (specialWorks())
		{
			LOG(Main, 1) << "I did special things!" << endl;
			return;
		}
	}
	else
	{
		LOG(Main, 1) << "I'm a normal guy!" << endl;
		if (normalWorks())
		{
			LOG(Main, 1) << "I did ordinary things!" << endl;
			return;
		}
	}

	if (fastSearch())
	{
		return;
	}

	//TODO Change Slices
	beTheDomGuy();

}

void PoliceForceAgent::beTheDomGuy()
{

	int minDistance = numeric_limits<int>::max() ,tmpDist;
	MotionlessObject* mo = nullptr;
	for (Civilian* civ : world->civilians)
	{
		if (!civ->isAvailable)
		{
			continue;
		}

		if (world->getTime() - civ->getLastCycleUpdated() < 20)
		{
			continue;
		}

		if (worldGraph->isReachable(self()->getWithoutBlockadeRepresentiveNodeIndex() ,civ->getWithoutBlockadeRepresentiveNodeIndex() ,
				GM_DEFAULT_BLOCKED))
		{
			continue;
		}

		if (minDistance < (tmpDist = worldGraph->getDistance(self()->getWithoutBlockadeRepresentiveNodeIndex(), civ->getWithoutBlockadeRepresentiveNodeIndex(),
				GM_DEFAULT_NOT_BLOCKED)))
		{
			minDistance = tmpDist;
			mo = civ->getMotionlessObject();
		}
	}


	if (mo == nullptr)
	{
		u_int size = self()->getMotionlessObject()->closerThan50R.size();
		mo = self()->getMotionlessObject()->closerThan50R[rand()% size];
	}

	moveAndClear(mo);
}

void PoliceForceAgent::orderPizza()
{
	buriedPizzaSlices.clear();
	healthyPizzaSlices.clear();

	// Find agents which have buriedness and those who don't have!
	vector<PoliceForce*> buriedPolices, healthyPolices;
	for (PoliceForce* pf : world->policeForces)
	{
		if (pf->job == PFJ_PIDGIN)
		{
			continue;
		}

		if (pf->getMotionlessObject()->isBuilding() || pf->getBuriedness() > 0)
		{
			buriedPolices.push_back(pf);
			pf->buriedAssign = true;
		}
		else
		{
			healthyPolices.push_back(pf);
			pf->buriedAssign = false;
		}
	}

	// Special Guys
	chooseSpecialForces(healthyPolices);
	for (u_int i = 0; i < healthyPolices.size(); i++)
	{
		if (healthyPolices[i]->job == PFJ_SPECIAL_FORCE)
		{
			healthyPolices.erase(healthyPolices.begin() + i);
			i--;
		}
	}

	PizzaClustering<MotionlessObject> pc(world->motionlessObjects, buriedPolices.size());

	// Buried Forces
	if (buriedPolices.size() > 0)
	{
		pc.calculate();

		for (Cluster* cluster : pc.getClusters())
		{
			Slice* slice = new Slice;
			slice->setIndex(buriedPizzaSlices.size());
			for (MotionlessObject* mo : cluster->getMembers())
			{
				if (mo->isBuilding())
				{
					slice->addBuilding((Building*) mo);
				}
				else
				{
					slice->addRoad((Road*) mo);
				}
			}
			buriedPizzaSlices.push_back(slice);
		}

		assignToSlices(buriedPolices, buriedPizzaSlices);
	}

	if (healthyPolices.size() > 0)
	{
		// Healthy Forces
		pc.setNumberOfClusters(healthyPolices.size());
		pc.calculate();

		for (Cluster* cluster : pc.getClusters())
		{
			Slice* slice = new Slice;
			slice->setIndex(healthyPizzaSlices.size());
			for (MotionlessObject* mo : cluster->getMembers())
			{
				if (mo->isBuilding())
				{
					slice->addBuilding((Building*) mo);
				}
				else
				{
					slice->addRoad((Road*) mo);
				}
			}
			healthyPizzaSlices.push_back(slice);
		}

		assignToSlices(healthyPolices, healthyPizzaSlices);
	}

	if (self()->job == PFJ_SPECIAL_FORCE)
	{
		mySlice = new Slice;
		mySlice->getBuildings().insert(mySlice->getBuildings().begin(), world->buildings.begin(), world->buildings.end());
		mySlice->getRoads().insert(mySlice->getRoads().begin(), world->roads.begin(), world->roads.end());
	}
	else
	{
		if (self()->buriedAssign)
		{
			mySlice = buriedPizzaSlices[self()->pizzaSlice];
		}
		else
		{
			mySlice = healthyPizzaSlices[self()->pizzaSlice];
		}
	}

	world->pizzaSlices = healthyPizzaSlices;
}

void PoliceForceAgent::assignToSlices(vector<PoliceForce*> agents, vector<Slice*> slices)
{
	vector<vector<int>> costMatrix;
	costMatrix.resize(agents.size());

	for (u_int y = 0; y < costMatrix.size(); y++)
	{
		costMatrix[y].resize(costMatrix.size());
		for (u_int x = 0; x < costMatrix[y].size(); x++)
		{
			costMatrix[y][x % slices.size()] = -slices[x % slices.size()]->getGroundDistanceToEntity(agents[y]->getMotionlessObject(),
					worldGraph, GM_WITHOUT_BLOCKADE);
		}
	}

	HungarianAssignment hungarianAss(costMatrix);

	vector<vector<bool>> assignment = hungarianAss.hungarian();
	for (u_int y = 0; y < assignment.size(); y++)
	{
		for (u_int x = 0; x < assignment[y].size(); x++)
		{
			if (assignment[y][x])
			{
				slices[x % slices.size()]->assignAgentToSlice((Platoon*) agents[y]);
			}
		}
	}
}

bool PoliceForceAgent::isInSlice(MotionlessObject* object, Slice* slice)
{
	if (object->isRoad())
	{
		if (find(slice->getRoads().begin(), slice->getRoads().end(), (Road*) object) != slice->getRoads().end())
		{
			return true;
		}
	}
	else
	{
		if (find(slice->getBuildings().begin(), slice->getBuildings().end(), (Building*) object) != slice->getBuildings().end())
		{
			return true;
		}
	}

	return false;
}

void PoliceForceAgent::chooseSpecialForces(vector<PoliceForce*> candidateForces)
{
	// 1/4 of forces will be chosen (1 person for center)
	specialForces.clear();
	const int SPECIAL_FORCES_COUNT = (int) round(candidateForces.size() / 4);

	// First we find nearest agent to map's center, He's very special!
	int nearestToCenterIndex = -1, minDistance = numeric_limits<int>::max(), tmpDistance;
	for (u_int i = 0; i < candidateForces.size(); i++)
	{
		tmpDistance = candidateForces[i]->getPos().distFromPoint(world->getCityCenter());
		if (tmpDistance < minDistance)
		{
			minDistance = tmpDistance;
			nearestToCenterIndex = i;
		}
	}
	if (nearestToCenterIndex == -1)
	{
		LOG(Main, 1) << "FUCK! No special guy can be chosen here!" << endl;
		return;
	}
	else
	{
		specialForces.push_back(candidateForces[nearestToCenterIndex]);
		candidateForces[nearestToCenterIndex]->job = PFJ_SPECIAL_FORCE;
		candidateForces.erase(candidateForces.begin() + nearestToCenterIndex);
	}

	if (SPECIAL_FORCES_COUNT <= 1)
	{
		return;
	}

	// We're going to find special guys by slicing and assigning. This step help us to spread special buddies in city!
	PizzaClustering<MotionlessObject> pc(world->motionlessObjects, SPECIAL_FORCES_COUNT - 1);
	pc.calculate();

	vector<Slice*> slices;
	for (Cluster* cluster : pc.getClusters())
	{
		Slice* slice = new Slice;
		slice->setIndex(slices.size());
		for (MotionlessObject* mo : cluster->getMembers())
		{
			if (mo->isBuilding())
			{
				slice->addBuilding((Building*) mo);
			}
			else
			{
				slice->addRoad((Road*) mo);
			}
		}
		slices.push_back(slice);
	}

	// Assign Step!
	for (Slice* slice : slices)
	{
		int nearestAgentIndex = -1, minDist = numeric_limits<int>::max(), tmpDist;
		for (u_int i = 0; i < candidateForces.size(); i++)
		{
			tmpDist = slice->getAirDistanceToEntity(candidateForces[i]->getPos());
			if (minDist > tmpDist)
			{
				minDist = tmpDist;
				nearestAgentIndex = i;
			}
		}

		if (nearestAgentIndex == -1)
		{
			LOG(Main, 1) << "WTF?! nearestAgentIndex == -1 ?! R u fuckin kidding me?!" << endl;
			specialForces.push_back(candidateForces[nearestAgentIndex]);
			candidateForces[nearestAgentIndex]->job = PFJ_SPECIAL_FORCE;
			candidateForces.erase(candidateForces.begin() + nearestAgentIndex);
			nearestAgentIndex = -1;
		}
	}
}

void PoliceForceAgent::updateJobs()
{
	LOG(Main, 1) << "updating jobs" << endl;

	for (PoliceForce* pf : availablePolices)
	{
		if (pf->job == PFJ_ARARO && pf->araroCluster != nullptr)
		{
			bool isValidCluster = false;

			for (MotionlessObject* motionless : pf->araroCluster->getMembers())
			{
				for (StuckAgent* sa : araroTargets)
				{
					if (motionless == sa->position)
					{
						isValidCluster = true;
						break;
					}
				}

				if (isValidCluster)
				{
					break;
				}
			}

			if (isValidCluster == false)
			{
				pf->job = PFJ_NONE;
				delete pf->araroCluster;
				pf->araroCluster = nullptr;
			}
		}
	}

	LOG(Main, 1) << "updating jobs finished" << endl;
}

void PoliceForceAgent::preDecision()
{
	for (PoliceForce* pf : world->policeForces)
	{
		if (pf->job == PFJ_PIDGIN)
		{
			continue;
		}

		if (pf->araroCluster != nullptr)
		{
			delete pf->araroCluster;
			pf->araroCluster = nullptr;
		}

		pf->fireBlock = nullptr;
		pf->job = PFJ_NONE;
	}

	for (u_int i = 0; i < availablePolices.size(); i++)
	{
		if (availablePolices[i]->job != PFJ_NONE)
		{
			availablePolices.erase(availablePolices.begin() + i);
			i--;
		}
	}
}

void PoliceForceAgent::globalDecision()
{
	LOG(Main, 1) << "global decision started" << endl;

	//Creating the costs matrix for Hungarian algorithm
	vector<bool> type; //false means araro cluster and true means fire block
	vector<vector<int>> matrixCost;
	vector<FireBlock*> fireBlocks;
	updateFire(); //Initializing fire blocks in the first step of the assignment

	LOG(Main, 1) << "cost : " << endl;
	for (u_int i = 0; i < availablePolices.size(); i++)
	{
		matrixCost.resize(matrixCost.size() + 1);

		LOG(Main, 1) << "[ ";
		for (FireBlock* fb : world->fireBlocks)
		{
			if (matrixCost[i].size() + fb->getNeededPoliceForce() > availablePolices.size() - 1 || isThisFireBlockOk(fb) == false) //-1 : at least one agent must be assigned to araro
			{
				continue;
			}

			for (int j = 0; j < fb->getNeededPoliceForce(); j++)
			{
				MotionlessObject* start = availablePolices[i]->getMotionlessObject();
				if (radar->getRadarMode() == RM_LOW || radar->getRadarMode() == RM_NO_COMMUNICATION || world->getTime() == 6)
				{
					start = availablePolices[i]->getFirstTimeMotionless();
				}
				else if (availablePolices[i] == self() && self()->positionHistory.size() > 1)
				{
					start = self()->positionHistory[self()->positionHistory.size() - 2];
				}
				else if (availablePolices[i] != self() && availablePolices[i]->getLastCycleUpdatedBySense() == world->getTime()
						&& availablePolices[i]->positionHistory.size() > 1)
				{
					start = self()->positionHistory[availablePolices[i]->positionHistory.size() - 2];
				}

				matrixCost[i].push_back(-1 * fb->getGroundDistToMotionless(start, worldGraph, GM_WITHOUT_BLOCKADE));
				LOG(Main, 1) << matrixCost[i][matrixCost[i].size() - 1] << " ";
				if (i == 0)
				{
					fireBlocks.push_back(fb);
					type.push_back(true);
				}
			}
		}

		if (i == 0) //Initializing araro clustering in the first step of the assignment
		{
			LOG(Main, 1) << "optimum : " << availablePolices.size() - matrixCost[i].size() << "   ";
			int availableCount = availablePolices.size() - matrixCost[i].size();
			clusterStuckAgents(round(availableCount * ARARO_ASSIGNMENT_COEFFICIENT));
		}

		for (u_int j = 0; j < araroClusters.size(); j++)
		{
			MotionlessObject* start = availablePolices[i]->getMotionlessObject();
			if (radar->getRadarMode() == RM_LOW || radar->getRadarMode() == RM_NO_COMMUNICATION || world->getTime() == 6)
			{
				start = availablePolices[i]->getFirstTimeMotionless();
			}
			else if (availablePolices[i] == self() && self()->positionHistory.size() > 1)
			{
				start = self()->positionHistory[self()->positionHistory.size() - 2];
			}
			else if (availablePolices[i] != self() && availablePolices[i]->getLastCycleUpdatedBySense() == world->getTime()
					&& availablePolices[i]->positionHistory.size() > 1)
			{
				start = self()->positionHistory[availablePolices[i]->positionHistory.size() - 2];
			}

			matrixCost[i].push_back(-1 * araroClusters[j]->groundDistToEntity(start, worldGraph, GM_WITHOUT_BLOCKADE));
			LOG(Main, 1) << matrixCost[i][matrixCost[i].size() - 1] << " ";
			if (i == 0)
			{
				type.push_back(false);
			}
		}

		//Adding fake clusters to make squares for a perfect assignment
		while (matrixCost[i].size() < availablePolices.size())
		{
			matrixCost[i].push_back(0);
			LOG(Main, 1) << matrixCost[i][matrixCost[i].size() - 1] << " ";
		}

		LOG(Main, 1) << " ]" << endl;
	}

	HungarianAssignment hg(matrixCost);
	vector<vector<bool>> result = hg.hungarian();
	for (u_int i = 0; i < result.size(); i++)
	{
		for (u_int j = 0; j < result[i].size(); j++)
		{
			if (j < type.size()) //Fake cluster management
			{
				if (result[i][j])
				{
					if (type[j])
					{
						availablePolices[i]->fireBlock = fireBlocks[j];
						availablePolices[i]->job = PFJ_FIRE;
						LOG(Main, 1) << "dis police aculy iz fier " << availablePolices[i]->getId() << " assigned to " << j << endl;
					}
					else
					{
						availablePolices[i]->araroCluster = new Cluster(araroClusters[j - fireBlocks.size()]->getMembers());
						availablePolices[i]->job = PFJ_ARARO;
						LOG(Main, 1) << "dis police aculy iz araro " << availablePolices[i]->getId() << " assigned to "
								<< j - fireBlocks.size() << endl;
					}
				}
			}
		}
	}

	LOG(Main, 1) << "global decision finished" << endl;
}

/************************ WORKS *************************/

bool PoliceForceAgent::specialWorks()
{
	if (checkSuspiciousBuildings())
	{
		LOG(Main, 1) << "I'm checking suspicious buildings!";
		return true;
	}

	if (openBestRefuge())
	{
		LOG(Main, 1) << "I'm opening best refuge!";
		return true;
	}

	// TODO Open way among slices

	return false;
}

bool PoliceForceAgent::normalWorks()
{
	if (openOnTheWayHuman())
	{
		LOG(Main, 1) << "I'm a normal person and I'm Madadkar Ejtemaie, I'm helping this poor guy!" << endl;
		return true;
	}

	if (openBestAgent())
	{
		LOG(Main, 1) << "I'm a normal person and I'm opening best agent" << endl;
		return true;
	}

	// TODO Fire Search

	if (fastSearch())
	{
		LOG(Main, 1) << "I'm a normal person and I'm just searching as usual :((" << endl;
		return true;
	}

	return false;
}

/************************ ARARO *************************/

bool PoliceForceAgent::isStuck(MotionlessObject* target)
{
	return isStuck(target->getRepresentiveNodeIndex());
}

bool PoliceForceAgent::isStuck(int nodeIndex)
{
	if (nodeIndex == -1)
	{
		return true;
	}

	if (worldGraph->isReachable(self()->getRepresentiveNodeIndex(), nodeIndex, GM_DEFAULT_BLOCKED))
	{
		return false;
	}

	return true;
}

void PoliceForceAgent::updateStuckAgents()
{
	LOG(Main, 1) << "updating stuck agents: " << endl;

	//Deallocating previously allocated data
	for (StuckAgent* sa : araroTargets)
	{
		delete sa;
	}
	araroTargets.clear();

	//Stuck agent detection
	for (Platoon* platoon : world->platoons)
	{
		if (platoon->isAvailable == false)
		{
			continue;
		}

		if (platoon->isPoliceForce() && platoon->getBuriedness() == 0)
		{
			continue;
		}

		if (isStuck(platoon->getRepresentiveNodeIndex()) == false)
		{
			continue;
		}

		araroTargets.push_back(new StuckAgent(platoon, platoon->getMotionlessObject()));
		LOG(Main, 1) << "ID: " << platoon->getId() << " Motionless: " << platoon->getMotionlessObject()->getId()
				<< " Representive Node: " << platoon->getRepresentiveNodeIndex() << " SELF: " << self()->getMotionlessObject()->getId()
				<< endl;
	}

	for (Refuge* refuge : world->refuges)
	{
		if (isStuck(refuge))
		{
			LOG(Main, 1) << "Motionless: " << refuge->getId() << endl;
			araroTargets.push_back(new StuckAgent(nullptr, refuge));
		}
	}

	LOG(Main, 1) << "stuck agent updating finished" << endl;
}

void PoliceForceAgent::clusterStuckAgents(int clusterCount)
{
	//Deallocating previously allocated data
	for (Cluster* cluster : araroClusters)
	{
		delete cluster;
		cluster = nullptr;
	}
	araroClusters.clear();

	//Setting members for clustering
	vector<MotionlessObject*> members;
	for (StuckAgent* sa : araroTargets)
	{
		members.push_back(sa->position);
	}

	LOG(Main, 1) << "done with : " << min(clusterCount, (int) araroTargets.size()) << endl;
	//Creating clusters using Hierarchical Clustering
	Hierarchical hc(members, min(clusterCount, (int) araroTargets.size()));
	hc.calculate();
	hc.logClustering();

	for (Cluster* cluster : hc.getClusters())
	{
		araroClusters.push_back(new Cluster(cluster->getMembers()));
	}
}

bool PoliceForceAgent::clearThisCluster(Cluster* araroCluster)
{
	MotionlessObject* target = nullptr;
	int minDist = numeric_limits<int>::max();
	Platoon* agent = nullptr;

	//Searching the cluster for fire brigades
	for (StuckAgent* sa : araroTargets)
	{
		if (sa->agent == nullptr || sa->agent->isFireBrigade() == false)
		{
			continue;
		}

		for (MotionlessObject* motionless : araroCluster->getMembers())
		{
			if (sa->position == motionless)
			{
				int distance = worldGraph->getDistance(self()->getWithoutBlockadeRepresentiveNodeIndex(),
						motionless->getWithoutBlockadeRepresentiveNodeIndex(), GM_WITHOUT_BLOCKADE);
				if (distance < minDist)
				{
					minDist = distance;
					target = motionless;
				}
			}
		}
	}

	if (target != nullptr) //Found a fire brigade
	{
		moveAndClear(target);
		return true;
	}

	//Choose the nearest stuck target
	for (MotionlessObject* motionless : araroCluster->getMembers())
	{
		for (StuckAgent* t : araroTargets)
		{
			if (t->position == motionless)
			{
				if (t->agent != nullptr) //Agent
				{
					int distance = worldGraph->getDistance(self()->getWithoutBlockadeRepresentiveNodeIndex(),
							t->agent->getWithoutBlockadeRepresentiveNodeIndex(), GM_WITHOUT_BLOCKADE);
					if (distance < minDist)
					{
						minDist = distance;
						target = t->position;
						agent = t->agent;
					}
				}
				else if (t->agent == nullptr) //Refuge
				{
					int distance = worldGraph->getDistance(self()->getWithoutBlockadeRepresentiveNodeIndex(),
							t->position->getWithoutBlockadeRepresentiveNodeIndex(), GM_WITHOUT_BLOCKADE);
					if (distance < minDist)
					{
						minDist = distance;
						target = t->position;
					}
				}
			}
		}
	}

	if (target != nullptr)
	{
		if (agent != nullptr && self()->getMotionlessObject() == target)
		{
			moveAndClear(self()->getMotionlessObject(), agent->getPos());
		}
		else
		{
			moveAndClear(target);
		}

		return true;
	}

	return false;
}

bool PoliceForceAgent::openBestAgent()
{
	Platoon* bestAgent = nullptr;
	double maxValue = numeric_limits<double>::lowest(), tmpValue;

	for (Platoon* platoon : world->platoons)
	{
		if (!isInSlice(platoon->getMotionlessObject(), mySlice))
		{
			continue;
		}

		if (!platoon->isAvailable)
		{
			continue;
		}

		if (platoon->isPoliceForce() && platoon->getBuriedness() == 0)
		{
			continue;
		}

		if (!isStuck(platoon->getRepresentiveNodeIndex()))
		{
			continue;
		}

		if (find(agentBlacklist.begin(), agentBlacklist.end(), platoon) != agentBlacklist.end())
		{
			continue;
		}

		tmpValue = -worldGraph->getDistance(self()->getWithoutBlockadeRepresentiveNodeIndex(),
				platoon->getWithoutBlockadeRepresentiveNodeIndex(), GM_WITHOUT_BLOCKADE);

		if (tmpValue > maxValue)
		{
			maxValue = tmpValue;
			bestAgent = platoon;
		}
	}

	if (bestAgent != nullptr)
	{
		LOG(Main, 1) << "Best Agent Award goes to .... " << bestAgent->getId() << endl;
		moveAndClear(bestAgent->getMotionlessObject(), bestAgent->getPos());
		return true;
	}

	return false;
}

bool PoliceForceAgent::openOnTheWayHuman()
{
	for (Human* human : world->humans)
	{
		if (world->getTime() - human->getLastCycleUpdatedBySense() > 3)
		{
			continue;
		}

		if (!isValidForClearing(human))
		{
			continue;
		}

		if (human->getPos().distFromPoint(self()->getPos()) > AVERAGE_AGENT_SPEED * 1.5)
		{
			continue;
		}

		if (human->isPoliceForce() && human->getBuriedness() == 0)
		{
			continue;
		}

		if (!isStuck(human->getRepresentiveNodeIndex()))
		{
			continue;
		}

		moveAndClear(human->getMotionlessObject(), human->getPos());
		return true;
	}

	return false;
}

/************************* Fire *************************/

bool PoliceForceAgent::updateFire()
{
	LOG(Main, 1) << "update fire started" << endl;
	LOG(Main, 1) << "1" << endl;
	onFireBuildings.clear();
	for (PoliceForce* pf : world->policeForces)
	{
		pf->fireBlock = nullptr;
	}

	for (Building* b : world->buildings)
	{
		if (b->getEstimatedData()->hasFire())
		{
			onFireBuildings.push_back(b);
		}
	}
	LOG(Main, 1) << "OnFire:" << onFireBuildings.size() << endl;

	if (onFireBuildings.empty())
	{
		LOG(Main , 1) << "fire brigades can take this no firey bilakh" << endl;
		return false;
	}

	createFireBlocks();

	LOG(Main, 1) << "FireBlocks:" << world->fireBlocks.size() << endl;

	for (u_int i = 0; i < world->fireBlocks.size(); i++)
	{
		world->fireBlocks[i]->setAroundRoadsForPoliceForces();

		LOG(Main, 1) << "Around valid roads" << endl;
		vector<Road*> validRoads;
		for (Road* road : world->fireBlocks[i]->getAroundRoadsForPoliceForces())
		{
			if (isStuck(road))
			{
				LOG(Main, 1) << road->getId() << ", " << endl;
				validRoads.push_back(road);
			}
		}

		LOG(Main, 1) << "Around roads: " << validRoads.size() << endl;
		int neededPolice = ceil(validRoads.size() / (double) AROUND_ROADS_FOR_EACH_POLICE);
		world->fireBlocks[i]->setNeededPoliceForce(min(neededPolice, MAX_POLICES_FOR_A_FIRE_BLOCK));
	}
	LOG(Main, 1) << "6" << endl;
	LOG(Main, 1) << "update finished" << endl;

	return true;
}

void PoliceForceAgent::createFireBlocks()
{
	for (u_int i = 0; i < world->buildings.size(); i++)
	{
		world->buildings[i]->setFireBlock(nullptr);
	}
	for (u_int i = 0; i < world->fireBlocks.size(); i++)
	{
		delete world->fireBlocks[i];
	}
	world->fireBlocks.clear();

	for (u_int i = 0; i < onFireBuildings.size(); i++)
	{
		if (onFireBuildings[i]->getFireBlock() == nullptr && onFireBuildings[i]->getEstimatedData()->hasFire())
		{
			FireBlock* fireBlock = new FireBlock();
			dfsOnBuildings(onFireBuildings[i], fireBlock);
			fireBlock->setAroundBuildingsForPolice(world);
			world->fireBlocks.push_back(fireBlock);
		}
	}
}

void PoliceForceAgent::dfsOnBuildings(Building* building, FireBlock* fireBlock)
{
	fireBlock->addBuilding(building);
	building->setFireBlock(fireBlock);

	stack<Building*> list;
	list.push(building);

	while (!list.empty())
	{
		Building* top = list.top();
		list.pop();

		for (InSightBuilding* isb : top->getInSightBuildings())
		{
			Building* neighbor = (Building*) world->buildings[isb->buildingIndex];
			if (neighbor->getFireBlock() != nullptr)
			{
				continue;
			}
			if (!neighbor->getEstimatedData()->hasFire())
			{
				continue;
			}

			neighbor->setFireBlock(fireBlock);
			fireBlock->addBuilding(neighbor);
			list.push(neighbor);
		}
	}
}

bool PoliceForceAgent::clearThisFireBlock(FireBlock* fb)
{
	MotionlessObject* target = nullptr;
	double maxScore = numeric_limits<int>::min();
	for (Road* road : fb->getAroundRoadsForPoliceForces())
	{
		if (isStuck(road) == false)
		{
			continue;
		}

		if (road == self()->getMotionlessObject())
		{
			continue;
		}

		LOG(Main, 1) << "Road: " << road->getId() << endl;

		//Distance score
		double score = (FIRE_BLOCK_CLEARING_ROADS_DISTANCE_COEFFICIENT
				* worldGraph->getDistance(self()->getWithoutBlockadeRepresentiveNodeIndex(),
						road->getWithoutBlockadeRepresentiveNodeIndex(), GM_WITHOUT_BLOCKADE));
		LOG(Main, 1) << "distance score: " << score << endl;

		//Distance to hydrant score
		if (road->isHydrant())
		{
			score += HYDRANT_SCORE;
		}

		//canSee score
		int canSee = 0;
		for (Building* b : fb->getAllBuildings())
		{
			if (find(b->canSee.begin(), b->canSee.end(), road) != b->canSee.end())
			{
				canSee++;
			}
		}

		score += (canSee * 20);
		LOG(Main, 1) << "cansee score: " << canSee * 20 << endl;

		if (score > maxScore)
		{
			maxScore = score;
			target = road;
		}
	}

	if (target != nullptr)
	{
		LOG(Main, 1) << "Fire Block Clearing, target: " << target->getId() << endl;
		moveAndClear(target);
		return true;
	}

	return false;
}

bool PoliceForceAgent::isThisFireBlockOk(FireBlock* fb)
{
	bool allClear = true;
	for (Road* r : fb->getAroundRoadsForPoliceForces())
	{
		if (isStuck(r))
		{
			allClear = false;
			break;
		}
	}

	if (allClear)
	{
		return false;
	}

	bool isExtinguished = true;
	for (Building* b : fb->getAllBuildings())
	{
		if (b->getEstimatedData()->hasFire())
		{
			isExtinguished = false;
		}
	}

	if (isExtinguished)
	{
		return false;
	}

	return true;
}

bool PoliceForceAgent::isReachableToFireGroup(PoliceForce* pf, vector<FireBrigade*> group)
{
	for (FireBrigade* fb : group)
	{
		if (worldGraph->isReachable(pf->getRepresentiveNodeIndex(), fb->getRepresentiveNodeIndex(), GM_DEFAULT_BLOCKED))
		{
			return true;
		}
	}

	return false;
}

bool PoliceForceAgent::fireBlockToFireBrigade()
{
	if (world->fireBrigades.empty())
	{
		return false;
	}

	vector<vector<FireBrigade*>> fireGroups;
	vector<bool> mark(world->fireBrigades.size(), false);

	//Fire group making
	for (FireBrigade* fb : world->fireBrigades)
	{
		if (mark[fb->fireBrigadeIndex])
		{
			continue;
		}

		mark[fb->fireBrigadeIndex] = true;
		vector<FireBrigade*> group =
		{fb};
		for (FireBrigade* fb2 : world->fireBrigades)
		{
			if (mark[fb2->fireBrigadeIndex])
			{
				continue;
			}

			if (worldGraph->isReachable(fb->getRepresentiveNodeIndex(), fb2->getRepresentiveNodeIndex(), GM_DEFAULT_BLOCKED))
			{
				group.push_back(fb2);
				mark[fb2->fireBrigadeIndex] = true;
			}
		}

		if (group.size() > 1)
		{
			fireGroups.push_back(group);
		}
	}

	//LOG
	LOG(Main, 1) << "Fire groups : " << endl;
	for (vector<FireBrigade*> group : fireGroups)
	{
		LOG(Main, 1) << "group : ";
		for (FireBrigade* fb : group)
		{
			LOG(Main, 1) << fb->getId() << " ";
		}

		LOG(Main, 1) << endl;
	}

	vector<PoliceForce*> noJobs;
	for (PoliceForce* pf : availablePolices)
	{
		if (pf->job == PFJ_NONE)
		{
			noJobs.push_back(pf);
		}
	}

	vector<PoliceForce*> blacklist;
	for (vector<FireBrigade*> group : fireGroups)
	{
		PoliceForce* chosen = nearestPoliceToFireGroup(group, blacklist);

		if (chosen != nullptr)
		{
			LOG(Main, 1) << "fucking chosen police force : " << chosen->getId() << endl;
			blacklist.push_back(chosen);
			if (chosen == self())
			{
				MotionlessObject* target = nullptr;
				int minimumDistance = numeric_limits<int>::max();
				for (FireBrigade* fb : group)
				{
					int distance = worldGraph->getDistance(fb->getFirstTimeMotionless()->getWithoutBlockadeRepresentiveNodeIndex(),
							self()->getWithoutBlockadeRepresentiveNodeIndex(), GM_WITHOUT_BLOCKADE);
					if (distance < minimumDistance)
					{
						target = fb->getMotionlessObject();
						distance = minimumDistance;
					}
				}

				if (target != nullptr)
				{
					moveAndClear(target);
					LOG(Main, 1) << "Executing police to fire group " << self()->getId() << endl;
					return true;
				}
			}
		}
		else
		{
			break;
		}
	}

	LOG(Main, 1) << "i has been recognized as a faggot " << endl;
	return false;
}

PoliceForce* PoliceForceAgent::nearestPoliceToFireGroup(vector<FireBrigade*> group, vector<PoliceForce*> blacklist)
{
	PoliceForce* nearest = nullptr;
	int minimumDistance = numeric_limits<int>::max();
	for (PoliceForce* pf : availablePolices)
	{
		if (pf->job != PFJ_NONE || isReachableToFireGroup(pf, group))
		{
			continue;
		}

		if (find(blacklist.begin(), blacklist.end(), pf) != blacklist.end())
		{
			continue;
		}

		int distance = numeric_limits<int>::max();
		for (FireBrigade* fb : group)
		{
			int tempDistance = worldGraph->getDistance(fb->getWithoutBlockadeRepresentiveNodeIndex(),
					pf->getWithoutBlockadeRepresentiveNodeIndex(), GM_WITHOUT_BLOCKADE);
			if (tempDistance < distance)
			{
				distance = tempDistance;
			}
		}

		if (distance < minimumDistance)
		{
			minimumDistance = distance;
			nearest = pf;
		}
	}

	return nearest;
}

bool PoliceForceAgent::openBestRefuge()
{
	double maxValue = numeric_limits<double>::lowest();
	Refuge* bestRefuge = nullptr;
	for (Refuge* refuge : world->refuges)
	{
		if (!isInSlice((MotionlessObject*) refuge, mySlice))
		{
			continue;
		}

		if (worldGraph->isReachable(self()->getRepresentiveNodeIndex(), refuge->getRepresentiveNodeIndex(), GM_DEFAULT_BLOCKED))
		{
			continue;
		}

		if (find(refugeBlacklist.begin(), refugeBlacklist.end(), refuge) != refugeBlacklist.end())
		{
			continue;
		}

		double tmpValue = 0;

		// Distance to agent
		tmpValue -= worldGraph->getDistance(self()->getRepresentiveNodeIndex(), refuge->getRepresentiveNodeIndex(), GM_WITHOUT_BLOCKADE)
				* 5.f;

		// Distance to map's center
		tmpValue -= refuge->getPos().distFromPoint(world->getCityCenter());

		// Fiery buildings
		int nearestFieryBuilding = numeric_limits<int>::max();
		for (Building* building : world->buildings)
		{
			int tmpDist = building->getPos().distFromPoint(refuge->getPos());
			if (building->getEstimatedData()->getTemperature() > 0
					&& (building->getEstimatedData()->hasFire() || building->getEstimatedData()->getFieryness() == 0
							|| building->getEstimatedData()->getFieryness() == 4))
			{
				if (tmpDist < nearestFieryBuilding)
				{
					nearestFieryBuilding = tmpDist;
				}
			}
		}
		tmpValue -= nearestFieryBuilding * 1.5f;

		if (tmpValue > maxValue)
		{
			maxValue = tmpValue;
			bestRefuge = refuge;
		}
	}

	if (bestRefuge != nullptr)
	{
		LOG(Main, 1) << "Best Refuge is " << bestRefuge->getId() << endl;

		moveAndClear(bestRefuge);
		return true;
	}

	return false;
}

bool PoliceForceAgent::checkSuspiciousBuildings()
{
	vector<Building*> suspiciousBuildings;
	for (Building* building : world->buildings)
	{
		if (!isSuspicious(building))
		{
			continue;
		}

		int distance = building->getPos().distFromPoint(self()->getPos());
		if (distance > inRangeDistance)
		{
			continue;
		}

		building->setValue(-distance);

		suspiciousBuildings.push_back(building);
	}

	sort_heap(suspiciousBuildings.begin(), suspiciousBuildings.end(), buildingValueComparator);

	if (suspiciousBuildings.size() > 0)
	{
		int targetIndex = min(1, (int) suspiciousBuildings.size() - 1);
		moveAndClear((MotionlessObject*) suspiciousBuildings[targetIndex]);
		return true;
	}

	return false;
}

bool PoliceForceAgent::isSuspicious(Building* building)
{
	if (building->getEstimatedData()->hasFire())
	{
		return false;
	}

	if (building->getEstimatedData()->getTemperature() == 0)
	{
		return false;
	}

	if (building->getEstimatedData()->getFieryness() != 0 && building->getEstimatedData()->getFieryness() != 4)
	{
		return false;
	}

	return true;
}

/*********************** Civilian ***********************/

bool PoliceForceAgent::isValidForUpdate(Civilian* civ)
{
	if (civ == nullptr)
	{
		return false;
	}

	if (civ->isAvailable == false)
	{
		return false;
	}

	if (world->getTime() - civ->getLastCycleUpdated() < 15)
	{
		return false;
	}

	return true;
}

bool PoliceForceAgent::updateCivilians()
{
	vector<Civilian*> mine;
	for (Civilian* civ : world->civilians)
	{
		bool isValid = false;
		if (isValidForUpdate(civ))
		{
			isValid = true;
			int selfDistance = worldGraph->getDistance(self()->getWithoutBlockadeRepresentiveNodeIndex(),
					civ->getWithoutBlockadeRepresentiveNodeIndex(), GM_WITHOUT_BLOCKADE);

			for (PoliceForce* pf : availablePolices)
			{
				if (pf->job == PFJ_NONE)
				{
					int distance = worldGraph->getDistance(pf->getWithoutBlockadeRepresentiveNodeIndex(),
							civ->getWithoutBlockadeRepresentiveNodeIndex(), GM_WITHOUT_BLOCKADE);
					if (distance < selfDistance)
					{
						isValid = false;
						break;
					}
				}
			}

			if (isValid)
			{
				mine.push_back(civ);
			}
		}
	}

	MotionlessObject* target = nullptr;
	int minimumDistance = numeric_limits<int>::max();
	for (Civilian* civ : mine)
	{
		int distance = worldGraph->getDistance(self()->getWithoutBlockadeRepresentiveNodeIndex(),
				civ->getWithoutBlockadeRepresentiveNodeIndex(), GM_WITHOUT_BLOCKADE);
		if (distance < minimumDistance)
		{
			target = civ->getMotionlessObject();
			minimumDistance = distance;
		}
	}

	if (target != nullptr)
	{
		moveAndClear(target);
		return true;
	}

	return false;
}

bool PoliceForceAgent::isValidForClearing(Human* human)
{
	if (human == nullptr)
	{
		return false;
	}

	if (human->isAvailable == false)
	{
		return false;
	}

	if (human->getMotionlessObject()->isBuilding() && ((Building*) human->getMotionlessObject())->hasFire())
	{
		return false;
	}

	if (worldGraph->isReachable(self()->getRepresentiveNodeIndex(), human->getRepresentiveNodeIndex(), GM_DEFAULT_BLOCKED))
	{
		return false;
	}

	return true;
}

bool PoliceForceAgent::clearCivilians()
{
	vector<Civilian*> mine;
	for (Civilian* civ : world->civilians)
	{
		bool isValid = false;
		if (isValidForClearing(civ))
		{
			isValid = true;
			int selfDistance = worldGraph->getDistance(self()->getWithoutBlockadeRepresentiveNodeIndex(),
					civ->getWithoutBlockadeRepresentiveNodeIndex(), GM_WITHOUT_BLOCKADE);

			for (PoliceForce* pf : availablePolices)
			{
				if (pf == self())
				{
					continue;
				}

				if (pf->job == PFJ_NONE)
				{
					int distance = worldGraph->getDistance(pf->getWithoutBlockadeRepresentiveNodeIndex(),
							civ->getWithoutBlockadeRepresentiveNodeIndex(), GM_WITHOUT_BLOCKADE);
					if (distance < selfDistance)
					{
						isValid = false;
						break;
					}
				}
			}

			if (isValid)
			{
				mine.push_back(civ);
			}
		}
	}

	MotionlessObject* target = nullptr;
	int minimumDistance = numeric_limits<int>::max();
	for (Civilian* civ : mine)
	{
		int distance = worldGraph->getDistance(self()->getWithoutBlockadeRepresentiveNodeIndex(),
				civ->getWithoutBlockadeRepresentiveNodeIndex(), GM_WITHOUT_BLOCKADE);
		if (distance < minimumDistance)
		{
			target = civ->getMotionlessObject();
			minimumDistance = distance;
		}
	}

	if (target != nullptr)
	{
		moveAndClear(target);
		return true;
	}

	return false;
}

/************************ Pidgin ************************/

void PoliceForceAgent::createPidginPath()
{
	if (world->refuges.empty())
	{
		return;
	}

	vector<MotionlessObject*> path =
	{self()->getMotionlessObject()};
	for (u_int i = 0; i < world->refuges.size(); i++)
	{
		Refuge* best = nullptr;
		int minDist = numeric_limits<int>::max();

		for (Refuge* ref : world->refuges)
		{
			if (find(path.begin(), path.end(), ref) != path.end())
			{
				continue;
			}

			int distance = worldGraph->getDistance(path[path.size() - 1]->getWithoutBlockadeRepresentiveNodeIndex(),
					ref->getWithoutBlockadeRepresentiveNodeIndex(), GM_WITHOUT_BLOCKADE);
			if (distance < minDist)
			{
				minDist = distance;
				best = ref;
			}
		}

		if (best == nullptr)
		{
			break;
		}

		path.push_back(best);
	}

	path.erase(path.begin() + 0); //Removing self from the path

	LOG(Main, 1) << "Pidgin Path: ";
	for (MotionlessObject* m : path)
	{
		LOG(Main, 1) << m->getId() << " ";
		pidginPath.push(m);
	}
	LOG(Main, 1) << endl;
}

bool PoliceForceAgent::pidgin()
{
	if (self()->job != PFJ_PIDGIN || pidginPath.empty())
	{
		return false;
	}

	if (self()->getMotionlessObject() == pidginPath.front()) //Pushing the head of the queue to its tail
	{
		pidginPath.push(pidginPath.front());
		pidginPath.pop();
	}

	moveAndClear(pidginPath.front());
	return true;
}

/********************** Don't Shas **********************/

bool PoliceForceAgent::dontShas()
{
	LOG(Main , 1) << "Emam (rah): dont shas!" << endl;

	Blockade* onMe = nullptr;
	if (self()->getMotionlessObject()->isRoad())
	{
		for (Blockade* blockade : ((Road*) self()->getMotionlessObject())->getBlockades())
		{
			if (blockade->getShape().isInPolygon(self()->getPos()))
			{
				onMe = blockade;
				break;
			}
		}
	}

	if (onMe != nullptr)
	{
		LOG(Main, 1) << "there is some blockade on me" << endl;

		if (onMe->getPos().distFromPoint(self()->getPos()) < maxClearDistance)
		{
			command->clear(*onMe);
		}
		else if (clearNearestBlockade())
		{
			LOG(Main , 1) << "I'm clearing ...." << endl;
		}
		else
		{
			int neighborIndex = world->getTime() % self()->getMotionlessObject()->getNeighbours().size();
			vector<int> newPath;
			newPath.push_back(self()->getMotionlessObject()->getId());
			newPath.push_back(world->motionlessObjects[self()->getMotionlessObject()->getNeighbours()[neighborIndex]]->getId());
			command->move(newPath, -1, -1);
		}

		return true;
	}

	if (lastRepNodeIndex[0] == -1 && lastRepNodeIndex[1] == -1 && lastRepNodeIndex[2] == -1)
	{
		LOG(Main, 1) << "Representive node index is -1" << endl;

		for (Blockade* blockade : world->blockades)
		{
			if (blockade->getShape().getMidPoint().distFromPoint(self()->getPos()) < NEAR_BLOCKADES_TO_CLEAR_AREA_BIG_MAP
					&& blockade->increasedShapes[1].isInPolygon(self()->getPos()))
			{
				LOG(Main, 1) << "Clearing this in motionless blockade: " << blockade->getId() << endl;
				command->clear(*blockade);
				return true;
			}
		}

		if (clearNearestBlockade())
		{
			LOG(Main, 1) << "Clearing nearest blockade" << endl;
			return true;
		}
	}

//	if (self()->getRepresentiveNodeIndex() == -1)
//	{
//		LOG(Main, 1) << "Representive node index is -1" << endl;
//
//		Blockade* nearestBlockingBlockade = nullptr;
//		double minDist = numeric_limits<double>::max(), tmpDist;
//		for (Blockade* blockade : world->blockades)
//		{
//			if (blockade->getShape().getMidPoint().distFromPoint(self()->getPos()) < maxClearDistance
//					&& blockade->increasedShapes[1].isInPolygon(self()->getPos()))
//			{
//				tmpDist = distanceToPolygon(blockade->increasedShapes[1], self()->getPos());
//				if (tmpDist < minDist)
//				{
//					minDist = tmpDist;
//					nearestBlockingBlockade = blockade;
//				}
//			}
//		}
//
//		if (nearestBlockingBlockade)
//		{
//			LOG(Main, 1) << "Clearing this fucking blockade: " << nearestBlockingBlockade->getId() << endl;
//
//			// Find nearest edge of blockade
//			minDist = numeric_limits<int>::max();
//			Segment nearestEdge;
//			for (int i = 0; i < nearestBlockingBlockade->getShape().size(); i++)
//			{
//				tmpDist = distanceToSegment(nearestBlockingBlockade->getShape().getSegment(i), self()->getPos());
//				if (tmpDist < minDist)
//				{
//					minDist = tmpDist;
//					nearestEdge = nearestBlockingBlockade->getShape().getSegment(i);
//				}
//			}
//
//			command->clear(getClearPoint(getPerpendicularPoint(nearestEdge.asLine(), self()->getPos())));
//			return true;
//		}
//	}

	if (iAmStucking())
	{
		return true;
	}

	LOG(Main, 1) << "i'm not stucking!" << endl;

	return false;
}

bool PoliceForceAgent::iAmStucking()
{
	int index = self()->positionHistory.size() - 2;

	if (world->getTime() <= 6)
	{
		return false;
	}

	LOG(Main , 1) << "Last command: " << self()->myLastCommand << endl;
	for (int i = 0; i < (int) self()->positionHistory.size(); i++)
	{
		LOG(Main , 1) << "Position history: " << self()->positionHistory[i]->getId() << endl;
	}

	if (world->getTime() % 3 == 0 && self()->myLastCommand == CT_MOVE
			&& self()->getMotionlessObject()->getId() == self()->positionHistory[index]->getId()
			&& self()->positionHistory[index]->getId() == self()->positionHistory[index - 1]->getId()
			&& self()->positionHistory[index - 1]->getId() == self()->positionHistory[index - 2]->getId()
			&& self()->positionHistory[index - 2]->getId() == self()->positionHistory[index - 3]->getId())
	{
		if (self()->getMotionlessObject()->isRoad() || ((Building *) self()->getMotionlessObject())->hasFire() == false)
		{
			if (clearNearestBlockade())
			{
				return true;
			}
		}

		vector<int> path;
		path.push_back(self()->getMotionlessObject()->getId());
		command->move(path, -1, -1);
		return true;
	}

	LOG(Main , 1) << ":P i'm not shasing! :P" << endl;

	return false;
}

void PoliceForceAgent::updateLastRepNodeIndex()
{
	lastRepNodeIndex[2] = lastRepNodeIndex[1];
	lastRepNodeIndex[1] = lastRepNodeIndex[0];
	lastRepNodeIndex[0] = self()->getRepresentiveNodeIndex();
}

/************************ Search ************************/

bool PoliceForceAgent::search()
{
	LOG(Main , 1) << "Main search! t: " << world->getTime() << endl;

	if (advanceSearch->doAroundFireSearch())
	{
		LOG(Main, 1) << "I have a Suspicious target" << endl;
		moveAndClear(self()->aroundFireSearchTarget);
		LOG(Main, 1) << "suspiciousTarget->roadIndex : " << self()->aroundFireSearchTarget->roadIndex << endl;
		return true;
	}

	if (advanceSearch->calculate() == false)
	{
		return false;
	}

	if (self()->searchTarget == nullptr || self()->searchTarget->isBuilding())
	{
		mySearchTarget = (Building*) self()->searchTarget;
	}
	else
	{
		return fastSearch();
	}

	if (world->teamConfig["Search"]["SearchArea"]["Use"].asBool())
	{
		LOG(Main, 1) << "world->teamConfig[Search][SearchArea][Use] = True" << endl;
		Point p;
		SearchShape* searchShape;
		bool doBreak = false;
		LOG(Main, 1) << (mySearchTarget == nullptr) << endl;
		LOG(Main, 1) << (mySearchTarget->searchArea == nullptr) << endl;
		for (SearchShape* sp : mySearchTarget->searchArea->seachShapes)
		{
			for (Point pp : sp->searchPoints)
			{
				p = pp;
				searchShape = sp;
				doBreak = true;
				LOG(Main, 1) << 1 << endl;
				break;
			}

			if (doBreak)
			{
				break;
			}
		}

		if (doBreak)
		{
			LOG(Main, 1) << 2 << endl;
			if (p.distFromPoint(self()->getPos()) < 3000) // Tof baraye bage server !!!! //XXX !!!
			{
				LOG(Main, 1) << 3 << endl;
				moveAndClear(mySearchTarget);
				mySearchTarget->searchArea->seachShapes.clear();
				return true;
			}

			if (self()->getMotionlessObject()->getId() == searchShape->searchMotionless->getId())
			{
				int index = self()->positionHistory.size() - 2;
				if (self()->myLastCommand == CT_MOVE && self()->getMotionlessObject()->getId() == self()->positionHistory[index]->getId()
						&& self()->positionHistory[index]->getId() == self()->positionHistory[index - 1]->getId()
						&& self()->positionHistory[index - 1]->getId() == self()->positionHistory[index - 2]->getId())
				{
					LOG(Main, 1) << "Costomise donting shus for search !!!" << endl;
					moveAndClear(mySearchTarget);
					mySearchTarget->searchArea->seachShapes.clear(); //TODO  tof

				}
				else
				{
					moveAndClear(searchShape->searchMotionless, p);
					LOG(Main, 1) << "self Pos : " << self()->getPos() << endl;
					LOG(Main, 1) << "Moving to : [moIndex:" << searchShape->searchMotionless->motionlessIndex << " c: "
							<< searchShape->searchMotionless->getPos() << "  p: " << p << "]" << endl;
				}

				return true;
			}
			else
			{
				LOG(Main, 1) << 5 << endl;
//				if (worldGraph->isReachable(self()->getWithoutBlockadeRepresentiveNodeIndex(), searchShape->searchMotionless->getWithoutBlockadeRepresentiveNodeIndex(),
//						GM_DEFAULT_BLOCKED))
//				{
//					LOG(Main, 1) << "5.1" << endl;
//					command->moveToPoint(searchShape->searchMotionless->motionlessIndex, p, GM_DEFAULT_BLOCKED);
//					return true;
//				}
				moveAndClear(searchShape->searchMotionless);
				return true;
			}
		}
		else
		{
			LOG(Main, 1) << 6 << endl;
			LOG(Main, 1) << mySearchTarget->getId() << endl;
			moveAndClear(mySearchTarget);
			return true;
		}
	}
	else
	{
		LOG(Main, 1) << 7 << endl;
		moveAndClear(mySearchTarget);
		return true;
	}

	return true;
}

bool PoliceForceAgent::warmBuildingSearch()
{
	LOG(Main, 1) << "Warm buildings: " << endl;
	vector<Building*> warmBuildings;
	for (Building* b : searchTerritory)
	{
		if (b->hasFire() == false && b->getFieryness() != 8 && (b->getEstimatedData()->getTemperature() >= 1 || b->getTemperature() > 0))
		{
			bool knowFire = false;
			for (InSightBuilding* isb : b->getInSightBuildings())
			{
				if (world->buildings[isb->buildingIndex]->hasFire())
				{
					knowFire = true;
					break;
				}
			}

			if (knowFire == false)
			{
				LOG(Main, 1) << b->getId() << ", ";
				warmBuildings.push_back(b);
			}
		}
	}

	int minimumDistance = numeric_limits<int>::max();
	Building* nearest = nullptr;
	for (Building* b : warmBuildings)
	{
		int dist = worldGraph->getDistance(self()->getWithoutBlockadeRepresentiveNodeIndex(), b->getWithoutBlockadeRepresentiveNodeIndex(),
				GM_WITHOUT_BLOCKADE);
		if (dist < minimumDistance)
		{
			dist = minimumDistance;
			nearest = b;
		}
	}

	if (nearest == nullptr)
	{
		return false;
		LOG(Main, 1) << "No warm building detected!!" << endl;
	}

	LOG(Main, 1) << endl << "Nearest building: " << nearest->getId() << endl;
	LOG(Main, 1) << "To search: " << endl;
	vector<Building*> toSearch;
	for (InSightBuilding* isb : nearest->getInSightBuildings())
	{
		if (world->getTime() - world->buildings[isb->buildingIndex]->getLastCycleUpdated() > 50
				|| world->buildings[isb->buildingIndex]->getLastCycleUpdated() == 0)
		{
			LOG(Main, 1) << world->buildings[isb->buildingIndex]->getId() << ", ";
			toSearch.push_back(world->buildings[isb->buildingIndex]);
		}
	}

	LOG(Main, 1) << "To see: " << endl;
	vector<Road*> toSee;
	for (Building* b : toSearch)
	{
		int minimumDistance = numeric_limits<int>::max();
		Road* nearestCanSee = nullptr;
		for (Road* canSee : b->canSee)
		{
			int dist = worldGraph->getDistance(self()->getWithoutBlockadeRepresentiveNodeIndex(),
					canSee->getWithoutBlockadeRepresentiveNodeIndex(), GM_WITHOUT_BLOCKADE);
			if (dist < minimumDistance)
			{
				nearestCanSee = canSee;
				minimumDistance = dist;
			}
		}

		if (nearestCanSee != nullptr)
		{
			LOG(Main, 1) << nearestCanSee->getId() << ", ";
			toSee.push_back(nearestCanSee);
		}
	}

	Road* target = nullptr;
	minimumDistance = numeric_limits<int>::max();
	for (Road* r : toSee)
	{
		int dist = worldGraph->getDistance(self()->getWithoutBlockadeRepresentiveNodeIndex(), r->getWithoutBlockadeRepresentiveNodeIndex(),
				GM_WITHOUT_BLOCKADE);
		if (dist < minimumDistance)
		{
			minimumDistance = dist;
			target = r;
		}
	}

	if (target != nullptr)
	{
		LOG(Main, 1) << "Doing warm building search, target: " << target->getId() << endl;
		moveAndClear(target);
		return true;
	}

	return false;
}

bool PoliceForceAgent::suspiciousSearch()
{
	if (advanceSearch->prepareForSouspeciouseSearch())
	{
		vector<Road*> legacies = advanceSearch->findFourLegacyRoads(self()->souspiciouseTarget);

		if (self()->getMotionlessObject()->isRoad())
		{
			if (find(legacies.begin(), legacies.end(), (Road*) self()->getMotionlessObject()) != legacies.end())
			{
				legacies.erase(find(legacies.begin(), legacies.end(), (Road*) self()->getMotionlessObject()));
			}
		}

		if (legacies.empty())
		{
			return false;
		}

		moveAndClear(legacies[0]);
	}
	return true;
}

void PoliceForceAgent::initializeSearchTerritory()
{
	if (self()->job == PFJ_PIDGIN)
	{
		return;
	}

	vector<MotionlessObject*> objects;
	for (Building* b : world->buildings)
	{
		objects.push_back(b);
	}

	vector<PoliceForce*> avPolices;
	int selfIndex = -1;
	for (u_int i = 0; i < world->policeForces.size(); i++)
	{
		if (world->policeForces[i]->job == PFJ_PIDGIN)
		{
			continue;
		}

		avPolices.push_back(world->policeForces[i]);

		if (world->policeForces[i] == self())
		{
			selfIndex = avPolices.size() - 1;
		}
	}

	if (selfIndex == -1)
	{
		return;
	}

	KMean k(objects, avPolices.size());
	k.fastCalculate();

	vector<vector<int>> cost(avPolices.size());
	for (u_int i = 0; i < avPolices.size(); i++)
	{
		for (Cluster* c : k.getClusters())
		{
			cost[i].push_back(-1 * c->groundDistToEntity(avPolices[i]->getMotionlessObject(), worldGraph, GM_WITHOUT_BLOCKADE));
		}
	}

	HungarianAssignment hgAssign(cost);
	vector<vector<bool>> result = hgAssign.hungarian();

	for (u_int i = 0; i < result[selfIndex].size(); i++)
	{
		if (result[selfIndex][i])
		{
			for (MotionlessObject* motionless : k.getClusters()[i]->getMembers())
			{
				searchTerritory.push_back((Building*) motionless);
			}

			break;
		}
	}

	LOG(Main, 1) << "Search cluster: " << endl;
	for (Building* b : searchTerritory)
	{
		LOG(Main, 1) << b->getId() << " ";
	}
	LOG(Main, 1) << endl;
}

bool PoliceForceAgent::fastSearch()
{
	/*if (warmBuildingSearch())
	 {
	 return true;
	 }*/

	if (mySlice == nullptr)
	{
		LOG(Main, 1) << "HOOOOLY SHIEEET! mySlice is null!";
		return false;
	}

	LOG(Main, 1) << "fastSearch()" << endl;
	vector<Building*> validToSearch;
	vector<Building*> validToCheck;
	for (Building* b : mySlice->getBuildings())
	{
		if ((radar->getRadarMode() == RM_HIGH || radar->getRadarMode() == RM_MEDIUM) && (b->isRefuge() || b->isCenter()))
		{
			continue;
		}

		if (b->hasFire() || b->getFieryness() == 8)
		{
			continue;
		}

		if (b->getBrokenness() == 0)
		{
			validToCheck.push_back(b);
			continue;
		}

		if (b->lastTimeSearchedForCivilian != -1)
		{
			validToCheck.push_back(b);
		}
		else
		{
			validToSearch.push_back(b);
		}
	}

	MotionlessObject* target = nullptr;
	if (validToSearch.empty())
	{
		int minimumLTS = numeric_limits<int>::max();
		for (Building* b : validToCheck)
		{
			if (b->lastTimeSearchedForCivilian < minimumLTS)
			{
				minimumLTS = b->lastTimeSearchedForCivilian;
				target = b;
			}
		}
	}
	else
	{
		int minimumDistance = numeric_limits<int>::max();
		for (Building* b : validToSearch)
		{
			int distance = worldGraph->getDistance(self()->getWithoutBlockadeRepresentiveNodeIndex(),
					b->getWithoutBlockadeRepresentiveNodeIndex(), GM_WITHOUT_BLOCKADE);
			if (distance < minimumDistance)
			{
				minimumDistance = distance;
				target = b;
			}
		}
	}

	if (target == nullptr)
	{
		LOG(Main, 1) << "No target found to search " << validToSearch.size() << " search valid : " << validToCheck.size() << endl;
		searchTerritory = world->buildings;
		return false;
	}

	LOG(Main, 1) << "Going to search : " << target->getId() << endl;
	moveAndClear(target);
	return true;
}

/******************** Other Bullshit ********************/

void PoliceForceAgent::fillTheBlacklists()
{
	for (Refuge* refuge : world->refuges)
	{
		if (worldGraph->isReachable(self()->getRepresentiveNodeIndex(), refuge->getRepresentiveNodeIndex(), GM_DEFAULT_BLOCKED)
				&& find(refugeBlacklist.begin(), refugeBlacklist.end(), refuge) == refugeBlacklist.end())
		{
			refugeBlacklist.push_back(refuge);
		}
	}

	for (Platoon* platoon : world->platoons)
	{
		if (worldGraph->isReachable(self()->getRepresentiveNodeIndex(), platoon->getRepresentiveNodeIndex(), GM_DEFAULT_BLOCKED)
				&& find(agentBlacklist.begin(), agentBlacklist.end(), platoon) == agentBlacklist.end())
		{
			agentBlacklist.push_back(platoon);
		}
	}
}

void PoliceForceAgent::moveAndClear(MotionlessObject* targetMotionless)
{
	moveAndClear(targetMotionless, targetMotionless->getPos());
}

void PoliceForceAgent::moveAndClear(MotionlessObject* targetMotionless, Point targetPoint)
{
	// If I'm in my building target
	if (targetMotionless->isBuilding() && targetMotionless->motionlessIndex == self()->getMotionlessObject()->motionlessIndex)
	{
		return command->moveToPoint(targetMotionless->motionlessIndex, targetPoint, GM_WITHOUT_BLOCKADE);
	}

	vector<int> pathNodes = worldGraph->getPath(self()->getWithoutBlockadeRepresentiveNodeIndex(),
			targetMotionless->getWithoutBlockadeRepresentiveNodeIndex(), GM_WITHOUT_BLOCKADE);

	vector<int> ids = worldGraph->getIDPath(pathNodes);
	LOG(Main, 1) << "Me " << self()->getMotionlessObject()->getId() << endl;
	for (int id : ids)
	{
		LOG(Main, 1) << id << " ";
	}
	LOG(Main, 1) << endl;

	if (self()->getMotionlessObject()->motionlessIndex != targetMotionless->motionlessIndex)
	{
		for (int i = ids.size() - 1; i > 0; i--)
		{
			if (worldGraph->isReachable(self()->getRepresentiveNodeIndex(),
					((MotionlessObject*) world->objects[ids[i]])->getRepresentiveNodeIndex(), GM_DEFAULT_BLOCKED)
					&& worldGraph->getDistance(self()->getRepresentiveNodeIndex(),
							((MotionlessObject*) world->objects[ids[i]])->getRepresentiveNodeIndex(), GM_WITHOUT_BLOCKADE)
							> AVERAGE_AGENT_SPEED / 2)
			{
				command->moveToPoint(targetMotionless->motionlessIndex, targetPoint, GM_WITHOUT_BLOCKADE);
				return;
			}
		}
	}

	// Correct if any inversion exists
	inversionRectifier(pathNodes);

	// Find proper clear point
	Point clearPoint;
	if (targetMotionless->motionlessIndex == self()->getMotionlessObject()->motionlessIndex)
	{
		clearPoint = getClearPoint(targetPoint);
		LOG(Main, 1) << "Clear Point found! (1st)";
	}
	else
	{
		clearPoint = getClearPoint(pathNodes, targetPoint);
		LOG(Main, 1) << "Clear Point found! (2st)";
	}
	world->clearrrrrpoint = clearPoint;
	world->clearArea = getClearArea(clearPoint);

	// Clear if any blockade exists
	if (isAnyBlockadeInClearArea(getClearArea(clearPoint)))
	{
		LOG(Main, 1) << "I'm Clearing!";
		command->clear(clearPoint);
		return;
	}

	// Move
	LOG(Main, 1) << "I'm Moving!";
	command->moveToPoint(targetMotionless->motionlessIndex, targetPoint, GM_WITHOUT_BLOCKADE);
}

void PoliceForceAgent::inversionRectifier(vector<int>& pathNodes)
{
	if (pathNodes.size() > 1)
	{
		if (worldGraph->getNodes()[pathNodes[0]]->getMotionlessIndex() == worldGraph->getNodes()[pathNodes[1]]->getMotionlessIndex())
		{
			pathNodes.erase(pathNodes.begin());
		}
	}
}

Point PoliceForceAgent::getClearPoint(Point destPoint)
{
	Vector agentToTarget(destPoint.getX() - self()->getPos().getX(), destPoint.getY() - self()->getPos().getY());
	agentToTarget = agentToTarget.normalized() * maxClearDistance;
	return self()->getPos() + agentToTarget;
}

Point PoliceForceAgent::getClearPoint(vector<int> pathNodes, Point destPoint)
{
	Point clearPoint = worldGraph->getNodes()[pathNodes[0]]->getRepresentivePoint();

	Segment mainSegment = Segment(self()->getPos(), clearPoint);
	float mainSegmentTheta = atan2f(clearPoint.getY() - self()->getPos().getY(), clearPoint.getX() - self()->getPos().getX());

	int distance = mainSegment.getLength();

	for (u_int i = 1; i < pathNodes.size(); i++)
	{
		Point prevClearPoint = worldGraph->getNodes()[pathNodes[i - 1]]->getRepresentivePoint();
		Point nextClearPoint = worldGraph->getNodes()[pathNodes[i]]->getRepresentivePoint();

		distance += prevClearPoint.distFromPoint(nextClearPoint);

		float prevTheta = atan2f(prevClearPoint.getY() - self()->getPos().getY(), prevClearPoint.getX() - self()->getPos().getX());
		float nextTheta = atan2f(nextClearPoint.getY() - self()->getPos().getY(), nextClearPoint.getX() - self()->getPos().getX());

		float theta = fabsf(nextTheta - mainSegmentTheta);
		float theta2 = fabsf(nextTheta - prevTheta);
		float length = self()->getPos().distFromPoint(nextClearPoint);
		if (theta > ACCEPTABLE_ANGLE_VARIANCE_1 || theta2 > ACCEPTABLE_ANGLE_VARIANCE_2 || length * sinf(theta) > maxClearDistance / 2
				|| distance > 2.5 * maxClearDistance)
		{
			return getClearPoint(prevClearPoint);
		}
	}

	return getClearPoint(clearPoint);
}

Polygon PoliceForceAgent::getClearArea(Point target)
{
	const int newClearDist = maxClearDistance - 300;
	const int newClearWidth = clearWidth - 300;

	Vector agentToTarget(target.getX() - self()->getPos().getX(), target.getY() - self()->getPos().getY());
	agentToTarget = agentToTarget.normalized() * (newClearDist + 450);

	Vector backAgent = Vector(self()->getPos().getX(), self()->getPos().getY()) + agentToTarget.normalized() * -450;
	Segment segment(Point(backAgent.getX(), backAgent.getY()), Point(backAgent.getX(), backAgent.getY()) + agentToTarget);

	Vector direction = agentToTarget.normalized() * newClearWidth;
	Vector tmp1(-direction.getY(), direction.getX());
	Vector tmp2(direction.getY(), -direction.getX());

	Polygon result;
	result.addVertex(segment.getFirstPoint() + tmp1);
	result.addVertex(segment.getSecondPoint() + tmp1);
	result.addVertex(segment.getSecondPoint() + tmp2);
	result.addVertex(segment.getFirstPoint() + tmp2);
	return result;
}

bool PoliceForceAgent::isAnyBlockadeInClearArea(Polygon clearArea)
{
	if (self()->getMotionlessObject()->isRoad())
	{
		for (Blockade* blockade : ((Road*) self()->getMotionlessObject())->getBlockades())
		{
			if (blockade->getShape().isInPolygon(self()->getPos()))
			{
				return true;
			}
		}
	}

	for (Blockade* blockade : world->blockades)
	{
		if ((distanceToPolygon(blockade->getShape(), clearArea.getMidPoint()) < NEAR_BLOCKADES_TO_CLEAR_AREA_SMALL_MAP
				&& !world->isMapLarge())
				|| (distanceToPolygon(blockade->getShape(), clearArea.getMidPoint()) < NEAR_BLOCKADES_TO_CLEAR_AREA_BIG_MAP
						&& world->isMapLarge()))
		{
			if (haveInstersection(blockade->getShape(), clearArea))
			{
				return true;
			}
		}
	}

	return false;
}

bool PoliceForceAgent::clearStuckFireBrigadesAroundMe()
{
	LOG(Main, 1) << "Near clearing tof" << endl;
	vector<MotionlessObject*> toClear;
	for (StuckAgent* sa : araroTargets)
	{
		if (sa->agent == nullptr || sa->agent->isFireBrigade() == false)
		{
			continue;
		}

		int distance = worldGraph->getDistance(self()->getWithoutBlockadeRepresentiveNodeIndex(),
				sa->agent->getWithoutBlockadeRepresentiveNodeIndex(), GM_WITHOUT_BLOCKADE);
		if (distance > NEAR_TARGET_DISTANCE)
		{
			continue;
		}

		if ((radar->getRadarMode() == RM_LOW || radar->getRadarMode() == RM_NO_COMMUNICATION) == false)
		{
			bool closerAgent = false;
			for (PoliceForce* pf : world->policeForces)
			{
				int dist = worldGraph->getDistance(pf->getWithoutBlockadeRepresentiveNodeIndex(),
						sa->agent->getWithoutBlockadeRepresentiveNodeIndex(), GM_WITHOUT_BLOCKADE);
				if (dist < distance)
				{
					closerAgent = true;
					break;
				}
			}

			if (closerAgent)
			{
				continue;
			}
		}

		toClear.push_back(sa->position);
	}

	MotionlessObject* target = nullptr;
	int minDist = numeric_limits<int>::max();
	for (MotionlessObject* motionless : toClear)
	{
		int distance = worldGraph->getDistance(self()->getWithoutBlockadeRepresentiveNodeIndex(),
				motionless->getWithoutBlockadeRepresentiveNodeIndex(), GM_WITHOUT_BLOCKADE);
		if (distance < minDist)
		{
			target = motionless;
			minDist = distance;
		}
	}

	if (target != nullptr)
	{
		LOG(Main, 1) << "i'm clearing near tof! " << target->getId() << (isStuck(target) ? " FUCKING STUCK" : " NOT STUCK") << endl;
		moveAndClear(target);
		return true;
	}

	LOG(Main, 1) << "Near clearing tof executed and was not necessary" << endl;
	return false;
}

bool PoliceForceAgent::clearOtherStucksAroundMe()
{
	LOG(Main, 1) << "Near clearing tof" << endl;
	vector<MotionlessObject*> toClear;
	for (StuckAgent* sa : araroTargets)
	{
		int distance = worldGraph->getDistance(self()->getWithoutBlockadeRepresentiveNodeIndex(),
				sa->position->getWithoutBlockadeRepresentiveNodeIndex(), GM_WITHOUT_BLOCKADE);
		if (distance > NEAR_TARGET_DISTANCE)
		{
			continue;
		}

		if ((radar->getRadarMode() == RM_LOW || radar->getRadarMode() == RM_NO_COMMUNICATION) == false)
		{
			bool closerAgent = false;
			for (PoliceForce* pf : world->policeForces)
			{
				int dist = worldGraph->getDistance(pf->getWithoutBlockadeRepresentiveNodeIndex(),
						sa->position->getWithoutBlockadeRepresentiveNodeIndex(), GM_WITHOUT_BLOCKADE);
				if (dist < distance)
				{
					closerAgent = true;
					break;
				}
			}

			if (closerAgent)
			{
				continue;
			}
		}

		toClear.push_back(sa->position);
	}

	MotionlessObject* target = nullptr;
	int minimumDistance = numeric_limits<int>::max();
	for (MotionlessObject* motionless : toClear)
	{
		int distance = worldGraph->getDistance(self()->getWithoutBlockadeRepresentiveNodeIndex(),
				motionless->getWithoutBlockadeRepresentiveNodeIndex(), GM_WITHOUT_BLOCKADE);
		if (distance < minimumDistance)
		{
			target = motionless;
			minimumDistance = distance;
		}
	}

	if (target != nullptr)
	{
		LOG(Main, 1) << "i'm clearing near tof!" << endl;
		moveAndClear(target);
		return true;
	}

	LOG(Main, 1) << "Near clearing tof executed and was not necessary" << endl;
	return false;
}

bool PoliceForceAgent::checkDamage()
{
	LOG(Main, 1) << "checking damage!" << endl;

	bool isDamaged = false;
	if (self()->getDamage() > 0)
	{
		LOG(Main, 1) << "even server knows that i'm damaged!!!" << endl;
		isDamaged = true;
	}
	else
	{
		int hSize = self()->getHpHistory().size() - 1;
		LOG(Main, 1) << "hp history size : " << hSize << endl;
		if (hSize > 2)
		{
			LOG(Main, 1) << "hp history was bigger than 2" << endl;
			if (self()->getHpHistory()[hSize].hp != self()->getHpHistory()[hSize - 1].hp)
			{
				LOG(Main, 1) << "damaged at last cycle!" << endl;
				isDamaged = true;
			}
			else if (self()->getHpHistory()[hSize - 1].hp != self()->getHpHistory()[hSize - 2].hp)
			{
				LOG(Main, 1) << "damaged in second stage" << endl;
				isDamaged = true;
			}
			else if (self()->getHpHistory()[hSize - 2].hp != self()->getHpHistory()[hSize - 3].hp)
			{
				LOG(Main, 1) << "damaged in third stage" << endl;
				isDamaged = true;
			}
		}
	}

	if (isDamaged)
	{
		return moveToRefuge();
	}

	LOG(Main, 1) << "i'm not damaged!" << endl;
	return false;
}

bool PoliceForceAgent::moveToRefuge()
{
	if (world->refuges.empty())
	{
		return false;
	}

	if (self()->getMotionlessObject()->isRefuge())
	{
		command->rest();
		return true;
	}

	int minDistance = numeric_limits<int>::max(), tempDistance;
	Refuge* bestRefuge = nullptr;
	for (int i = 0; i < (int) world->refuges.size(); i++)
	{
		tempDistance = worldGraph->getDistance(self()->getWithoutBlockadeRepresentiveNodeIndex(),
				world->refuges[i]->getWithoutBlockadeRepresentiveNodeIndex(), GM_WITHOUT_BLOCKADE);

		if (minDistance > tempDistance)
		{
			minDistance = tempDistance;
			bestRefuge = world->refuges[i];
		}
	}

	moveAndClear((MotionlessObject*) bestRefuge);
	return true;
}

bool PoliceForceAgent::setAvailablePoliceForces()
{
	LOG(Main, 1) << "setting available police forces" << endl;

	int lastTimeSize = availablePolices.size();
	availablePolices.clear();

	for (int i = 0; i < (int) world->policeForces.size(); i++)
	{
		if (world->policeForces[i]->getBuriedness() != 0)
		{
			continue;
		}
		if (!world->policeForces[i]->isAvailable)
		{
			continue;
		}
		if (world->policeForces[i]->job == PFJ_PIDGIN)
		{
			continue;
		}

		availablePolices.push_back(world->policeForces[i]);
	}

	LOG(Main, 1) << "available police forces setting finished" << endl;
	return lastTimeSize != (int) availablePolices.size();
}

bool PoliceForceAgent::clear(MotionlessObject * target, bool setCommand) //TODO clearArea command
{
	vector<Road*> myFinalPath;
	int maxDistanceFromRoadWithBlockade = 4 * maxClearDistance;

	LOG(Main , 1) << "I am clearing : " << target->getId() << endl;
	LOG(Main , 1) << Circle(target->getPos(), 1000) << endl;

	vector<int> path = worldGraph->getPath(self()->getWithoutBlockadeRepresentiveNodeIndex(),
			target->getWithoutBlockadeRepresentiveNodeIndex(), GM_WITHOUT_BLOCKADE);
	path = worldGraph->getIDPath(path);

	if (self()->getMotionlessObject()->isRoad())
	{
		myFinalPath.push_back((Road *) (self()->getMotionlessObject()));
	}

	for (int i = 0; i < (int) path.size(); i++)
	{
		MotionlessObject * thisPiece = ((MotionlessObject *) world->objects[path[i]]);
		if (thisPiece->isRoad())
		{
			myFinalPath.push_back((Road *) (thisPiece));
		}
	}

	if (target->isRoad())
	{
		myFinalPath.push_back((Road *) target);
	}

	for (int i = 0; i < (int) myFinalPath.size(); i++)
	{
		LOG(Main, 1) << myFinalPath[i]->getId() << ",";
	}
	LOG(Main, 1) << endl;

	sort(myFinalPath.begin(), myFinalPath.end());
	myFinalPath.resize(unique(myFinalPath.begin(), myFinalPath.end()) - myFinalPath.begin());

	for (int i = 0; i < (int) myFinalPath.size(); i++)
	{
		Road * thisRoad = myFinalPath[i];

		if (thisRoad->getBlockades().size() != 0)
		{
			LOG(Main, 1) << "this road blockade size:  " << thisRoad->getBlockades().size() << endl;
			if (self()->getPos().distFromPoint(thisRoad->getPos()) < maxDistanceFromRoadWithBlockade)
			{
				for (int blCounter = 0; blCounter < (int) thisRoad->getBlockades().size(); blCounter++)
				{
					for (int pCounter = 0; pCounter < (int) thisRoad->getBlockades()[blCounter]->getShape().getVertices().size();
							pCounter++)
					{
						if (self()->getPos().distFromPoint(thisRoad->getBlockades()[blCounter]->getShape().getVertices()[pCounter])
								< maxClearDistance)
						{
							LOG(Main , 1) << Circle((*thisRoad->getBlockades()[blCounter]).getPos(), 3000) << endl;
							if (setCommand)
							{
								command->clear(*thisRoad->getBlockades()[blCounter]);
							}

							return true;
						}
					}
				}
			}
		}
	}

	return false;
}

bool PoliceForceAgent::clearNearestBlockade()
{
	int minDistance = numeric_limits<int>::max(), tempDistance;
	Blockade* bestBlockade = nullptr;

	for (Blockade* blockade : world->blockades)
	{
		for (int i = 0; i < (int) blockade->getShape().getVertices().size(); i++)
		{
			if (self()->getPos().distFromPoint(blockade->getShape().getVertices()[i]) < maxClearDistance)
			{
				tempDistance = self()->getPos().distFromPoint(blockade->getShape().getVertices()[i]);
				if (minDistance > tempDistance)
				{
					minDistance = tempDistance;
					bestBlockade = blockade;
				}
			}
		}
	}

	if (bestBlockade)
	{
		command->clear(*bestBlockade);
		return true;
	}

	return false;
}

void PoliceForceAgent::noCommAct()
{
	if (world->getTime() < ignoreCommandsUntil)
	{
		return;
	}

	act();
}

PoliceForce * PoliceForceAgent::self()
{
	return ((PoliceForce *) world->self);
}

StuckAgent::StuckAgent(Platoon* agent, MotionlessObject* position)
{
	this->agent = agent;
	this->position = position;
}

void PoliceForceAgent::setFireEstimatorNecessity()
{
// TODO XXX Check Noise
	if (radar->getRadarMode() == RM_HIGH || radar->getRadarMode() == RM_MEDIUM)
	{
		useFireEstimator = true;
	}
	else
	{
		useFireEstimator = false;
	}
}

bool PoliceForceAgent::superTof()
{
	vector<Road*> toClear;
	for (Refuge* r : world->refuges)
	{
		for (int index : r->getNeighbours())
		{
			if (world->motionlessObjects[index]->isRoad())
			{
				toClear.push_back((Road*) world->motionlessObjects[index]);
				break;
			}
		}

		for (Road* r : toClear)
		{
			for (Blockade* bl : r->getBlockades())
			{
				if (bl->getPos().distFromPoint(self()->getPos()) < maxClearDistance)
				{
					command->clear(*bl);
					return true;
				}
			}
		}
	}

//	for (Human* human : world->humans)
//	{
//		if (human == self())
//		{
//			continue;
//		}
//
//		if (isValidForClearing(human) && human->getPos().distFromPoint(self()->getPos()) < maxClearDistance)
//		{
//			LOG(Main, 1) << "Clearing near civilians " << human->getLastCycleUpdatedBySense() << ", " << human->getMotionlessObject()->getLastCycleUpdatedBySense() << endl;
//
//			moveAndClear(human->getMotionlessObject());
//			return true;
//		}
//	}

	if (clearStuckFireBrigadesAroundMe())
	{
		LOG(Main, 1) << "Clearing stuck fire brigades around me" << endl;
		return true;
	}

	return false;
}

bool PoliceForceAgent::tof()
{
	if (clearOtherStucksAroundMe())
	{
		LOG(Main, 1) << "Clearing stuck ambulances around me" << endl;
		return true;
	}

	if (fireBlockToFireBrigade())
	{
		LOG(Main, 1) << "Fire block to fire brigade state" << endl;
		return true;
	}

	if (world->getTime() % DECISION_CYCLE_TIME != 6)
	{
		updateFire();
		for (FireBlock* fb : world->fireBlocks)
		{
			if (isThisFireBlockOk(fb) == false)
			{
				continue;
			}

			vector<PoliceForce*> chosen;
			for (int i = 0; i < floor(fb->getNeededPoliceForce() / 2.); i++)
			{
				int minDist = numeric_limits<int>::max();
				PoliceForce* nearest = nullptr;
				for (PoliceForce* pf : availablePolices)
				{
					if (pf->job != PFJ_NONE)
					{
						continue;
					}
					if (find(chosen.begin(), chosen.end(), pf) != chosen.end())
					{
						continue;
					}

					int dist = fb->getGroundDistToMotionless(pf->getMotionlessObject(), worldGraph, GM_WITHOUT_BLOCKADE);
					if (dist < minDist)
					{
						minDist = dist;
						nearest = pf;
					}
				}

				if (nearest == self() && clearThisFireBlock(fb))
				{
					LOG(Main, 1) << "Fireblock clearing [SELF_DECISION]" << endl;
					return true;
				}
				else
				{
					chosen.push_back(nearest);
				}
			}
		}
	}

	if (clearCivilians())
	{
		LOG(Main, 1) << "Clearing civilians around me" << endl;
		return true;
	}

	return false;
}
