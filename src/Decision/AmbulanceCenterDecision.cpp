/*
 * AmbulanceCenterDecision.cpp
 *
 *  Created on: Dec 16, 2013
 *      Author: pedram
 */

#include "AmbulanceCenterDecision.h"

#define RADAR_DELAY 3
#define LOGLEVEL 1
#define SCALE 3000
#define FIRST_ASSIGNMENT_TIME 30

double rescueCoeff = -12;
double distanceToAmbCoeff = 105;
double distanceToRefugeCoeff = 4;

bool riskComparator(Human* h1, Human* h2)
{
	return h2->risk > h1->risk;
}

bool humanValueComparator(Human * h1, Human * h2)
{
	return h1->value > h2->value;
}

AmbulanceCenterDecision::AmbulanceCenterDecision(WorldModel* world, WorldGraph* worldGraph, int hpPerception, int ignoreCommandsUntil)
{
	this->self = (AmbulanceTeam*) world->self;
	this->hpPerception = hpPerception;
	this->ignoreCommandsUntil = ignoreCommandsUntil;
	this->worldGraph = worldGraph;
	this->world = world;
}

AmbulanceCenterDecision::~AmbulanceCenterDecision()
{
}

/********************** Decision **********************/

void AmbulanceCenterDecision::act()
{
	//TODO Are we close together

	if (world->getTime() < ignoreCommandsUntil)
	{
		return;
	}

	LOG(Main , 1) << "PROGRESS" << endl;
	progressStates();

	LOG(Main , 1) << "ON FIRE SETTING" << endl;
	setOnFireBuildings();

	setFreeAmbulances();
	LOG(Main , 1) << "FREE AMBULANCE SETTING " << freeAmbulances.size() << endl;
	checkPastAssignments();

	setValidHumans();

	LOG(Main , 1) << "VALID HUMAN SETTING; Valid quantity : " << validHumans.size() << endl;

	for (u_int i = 0; i < freeAmbulances.size(); i++)
	{
		bool isReachable = false;
		for (Human* hm : validHumans)
		{
			if (worldGraph->isReachable(freeAmbulances[i]->getRepresentiveNodeIndex(), hm->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED))
			{
				isReachable = true;
				break;
			}
		}

		if (!isReachable)
		{
			freeAmbulances.erase(freeAmbulances.begin() + i);
			i--;
		}
	}

	LOG(Main , 1) << "VALID HUMAN SORTING" << endl;

	sort(validHumans.begin(), validHumans.end(), humanValueComparator);

	LOG(Main , 1) << "FILTERING" << endl;

	for (u_int i = 0; i < freeAmbulances.size(); i++)
	{
		LOG(Main , 1) << "FREE AMBS : " << freeAmbulances.size() << endl;
	}

	filterHumans();

	assign();
}

void AmbulanceCenterDecision::checkPastAssignments()
{
	for (u_int i = 0; i < assignment.size(); i++)
	{
		if ((assignment[i].second->getHp() == 0 && assignment[i].second->getLastCycleUpdatedBySense() != 0) || !assignment[i].second->isAvailable)
		{
			LOG(Main , 1) << "this assign from the starting point was a big mistake. :-(" << endl;
			for (u_int j = 0; j < assignment[i].first->stateEstimator->getWorkingOnMyTarget().size(); j++)
			{
				vector<AmbulanceTeam*> vect;
				assignment[i].first->stateEstimator->getWorkingOnMyTarget()[j]->centerTarget = nullptr;
				assignment[i].first->stateEstimator->getWorkingOnMyTarget()[j]->centerTargetPos = nullptr;
				assignment[i].first->stateEstimator->getWorkingOnMyTarget()[j]->stateEstimator->setState(nullptr, 0, 0, vect);
			}
		}
		if (!worldGraph->isReachable(assignment[i].first->getRepresentiveNodeIndex(), assignment[i].second->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED))
		{
			LOG(Main , 1) << "FUCKING REACHABILITY" << endl;
			vector<AmbulanceTeam*> vect;
			assignment[i].first->stateEstimator->setState(nullptr, 0, 0, vect);
			for (u_int j = 0; j < assignment[i].first->stateEstimator->getWorkingOnMyTarget().size(); j++)
			{
				assignment[i].first->centerTarget = nullptr;
				assignment[i].first->centerTargetPos = nullptr;
				assignment[i].first->stateEstimator->getWorkingOnMyTarget()[j]->stateEstimator->substractTeamMate(assignment[i].first);
			}
			if ((int) assignment[i].first->stateEstimator->getWorkingOnMyTarget().size() < neededAmbulanceCount(assignment[i].second))
			{
				for (u_int j = 0; j < assignment[i].first->stateEstimator->getWorkingOnMyTarget().size(); j++)
				{
					vector<AmbulanceTeam*> vect;
					assignment[i].first->stateEstimator->getWorkingOnMyTarget()[j]->centerTarget = nullptr;
					assignment[i].first->stateEstimator->getWorkingOnMyTarget()[j]->centerTargetPos = nullptr;
					assignment[i].first->stateEstimator->getWorkingOnMyTarget()[j]->stateEstimator->setState(nullptr, 0, 0, vect);
				}
			}
		}
	}
}

void AmbulanceCenterDecision::setFreeAmbulances()
{
	freeAmbulances.clear();
	for (AmbulanceTeam* at : world->ambulanceTeams)
	{
		if (at->getBuriedness() != 0)
		{
			continue;
		}
		if (at->isAvailable == false)
		{
			continue;
		}
		if (at->stateEstimator->getEstimatedBusyTime() > getFreeAmbulancesLimit())
		{
			LOG(Main, 1) << "not accepted " << at->stateEstimator->getEstimatedBusyTime() << endl;
			continue;
		}

		LOG(Main, 1) << "accepted " << at->stateEstimator->getEstimatedBusyTime() << endl;

		freeAmbulances.push_back(at);
	}
}

void AmbulanceCenterDecision::setValidHumans()
{
	validHumans.clear();
	for (u_int i = 0; i < world->humans.size(); i++)
	{
		if (isValid(world->humans[i]))
		{
			setRisk(world->humans[i]);
			setScore(world->humans[i]);
			validHumans.push_back(world->humans[i]);
		}
	}
}

void AmbulanceCenterDecision::filterHumans()
{
	if (validHumans.empty() || freeAmbulances.empty())
	{
		return;
	}

	vector<Human*> filteredValidHumans;
	u_int ambCount = 0;
	for (u_int i = 0; i < validHumans.size() && ambCount < freeAmbulances.size(); i++)
	{
		int needed = neededAmbulanceCount(validHumans[i]);
		if (validHumans[i]->isPlatoon())
		{
			needed = suggestedAmbulanceCount(validHumans[i]);
		}

		for (AmbulanceTeam* at : world->ambulanceTeams)
		{
			if (validHumans[i]->isPlatoon() && at->centerTarget->isPlatoon() && at->centerTarget == validHumans[i])
			{
				needed -= at->stateEstimator->getWorkingOnMyTarget().size();
				if (!at->stateEstimator->getWorkingOnMyTarget().empty() && needed == 1) //یحتمل چرت میگه یه آمبولانس اضافه لازم نداریم کلا
				{
					needed = 0;
				}

				break;
			}
			else if (validHumans[i]->isCivilian() && at->centerTarget->isCivilian() && ((Civilian*) at->centerTarget)->getFakeId() == ((Civilian*) validHumans[i])->getFakeId())
			{
				needed -= at->stateEstimator->getWorkingOnMyTarget().size();
				if (!at->stateEstimator->getWorkingOnMyTarget().empty() && needed == 1) //یحتمل چرت میگه یه آمبولانس اضافه لازم نداریم کلا
				{
					needed = 0;
				}

				break;
			}
		}

		int limit = 6;
		if (world->getTime() >= 180)
		{
			limit = freeAmbulances.size() / 2;
		}
		if (needed <= 0 || needed > limit || ambCount + needed > freeAmbulances.size())
		{
			continue;
		}

		for (int j = 0; j < needed; j++)
		{
			filteredValidHumans.push_back(validHumans[i]);
		}
		ambCount += needed;
	}

	sort(validHumans.begin(), validHumans.end(), riskComparator);

	if (IS_CHALLENGE == false)
	{
		u_int i = 0;
		while (ambCount < freeAmbulances.size())
		{
			filteredValidHumans.push_back(validHumans[i]);
			ambCount++;
			i++;
			if (i == validHumans.size())
			{
				i = 0;
			}
		}
	}

	validHumans = filteredValidHumans;
}

/*********************** Assign ***********************/

void AmbulanceCenterDecision::assign()
{
	if (shouldAssignThisCycle())
	{
		LOG(Main , 1) << "#" << world->getTime() << "    ASSIGNING" << endl;
		cout << "#" << world->getTime() << " " << getFreeAmbulancesLimit() << " ASSIGN" << endl;
		vector<vector<int> > matrixCost = createMatrixCost();

		LOG(Main , 1) << "Matrix cost created" << endl;

		HungarianAssignment hgAssign(matrixCost);

		LOG(Main , 1) << "hungarian assign done" << endl;

		applyAssign(hgAssign.hungarian());
		LOG(Main , 1) << "assign applied" << endl;
	}
}

vector<vector<int> > AmbulanceCenterDecision::createMatrixCost()
{
	vector<vector<int> > matrixCost;
	if (validHumans.empty() || freeAmbulances.empty())
	{
		return matrixCost;
	}

	for (u_int i = 0; i < freeAmbulances.size(); i++)
	{
		vector<int> v;
		matrixCost.push_back(v);

		for (u_int j = 0; j < validHumans.size(); j++)
		{
			int start;
			if (freeAmbulances[i]->centerTarget && freeAmbulances[i]->stateEstimator->loader() && freeAmbulances[i]->stateEstimator->loader() == freeAmbulances[i])
			{
				Refuge* bestRefuge = nullptr;
				int minDistance = MAX_INT;
				if (!world->refuges.empty())
				{
					for (u_int k = 0; k < world->refuges.size(); k++)
					{
						if (!worldGraph->isReachable(freeAmbulances[i]->centerTarget->getRepresentiveNodeIndex(), world->refuges[k]->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED))
						{
							continue;
						}

						int dist = worldGraph->getDistance(freeAmbulances[i]->centerTarget->getRepresentiveNodeIndex(), world->refuges[k]->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED);
						if (dist < minDistance)
						{
							minDistance = dist;
							bestRefuge = world->refuges[k];
						}
					}
				}

				if (bestRefuge)
				{
					start = bestRefuge->getRepresentiveNodeIndex();
				}
				else
				{
					start = freeAmbulances[i]->getRepresentiveNodeIndex();
				}
			}
			else
			{
				start = freeAmbulances[i]->getRepresentiveNodeIndex();
			}
			if (worldGraph->isReachable(freeAmbulances[i]->getRepresentiveNodeIndex(), validHumans[j]->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED))
			{
				int cost = -worldGraph->getDistance(start, validHumans[j]->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED);
				matrixCost[i].push_back(cost);
			}
			else
			{
				matrixCost[i].push_back(MAX_INT);
			}
		}
	}

	return matrixCost;
}

void AmbulanceCenterDecision::applyAssign(vector<vector<bool> > assignment)
{
	if (assignment.empty())
	{
		self->shouldSendAssignThisCycle = false;
		return;
	}

	this->assignment.clear();
	for (u_int i = 0; i < assignment.size(); i++)
	{
		for (u_int j = 0; j < assignment[i].size(); j++)
		{
			if (assignment[i][j])
			{
				freeAmbulances[i]->centerTarget = validHumans[j];
				freeAmbulances[i]->assignmentTime = world->getTime();
				freeAmbulances[i]->centerTargetPos = validHumans[j]->getMotionlessObject();
				this->assignment.push_back(pair<AmbulanceTeam *, Human *>(freeAmbulances[i], validHumans[j]));
			}
		}
	}
	LOG(Main, 1) << "first assignment level finished" << endl;

	for (u_int i = 0; i < this->assignment.size(); i++)
	{
		vector<AmbulanceTeam*> coWorkers;
		for (u_int j = 0; j < world->ambulanceTeams.size(); j++)
		{
			if (world->ambulanceTeams[j]->centerTarget == nullptr)
			{
				continue;
			}

			int id = (this->assignment[i].second->isCivilian() ? ((Civilian*) this->assignment[i].second)->getFakeId() : this->assignment[i].second->getId());
			int id2 = (world->ambulanceTeams[j]->centerTarget->isCivilian() ? ((Civilian*) world->ambulanceTeams[j]->centerTarget)->getFakeId() : world->ambulanceTeams[j]->centerTarget->getId());

			if (id == id2)
			{
				coWorkers.push_back(world->ambulanceTeams[j]);
			}
		}

		int distance = worldGraph->getDistance(this->assignment[i].first->getRepresentiveNodeIndex(), this->assignment[i].second->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED);
		int moveToTarget = round(distance / AVERAGE_AGENT_SPEED) + RADAR_DELAY;

		int distToRefuge = MAX_INT;
		if (!world->refuges.empty())
		{
			for (int j = 0; j < (int) world->refuges.size(); j++)
			{
				if (!worldGraph->isReachable(this->assignment[i].second->getRepresentiveNodeIndex(), world->refuges[j]->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED))
				{
					continue;
				}

				int dist = worldGraph->getDistance(this->assignment[i].second->getRepresentiveNodeIndex(), world->refuges[j]->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED);
				if (dist < distToRefuge)
				{
					distToRefuge = dist;
				}
			}
		}

		int moveToRefuge;
		if (distToRefuge == MAX_INT)
		{
			moveToRefuge = 0;
			LOG(Main , 1) << "no refuges available!" << endl;
		}
		else
		{
			moveToRefuge = round(distToRefuge / AVERAGE_AGENT_SPEED);
		}

		if (moveToRefuge == 0)
		{
			moveToRefuge++;
		}
		if (moveToTarget == 0)
		{
			moveToRefuge++;
		}

		LOG(Main , 1) << "civilian : " << this->assignment[i].second->getId() << " move to refuge : " << moveToRefuge << " move to reach : " << moveToTarget << endl;
		this->assignment[i].first->stateEstimator->setState(this->assignment[i].second, moveToRefuge, moveToTarget, coWorkers);
	}
}

/********************** Scoring **********************/

int AmbulanceCenterDecision::setScore(Human* human)
{
	if (!isValid(human) || human->getTimeToDeath() == 0)
	{
		human->value = numeric_limits<int>::min();
		return human->value;
	}

	int value = 0;

	if (human->isFireBrigade())
		value += 2000;
	else if (human->isPoliceForce())
		value += 1600;
	else if (human->isAmbulanceTeam())
		value += 1300;

	LOG(Main , 1) << "start point score : " << value << endl;

	value += getFireScore(human);

	LOG (Main, 1) << "fire score : " << getFireScore(human) << endl;

	value += getGasStationScore(human);

	LOG (Main, 1) << "gas station score : " << getGasStationScore(human) << endl;

	value += getTimeToRescueScore(human);

	LOG (Main, 1) << "time to rescue score : " << getTimeToRescueScore(human) << endl;

	value += getTimeToDeathScore(human);

	LOG (Main, 1) << "death time score : " << getTimeToDeathScore(human) << endl;

	value += getAroundCiviliansScore(human);

	LOG (Main, 1) << "around civilian score : " << getAroundCiviliansScore(human) << endl;

	value += getDistanceToAmbulancesScore(human);

	LOG (Main, 1) << "ambulance distance score : " << getDistanceToAmbulancesScore(human) << endl;

//	value = ceil(100000 / human->getTimeToDeath());

	human->value = value;

	LOG (Main, 1) << "final score : " << value << endl << endl;

	return value;
}

int AmbulanceCenterDecision::getFireScore(Human* human)
{
	if (onFireBuildings.empty())
	{
		return 0;
	}

	int minDist = MAX_INT;
	for (int i = 0; i < (int) onFireBuildings.size(); i++)
	{
		int dist = human->getPos().distFromPoint(onFireBuildings[i]->getPos());
		if (dist < minDist)
		{
			minDist = dist;
		}
	}

	int scale = SCALE / 3;

	int value = -1 * ((minDist / scale - 25 * SCALE / scale) * (minDist / scale - 25 * SCALE / scale) + 3) / 8;

	if (value < -1000)
	{
		value = -1000;
	}

	return value;
}

int AmbulanceCenterDecision::getGasStationScore(Human* human)
{
	if (world->gasStations.empty())
	{
		return 0;
	}

	int distanceToGasStationWeight = -6;
	if (world->isMapLarge())
	{
		distanceToGasStationWeight = -11;
	}

	int distToNearestGasStation = MAX_INT;
	for (int i = 0; i < (int) world->gasStations.size(); i++)
	{
		int dist = worldGraph->getAirDistance(human->getRepresentiveNodeIndex(), world->gasStations[i]->getRepresentiveNodeIndex());
		if (dist < distToNearestGasStation)
		{
			distToNearestGasStation = dist;
		}
	}

	distToNearestGasStation = distToNearestGasStation / SCALE;

	if (distToNearestGasStation > 20)
	{
		return 0;
	}

	return distToNearestGasStation * distanceToGasStationWeight;
}

int AmbulanceCenterDecision::getTimeToRescueScore(Human* human)
{
	int timeToRefuge = MAX_INT;
	if (human->isCivilian())
	{
		if (!world->refuges.empty())
		{
			for (int i = 0; i < (int) world->refuges.size(); i++)
			{
				if (!worldGraph->isReachable(human->getRepresentiveNodeIndex(), world->refuges[i]->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED))
				{
					continue;
				}

				int dist = worldGraph->getDistance(human->getRepresentiveNodeIndex(), world->refuges[i]->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED);
				if (dist < timeToRefuge)
				{
					timeToRefuge = dist;
				}
			}
		}
		if (timeToRefuge == MAX_INT)
		{
			return -100;
		}

		timeToRefuge /= (AVERAGE_AGENT_SPEED / distanceToRefugeCoeff);
	}
	else
	{
		timeToRefuge = 0;
	}

	int counter = 0;
	int timeToReach = 0;
	for (int i = 0; i < (int) freeAmbulances.size(); i++)
	{
		if (!worldGraph->isReachable(freeAmbulances[i]->getRepresentiveNodeIndex(), human->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED))
		{
			continue;
		}

		timeToReach += round(worldGraph->getDistance(freeAmbulances[i]->getRepresentiveNodeIndex(), human->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED) / AVERAGE_AGENT_SPEED);
		counter++;
	}

	if (counter == 0)
	{
		timeToReach = MAX_INT;
//		return -1 * MAX_INT;
		return numeric_limits<int>::min();
	}

	timeToReach = round(timeToReach / counter);

	int TTR = timeToReach + human->getFirstTimeBuriedness() + timeToRefuge; //Time To Rescue
	return TTR * rescueCoeff;
}

int AmbulanceCenterDecision::getTimeToDeathScore(Human* human)
{
	if (human->getTimeToDeath() > 510)
	{
		return 0;
	}

	float timeToDeathWeight = -0.5;
	if (world->refuges.empty())
	{
		timeToDeathWeight *= -2;
	}

	return human->getTimeToDeath() * timeToDeathWeight;
}

int AmbulanceCenterDecision::getAroundCiviliansScore(Human* human)
{
	int aroundCivilianWeight = 20;
	int numberOfCivilianNeighbor = 0;

	for (int i = 0; i < (int) human->getMotionlessObject()->closerThan50B.size(); i++)
	{
		for (int j = 0; j < (int) human->getMotionlessObject()->closerThan50B[i]->getInsideCivilianIndexes().size(); j++) //Valid Civilians Around
		{
			int thisIndex = human->getMotionlessObject()->closerThan50B[i]->getInsideCivilianIndexes()[j];

			if (isValid(world->civilians[thisIndex]))
			{
				numberOfCivilianNeighbor++;
			}
		}

		for (int j = 0; j < (int) human->getMotionlessObject()->closerThan50B[i]->getInsidePlatoonIndexes().size(); j++) //Buried Agents Around
		{
			int thisIndex = human->getMotionlessObject()->closerThan50B[i]->getInsidePlatoonIndexes()[j];

			if (isValid(world->platoons[thisIndex]))
			{
				numberOfCivilianNeighbor++;
			}
		}
	}

	return numberOfCivilianNeighbor * aroundCivilianWeight;
}

int AmbulanceCenterDecision::setRisk(Human* human)
{
	if (!isValid(human))
	{
		LOG(Main , 1) << "shit was not valid" << endl;
		return 0;
	}

	LOG(Main , 1) << "starting the risk calculation" << endl;
	int needed = neededAmbulanceCount(human);
	int assigned = 0;
	for (u_int i = 0; i < freeAmbulances.size(); i++)
	{
		if (freeAmbulances[i]->centerTarget && freeAmbulances[i]->centerTarget == human)
		{
			assigned++;
		}
	}

	if (assigned > 0)
	{
		needed = assigned;
	}

	if (needed == 0)
	{
		human->risk = MAX_INT;
		return human->risk;
	}

	int buriedness = human->getBuriedness();
	int TOR = 0; //Time of Reaching to target
	int total = 0;
	int availableCount = 0;
	for (u_int i = 0; i < freeAmbulances.size(); i++)
	{
		if (!worldGraph->isReachable(freeAmbulances[i]->getRepresentiveNodeIndex(), human->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED))
		{
			continue;
		}

		availableCount++;
		total += round(worldGraph->getDistance(freeAmbulances[i]->getRepresentiveNodeIndex(), human->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED) / AVERAGE_AGENT_SPEED);
	}

	if (availableCount == 0)
	{
		human->risk = MAX_INT;
		return human->risk;
	}
	else
	{
		TOR += round(total / availableCount);
	}

	LOG(Main , 1) << " ambulances average move time setted " << TOR << " going to set time to refuge" << endl;

	TOR += 2; //Load and Unload

	int distToRefuge = MAX_INT;
	if (!human->isPlatoon() && !world->refuges.empty())
	{
		for (int i = 0; i < (int) world->refuges.size(); i++)
		{
			if (!worldGraph->isReachable(human->getRepresentiveNodeIndex(), world->refuges[i]->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED))
			{
				continue;
			}

			int dist = worldGraph->getDistance(human->getRepresentiveNodeIndex(), world->refuges[i]->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED);
			if (dist < distToRefuge)
			{
				distToRefuge = dist;
			}
		}
	}

	if (distToRefuge != MAX_INT)
	{
		TOR += (distToRefuge / AVERAGE_AGENT_SPEED);
	}

	LOG(Main , 1) << "time to refuge setted to " << TOR << " going to calculate final risk" << endl;

	TOR += max((hpPerception / 100), 5); //Error Cancellation (محاسبه ی خطا!)
	TOR += (world->getTime() - human->getLastCycleUpdated()) / 8; //Error Cancellation (محاسبه ی خطا!)

	human->risk = human->getTimeToDeath() - (TOR + (buriedness / needed));

	LOG(Main , 1) << "risk setted for human : " << human->getId() << " the risk value is : " << human->risk << endl;

	return human->risk;
}

int AmbulanceCenterDecision::getDistanceToAmbulancesScore(Human* human)
{
	int score = 0;
	int maximumDistance = (world->isMapLarge() ? 150000 : 50000);

	for (AmbulanceTeam* at : freeAmbulances)
	{
		if (at->getMotionlessObject()->getPos().distFromPoint(human->getMotionlessObject()->getPos()) < maximumDistance)
		{
			score += 400;
		}
	}

	return score;
}

/********************** Utilities **********************/

void AmbulanceCenterDecision::setOnFireBuildings()
{
	onFireBuildings.clear();

	for (int i = 0; i < (int) world->buildings.size(); i++)
	{
		if (world->buildings[i]->hasFire())
		{
			onFireBuildings.push_back(world->buildings[i]);
		}
	}
}

void AmbulanceCenterDecision::progressStates()
{
	for (u_int i = 0; i < world->ambulanceTeams.size(); i++)
	{
		world->ambulanceTeams[i]->stateEstimator->progress(world->getTime());
		LOG (Main , 1) << "ambulance : " << i << " busy time : " << world->ambulanceTeams[i]->stateEstimator->getEstimatedBusyTime() << " ";
		if (world->ambulanceTeams[i]->centerTarget)
		{
			LOG (Main , 1) << "target buriedness : " << world->ambulanceTeams[i]->centerTarget->getBuriedness();
			LOG (Main , 1) << " assignment time : " << world->ambulanceTeams[i]->assignmentTime << " ";
			LOG (Main , 1) << " sent time : " << world->ambulanceTeams[i]->sentOnRadarTime << " ";
			LOG (Main , 1) << " working on : " << world->ambulanceTeams[i]->stateEstimator->getWorkingOnMyTarget().size() << endl;
		}
		else
		{
			LOG(Main , 1) << endl;
		}
	}
}

bool AmbulanceCenterDecision::shouldAssignThisCycle()
{
	if (validHumans.empty() || freeAmbulances.empty() || world->getTime() < FIRST_ASSIGNMENT_TIME)
	{
		self->shouldSendAssignThisCycle = false;
		return false;
	}
	for (u_int i = 0; i < freeAmbulances.size(); i++)
	{
		if (freeAmbulances[i]->stateEstimator->getEstimatedBusyTime() < RADAR_DELAY)
		{
			self->shouldSendAssignThisCycle = true;
			return true;
		}
	}

	self->shouldSendAssignThisCycle = false;
	return false;
}

bool AmbulanceCenterDecision::isValid(Human* human)
{
	if (!human)
	{
		LOG(Main , 1) << "nullptr human" << endl;
		return false;
	}
	if (!human->isAvailable)
	{
		LOG(Main , 1) << "not available human" << endl;
		return false;
	}
	if (human->getLastCycleUpdatedBySense() != 0 && human->getHp() == 0)
	{
		LOG(Main , 1) << "dead human" << endl;
		return false;
	}
	if (human->isPlatoon() && human->getBuriedness() == 0)
	{
		LOG(Main , 1) << "faggot agent" << endl;
		return false;
	}
	if (human->isCivilian() && (isLoaded((Civilian*) human) || human->getTimeToDeath() > 510 || human->getMotionlessObject()->isRefuge()))
	{
		LOG(Main , 1) << "loaded, going to die or in refuge human" << endl;
		return false;
	}
	if (human->getMotionlessObject()->isBuilding())
	{
		Building * humanPos = ((Building *) human->getMotionlessObject());
		if (humanPos->hasFire() && human->getBuriedness() > 0)
		{
			LOG(Main , 1) << "on fire human" << endl;
			return false;
		}
		if (human->getLastCycleUpdatedBySense() != 0 && !humanPos->hasFire() && human->getBuriedness() == 0 && human->getDamage() == 0 && human->getHp() == MAX_HP)
		{
			LOG(Main , 1) << "shit human" << endl;
			return false;
		}
	}
	else
	{
		if (human->getBuriedness() == 0 && human->getDamage() == 0)
		{
			LOG(Main , 1) << "in road no buriedness human" << endl;
			return false;
		}
	}
	bool isReachableToAnyAmb = false;
	for (u_int i = 0; i < freeAmbulances.size(); i++)
	{
		if (worldGraph->isReachable(freeAmbulances[i]->getRepresentiveNodeIndex(), human->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED))
		{
			isReachableToAnyAmb = true;
			break;
		}
	}
	if (!isReachableToAnyAmb)
	{
		LOG(Main , 1) << "not reachable human" << endl;
		return false;
	}

	return true;

}

bool AmbulanceCenterDecision::isLoaded(Civilian* civ)
{
	for (int i = 0; i < (int) world->ambulanceTeams.size(); i++)
	{
		if (civ->getPosition() == world->ambulanceTeams[i]->getId())
		{
			return true;
		}
	}

	return false;
}

int AmbulanceCenterDecision::getFreeAmbulancesLimit() //TODO not so hardcode :|
{
	vector<int> datas;
	for (AmbulanceTeam* at : world->ambulanceTeams)
	{
		datas.push_back(at->stateEstimator->getEstimatedBusyTime());
	}

	if (datas.empty())
	{
		return 20;
	}

	sort(datas.begin(), datas.end());
	for (u_int i = 0; i < datas.size() - 1; i++)
	{
		if (datas[i + 1] - datas[i] > 5)
		{
			return min(datas[i], 20);
		}
	}

	return min(datas[datas.size() - 1], 20);
}

int AmbulanceCenterDecision::neededAmbulanceCount(Human* human)
{
	if (!isValid(human))
	{
		return -1;
	}

	int TOR = 0; //Time of Reaching to target
	if (freeAmbulances.empty())
	{
		TOR = 5;
		if (world->isMapLarge())
		{
			TOR = 20;
		}
	}
	else
	{
		int total = 0;
		vector<AmbulanceTeam*> ambulances;
		for (u_int i = 0; i < freeAmbulances.size(); i++)
		{
			if (!worldGraph->isReachable(freeAmbulances[i]->getRepresentiveNodeIndex(), human->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED))
			{
				continue;
			}

			total += round(worldGraph->getDistance(freeAmbulances[i]->getRepresentiveNodeIndex(), human->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED) / AVERAGE_AGENT_SPEED);

			total += freeAmbulances[i]->stateEstimator->getEstimatedBusyTime();
		}

		TOR += round(total / freeAmbulances.size());
	}

	TOR += 2; //Load and Unload
	TOR += RADAR_DELAY;

	int distToRefuge = MAX_INT;
	if (!world->refuges.empty())
	{
		for (int i = 0; i < (int) world->refuges.size(); i++)
		{
			if (!worldGraph->isReachable(human->getRepresentiveNodeIndex(), world->refuges[i]->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED))
			{
				continue;
			}

			int dist = worldGraph->getDistance(human->getRepresentiveNodeIndex(), world->refuges[i]->getRepresentiveNodeIndex(), GM_DEFAULT_NOT_BLOCKED);
			if (dist < distToRefuge)
			{
				distToRefuge = dist;
			}
		}
	}

	if (distToRefuge != MAX_INT)
	{
		TOR += (distToRefuge / AVERAGE_AGENT_SPEED);
	}

	LOG (Main , 1) << "DIS IS ORIGINAL TOR : " << TOR << endl;

	TOR += max((hpPerception / 100), 5); //Error Cancellation (محاسبه ی خطا!)
	TOR += (world->getTime() - human->getLastCycleUpdated()) / 8; //Error Cancellation (محاسبه ی خطا!)

	LOG (Main , 1) << "DIS IS FINAL TOR : " << TOR << endl;

	int CTD = human->getTimeToDeath();

	if (TOR >= CTD)
	{
		return -1;
	}

	int count = 1;
	while (TOR + ceil(human->getBuriedness() / count) > CTD)
	{
		count++;
	}

	return count;
}

int AmbulanceCenterDecision::suggestedAmbulanceCount(Human* human)
{
	if (!isValid(human))
	{
		return -1;
	}

	int suggested = 4 - abs((int) ((60 - human->getBuriedness()) / 20));

	int needed = neededAmbulanceCount(human);

	if (needed != -1 && human->getBuriedness() == 0)
	{
		return 1;
	}
	else if (needed == -1)
	{
		return -1;
	}

	if (suggested < needed)
	{
		return needed;
	}
	else
	{
		return suggested;
	}
}
