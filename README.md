Soshiant’s Rescue Agent Simulation Base

The following guide is written specifically for recent versions of Ubuntu and is not tested on other distributions.

Dependencies:
In order to run this software you must have the followings installed:
	boost_thread
	boost_system
	sfml-graphics
	sfml-system
	sfml-window
	polyclipping

	Installing SFML:
		$ sudo apt-get install libsfml-dev

	Installing Clipper:
		First download the latest version from: http://www.angusj.com/delphi/clipper.php
		Then install the library following the publisher's instructions.

	Installing Boost:
		$ sudo apt-get install libboost-thread-dev libboost-system-dev

Compilation:
	After insatlling all required dependencies, you may compile the code as described below.
	First, cd to /Debug directory
	Then, run $ cmake ../
	And finally, run $ make

Running the Software:
	To run the software you might use the scripts in /Debug directory or, run each agent individually yourself. Just make sure you have the binary placed in the same directory as the /Config directory, otherwise you’ll face segmentation faults.
